<?php

return [
"OTP_KEY"=>"www.any-cards.com",
"RESPONSES"=>[
    "STATUS"=>"status",
    "MESSAGE"=>"message",
    "SUCCESS"=>"success",
    "FALIURE"=>"faliure"
],
"RESPONSES_MESSAGES"=>[
"UNAUTHORIZED"=>"unauthorized"
],
"GUARDS"=>[
    "USER"=>"web",
    "ADMIN"=>"admin"
],
"USER_STATUS"=>[
    "ACTIVE"=>"active",
    "INACTIVE"=>"inactive"
],
"BALANCE_TYPE"=>[
    "INITAL"=>"inital",
    "TRANSACTION"=>"transaction",
    
],
"TRANSACTION_TYPE"=>[
    "DEPOSIT"=>"deposit",
    "WITHDRAW"=>"withdraw",
    'DEDUCTION'=>"deduction",
    "DEPOSIT_ADMIN_BALANCE"=>"deposit_admin_balance",
    "WITHDRAW_ADMIN_BALANCE"=>"withdraw_admin_balance"
],
"SETTINGS"=>[
    "CARDS_ZERO_VALUE"=>"CARDS_ZERO_VALUE"
],
"EXCEL_TYPES"=>[
    "PINCODE_WITH_SERIAL"=>"1",
    "PINCODE"=>"2",
    "GROUP"=>"3",
    "GROUP_OTHER"=>"4"
],
"MESSAGES"=>[
"NO_CARDS"=>"NO_CARDS",
"NOT_ENOUGH_PRICE"=>"NOT_ENOUGH_PRICE",
"QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY"=>"QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY",
"QUANTITY_LIMIT"=>"QUANTITY_LIMIT",
],
"REQUEST_RECHARGE_STATUS"=>[
"PENDING"=>"pending",
"REJECTED"=>"rejected",
"APPROVED"=>"approved"
],
"REQUEST_RECHARGE_MESSAGES"=>[
"USER_ALREADY_HAVE_PENDING_REQUEST"=>"USER_ALREADY_HAVE_PENDING_REQUEST"
],
"NOTIFICATION_MESSAGES"=>[
    "NEW_REQUEST_RECHARGE_EN"=>"New Request Recharge From %s with value %s points",
    "NEW_REQUEST_RECHARGE_AR"=>"طلب شحن جديد من  %s بقيمة  %s نقطة" ,
    "CARD_TYPE_REACHED_LIMIT_VALUE_EN"=>"Card type %s reached the limit value",
    "CARD_TYPE_REACHED_LIMIT_VALUE_AR"=>"نوع البطاقة %s وصل الحد الادنى",
    "BALANCE_IS_ABOUT_TO_BE_EMPTY_EN"=>"Your balance is about to expire",
    "BALANCE_IS_ABOUT_TO_BE_EMPTY_AR"=>"يرجى العمل على إعادة شحن حسابك في أقرب وقت",
    "BALANCE_IS_ABOUT_TO_BE_EMPTY_TITLE_AR"=>"رصيدك قارب على الإنتهاء",
    "BALANCE_IS_ABOUT_TO_BE_EMPTY_TITLE_EN"=>"Your balance is about to expire"

],
"USER_NOTIFICATION_MESSAGES"=>[
"BALANCE_DEPOSIT_EN"=>"Admin add %s to your balance",
"BALANCE_DEPOSIT_AR"=>"مدير النظام اضاف %s الى رصيدك" ,
"BALANCE_WITHDRAW_EN"=>"Admin deduct %s from your balance",
"BALANCE_WITHDRAW_AR"=>"تم إقتطاع %s نقطة من رصيدك",
],
"NOTIFICATION_TYPE"=>[
"CARD_LIMIT"=>"cards_limit",
"RECHARGE_REQUEST"=>"recharge_request",
'CARDS_EXPIRY_SOON'=>"cards_expiry_soon",
"EXCEEDED_CHARGING_QUOTA"=>"exceeded_charging_quota",
],
"USER_NOTIFICATION_TYPE"=>[
    "BALANCE_DEPOSIT"=>"balance_deposit",
    "BALANCE_WITHDRAW"=>"balance_withdraw"
],
"TEMPLATES"=>[
    "ZAIN_TEMPLATE"=>"zain_template",
    "UMNIAH_TEMPLATE"=>"umniah_template",
    "ORANGE_TEMPLATE"=>"orange_template",
    "WITHOUT_TEMPLATE"=>"without_template"
],
"EXCEPTIONS_CODE"=>[
"CONSTRAINT"=>1021
],
"PAGINATION"=>10,
"CATEGORIES"=>[
    "Telecommunication"=>"Telecommunication"
        ],

];