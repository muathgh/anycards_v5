<table>
    <thead>
    <tr>
        <th>Total Of Stock</th>
        <th>Stock Notes</th>
        <th>Total Of Current Balance</th>
        <th>Current Balance Notes</th>
        <th>Receivables</th>
        <th>Receivables Notes</th>
        <th>Unreceived</th>
        <th>Unreceived Notes</th>
        <th>Other</th>
        <th>Other Notes</th>
        <th>Monthly Net Profit</th>
        <th>Monthly Net Notes</th>
        <th>Net Profit</th>
        <th>Net Profit Notes</th>
     
        
    </tr>
    </thead>
    <tbody>
    @foreach($netProfits as $netProfit)
        <tr>
            <td>{{ $netProfit->stock_amount }}</td>
            <td>{{ $netProfit->stock_notes }}</td>
            <td>{{ $netProfit->current_balance_amount }}</td>
            <td>{{ $netProfit->current_balance_notes }}</td>
            <td>{{ $netProfit->receivables_amount }}</td>
            <td>{{ $netProfit->receivables_notes }}</td>
            <td>{{ $netProfit->unreceived_amount }}</td>
            <td>{{ $netProfit->unreceived_notes }}</td>
            <td>{{ $netProfit->other_amount }}</td>
            <td>{{ $netProfit->other_notes }}</td>
            <td>{{ $netProfit->monthly_profit_amount }}</td>
            <td>{{ $netProfit->monthly_profit_notes }}</td>
            <td>{{ $netProfit->net_profit_amount }}</td>
            <td>{{ $netProfit->net_profit_notes }}</td>
          
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>{{$stock_amount_sum}}</td>
        <td></td>
        <td>{{$current_balance_amount_sum}}</td>
        <td></td>
        <td>{{$receivables_amount_sum}}</td>
        <td></td>
        <td>{{$unreceived_amount_sum}}</td>
        <td></td>
        <td>{{$other_amount_sum}}</td>
        <td></td>
        <td>{{$monthly_profit_sum}}</td>
        <td></td>
        <td>{{$net_profit_amount_sum}}</td>
        <td></td>
    </tr>
    </tbody>
</table>