@extends('layouts.default')
@section('content')

    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Packages') }}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            @if (session()->has('success_message'))
                                <div class="alert alert-success">
                                    {{ session()->get('success_message') }}
                                </div>
                            @endif
                            @if (session()->has('error_message'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error_message') }}
                                </div>
                            @endif
                            <br /><br />
                            @if (Auth::guard('admin')->user()->accessType() != null &&
        Auth::guard('admin')->user()->accessType() != 'created_users')
                                <div class="row">

                                    <div class="col-md-3">
                                        {!! Form::label('admin_id', __('Admins')) !!}
                                        <div class="form-group">
                                            {!! Form::select('admin_id', $adminsLookup, old('admin_id'), ['class' => 'form-control select2', 'placeholder' => __('All')]) !!}
                                        </div>

                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="button" id="search" onclick="search()" value="{{ __('Search') }}"
                                                class="btn btn-primary" />
                                        </div>

                                    </div>
                                </div>
                            @else
                                <input type="hidden" id="admin_id" value="0" />
                            @endif
                            <a class="btn btn-success" href="{{ route('packages.create') }}"
                                style="color:white;">{{ __('Add New Package') }}</a>
                            <br /><br />
                            <div class="table-responsive">
                                <table id="packagesDataTable"
                                    class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                        <tr>
                                            <th>{{ __('#') }}</th>


                                            <th>{{ __('Title Ar') }}</th>

                                            <th>{{ __('Created By') }}</th>
                                            <th>{{ __('Created At') }}</th>
                                            <th>{{ __('Options') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{ __('#') }}</th>


                                            <th>{{ __('Title Ar') }}</th>

                                            <th>{{ __('Created By') }}</th>
                                            <th>{{ __('Created At') }}</th>
                                            <th>{{ __('Options') }}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
    </div>
    {{-- datatable --}}
    <script>
        var formData = {
            admin_id: null
        }
        var datatable = null;
        $(function() {
            dt();

        })

        function dt() {
            $('#packagesDataTable').DataTable({
                "processing": true,
                "serverSide": true,
                order: [],
                "lengthMenu": [
                    [100, 150, 200, -1],
                    [100, 150, 200, "All"]
                ],
                responsive: true,
                autoWidth: true,
                fixedColumns: true,
                buttons: [{
                        extend: 'copyHtml5',
                        footer: true
                    },
                    {
                        extend: 'excelHtml5',
                        footer: true
                    },
                    {
                        extend: 'csvHtml5',
                        footer: true
                    },
                    {
                        extend: 'pdfHtml5',
                        footer: true
                    },
                    'print'

                ],
                "pageLength": 100,
                dom: 'Blfrtip',
                "ajax": {
                    "url": "{{ route('packages.index') }}",
                    "type": "GET",
                    "data": formData
                },
                columns: [{
                        "data": "#",
                        render: function(data, type, row, meta) {

                            return meta.row + meta.settings._iDisplayStart + 1;

                        }
                    },

                    {
                        "data": "title_ar"
                    },

                    {
                        "data": "created_by"
                    },
                    {
                        "data": "created_at"
                    },

                    {
                        "data": "options",
                        render: function(data, type, row) {
                            
                            if(row.show_options)
                            return "<a href='/admin/packages/edit/" + row.id +
                                "'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id=" +
                                row.id + "></i> |<a href='/admin/pricinglists/create?package_id=" + row.id +
                                "'> <i class='fas fa-copy cursor-pointer'></i></a>"

                                return "<a href='/admin/pricinglists/create?package_id=" + row.id +
                                "'> <i class='fas fa-copy cursor-pointer'></i></a>"
                                
                        }
                    }
                ]
            });
        }

        $(document).on('click', '.edit', function() {
            var id = $(this).data('id');
            var url = "/admin/packages/edit/" + id;
            location.href = url;
        })
        $(document).on('click', '.delete', function() {
            var id = $(this).data('id');
            $.ajax({
                url: "/admin/packages/destroy",
                type: "GET",
                dataType: "JSON",
                data: {
                    id: id
                },
                success: function(response) {
                    if (response.data) {
                        $("#delete-popup .modal-body").html(response.data.html)
                        $("#delete-popup").modal('toggle');
                    }
                }
            })

        })

        function search(){
            var adminId = $("#admin_id").val();
            formData.admin_id = adminId;
            $("#packagesDataTable").DataTable().destroy();
            dt();
        }
    </script>
@stop
