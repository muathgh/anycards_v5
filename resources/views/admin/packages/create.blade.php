@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Add New Package')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'packages.store']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('title_en', __('Title En'))!!}
                                <div class="form-group">
                                    {!! Form::text('title_en', old('title_en'),['class'=>'form-control','placeholder'=>__('Title En')]) !!}
                                    {!! $errors->first('title_en', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('title_ar', __('Title Ar'))!!}
                                <div class="form-group">
                                    {!! Form::text('title_ar', old('title_ar'),['class'=>'form-control','placeholder'=>__('Title Ar')]) !!}
                                    {!! $errors->first('title_ar', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                          

                          
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('description_en', __('Description En'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('description_en', old('description_en'),['class'=>'form-control','placeholder'=>__('Description En'),"cols"=>"20","rows"=>"5"]) !!}
                                    {!! $errors->first('description_en', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('description_ar', __('Description Ar'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('description_ar', old('description_ar'),['class'=>'form-control','placeholder'=>__('Description Ar'),"cols"=>"20","rows"=>"5"]) !!}
                                    {!! $errors->first('description_ar', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                        </div>
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
            
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop