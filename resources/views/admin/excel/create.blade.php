@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Attach Excel File') }}</h3>
                        </div>
                        <div class="box-body">
                            @if (session()->has('success_message'))
                                <div class="alert alert-success">
                                    {{ session()->get('success_message') }}
                                </div>
                            @endif
                            @if (session()->has('warning_message'))

                                @if (in_array('large_number_of_digits', (array)session()->get('warning_message')))
                                    <div class="alert alert-danger">
                                        {{ __('Cards with more or less than 14 digits have not been uploaded') }}
                                    </div>
                                @endif
                                @if (in_array('duplicate_cards', (array)session()->get('warning_message')))
                                    <div class="alert alert-danger">
{{__('Duplicate cards have not been uploaded')}}
                                        @if (session()->has('duplicated_cards'))
                                            @foreach (session()->get('duplicated_cards') as $card)
                                                <li>{{ $card }}</li>
                                            @endforeach
                                        @endif
                                    </div>
                                @endif
                            @endif
                            <br /><br />
                            <div id="content">

                                <div class="container-fluid">
                                    {!! Form::open(['route' => 'excel.store', 'files' => true, 'id' => 'addForm']) !!}
                                    <div class="row">

                                        <div class="col-md-4">
                                            {!! Form::label('category_id', __('Categories')) !!}
                                            <div class="form-group">
                                                {!! Form::select('category_id', $categoryLookup, old('category_id'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            {!! Form::label('provider_id', __('Providers')) !!}
                                            <div class="form-group">
                                                {!! Form::select('provider_id', [], old('provider_id'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>


                                        <div class="col-md-4">
                                            {!! Form::label('sub_provider_id', __('SubProviders')) !!}
                                            <div class="form-group">
                                                {!! Form::select('sub_provider_id', [], old('sub_provider_id'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>


                                    </div>

                                    <div class="row">


                                        <div class="col-md-4">
                                            {!! Form::label('card_type_id', __('Cards Types')) !!}
                                            <div class="form-group">
                                                {!! Form::select('card_type_id', [], old('card_type_id'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('excel_type', __('Excel Type')) !!}
                                            <div class="form-group">
                                                {!! Form::select('excel_type', $lookup, old('excel_type'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('excel_type', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('expiry_date', __('Expiry Date')) !!}
                                            <div class="form-group">
                                                {!! Form::text('expiry_date', old('expiry_date'), ['class' => 'form-control', 'placeholder' => __('Expiry Date')]) !!}
                                            </div>

                                        </div>








                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            {!! Form::label('excel', __('Excel')) !!}
                                            <div class="input-group">
                                                <input type="text" class="form-control file-upload-text" disabled
                                                    placeholder="{{ __('Select Excel') }}" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default file-upload-btn">
                                                        {{ __('Browse...') }}
                                                        <input name="excel" id="file" type="file" class="file-upload" />
                                                    </button>
                                                </span>
                                            </div>
                                            <p style="display:none" id="file_error" class="error">
                                                {{ __('This field is required') }}</p>
                                            {!! $errors->first('excel', '<p class="error">:message</p>') !!}
                                            @if (\Session::has('excel_error'))
                                                <p class="error">{!! \Session::get('excel_error') !!}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <br />

                                    {!! Form::submit(__('Upload'), ['class' => 'btn btn-primary', 'id' => 'submit']) !!}
                                    <button type="button" onclick="removeFilters()"
                                        class="btn btn-danger">{{ __('Remove Filters') }}</button>
                                    {!! Form::close() !!}








                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        </div>
    </div>
    <script>
        function initFromExcelFilters() {
            var excelType = "{{ session('excel_filters') ? session('excel_filters')['excel_type'] : '' }}"
            var expiryDate = "{{ session('excel_filters') ? session('excel_filters')['expiry_date'] : '' }}"

            if (excelType != "") {
                $("#excel_type").val(excelType);
            }

            if (expiryDate != "") {

                $("#expiry_date").datepicker("setDate", moment(expiryDate).toDate());
            }

            initProvidersFromExcelFilters();
        }

        function initProvidersFromExcelFilters() {
            var categoryId = "{{ session('excel_filters') ? session('excel_filters')['category_id'] : 0 }}"

            if (categoryId != 0) {
                $("#category_id").val(categoryId)
                // fetch providers
                var providerId = "{{ session('excel_filters') ? session('excel_filters')['provider_id'] : 0 }}"
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.providersbycategory') }}",
                    data: {
                        category_id: categoryId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#provider_id").empty();
                        $("#provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var provider in response.data) {
                                var p = response.data[provider]
                                if (providerId != 0 && p.id == providerId)
                                    $("#provider_id").append("<option selected value=" + p.id + ">" + p
                                        .title_en + "</option>");
                                else
                                    $("#provider_id").append("<option value=" + p.id + ">" + p.title_en +
                                        "</option>");
                            }

                            initSubProviderFromExcelFilters(providerId);
                        }
                    }
                })

            }


        }

        function initSubProviderFromExcelFilters(providerId) {

            if (providerId != 0) {
                var subProviderId = "{{ session('excel_filters') ? session('excel_filters')['sub_provider_id'] : 0 }}"
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.subprovidersbyprovider') }}",
                    data: {
                        provider_id: providerId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#sub_provider_id").empty();
                        $("#sub_provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var subProvider in response.data) {
                                var s = response.data[subProvider]
                                if (subProviderId != 0 && subProviderId == s.id)
                                    $("#sub_provider_id").append("<option selected value=" + s.id + ">" + s
                                        .title_en + "</option>");
                                else
                                    $("#sub_provider_id").append("<option value=" + s.id + ">" + s.title_en +
                                        "</option>");
                            }
                            initCardTypeFromExcelFilters(subProviderId)
                        }
                    }
                })
            }
        }

        function initCardTypeFromExcelFilters(subProviderId) {
            if (subProviderId != 0) {
                var cardTypeId = "{{ session('excel_filters') ? session('excel_filters')['card_type_id'] : 0 }}"

                $.ajax({
                    type: "GET",
                    url: "{{ route('common.cardtypesbysubprovider') }}",
                    data: {
                        sub_provider_id: subProviderId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#card_type_id").empty();
                        $("#card_type_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var cardType in response.data) {
                                var c = response.data[cardType]
                                if (cardTypeId != 0 && cardTypeId == c.id)
                                    $("#card_type_id").append("<option selected value=" + c.id + ">" + c
                                        .title_en + "</option>");
                                else
                                    $("#card_type_id").append("<option value=" + c.id + ">" + c.title_en +
                                        "</option>");
                            }
                        }
                    }
                })
            }
        }
        $(function() {



            $("#expiry_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy',

            });
            var todate = moment();
            var fromdate = moment().add(1, "y");
            $("#expiry_date").datepicker("setDate", fromdate.toDate());

            initFromExcelFilters();

            $("#addForm").validate({
                rules: {
                    category_id: "required",
                    provider_id: "required",
                    sub_provider_id: "required",
                    card_type_id: "required",
                    excel_type: "required",
                    expiry_date: "required"


                },

                messages: {
                    category_id: "{{ __('This field is required') }}",
                    provider_id: "{{ __('This field is required') }}",
                    sub_provider_id: "{{ __('This field is required') }}",
                    card_type_id: "{{ __('This field is required') }}",
                    excel_type: "{{ __('This field is required') }}",
                    expiry_date: "{{ __('This field is required') }}"



                },
                submitHandler: function(form) {

                    if (!$("#expiry_date").val()) {
                        $("#expiry_date_error").show();
                        return false;
                    }
                    if (document.getElementById("file").files.length == 0) {
                        $("#file_error").show();
                        return false;
                    }


                    form.submit();
                }
            })



            $("#category_id").change(function() {
                $("#provider_id").empty().append("<option value=''>Select</option>");
                $("#sub_provider_id").empty().append("<option value=''>Select</option>");
                $("#card_type_id").empty().append("<option value=''>Select</option>");
                var categoryId = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.providersbycategory') }}",
                    data: {
                        category_id: categoryId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#provider_id").empty();
                        $("#provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var provider in response.data) {
                                var p = response.data[provider]

                                $("#provider_id").append("<option value=" + p.id + ">" + p
                                    .title_en + "</option>");
                            }
                        }
                    }
                })
            })
            $("#provider_id").change(function() {
                $("#sub_provider_id").empty().append("<option value=''>Select</option>");
                $("#card_type_id").empty().append("<option value=''>Select</option>");
                var providerId = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.subprovidersbyprovider') }}",
                    data: {
                        provider_id: providerId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#sub_provider_id").empty();
                        $("#sub_provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var subProvider in response.data) {
                                var s = response.data[subProvider]

                                $("#sub_provider_id").append("<option value=" + s.id + ">" + s
                                    .title_en + "</option>");
                            }
                        }
                    }
                })
            })
            $("#sub_provider_id").change(function() {
                $("#card_type_id").empty().append("<option value=''>Select</option>");
                var subProviderId = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.cardtypesbysubprovider') }}",
                    data: {
                        sub_provider_id: subProviderId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#card_type_id").empty();
                        $("#card_type_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var cardType in response.data) {
                                var c = response.data[cardType]

                                $("#card_type_id").append("<option value=" + c.id + ">" + c
                                    .title_en + "</option>");
                            }
                        }
                    }
                })
            })

        })

        function removeFilters() {
            $.post("{{ route('excel.remove-filter') }}", {
                _token: "{{ csrf_token() }}"
            }, function(response) {
                location.reload();
            })
        }
    </script>

    <script>
        $(function() {
            $('html').on("keypress", function(e) {


                if (e.keyCode == 13) {

                    var elements = $("form textarea");

                    for (var i = 0; i < elements.length; i++) {

                        if ($(elements[i]).is(":focus")) {

                            return;
                        }
                    }


                    document.getElementById("submit").click();
                }
            });
        })
    </script>
@stop
