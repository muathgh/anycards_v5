@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Download Excel File')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open() !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('excel_type', __('Excel Type'))!!}
                                <div class="form-group">
                                    {!! Form::select('excel_type', $lookup, old('excel_type'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('excel_type', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            
                          

                          
                            
                        </div>
                    
                        
                        {!! Form::button(__('Download Excel'),['class'=>"btn btn-primary" , "id"=>"download"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
  
    $(function(){
        $("#download").click(function(){
            var excelType = $("#excel_type").val();
        if(excelType)
{
var url = "/admin/attach?excel_type="+excelType
location.href=url;
}
        })
    })

</script>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
            
              
                  document.getElementById("download").click();
              }
          });
    })
      </script>
@stop