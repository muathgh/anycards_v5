@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Cards Expiry')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('cardsexpiry.create')}}" style="color:white;">{{__('Add / Edit Card Expiry')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="cardsExpiryDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Expiry')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Expiry')}}</th>
                               <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
    var datatable = null;
   $(function(){
       datatable =  $('#cardsExpiryDataTable').DataTable({
           "processing":true,
       "serverSide": true,
       responsive: true,
       order:[],
            autoWidth:true,
            order:[],
            fixedColumns: true,
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
         
     ],
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":"{{route('cardsexpiry.index')}}",
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"expiry"},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
   return "<i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
       ]
       });

   })


   $(document).on('click', '.delete', function(){ 
       var id = $(this).data('id');
        $.ajax({
            url:"/admin/notifications/cardsexpiry/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
 
   })

</script>
@stop