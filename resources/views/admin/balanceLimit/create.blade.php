@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Balance Limit') }}</h3>
                        </div>
                        <div class="box-body">
                            @if(session()->has('success_message'))
                            <div class="alert alert-success">
                                {{ session()->get('success_message') }}
                            </div>
                            @endif
                            @if(session()->has('error_message'))
                            <div class="alert alert-danger">
                                {{ session()->get('error_message') }}
                            </div>
                            @endif
                            <br /><br />
                            <div id="content">

                                <div class="container-fluid">
                                    {!! Form::open(['route' => 'balancelimit.store']) !!}
                                    <div class="row">
                                        <div class="col-md-4">
                                            {!! Form::label('amount', __('Amount')) !!}
                                            <div class="form-group">
                                                {!! Form::text('amount',old('amount'), ['class' => 'form-control numeric', 'placeholder' => __('Amount')]) !!}
                                                {!! $errors->first('amount', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('admin_id', __('Admins')) !!}
                                            <div class="form-group">
                                                {!! Form::select('admin_id', $adminsLookup, old('admin_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('admin_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('type', __('Type')) !!}
                                            <div class="form-group">
                                                {!! Form::select('type', $typesLookup, old('type') ?? 'deposit_admin_balance', ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('type', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>




                                    </div>
                             


                                    {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        </div>
    </div>
    <script>
        $(function() {

            // $("#admin_id").change(function(e){
            //     var id = $("#admin_id").val();
            //     if(id){
            //         $.ajax({
            //             url:"{{route('balancelimit.amount')}}",
            //             data:{id:id},
            //             type:"GET",
            //             datatype:"json",
            //             success:function(response){
            //                 if(response.status =="success" && response.data){
            //                     $("#amount").val(response.data.amount);
            //                 }
            //             }
            //         })
            //     }
            // })
            $("#amount").on('keypress',function(e){
                if (e.keyCode == 13) {
                    document.getElementById("submit").click();
                }
            })
            $('html').on("keypress", function(e) {


                if (e.keyCode == 13) {
                    
                    var elements = $("form textarea");

                    for (var i = 0; i < elements.length; i++) {

                        if ($(elements[i]).is(":focus")) {

                            return;
                        }
                    }


                    document.getElementById("submit").click();
                }
            });
        })
    </script>
@stop
