<form action="#" id="notesForm" method="POST">
    <input type="hidden" name="_id" id="_id" data-id="{{ $id }}" />
    <div class="row">
        <div class="col-md-4">
            <label>Notes</label>
            <div class="form-group">
                <input type="text" id="notes" name="notes" class="form-control" placeholder="{{ __('Notes') }}" />
            </div>
        </div>

        <div class="col-md-4">
            <label>Received Amount</label>
            <div class="form-group">
                <input type="text" class="form-control numeric" id="received_amount" name="received_amount"
                    placeholder="{{ __('Received Amount') }}" />
            </div>
        </div>
     
    </div>
    <div class="row">
        <div class="col-md-4">
            <input type="submit" class="btn btn-primary" id="submit" value="{{ __('Save') }}" />
        </div>
    </div>
</form>

<script>
   function isNumber(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
            (charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    $(function() {
        $(".numeric").keypress(function(event) {

return isNumber(event, this)
});

        $("#notesForm").validate({
         
            submitHandler: function(form) {
                var url = "{{ route('balancelimit.storenote') }}"
                var id = $("#_id").data('id');

                var notes = $("#notes").val();
                var receivables = $("#received_amount").val();

                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        id: id,
                        notes: notes,
                        received_amount: receivables,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(response) {
                        $('#notes-popup').modal('toggle');
                        location.reload();
                    }
                })
                return false;

            }
        })
    });
</script>

<script>
    $(function() {

        $("#notes").on('keypress', function(e) {
            if (e.keyCode == 13) {
                document.getElementById("submit").click();
            }
        })
        $("#received_amount").on('keypress', function(e) {
            if (e.keyCode == 13) {
                document.getElementById("submit").click();
            }
        })

        $('html').on("keypress", function(e) {


            if (e.keyCode == 13) {
                var elements = $("form textarea");

                for (var i = 0; i < elements.length; i++) {

                    if ($(elements[i]).is(":focus"))
                        return;
                }


                document.getElementById("submit").click();
            }
        });
    })
</script>
