@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <section class="content">
            <div class="row">
              <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">{{__('Users Verification Code')}}</h3>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                        <table  id="otpsDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                       
                       
                            <thead>
                                <tr>
                                    <th>{{__('#')}}</th>
                       
                                    <th>{{__('UserName')}}</th>
                                
                             
                             
                                    <th>{{__('OTP')}}</th>
                                    <th>{{__('Created At')}}</th>
                                   
                        
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{__('#')}}</th>
                       
                                    <th>{{__('UserName')}}</th>
                                
                             
                             
                                    <th>{{__('OTP')}}</th>
                                    <th>{{__('Created At')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
        </section>
    </div>
</div>
|
<script>
    var datatable = null;
    $(function(){
        datatable =  $('#otpsDataTable').DataTable({
            "processing":true,
        "serverSide": true,
        order:[],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        responsive: true,
            autoWidth:true,
            fixedColumns: true,
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
            'print'
            
        ],
        
        dom: 'Blfrtip',
        "ajax":"/admin/users/otps/",
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},
{"data":"identifier"},


{"data":"token"},

{"data":"created_at"},



        ]
        });
    });
    </script>

    @stop