{{-- V_5 UPDATES 26/12/2021 --}}
@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Add/Edit Admin Offical Prices') }}</h3>
                        </div>
                        <div class="box-body">

                            <br /><br />
                            <div id="content">

                                <div class="container-fluid">
                                    {!! Form::open(['route' => 'adminofficalprice.store']) !!}
                                    <div class="row">

                                        <div class="col-md-4">
                                            {!! Form::label('category_id', __('Categories')) !!}
                                            <div class="form-group">
                                                {!! Form::select('category_id', $categoriesLookup, old('category_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            {!! Form::label('provider_id', __('Providers')) !!}
                                            <div class="form-group">
                                                {!! Form::select('provider_id', [], old('provider_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>


                                        <div class="col-md-4">
                                            {!! Form::label('sub_provider_id', __('SubProviders')) !!}
                                            <div class="form-group">
                                                {!! Form::select('sub_provider_id', [], old('sub_provider_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>


                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            {!! Form::label('package_id', __('Packages')) !!}
                                            <div class="form-group">
                                                {!! Form::select('package_id', $packagesLookup, old('package_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('package_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>
                                        <div class="col-md-4" id="discount" style="display:none">
                                            {!! Form::label('discount', __('Discount')) !!}
                                            <div class="form-group">
                                                {!! Form::text('discount', '', ['class' => 'form-control', 'disabled' => true,'id'=>'discount']) !!}
                                            </div>

                                        </div>
                                        @if ($copiedFrom)
                                            <div class="col-md-4">
                                                {!! Form::label('copied_from', __('Copied From')) !!}
                                                <div class="form-group">
                                                    {!! Form::text('copied_from', $copiedFrom->title_ar, ['class' => 'form-control','id'=>"copied_from", 'disabled' => true]) !!}
                                                </div>

                                            </div>
                                        @endif







                                    </div>

                                    <div id="tblContainer" class="container">

                                    </div>

                                    <input style="display:none;" onclick="save()" id="submit" type="button"
                                        class="btn btn-primary" value="{{ __('Save') }}" />
                                    {{-- {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!} --}}
                                    {!! Form::close() !!}
                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        </div>
    </div>

    <script>
        $(function() {

            var copiedFrom = "{{ $copiedFrom }}"

            $("#category_id").change(function() {
                $("#provider_id").empty().append("<option value=''>Select</option>");
                $("#sub_provider_id").empty().append("<option value=''>Select</option>");
                $("#card_type_id").empty().append("<option value=''>Select</option>");
                var categoryId = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.providersbycategory') }}",
                    data: {
                        category_id: categoryId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#provider_id").empty();
                        $("#provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var provider in response.data) {
                                var p = response.data[provider]

                                $("#provider_id").append("<option value=" + p.id + ">" + p
                                    .title_en + "</option>");
                            }
                        }
                    }
                })
            })

            $("#provider_id").change(function() {
                $("#sub_provider_id").empty().append("<option value=''>Select</option>");
                $("#card_type_id").empty().append("<option value=''>Select</option>");
                var providerId = $(this).val();

             
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.subprovidersbyprovider') }}",
                    data: {
                        provider_id: providerId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#sub_provider_id").empty();
                        $("#sub_provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var subProvider in response.data) {
                                var s = response.data[subProvider]

                                $("#sub_provider_id").append("<option value=" + s.id + ">" + s
                                    .title_en + "</option>");
                            }
                        }


                    }
                })
            })
            $("#sub_provider_id,#package_id").change(function() {
                $("#tblContainer").html("");
                var packageId = $("#package_id").val();
                var subProviderId = $("#sub_provider_id").val();
                var providerId = $("#provider_id").val();
                var categoryId = $("#category_id").val();
                if (copiedFrom && packageId && (!subProviderId && !providerId && !categoryId)) {
                    generateAdminOfficalPriceAllTable()
                } else {
                    if (subProviderId && packageId)
                        generateAdminOfficalPriceTable("sub_provider_id", subProviderId);
                    else if (providerId && packageId)
                        generateAdminOfficalPriceTable("provider_id", providerId);
                    else if (categoryId && packageId)
                        generateAdminOfficalPriceTable("category_id", categoryId);
                    else if(packageId)
                    generateAdminOfficalPriceAllTable();
                }

            })

            $("#provider_id,#package_id").change(function() {
                $("#tblContainer").html("");
                var packageId = $("#package_id").val();
                var providerId = $("#provider_id").val();
                var subProviderId = $("#sub_provider_id").val();
                var categoryId = $("#category_id").val();
                if (copiedFrom && packageId && (!subProviderId && !providerId && !categoryId)) {
                    generateAdminOfficalPriceAllTable()
                }
                else{
                if (subProviderId && packageId)
                    generateAdminOfficalPriceTable("sub_provider_id", subProviderId);
                else if (providerId && packageId)
                    generateAdminOfficalPriceTable("provider_id", providerId);
                else if (categoryId && packageId)
                    generateAdminOfficalPriceTable("category_id", categoryId);
                    else if(packageId && packageId) 
                    generateAdminOfficalPriceAllTable();
                }

            })

            $("#category_id,#package_id").change(function() {
                $("#tblContainer").html("");
                var packageId = $("#package_id").val();
                var providerId = $("#provider_id").val();
                var subProviderId = $("#sub_provider_id").val();
                var categoryId = $("#category_id").val();
                if (copiedFrom && packageId && (!subProviderId && !providerId && !categoryId)) {
                    generateAdminOfficalPriceAllTable()
                }
                {
                if (subProviderId && packageId)
                    generateAdminOfficalPriceTable("sub_provider_id", subProviderId);
                else if (providerId && packageId)
                    generateAdminOfficalPriceTable("provider_id", providerId);
                else if (categoryId && packageId)
                    generateAdminOfficalPriceTable("category_id", categoryId);
                    else if(packageId && packageId)
                    generateAdminOfficalPriceAllTable();
                }

            })







        })

        function save() {

            if ($(".price_error:visible").length > 0) {
                $('html, body').animate({
                    scrollTop: $(".price_error:visible").offset().top - 500
                }, 10);
                return;
            }

            var formData = [];
            var packageId = $("#package_id").val();


            $("table tbody .price_input input").each(function(index, value) {
                var price = $(value).val()
                var cardTypeId = $(value).data('id');
                formData.push({
                    card_type_id: cardTypeId,
                    price: price
                });

            });
            $.ajax({
                url: "{{ route('adminofficalprice.store') }}",
                type: "POST",
                dataType: "JSON",
                data: {
                    data: JSON.stringify(formData),
                    package_id: packageId,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response.status == CONSTANTS.SUCCESS)
                        location.href = "{{ route('adminofficalprice.index') }}"
                }
            })
        }

        function generateAdminOfficalPriceAllTable() {
            var packageId = "{{ $packageId }}"
            var copiedFrom = "{{ $copiedFrom }}"
            if (!packageId)
                packageId = $("#package_id").val();
            $("#submit").hide();
            if (packageId) {
                $.ajax({
                    url: "{{ route('adminofficalprice.form-all') }}",

                    type: "GET",
                    dataType: "json",
                    data: {
                        package_id: packageId,
                        copied_from:copiedFrom
                    },
                    success: function(response) {
                        if (response.status == CONSTANTS.SUCCESS) {
                            if (response.data) {
                                $("#tblContainer").html(response.data);
                                $("#submit").show();
                            }
                        }
                    }
                })
            }
        }

        function generateAdminOfficalPriceTable(key, value) {
            var packageId = "{{ $packageId }}"
            var copiedFrom = "{{ $copiedFrom }}"
            
            if (!packageId)
                packageId = $("#package_id").val();

            


            $("#submit").hide();
            if (packageId && value) {
                $.ajax({
                    url: "{{ route('adminofficalprice.form') }}",
                    data: {
                        [key]: value,
                        package_id: packageId,
                        copied_from:copiedFrom
                    },
                    type: "GET",
                    dataType: "json",
                    success: function(response) {
                        if (response.status == CONSTANTS.SUCCESS) {
                            if (response.data) {
                                $("#tblContainer").html(response.data);
                                var adminId = "{{Auth::guard('admin')->user()->id}}"
                                var packageCreatedBy = $("#package_created_by").val();
                          
                                $("#submit").show();
                               

                               
                            }
                        }
                    }
                })
            }
        }
    </script>

    <script>
        $(function() {
            $('html').on("keypress", function(e) {


                if (e.keyCode == 13) {

                    var elements = $("form textarea");

                    for (var i = 0; i < elements.length; i++) {

                        if ($(elements[i]).is(":focus")) {

                            return;
                        }
                    }
                    var subProviderId = $("#sub_provider_id").val();
                    var providerId = $("#provider_id").val();
                    var categoryId = $("#category_id").val();
                    var packageId = $("#package_id").val();
                    if (subProviderId && providerId && categoryId && packageId)

                        document.getElementById("submit").click();
                }
            });
        })
    </script>

@stop

{{-- END V_5 UPDATES 26/12/2021 --}}