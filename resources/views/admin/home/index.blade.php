@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container" style="margin-top:50px;">
      <!-- Main content -->
      <section class="content">
<div class="row">
    <div class="col-12">
        <div class="box">
          <div class="row no-gutters py-2">

            <div class="col-12 col-lg-3">
              <div class="box-body br-1 border-light">
                <div class="flexbox mb-1">
                  <span>
                      <span class="icon-Layout-arrange font-size-40"><span class="path1"></span><span class="path2"></span></span><br>
                    {{__("Categories")}}
                  </span>
                  <span class="text-primary font-size-40">{{$categories}}</span>
                </div>
                <div class="progress progress-xxs mt-10 mb-0">
                  <div class="progress-bar" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>


            <div class="col-12 col-lg-3 hidden-down">
              <div class="box-body br-1 border-light">
                <div class="flexbox mb-1">
                  <span>
                      <span class="icon-Layout-arrange font-size-40"><span class="path1"></span><span class="path2"></span></span><br>
                    {{__('Providers')}}
                  </span>
                  <span class="text-info font-size-40">{{$providers}}</span>
                </div>
                <div class="progress progress-xxs mt-10 mb-0">
                  <div class="progress-bar bg-info" role="progressbar" style="width: 55%; height: 4px;" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>


            <div class="col-12 col-lg-3 d-none d-lg-block">
              <div class="box-body br-1 border-light">
                <div class="flexbox mb-1">
                  <span>
                      <span class="icon-Layout-arrange font-size-40"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span><br>
                    {{__('SubProviders')}}
                  </span>
                  <span class="text-warning font-size-40">{{$subProviders}}</span>
                </div>
                <div class="progress progress-xxs mt-10 mb-0">
                  <div class="progress-bar bg-warning" role="progressbar" style="width: 65%; height: 4px;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>


            <div class="col-12 col-lg-3 d-none d-lg-block">
              <div class="box-body">
                <div class="flexbox mb-1">
                  <span>
                      <span class="icon-Layout-arrange font-size-40"><span class="path1"></span><span class="path2"></span></span><br>
                   {{__('Cards Types')}}
                  </span>
                  <span class="text-danger font-size-40">{{$cardsTypes}}</span>
                </div>
                <div class="progress progress-xxs mt-10 mb-0">
                  <div class="progress-bar bg-danger" role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>


          </div>
        </div>
    </div>
    <!-- /.col -->

  </div>
  <div class="row">
    <div class="col-xl-4">
        <div class="row">
            <div class="col-md-12">
        <a href="#" class="box bg-success bg-hover-success">
            <div class="box-body">
                <span class="text-white icon-Equalizer font-size-40"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
            <div class="text-white font-weight-600 font-size-18 mb-2 mt-5">{{__('Card Transactions')}}</div>
            <div class="text-white font-size-16">{{$cardTransactions}}</div>
            </div>
        </a>
    </div>
    <div class="col-md-12">
        <a href="#" class="box bg-info bg-hover-success">
            <div class="box-body">
                <span class="text-white icon-User font-size-40"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
            <div class="text-white font-weight-600 font-size-18 mb-2 mt-5">{{__('Registered users')}}</div>
            <div class="text-white font-size-16">{{$registeredUsers}}</div>
            </div>
        </a>
    </div>
        </div>
    </div>
    <div class="col-xl-8 col-12">
        <div class="box" data-toggle="tooltip" data-placement="left" title="{{__('TooltipChart')}}">
            <div class="box-body analytics-info">
                <h4 class="box-title"></h4>
                <div id="basic-pie" class="pie" style="height:400px;"></div>
                <br>  <br>
            </div>
        </div>
    </div>
  </div>
      </section>
    </div>
</div>
<script>
 
  var title = "{{__('Transactions By Card Type')}}"
  </script>
<script src="{{asset('js/pages/echart-pie-doghnut.js')}}"></script>
<script src="{{asset('assets/vendor_components/echarts/dist/echarts-en.min.js')}}"></script>
@stop