
{{-- V_6 UPDATES 04/01/2022  --}}
@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  
                </div>
                <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::label('from_date', __('From Date'))!!}
                        <div class="form-group">
                            {!! Form::text('from_date', old('from_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('From Date')]) !!}
                        </div>
                       
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('to_date', __('To Date'))!!}
                        <div class="form-group">
                            {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                        <p class="error" id="to_date_error" style="display:none">{{__('Both of from date and to date are requireds')}}</p>
                        
                        </div>
                       
                    </div>
                </div>
                <div class="row">
					<div class="col-md-3">
						<div class="form-group">
						<input type="button" onclick="search()" value="{{__('Search')}}" class="btn btn-info"/>
						</div>
					</div>
				</div>
            </div>
                		<!-- /.box-header -->
				<div class="box-body">
                    <div class="table-responsive">
                        <table  id="transactionsDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                   <thead>
                       <tr>
                        <th>{{__('#')}}</th>
                        <th>{{__('Amount')}}</th>
                        <th>{{__('Type')}}</th>
                        <th>{{__('Created at')}}</th>

                       </tr>
                   </thead>
                   <tbody></tbody>
                   <tfoot>
                    <tr>
                        <th>{{__('#')}}</th>
                        <th>{{__('Amount')}}</th>
                        <th>{{__('Type')}}</th>
                        <th>{{__('Created at')}}</th>

                       </tr>
                   </tfoot>
                        </table>
                    </div>
                    
                </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
		var formData = {
		
        from_date:null,
        to_date:null
        
	}
function dt (){
    $('#transactionsDataTable').DataTable({
         "processing":true,
     "serverSide": true,
     order:[],
     buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
         
     ],
     "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
     "pageLength":100,
     dom: 'Blfrtip',
     "ajax":{
           url:"{{route('balancelimit.mytransactions')}}",
           data:formData
         
       },

       "footerCallback": function (row, data, start, end, display) {
          var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total = api
            .column(1)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);

  
    $(api.column(1).footer()).html(
            "<h3>Total "+toFixed(intVal(total),3)+"</h3>"
            );
          


   
  
   
  
   
},
     
     columns:[
         {"data": "#",
 render: function (data, type, row, meta) {
     
     return meta.row + meta.settings._iDisplayStart + 1;

 }
},
{"data":"amount"},
{"data":"type",render:function(data,type,row){
  if(row.type.toLowerCase()=="deposit_admin_balance")
  return "<span class='badge badge-success'>Deposit</span>"

  return "<span class='badge badge-warning'>WithDraw</span>"
}},


{"data":"created_at"},










     ]
     });
  }
  $(function(){
      dt();
  })

  
 function search(){
    $("#to_date_error").hide();
    var fromDate = $("#from_date").val();
        var toDate = $("#to_date").val();
        if((!fromDate && toDate) || (fromDate && !toDate))
        {
            $("#to_date_error").show();
            return ;
        }

     formData.from_date = $("#from_date").val();
     formData.to_date = $("#to_date").val();
	 $('#transactionsDataTable').DataTable().destroy();
	 dt();
 }
</script>
@endsection
{{--END V_6 UPDATES 04/01/2022  --}}