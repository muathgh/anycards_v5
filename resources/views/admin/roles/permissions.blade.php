@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Assign Sub Permission')}}</h3>
                </div>
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'role.assign.permissions.post']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('admin_id', __('Admins'))!!}
                                <div class="form-group">
                                    {!! Form::select('admin_id', $admins, old('admin_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('admin_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('sub_screen_id', __('Sreens'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_screen_id', $subScreens, old('sub_screen_id'),["class"=>"form-control select2",'multiple'=>true,'name'=>"sub_screen_id[]"]) !!}
                                    {!! $errors->first('sub_screen_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                          

                          
                            
                        </div>
                      
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){

        $("#admin_id").change(function(){
         
         var adminId=    $("#admin_id").val()
             $.ajax({
                 url:"/common/getAdminSubPermissions/"+adminId,
                 type:"GET",
                 success:function(response){
                    var screens = response.data
                    $("#sub_screen_id").val(screens).change();
                    
 
                 }
             })
         })
    })
    </script>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
                 
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop