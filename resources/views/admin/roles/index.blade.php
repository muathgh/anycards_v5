@extends('layouts.default')
@section('content')

<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Roles')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('role.create')}}" style="color:white;">{{__('Add New Role')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="rolesDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Role Name')}}</th>
                                <th>{{__('Permissions')}}</th>
                                <th>{{__('Options')}}</th>
                             
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Role Name')}}</th>
                                <th>{{__('Permissions')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
<script>
    var datatable = null;
   $(function(){
       datatable =  $('#rolesDataTable').DataTable({
           "processing":false,
       "serverSide": false,
       order:[],
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       responsive: true,
            autoWidth:true,
            fixedColumns: true,
       buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
           'print'
           
       ],
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":"{{route('role.index')}}",
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"name"},
{"data":"permissions",render:function(data, type, row, meta){
    return row.permissions.length
  

   
}},
{"data":"options",render:function(data,type,row){
   return "<a href='/admin/role/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i> "
}}


       ]
       });

   })

   $(document).on('click', '.edit', function(){ 
       var id = $(this).data('id');
       var url = "/admin/role/edit/"+id;
       location.href=url;
   })

   $(document).on('click', '.delete', function(){ 
       var id = $(this).data('id');
      
       $.ajax({
            url:"/admin/role/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
   })

    </script>
@stop