@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Add New Role') }}</h3>
                        </div>
                        <div class="box-body">

                            <br /><br />
                            <div id="content">

                                <div class="container-fluid">
                                    {!! Form::open(['route' => 'role.store']) !!}
                                    <div class="row">
                                        <div class="col-md-4">
                                            {!! Form::label('name', __('Role Name')) !!}
                                            <div class="form-group">

                                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => __('Role Name')]) !!}
                                                {!! $errors->first('name', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('permissions', __('Permissions')) !!}
                                            <div class="form-group">
                                                {!! Form::select('permissions', $permissions, old('permissions'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'name' => 'permissions[]']) !!}
                                                {!! $errors->first('permissions', '<p class="error">:message</p>') !!}
                                                <p class="permissions_error error" style="display: none">{{__('This field is required')}}</p>

                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('access_type', __('Access Type')) !!}
                                            <div class="form-group">
                                                {!! Form::select('access_type', ['all' => 'All', 'created_users' => 'By Users'], old('access_type'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('access_type', '<p class="error">:message</p>') !!}
                                                <p class="access_type_error error" style="display: none">{{__('This field is required')}}</p>

                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('is_super_admin', __('Super Admin')) !!}
                                            <div class="form-group">
<input type="checkbox"  id="is_super_admin"/>
<input type="hidden" name="is_super_admin" id="is_super_admin_value" value="false" />
                                            </div>

                                        </div>




                                    </div>

                                    {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        </div>
    </div>

    <script>
        $(function() {
      
            if("{{old('is_super_admin')}}" == "true"){
                $("#is_super_admin").attr('checked',true);
                $("#is_super_admin_value").val(true);
                $("#permissions").attr('disabled',true)
                $("#access_type").attr('disabled',true);
            }else{
                $("#is_super_admin").attr('checked',false);
                $("#is_super_admin_value").val(false); 
                $("#permissions").attr('disabled',false)
                $("#access_type").attr('disabled',false);
            }
            $("#is_super_admin").change(function(){
                if($(this).is(":checked")){
                    $("#permissions").val([]).change();
                    $("#permissions").attr('disabled',true);
                    $("#access_type").val("");
                    $("#access_type").attr('disabled',true);
                   
                }else{
                    $("#access_type").attr('disabled',false);
                    $("#permissions").attr('disabled',false);
                }
                $("#is_super_admin_value").val($(this).is(":checked"))
            })
            $("form").submit(function(e){
            
            if($("#is_super_admin_value").val() != "true"){
              
                var isValid = true;
                $(".permissions_error").hide();
                $(".access_type_error").hide();
             
                if($("#permissions").val().length == 0){
                    $(".permissions_error").show();
                    isValid= false;
                }
                if(!$("#access_type").val()){
                    $(".access_type_error").show();
                    isValid = false;
                }
                if(!isValid){
                    e.preventDefault();
                }
          
                return isValid;
 
             
            }
         })
            $('html').on("keypress", function(e) {


                if (e.keyCode == 13) {

                    var elements = $("form textarea");

                    for (var i = 0; i < elements.length; i++) {

                        if ($(elements[i]).is(":focus")) {

                            return;
                        }
                    }


                    document.getElementById("submit").click();
                }
            });
        })
    </script>

@stop
