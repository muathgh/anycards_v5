@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Assign Role')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'role.assign.post']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('admin_id', __('Admins'))!!}
                                <div class="form-group">
                                    {!! Form::select('admin_id', $admins, old('admin_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('admin_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('role', __('Roles'))!!}
                                <div class="form-group">
                                    {!! Form::select('role', $roles, old('role'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('role', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                          

                          
                            
                        </div>
                      
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){

        $("#admin_id").change(function(){
         
        var adminId=    $("#admin_id").val()
            $.ajax({
                url:"/common/getAdminRole/"+adminId,
                type:"GET",
                success:function(response){
                   var role = response.data[0]
                   $("#role").val(role).change();
                   

                }
            })
        })
    })
    </script>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
                 
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop