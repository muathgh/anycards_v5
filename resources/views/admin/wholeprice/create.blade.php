@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Add/Edit Official Price')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'wholeprice.store',"id"=>"addForm"]) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',[], old('provider_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',[], old('sub_provider_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                       

              
                        <div id="tblContainer" class="container">

                        </div>

                    <input style="display:none;" onclick="save()" id="submit" type="button" class="btn btn-primary" value="{{__('Save')}}"/>

                    {{-- <input   id="submit" type="submit" class="btn btn-primary" value="{{__('Save')}}"/> --}}
                        {{-- {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!} --}}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){
        

    
           $("#category_id").change(function(){
            $("#provider_id").empty().append("<option value=''>Select</option>");
       $("#sub_provider_id").empty().append("<option value=''>Select</option>");
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var categoryId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
    }
           }
       }
   })
})
$("#provider_id").change(function(){
    $("#sub_provider_id").empty().append("<option value=''>Select</option>");
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var providerId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_en+"</option>");
    }
           }
       }
   })
})
$("#sub_provider_id").change(function(){
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var subProviderId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.cardtypesbysubprovider')}}",
       data:{sub_provider_id:subProviderId},
       dataType:"json",
       success:function(response){
        $("#card_type_id").empty();
        $("#card_type_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var cardType in response.data) {
      var s = response.data[cardType]
   
       $("#card_type_id").append("<option value="+s.id+">"+s.title_en+"</option>");
    }
           }
       }
   })
})

$("#sub_provider_id,#package_id").change(function(){
    $("#tblContainer").html("");
    var subProviderId = $("#sub_provider_id").val();
    if(subProviderId)
    generateWholePriceTable("sub_provider_id",subProviderId);

})

$("#provider_id,#package_id").change(function(){
    $("#tblContainer").html("");
    var providerId = $("#provider_id").val();
    if(providerId)
    generateWholePriceTable("provider_id",providerId);

})

$("#category_id,#package_id").change(function(){
    $("#tblContainer").html("");
    var categoryId = $("#category_id").val();
    if(categoryId)
    generateWholePriceTable("category_id",categoryId);

})


    })

    
    function save(){
        var formData = [];
        
        $("table tbody .price_input input").each(function(index,value){
          var price =   $(value).val()
          var cardTypeId = $(value).data('id');
          formData.push({
              card_type_id:cardTypeId,
              price:price
          });

        });
      
        $.ajax({
            url:"{{route('wholeprice.store')}}",
            type:"POST",
            dataType:"JSON",
            data:{data:JSON.stringify(formData), "_token": "{{ csrf_token() }}"},
            success:function(response){
                if(response.status == CONSTANTS.SUCCESS)
                location.href="{{route('wholeprice.index')}}"
            }
        })
    }
    function generateWholePriceTable(key,value){
        $.LoadingOverlay("show")
       $("#submit").hide();

            $.ajax({
                url:"{{route('wholeprice.form')}}",
                data:{[key]:value},
                type:"GET",
                dataType:"json",
                success:function(response){
                    $.LoadingOverlay("hide")
                    if(response.status == CONSTANTS.SUCCESS){
                        if(response.data)
                        {
                        $("#tblContainer").html(response.data);
                        $("#submit").show();
                        }
                    }
                }
            })
        
    }
    </script>
    <script>
  
        $(function(){
          $('html').on("keypress", function(e) {
              
               
                  if (e.keyCode == 13) {
                   
                      var elements = $("form textarea");
                     
                      for(var i = 0 ; i <elements.length ; i ++){
                         
                        if($(elements[i]).is(":focus"))
                        {
                            
                        return;
                        }
                      }
                      var subProviderId = $("#sub_provider_id").val();
                      var categoryId = $("#category_id").val();
                      var providerId = $("#provider_id").val();
                      if(subProviderId || categoryId || providerId)
                      document.getElementById("submit").click();
                  }
              });
        })
          </script>
@stop