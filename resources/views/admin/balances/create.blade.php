{{-- V_5 UPDATES 27/12/2021 --}}
@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Balance') }}</h3>
                        </div>
                        <div class="box-body">
                            @if(session()->has('error_message'))
                            <div class="alert alert-danger">
                                {{ session()->get('error_message') }}
                            </div>
                            @endif
                            <br /><br />
                            <div id="content">

                                <div class="container-fluid">
                                    {!! Form::open(['route' => 'balances.store']) !!}
                                    <div class="row">
                                        <div class="col-md-4">
                                            {!! Form::label('amount', __('Amount')) !!}
                                            <div class="form-group">
                                                {!! Form::text('amount', $requestAmount ? $requestAmount: old('amount'), ['class' => 'form-control numeric', 'placeholder' => __('Amount')]) !!}
                                                {!! $errors->first('amount', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('user_id', __('Users')) !!}
                                            <div class="form-group">
                                                {!! Form::select('user_id', $usersLookup, $userId ? $userId : old('user_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('user_id', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            {!! Form::label('type', __('Type')) !!}
                                            <div class="form-group">
                                                {!! Form::select('type', $typesLookup, old('type') ?? 'deposit', ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('type', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>




                                    </div>
                                    @if ($document != null)
                                        <div class="row">

                                            <div class="col-md-6">
                                                <h3>{{ __('Attachment') }}</h3>
                                                @if ($type == 'text')
                                                    <h5>{{ $document }}</h5>
                                                @elseif($type=="image" || $type =="mobile_image")
                                                    <div><img  src="{{ $document }}" /></div>
                                               @endif
                                            </div>
                                        </div>
      
                                        <br />
                                        <div class="row">
                                            <div class="col-md-4">
                                                {!! Form::label('request_date', __('Request Date')) !!}
                                                <div class="form-group">
                                                    {!! Form::text('request_date', $requestDate, ['class' => 'form-control','disabled'=>true, 'placeholder' => __('Request Date')]) !!}
                                                </div>
    
                                            </div>
                                        </div>
                                    @endif


                                    {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        </div>
    </div>
    <script>
        $(function() {
            $("#amount").on('keypress',function(e){
                if (e.keyCode == 13) {
                    document.getElementById("submit").click();
                }
            })
            $('html').on("keypress", function(e) {


                if (e.keyCode == 13) {
                    
                    var elements = $("form textarea");

                    for (var i = 0; i < elements.length; i++) {

                        if ($(elements[i]).is(":focus")) {

                            return;
                        }
                    }


                    document.getElementById("submit").click();
                }
            });
        })
    </script>
@stop

{{-- END V_5 UPDATES 27/12/2021 --}}