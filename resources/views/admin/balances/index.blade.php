@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Charge/WithDraw Balances')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
          @if(session()->has('success_message'))
          <div class="alert alert-success">
              {{ session()->get('success_message') }}
          </div>
          @endif
          <a class="btn btn-success" href="{{route('balances.create')}}" style="color:white;">{{__('Charge / WithDraw')}}</a>

                    <br/><br/>
					<div class="table-responsive">
					  <table  id="balancesDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Full Name')}}</th>
                                <th>{{__('User Name')}}</th>
                                <th>{{__('Current Balance')}}</th>
                                <th>{{__("Updated At")}}</th>
              <th>{{__('Transactions')}}</th>
            
                            
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('Full Name')}}</th>
                                <th>{{__('User Name')}}</th>
                                <th>{{__('Current Balance')}}</th>
                                <th>{{__("Updated At")}}</th>
                                <th>{{__('Transactions')}}</th>
                               
                            
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
  var datatable = null;
  
 $(function(){
     datatable =  $('#balancesDataTable').DataTable({
         "processing":true,
     "serverSide": true,
     responsive: true,
            autoWidth:true,
            fixedColumns: true,
     
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
         
     ],
     "footerCallback": function (row, data, start, end, display) {
        var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total = api
            .column(3)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(3).footer()).html(
            "<h3>Total "+toFixed(intVal(total),3)+"</h3>"
            );
     },
     order: [],
     "pageLength":100,
     dom: 'Blfrtip',
     "ajax":"/admin/balances/data/",
     columns:[
         {"data": "#",
 render: function (data, type, row, meta) {
     
     return meta.row + meta.settings._iDisplayStart + 1;

 }
},
{"data":"full_name"},
{"data":"username"},
{"data":"balance"},
{"data":"updated_at"},
{"data":"options",render:function(data,type,row){
    return "<a href='/admin/balances/transactions/"+row.user_id+"' ><i class='fas fa-eye cursor-pointer'></i></a>"
}}







     ]
     });

 })


</script>
@stop