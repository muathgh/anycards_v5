@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Cards Prices')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('cardsprices.create')}}" style="color:white;">{{__('Add New Card Price')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="cardsPricesDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('User Name')}}</th>
                                <th>{{__('Full Name')}}</th>
                                <th>{{__('Card Type')}}</th>
                            <th>{{__('Price')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('User Name')}}</th>
                                <th>{{__('Full Name')}}</th>
                                <th>{{__('Card Type')}}</th>
                                <th>{{__('Price')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
    var datatable = null;
    
   $(function(){
       datatable =  $('#cardsPricesDataTable').DataTable({
           "processing":true,
       "serverSide": true,
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       responsive: true,
            autoWidth:true,
            fixedColumns: true,
       buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
           'print'
           
       ],
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":"/admin/cardsprices",
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"username"},
{"data":"full_name"},
{"data":"card_type"},
{"data":"price"},

{"data":"options",render:function(data,type,row){
   return "<a href='/admin/cardsprices/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
       ]
       });

   })

 
   $(document).on('click', '.delete', function(){ 
       var id = $(this).data('id');
       var url = "/admin/cardsprices/destroy/"+id;
       location.href=url;
      
   })
</script>
@stop