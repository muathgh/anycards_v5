@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{$cardPrice->user->name}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'cardsprices.update']) !!}
                        {!! Form::hidden('id',$cardPrice->id) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('user_id', __('Users'))!!}
                                <div class="form-group">
                                    {!! Form::select('user_id', $usersLookup, $cardPrice->user->id,["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('user_id', '<p class="error">:message</p>') !!}
                                    @if(\Session::has('duplicate_value_error_message'))
                                    <p class="error">{!! \Session::get('duplicate_value_error_message') !!}</p>
                                    @endif
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Cards'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id', $cardsTypeLookup, $cardPrice->cardType->id,["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                            <div class="col-md-4">
                                {!! Form::label('price', __('Price'))!!}
                                <div class="form-group">
                                    {!! Form::number('price', $cardPrice->price,['class'=>'form-control','placeholder'=>__('Price')]) !!}
                                    {!! $errors->first('price', '<p class="error">:message</p>') !!}
                                    @if(\Session::has('price_error_message'))
										<p class="error">{!! \Session::get('price_error_message') !!}</p>
										@endif
                                </div>
                                
                            </div>
                          
                            
                        </div>
                   
                        {!! Form::submit(__('Save'),['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

@stop