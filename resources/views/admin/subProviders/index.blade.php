@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('SubProviders')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                    <div class="container">
                        <div class="row">
                    <div class="col-md-3">
						{!! Form::label('category_id', __('Categories'))!!}
						<div class="form-group">
							{!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
						</div>
						
					</div>
					<div class="col-md-3">
						{!! Form::label('provider_id', __('Providers'))!!}
						<div class="form-group">
							{!! Form::select('provider_id', [], old('provider_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}

						</div>
						
                    </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <button class="btn btn-primary" id="search">{{__('Search')}}</button>
                            </div>
                        </div>
                        <br/>
                    </div>
                <a class="btn btn-success" href="{{route('subProviders.create')}}" style="color:white;">{{__('Add New SubProvider')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="subProvidersDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                             
                                <th>{{__('Description Ar')}}</th>
                         
                            <th>{{__('Provider Title Ar')}}</th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Activation')}}</th>
                            <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                          
                                <th>{{__('Description Ar')}}</th>
                             
                                <th>{{__('Provider Title Ar')}}</th>
                                <th>{{__('Image')}}</th>
                                <th>{{__('Activation')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
     var formData = {
         category_id:null,
         provider_id : null
     }
    $(function(){
        tableId =   $("table").attr("id");
        if(localStorage.getItem(tableId+"_state")){
  
            var json = localStorage.getItem(tableId+"_state");
            json = JSON.parse(json);
            formData = json;
            $("#category_id").val(formData.category_id);
            loadProviders(formData.category_id).then(function(response){
              
                $("#provider_id").val(formData.provider_id).trigger("change");
                
              
         }).catch(error=>console.log("err"));
        
        }
        

        // dropdown
        		//dropdowns
		$("#category_id").change(function(){
            $("#provider_id").empty().append("<option value=''>{{__('All')}}</option>");
       $("#sub_provider_id").empty().append("<option value=''>{{__('All')}}</option>");
       $("#card_type_id").empty().append("<option value=''>{{__('All')}}</option>");
    var categoryId = $(this).val();
    loadProviders(categoryId)
  
})
        // end
       dt();

    })

    function loadProviders(categoryId){
        return new Promise((resolve, reject) => {
        $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>{{__('All')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
    }
           }
           resolve(response);
       }
   })

        });
    }


    function dt(){
        $('#subProvidersDataTable').DataTable({
            "processing":true,
        "serverSide": true,
        order:[],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        responsive: true,
            autoWidth:true,
            fixedColumns: true,
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
            'print',
            {
                text:"Save State",
                action: function ( e, dt, node, config ) {
                  
                    toast("Success","State Saved Successfully","State Saved Successfully")
           
                  
                    localStorage.setItem(tableId+"_state",JSON.stringify(formData));
                   
                    
                }
            },
            {
                text:"Clear State",
                action: function ( e, dt, node, config ) {
                    localStorage.removeItem(tableId+"_state");
                    toast("Success","State Cleard Successfully","State Cleard Successfully")
                


                     setTimeout(() => {
                        location.reload();  
                     }, 1000);
                }
            }
        ],
        "pageLength":100,
        dom: 'Blfrtip',
        "ajax":{
            url:"/admin/subProviders/data/",
            data:formData
        },
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},
{"data":"title_en"},
{"data":"title_ar"},

{"data":"description_ar",render:function(data,type,row){
    return trimStringAr(20,row.description_ar)
}},

{"data":"provider_title_ar"},
{"data":"image",render:function(data,type,row){
    return "<img style='width:50px;height:auto;' class='img-responsive' src="+row.image+">";
}},
{"data":"is_active",render:function(data,type,row){
if(row.is_active)
return "<span class='badge badge-success'>Active</span";
    return "<span class='badge badge-danger'>InActive</span";
}},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
    return "<a href='/admin/subProviders/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
        ]
        });
    }

    $(document).on('click',"#search",function(){
        formData.category_id = $("#category_id").val();
        formData.provider_id = $("#provider_id").val();
        $("#subProvidersDataTable").DataTable().destroy();
        dt();
    })

  
    $(document).on('click', '.delete', function(){ 
  
        var id = $(this).data('id');
        $.ajax({
            url:"/admin/subProviders/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
       
    })

</script>
@stop