@inject('helper', 'App\Http\Helper')
@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Push Notifications')}}</h3>
                </div>
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'pushnotifications.store',"id"=>"pushNotification"]) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('title', __('Title'))!!}
                                <div class="form-group">
                                    
                                    {!! Form::text('title', old('title'),['class'=>'form-control','placeholder'=>__('Title'),"maxlength"=>$helper->getMaxNotificationTitle()]) !!}
                                    {!! $errors->first('title', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('description', __('Description'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('description', old('description'),['class'=>'form-control','placeholder'=>__('Description'),"cols"=>"20","rows"=>"5" ,"maxlength"=>$helper->getMaxNotificationDescription()]) !!}
                                    {!! $errors->first('description', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('users', __('Users'))!!}
                                <div class="form-group">
                                    {!! Form::select('users', ["all_users"=>"All Users","specfic_users"=>"Specfic Users"], old("users"),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('users', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-5">
                                {!! Form::label('user_id', __('Users'))!!}
                                <div class="form-group">
                                    {!! Form::select('user_id', $usersLookup, old('user_id'),["class"=>"form-control select2","multiple"=>"multiple","name"=>"user_id[]","disabled"=>true]) !!}
                                </div>
                                <p id="user_id_error" class="error" style="display:none">
                                    This field is required
                                    </p>
                            </div>
                          
                          

                          
                            
                        </div>
                      
                        {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){
        $("#users").change(function(){
            $("#user_id").attr('disabled',true);
          
            if($(this).val()=="specfic_users")
            $("#user_id").attr('disabled',false);
        })

$("#pushNotification").submit(function(e){
 
    $("#user_id_error").hide();
    if($("#users").val() == "specfic_users"){
        if($("#user_id").val()==null || $("#user_id").val()=="")
        {
        $("#user_id_error").show();
        return false;
        }
    }
 $(this).submit();
})

    })
    </script>
@stop