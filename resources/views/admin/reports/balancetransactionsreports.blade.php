@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Balance Transactions Report')}}</h3>
				</div>
				<br/><br/>
				<div class="container">
                <div class="row">

                    <div class="col-md-3">
						{!! Form::label('user_id', __('Users'))!!}
						<div class="form-group">
							{!! Form::select('user_id', $usersLookup, old('user_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
						</div>
						
					</div>

                    <div class="col-md-3">
                        {!! Form::label('from_date', __('From Date'))!!}
                        <div class="form-group">
                            {!! Form::text('from_date', old('from_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('From Date')]) !!}
                        </div>
                       
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('to_date', __('To Date'))!!}
                        <div class="form-group">
                            {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                        <p class="error" id="to_date_error" style="display:none">{{__('Both of from date and to date are requireds')}}</p>
                        
                        </div>
                       
                    </div>

                    @if(Auth::guard('admin')->user()->accessType() != null && Auth::guard('admin')->user()->accessType() != "created_users")
                    <div class="col-md-3">
						{!! Form::label('admin_id', __('Admins'))!!}
						<div class="form-group">
							{!! Form::select('admin_id', $adminsLookup, old('admin_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
						</div>
						
					</div>
                    @else
                    <input type="hidden" id="admin_id" value="0"/>
                    @endif
					
{{-- 					
					<div class="col-md-3">
						{!! Form::label('card_type_id', __('Card Type'))!!}
						<div class="form-group">
							{!! Form::select('card_type_id', [], old('card_type_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}

						</div>
						
                    </div> --}}
                   
                   
					
					
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
						<input type="button" onclick="search()" value="{{__('Search')}}" class="btn btn-info"/>
						</div>
					</div>
				</div>
				
				</div>
				<!-- /.box-header -->
				<div class="box-body">
                    <br/><br/>
					<div class="table-responsive">
                        <table  id="balanceTransactionsReportsDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                <th>{{__('#')}}</th>
                                    <th>{{__('Full Name')}}</th>
                              
                                    
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Type')}}</th>
                                  
                                    <th>{{__('Receivables')}}</th>
                                    <th>{{__('Notes')}}</th>
                                    <th>{{__('Created By')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th>{{__('Options')}}</th>
                                </tr>
                            </thead>
                        <tbody>
                        </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{__('#')}}</th>
                                    <th>{{__('Full Name')}}</th>
                              
                                    
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Type')}}</th>
                                  
                                    <th>{{__('Receivables')}}</th>
                                    <th>{{__('Total Of Amount - Total Of Receivables')}}</th>
                                    <th>{{__('Created By')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th>{{__('Options')}}</th>
                                </tr>
                            </tfoot>
                          </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
<script>
        var formData = {
            from_date:null,
        to_date:null,
        user_id : null,
        admin_id:null
     
    }
       $(function(){
        tableId =   $("table").attr("id");
        if(localStorage.getItem(tableId+"_state")){
            var json = localStorage.getItem(tableId+"_state");
            json = JSON.parse(json);
            formData = json;
            $('#user_id').val(formData.user_id)
            $('#from_date').val(formData.from_date);
            $("#to_date").val(formData.to_date)
        }
        $('#notes-popup').on('hidden.bs.modal', function () {
            $('#balanceTransactionsReportsDataTable').DataTable().destroy();
        dt();
 
});



dt()
   })

   function dt(){
    datatable =  $('#balanceTransactionsReportsDataTable').DataTable({
        order:[],
           "processing":true,
       "serverSide": true,
       responsive: true,
            autoWidth:true,
            fixedColumns: true,
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
           'print', {
                text:"Save State",
                action: function ( e, dt, node, config ) {
                  
                    toast("Success","State Saved Successfully","State Saved Successfully")
           
                  
                    localStorage.setItem(tableId+"_state",JSON.stringify(formData));
                   
                    
                }
            }, {
                text:"Clear State",
                action: function ( e, dt, node, config ) {
                    localStorage.removeItem(tableId+"_state");
                    toast("Success","State Cleard Successfully","State Cleard Successfully")
                


                     setTimeout(() => {
                        location.reload();  
                     }, 1000);
                }
            }
           
       ],
       "pageLength":-1,
       dom: 'Blfrtip',
       "ajax":{
           url:"{{route('reports.balancetransactionsreports')}}",
           data:{filter:formData}
       },
       "footerCallback": function (row, data, start, end, display) {
          var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
       
  
   
          
            total2 = api
            .column(2)
            .data()
            .reduce(function (a, b) {
                return ((intVal(a) + intVal(b)));
            }, 0);
  
    $(api.column(2).footer()).html(
            "<h3>Total "+toFixed(total2,3)+"</h3>"
            );

            total4 = api
            .column(4)
            .data()
            .reduce(function (a, b) {
                return ((intVal(a) + intVal(b)));
            }, 0);
  
    $(api.column(4).footer()).html(
            "<h3>Total "+toFixed(total4,3)+"</h3>"
            );

            var def = total2 - total4;
            $(api.column(5).footer()).html(
            "<h3>"+toFixed(def,3)+"</h3>"
            );
  
   
},
    
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},

{"data":"full_name"},

{"data":"amount"},
{"data":"type",render:function(data,type,row){
    if(row.type == "deposit"){
        return "<span class='badge badge-success'>Deposit</span>"
    }
    return "<span class='badge badge-danger'>Withdraw</span>"
}},
{"data":"receivables" , render : function(data,type,row){
    if(!row.receivables)
    return "<span></span>";

    return row.receivables;
}},
{"data":"notes" , render : function(data,type,row){
    if(!row.notes)
    return "<span></span>";

    return row.notes;
}},

{"data":"created_by"},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
    var options =  "<i class='fas fa-edit edit cursor-pointer' data-transactionid="+row.transaction_id+" data-userid="+row.user_id+"></i> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.transaction_id+"></i>"
if(row.request_recharge_id){
    options +=" | <i class='fas fa-eye view cursor-pointer' data-requestid="+row.request_recharge_id+"></i>"
}
return options
}}

       ]
       });
}


$(document).on('click', '.edit', function(){ 
        var userId = $(this).data('userid');
        var transactionId = $(this).data('transactionid');
       $.ajax({
           type:"get",
           data:{user_id:userId,transaction_id:transactionId},
           dataType:"json",
           url :"{{route('reports.notespopup')}}",
           success:function(response){
               if(response.status == "success"){
                   var html  = response.data;
                   $("#notes-popup .modal-body").html(html);
                   $("#notes-popup").modal('toggle');
               }
           }
       })
      
     
    })

    $(document).on('click', '.view', function() {
            var id = $(this).data("requestid")
            $.ajax({
                url: "{{ route('balances.balancesrequestspopup') }}",
                type: "POST",
                data: {
                    id: id,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    if (response.status == "success") {
                        var data = response.data;
                        var html = data.html

                        $("#modal .modal-body").html(html);
                        $("#modal").modal('toggle');
                    }
                }
            })

        })

    $(document).on('click', '.delete', function(){ 
        var id = $(this).data('id');
       $.ajax({
           type:"get",
           data:{id:id},
           dataType:"json",
           url :"{{route('reports.deletenotes')}}",
           success:function(response){
               if(response.status == "success"){
                   var html  = response.data;
                   $("#delete-popup .modal-body").html(html);
                   $("#delete-popup").modal('toggle');
               }
           }
       })
      
     
    })

function search(){
    $("#to_date_error").hide();
     
   
       
     
    var userId = $("#user_id").val();
    var adminId = $("#admin_id").val();
        var fromDate = $("#from_date").val();
        var toDate = $("#to_date").val();
        if((!fromDate && toDate) || (fromDate && !toDate))
        {
            $("#to_date_error").show();
            return ;
        }
        formData.from_date = fromDate;
        formData.to_date =toDate;
        formData.user_id = userId;
        formData.admin_id = adminId;
        $('#balanceTransactionsReportsDataTable').DataTable().destroy();
        dt();
    
     
        
    }

    </script>

    {{-- popup --}}
    <div id="notes-popup" class="modal fade" role="dialog">

        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" style="margin:0px;" onclick="$('#notes-popup').modal('toggle');"  class="close" >&times;</button>
              <h4 class="modal-title">{{__('Notes')}}</h4>
            </div>
            <div class="modal-body">
               
              </div>
            </div>
         
          </div>
      
        </div>
        <div id="delete-popup" class="modal fade" role="dialog">

            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" style="margin:0px;" onclick="$('#delete-popup').modal('toggle');"  class="close" >&times;</button>
                  <h4 class="modal-title">{{__('Delete Notes')}}</h4>
                </div>
                <div class="modal-body">
                   
                  </div>
                </div>
             
              </div>
          
            </div>
      </div>
@stop