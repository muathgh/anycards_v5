

  <form action="#" id="notesForm" method="POST">
    <input type="hidden" name="_user_id" id="_user_id" data-id="{{$userId}}"/>
    <input type="hidden" name="_transaction_id" id="_transaction_id" data-id="{{$transactionId}}"/>
<div class="row">
  <div class="col-md-4">
      <label>Notes</label>
      <div class="form-group">
          <input type="text" id="notes" name="notes" class="form-control" placeholder="{{__('Notes')}}"/>
      </div>
  </div>
  <div class="col-md-4">
    <label>Unreceived</label>
    <div class="form-group">
        <input type="text" class="form-control numeric" id="unreceived" name="unreceived" placeholder="{{__('Unreceived')}}"/>
    </div>
</div>
</div>
<div class="row">
  <div class="col-md-4">
<input type="submit" class="btn btn-primary" id="submit" value="{{__('Save')}}"/>
  </div>
</div>
</form>

<script>
  function isNumber(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (            
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;
        return true;
}
$(function(){
  $(".numeric").keypress(function (event) {
       
       return isNumber(event, this)
});

$("#notesForm").validate({
    rules:{
      
      notes:"required",
      unreceived:"required"
    },
    messages:{
   
      notes:{
            required:"{{__('This field is required')}}"
        },
        unreceived:{
            required:"{{__('This field is required')}}"
        },
        
    },
    submitHandler:function(form){
      var url = "{{route('reports.unreceived')}}"
      var userId = $("#_user_id").data("id");
      var transactionId = $("#_transaction_id").data('id');
    
      var notes = $("#notes").val();
      var unreceived = $("#unreceived").val();

   

      $.ajax({
        url:url,
        type:"POST",
        data:{user_id:userId,transaction_id:transactionId,notes:notes,unreceived:unreceived,"_token": "{{ csrf_token() }}",},
        success:function(response){
          $('#notes-popup').modal('toggle');
        }
      })
        return false;

    }
})
});
</script>

<script>
  
  $(function(){

    $("#notes").on('keypress', function(e) {
            if (e.keyCode == 13) {
                document.getElementById("submit").click();
            }
        })
        $("#unreceived").on('keypress', function(e) {
            if (e.keyCode == 13) {
                document.getElementById("submit").click();
            }
        })

    $('html').on("keypress", function(e) {
        
           
            if (e.keyCode == 13) {
                 var elements = $("form textarea");
               
                for(var i = 0 ; i <elements.length ; i ++){
                   
                  if($(elements[i]).is(":focus"))
                  return;
                }
          
                
                document.getElementById("submit").click();
            }
        });
  })
    </script>