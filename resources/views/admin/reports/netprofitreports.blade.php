@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Net Profit Report')}}</h3>
				</div>
				<br/><br/>
                <div class="container">
                    {{-- <div class="row">
                        <div class="col-md-4">
                        <h3>{{__('Total Stock')}} : {{$totalStock}}</h3>
                        </div>
                        <div class="col-md-4">
                            <h3>{{__('Total Balance')}} : {{$totalBalance}}</h3>
                            </div>
                            <div class="col-md-4">
                                <h3>{{__('Net Profit')}} : {{$netProfit}}</h3>
                                </div>
                    </div> --}}
                    <div  id="tblContainer" class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <input type="button" value="{{__('Save')}}" onclick="save()" class="btn btn-success"/>

                            </div>
                           
                            <div class="offset-md-6 col-md-2 text-right">
                                <input type="button" value="{{__('Add')}}" onclick="add()" class="btn btn-primary"/>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2 text-left">
                                <form action="{{route('reports.exportnetprofit')}}" method="POST">
                                    @csrf
                                <input type="submit" value="{{__('Export')}}" class="btn btn-primary"/>
                                </form>
                                </div>
                        </div>
                        <br/>
                        <table id="netProfitTable"  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th>
                                        {{__("No")}}
                                    </th>
                                    <th>
                                        {{__('Total Of Stock')}}
                                    </th>
                                    <th>
                                        {{__('(-) Total Of Current Balance')}}
                                    </th>
                                    <th>
                                        {{__("(-) Receivables")}}
                                    </th>
                                    <th>
                                        {{__('(+) Unreceived')}}
                                    </th>
                                    <th>
                                        {{__('(+) Other')}}
                                    </th>
                                    <th>
                                        {{__('(-) Monthly Profit')}}
                                    </th>
                                    <th>
                                        {{__('(-) Net Profit')}}
                                    </th>
                                    <th>
                                        {{__('Options')}}
                                    </th>
                                </tr>
                            </thead>
                           <tbody>
                               @if(sizeof($data)<=0)
                               <tr id="new_1"  data-num="1">
                                <td>
                               1
                                </td>
                                <td>

                                 <div>
                                     <input type="text"  class="form-control stock_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                    <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3"  type="text" style="display:none;"  class="form-control m-t-10 stock_notes _input" placeholder="notes"></textarea>
                           </div>

                                </td>
                                <td>
                                 <div>
                                     <input type="text"  class="form-control current_balance_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                    <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 current_balance_notes _input" placeholder="notes" ></textarea>
                           </div>
                             </td>
                             <td>
                                 <div>
                                     <input type="text"  class="form-control receivables_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                    <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 receivables_notes _input" placeholder="notes" ></textarea>
                           </div>
                             </td>
                             <td>
                                 <div>
                                     <input type="text"  class="form-control unreceived_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                    <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3" type="text"  style="display:none;" class="form-control m-t-10 unreceived_notes _input" placeholder="notes" ></textarea>
                           </div>
                             </td>
                             <td>
                                 <div>
                                     <input type="text"  class="form-control other_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                     <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 other_notes _input" placeholder="notes" ></textarea>
                           </div>
                             </td>
                             <td>
                                 <div>
                                     <input type="text"  class="form-control monthly_profit_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                     <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 monthly_profit_notes _input" placeholder="notes" ></textarea>
                           </div>
                             </td>
                             <td>
                                 <div>
                                     <input type="text" value=""  class="form-control net_profit_amount" placeholder="amount" />
                              
                                  </div>
                                  <hr/>
                                  <div style="margin-top:10px">
                                     <span class="_text"></span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                  <textarea cols="5" rows="3" type="text" style="display:none;"   class="form-control m-t-10 net_profit_notes _input" placeholder="notes" ></textarea>
                           </div>
                             </td>
                             <td>
                                 <i class='fas fa-trash delete cursor-pointer' onclick="remove(this)"></i>
                             </td>
                            </tr>
                               @endif
                               @foreach($data as $key=>$value)
                             
                               <tr id="tr_{{($key+1)}}" data-id="{{$value->id}}" data-num="{{$key+1}}">
                                   <td>
                                   {{($key+1)}}
                                   </td>
                                   <td>

                                    <div>
                                        <input type="text" value="{{$value->stock_amount}}" class="form-control stock_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->stock_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>
                                     <textarea cols="5" rows="3" type="text" style="display:none;" style="display:none;"  class="form-control m-t-10 stock_notes _input" placeholder="notes">{{$value->stock_notes}}</textarea>
                              </div>

                                   </td>
                                   <td>
                                    <div>
                                        <input type="text" value="{{$value->current_balance_amount}}" class="form-control current_balance_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->current_balance_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>
                                     <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 current_balance_notes _input" placeholder="notes" >{{$value->current_balance_notes}}</textarea>
                              </div>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" value="{{$value->receivables_amount}}" class="form-control receivables_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->receivables_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                     <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 receivables_notes _input" placeholder="notes" >{{$value->receivables_notes}}</textarea>
                              </div>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" value="{{$value->unreceived_amount}}" class="form-control unreceived_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->unreceived_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                     <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 unreceived_notes _input" placeholder="notes" >{{$value->unreceived_notes}}</textarea>
                              </div>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" value="{{$value->other_amount}}" class="form-control other_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->other_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                     <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 other_notes _input" placeholder="notes" >{{$value->other_notes}}</textarea>
                              </div>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" value="{{$value->monthly_profit_amount}}"  class="form-control monthly_profit_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->monthly_profit_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                     <textarea cols="5" rows="3" type="text" style="display:none;"  class="form-control m-t-10 monthly_profit_notes _input" placeholder="notes" >{{$value->monthly_profit_notes}}</textarea>
                              </div>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" value="{{$value->net_profit_amount}}"  class="form-control net_profit_amount" placeholder="amount" />
                                 
                                     </div>
                                     <hr/>
                                     <div style="margin-top:10px">
                                        <span class="_text">{{$value->net_profit_notes}}</span><span class="fas fa-edit edit" style="float:right;line-height:20px;cursor:pointer;"></span>

                                     <textarea cols="5" rows="3" type="text" style="display:none;"   class="form-control m-t-10 net_profit_notes _input" placeholder="notes" >{{$value->net_profit_notes}}</textarea>
                              </div>
                                </td>
                                <td>
                                    <i class='fas fa-trash delete cursor-pointer' onclick="remove(this)"></i>
                                </td>
                               </tr>
                               @endforeach
                           </tbody>
                            <tfoot>
                                <tr>
                                    <th>
                                        {{__("No")}}
                                    </th>
                                    <th>Total {{$totalStock}}</th>
                                    <th>Total {{$totalBalance}}</th>
                                    <th>
                                       Total {{$totalReceivables}}
                                    </th>
                                    <th>
                                        Total {{$totalUnreceived}}
                                    </th>
                                    <th>
                                        Total {{$totalOthers}}
                                    </th>
                                    <th>Total {{$totalmonthlyProfitAmount}}</th>
                                    <th>Net Profit {{$netProfit}}</th>
                                    <th>
                                        {{__('Options')}}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
              
            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){
        $(document).on('click','.edit',function(e){
            var ele = $(this).parent();;
            var input = $(ele).find("._input");
            var text = $(ele).find('._text');
            $(text).hide();
            $(this).hide();
            $(input).show();
           
        })
    })
    function add(){
       
        var table = $("#netProfitTable");
        var lastRow = $('#netProfitTable tbody tr:last');
        var newRow = lastRow.clone();
        var num = $(newRow).data("num");
        num= num+1;
        $(newRow).find("td input").val("");
        $(newRow).find("td ._input").val('');
        $(newRow).find('td ._text').html("");
        $(newRow).find("td:first").html(num);
        $(newRow).attr('data-num',num);
        $(newRow).attr('id','new_'+num);
        $(table).find("tbody").append(newRow);
      

       
        
    }
    function save(){
        var formData = [];
        var table = $("#netProfitTable");
        $(table).find("tbody tr").each(function(index,tr){
            var stockAmount = $(tr).find(".stock_amount");
            var stockNotes = $(tr).find(".stock_notes");
            var receivablesAmount = $(tr).find(".receivables_amount");
            var receivablesNotes = $(tr).find(".receivables_notes");
            var unreceivedAmount = $(tr).find(".unreceived_amount");
            var unreceivedNotes = $(tr).find(".unreceived_notes");
            var otherAmount = $(tr).find(".other_amount");
            var otherNotes = $(tr).find(".other_notes");
            var currentBalanceAmount = $(tr).find('.current_balance_amount')
            var currentBalanceNotes = $(tr).find('.current_balance_notes')
            var netProfitAmount = $(tr).find('.net_profit_amount')
            var netProfitNotes = $(tr).find('.net_profit_notes')
            var monthlyProfitAmount = $(tr).find(".monthly_profit_amount")
            var monthlyProfitNotes = $(tr).find(".monthly_profit_notes")
            if(
                $(stockAmount).val() ||  $(stockNotes).val()
               || $(receivablesAmount).val() || $(receivablesNotes).val() 
            || $(unreceivedAmount).val() 
            || $(unreceivedNotes).val() || $(otherAmount).val() || $(otherNotes).val() 
            || $(currentBalanceAmount).val() || $(currentBalanceNotes).val() 
            ||  $(netProfitAmount).val() || $(netProfitNotes).val()
            || $(monthlyProfitAmount).val() || $(monthlyProfitNotes).val()
            )
           formData.push({
            receivables_amount:$(receivablesAmount).val(),
            receivables_notes:$(receivablesNotes).val(),
            unreceived_amount:$(unreceivedAmount).val(),
            unreceived_notes:$(unreceivedNotes).val(),
            other_amount:$(otherAmount).val(),
            other_notes:$(otherNotes).val(),
            current_balance_amount:$(currentBalanceAmount).val(),
            current_balance_notes:$(currentBalanceNotes).val(),
            net_profit_amount:$(netProfitAmount).val(),
            net_profit_notes :$(netProfitNotes).val(),
            stock_amount : $(stockAmount).val(),
            stock_notes :$(stockNotes).val(),
            monthly_profit_amount:$(monthlyProfitAmount).val(),
            monthly_profit_notes:$(monthlyProfitNotes).val(),



            status : $(tr).attr("id").includes('new') ? "new" :"update",
            id:$(tr).data('id')
          })
          
        
        })
        $.LoadingOverlay("show")
        $.ajax({
            url:"{{route('reports.savenetprofit')}}",
            data:{data:JSON.stringify(formData), "_token": "{{ csrf_token() }}"},
            type:"POST",
            success:function(response){
                $.LoadingOverlay("hide")
               location.reload();
            }
        })

    }

    function remove(ele){
var parent = $(ele).closest("tr");
var id = $(parent).attr("id");
var reload = false;
if(id.includes("new")){
    if($(parent).attr('class')=="first-element")
    reload=true;
   $(parent).remove()

   if(reload)
   location.reload()
   //reset #
   var table = $("#netProfitTable");
   var num = 1
        $(table).find("tbody tr").each(function(index,tr){
            $(tr).attr('data-num',num);
            $(tr).find("td:first").html(num);
            num++
        })
}else{
$("#popup").modal('toggle');
$("#delete").val($(parent).data('id'));
}
    }
    
    $(document).on('click','#delete',function(){
        $('#popup').modal('toggle')
        $.LoadingOverlay("show")
        var id = $("#delete").val();
        $.ajax({
            url:"{{route('reports.deletenetprofit')}}",
            type:"POST",
            data:{id:id,"_token": "{{ csrf_token() }}"},
            success:function(){
                $.LoadingOverlay("hide")
                location.reload();
            }
        })
    })
    </script>

<div id="popup" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header flex-column">
      
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
                <input type="hidden" id="delete" value="" />
<div class="row">
    <div class="col-md-12">
  <p style="text-align:center">Do you really want to delete this record? This process cannot be undone.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 offset-md-3">
      <input style="width:100px;" type="button" class="btn btn-success" id="delete" value="YES" id="yes" />
    </div>
    <div class="col-md-3">
      <input style="width:100px;" type="button" class="btn btn-danger" onclick="$('#popup').modal('toggle')" value="CANCEL" id="cancel" />
    </div>
  </div>
			</div>
		
		</div>
	</div>
</div> 
@stop