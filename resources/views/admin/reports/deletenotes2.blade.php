<style>
    input{
      width:100px;
    }
    p{
      text-align:center
    }
    </style>
  <input type="hidden" id="transaction-id" value="{{$id}}"/>
  <div class="row">
      <div class="col-md-12">
    <p>Do you really want to delete this record notes? This process cannot be undone.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 offset-md-3">
        <input type="button" class="btn btn-success" onclick="remove()" value="YES" id="yes" />
      </div>
      <div class="col-md-3">
        <input type="button" class="btn btn-danger" onclick="$('#delete-popup').modal('toggle')" value="CANCEL" id="cancel" />
      </div>
    </div>

    <script>
        function remove(){
            var id = $("#transaction-id").val();
            $.ajax({
                url :"{{route('reports.deletenotes2.post')}}",
                data:{id:id,"_token": "{{ csrf_token() }}"},
                type:"POST",
                success:function(response){
                    location.reload();
                }
            })
        }
        </script>