@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Users Report')}}</h3>
				</div>
				<br/><br/>
				<div class="container">
              
				<div class="row">
                    
                    
					<div class="offset-md-11 col-md-3">
						<div class="form-group">
						<input type="button" onclick="save()" value="{{__('Save')}}" class="btn btn-success"/>
						</div>
					</div>
				</div>
				
				</div>
				<!-- /.box-header -->
				<div class="box-body">
                    <br/><br/>
					<div class="table-responsive">
                        <table  id="usersReportsDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('User Name')}}</th>
                              
                                      <th>{{__('Current Balance')}}</th>
                                    <th>{{__('Last Deposit Transaction')}}</th>
                                  <th>{{__('Balance Per Year')}}</th>
                                
                                    <th>{{__('Receivables')}}</th>
                                    <th>{{__('Notes')}}</th>
                                 
                                </tr>
                            </thead>
                        <tbody>
                            @foreach($data as $key=>$d)
<tr data-id={{$d['user_id']}}>
    <td>{{($key+1)}}</td>
    <td>{{$d["username"]}}</td>
 
    <td>{{$d["current_balance"]}}</td>
       <td>{{$d["last_transaction"]}}</td>
       <td>{{$d["balance_per_year"]}}</td>
       
    <td><input type="text"  value="{{$d['notes']}}" placeholder="{{__('Notes')}}" class='form-control notes'/></td>
    <td><input type="text"  value="{{$d['note']}}" placeholder="{{__('Notes')}}" class='form-control note'/></td>
</tr>
                            @endforeach
                            
                        </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{__('#')}}</th>
                                    <th>{{__('User Name')}}</th>
                              
                                    
                           <th>{{__('Current Balance')}}</th>
                                    <th>{{__('Last Deposit Transaction')}}</th>

                                    <th>{{__('Balance Per Year')}}</th>
                                
                                    <th><h3>{{__('Total ')}} {{$totalOfReceivables}}</h3></th>
                                    <th>{{__('Notes')}}</th>
                                   
                                  
                                </tr>
                            </tfoot>
                          </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>

<script>
    $(function(){

//         $(".numeric").keypress(function (evt) {
 
//             if (evt.which < 48 || evt.which > 57)
//     {
//         evt.preventDefault();
//     }
// });

        $("#usersReportsDataTable").DataTable({
            order:[],
            "pageLength":-1,
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            "footerCallback": function (row, data, start, end, display) {
          var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
       
  
   
          
            total2 = api
            .column(2)
            .data()
            .reduce(function (a, b) {
                return ((intVal(a) + intVal(b)));
            }, 0);
  
    $(api.column(2).footer()).html(
            "<h3>Total "+toFixed(total2,3)+"</h3>"
            );

            total3 = api
            .column(3)
            .data()
            .reduce(function (a, b) {
                return ((intVal(a) + intVal(b)));
            }, 0);

            total4 = api
            .column(4)
            .data()
            .reduce(function (a, b) {
                return ((intVal(a) + intVal(b)));
            }, 0);
  
    $(api.column(3).footer()).html(
            "<h3>Total "+toFixed(total3,3)+"</h3>"
            );

            $(api.column(4).footer()).html(
            "<h3>Total "+toFixed(total4,3)+"</h3>"
            );
},
        });
    })

    function save(){
        var formData = [];
        $("#usersReportsDataTable tbody tr").each(function(index,item){
         
          var userId =  $(item).data('id');
          
          var notes=  $(item).find('.notes').val();
          var note=  $(item).find('.note').val();
        //   if(notes.trim() != ""){
             
              formData.push({
                  user_id : userId,
                  notes:notes,
                  note:note
              })
         // }
          
        })
        $.LoadingOverlay('show')
        $.ajax({

            url:"{{route('reports.receivables')}}",
            data:{data:JSON.stringify(formData),"_token": "{{ csrf_token() }}"},
            type:"POST",
            dataType:"JSON",
            success:function(result){
                $.LoadingOverlay('hide')
                toast('Success',"{{__('Saved Successfully')}}","{{__('Saved Successfully')}}");
                setTimeout(() => {
                    location.reload()
                }, 1000);
            }

        })
        
    }

    </script>
@stop
