@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{$cardType->title_en}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'cardsTypes.update','files' => true]) !!}
                        {!! Form::hidden('id',$cardType->id) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('title_en', __('Title En'))!!}
                                <div class="form-group">
                                    {!! Form::text('title_en', $cardType->title_en,['class'=>'form-control','placeholder'=>__('Title En')]) !!}
                                    {!! $errors->first('title_en', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('title_ar', __('Title Ar'))!!}
                                <div class="form-group">
                                    {!! Form::text('title_ar', $cardType->title_ar,['class'=>'form-control','placeholder'=>__('Title Ar')]) !!}
                                    {!! $errors->first('title_ar', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                       
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('Sub Provider'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id', $lookup, $cardType->sub_provider_id,["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                          
                            
                        </div>
                        <div class="row">

                            <div class="col-md-3">
                                {!! Form::label('price', __('Price'))!!}
                                <div class="form-group">
                                    {!! Form::text('price', $cardType->price,['class'=>'form-control numeric','placeholder'=>__('Price')]) !!}
                                    {!! $errors->first('price', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('image', __('Image'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control file-upload-text" disabled placeholder="{{__('Select Image')}}" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default file-upload-btn">
                                            {{__('Browse...')}}
                                            <input name="image" type="file" class="file-upload"/>
                                        </button>
                                    </span>
                                </div>
                                {!! $errors->first('image', '<p class="error">:message</p>') !!}
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('description_en', __('Description En'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('description_en', $cardType->description_en,['class'=>'form-control','placeholder'=>__('Description En'),"cols"=>"20","rows"=>"5"]) !!}
                                    {!! $errors->first('description_en', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('description_ar', __('Description Ar'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('description_ar', $cardType->description_ar,['class'=>'form-control','placeholder'=>__('Description Ar'),"cols"=>"20","rows"=>"5"]) !!}
                                    {!! $errors->first('description_ar', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                {!! Form::label('order', __('Order'))!!}
                                <div class="form-group">
                                    {!! Form::text('order', $cardType->order,['class'=>'form-control','placeholder'=>__('Order')]) !!}
                                    {!! $errors->first('order', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>

                            <div class="col-md-3">
                                {!! Form::label('card_code', __('Code'))!!}
                                <div class="form-group">
                                    {!! Form::text('card_code', $cardType->card_code,['class'=>'form-control','placeholder'=>__('Code')]) !!}
                                    {!! $errors->first('card_code', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('is_active', __('Is Active'))!!}
                                <div class="form-group">
                                    {!! Form::select('is_active', [true=>"Active",false=>"InActive"], $cardType->is_active,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('is_active', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>

                        </div>

                        {{-- <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('template', __('Template'))!!}
                                <div class="form-group">
                                    <textarea maxlength="65"  name="template"  class="form-control">{{$cardType->template}}</textarea>
                                </div>

                            </div>
                        </div> --}}
                       
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
             
              if (e.keyCode == 13) {
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    return;
                  }
            
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop