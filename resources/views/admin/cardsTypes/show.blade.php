@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Cards Types')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                <a class="btn btn-success" href="/admin/cardsTypes/create/{{$subProvider->id}}" style="color:white;">{{__('Add New Card Type')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="cardsTypesDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Description En')}}</th>
                                <th>{{__('Description Ar')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Description En')}}</th>
                                <th>{{__('Description Ar')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
     var datatable = null;
     var urlParameter = "{{$subProvider->id}}"
     
    $(function(){
        datatable =  $('#cardsTypesDataTable').DataTable({
            "processing":true,
        "serverSide": true,
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
            'print'
            
        ],
        "pageLength":10,
        dom: 'Bfrtip',
        "ajax":"/admin/cardsTypes/data/"+urlParameter,
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"description_en"},
{"data":"description_ar"},

{"data":"options",render:function(data,type,row){
    return "<i class='fas fa-plus add-card-type cursor-pointer' data-id="+row.id+"></i> | <i class='fas fa-edit edit cursor-pointer' data-id="+row.id+"></i> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
        ]
        });

    })

    $(document).on('click', '.edit', function(){ 
        var id = $(this).data('id');
        var url = "/admin/cardsTypes/edit/"+id;
        location.href=url;
      
     
    })
    $(document).on('click', '.delete', function(){ 
        var id = $(this).data('id');
        var url = "/admin/cardsTypes/destroy/"+id;
        location.href=url;
       
    })
</script>
@stop