@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Cards Types')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('cardsTypes.create')}}" style="color:white;">{{__('Add New Card Type')}}</a>
                    <br/><br/>
                    <div class="row">
                       
                        <div class="col-md-3">
                            {!! Form::label('category_id', __('Categories'))!!}
                            <div class="form-group">
                                {!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                            <p class="error" style="display: none;" id="category_id_error">{{__('This field is required')}}</p>
                            </div>
                            
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('provider_id', __('Providers'))!!}
                            <div class="form-group">
                                {!! Form::select('provider_id', [], old('provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                <p class="error" style="display: none;" id="provider_id_error">{{__('This field is required')}}</p>

                            </div>
                            
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                            <div class="form-group">
                                {!! Form::select('sub_provider_id', [], old('sub_provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                <p class="error" style="display: none;" id="sub_provider_id_error">{{__('This field is required')}}</p>

                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <input type="button" id="search" onclick="search()" value="{{__("Search")}}" class="btn btn-primary"/>                                
                        </div>
                        
                    </div>
                    <br/>
					<div class="table-responsive">
					  <table  id="cardsTypesDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                         
                      
                                <th>{{__('Description Ar')}}</th>
                            <th>{{__('Price')}}</th>
                             
                                <th>{{__('Sub Provider')}}</th>
                                <th>{{__('Image')}}</th>
                                <th>{{__('Activation')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                            
                          
                                <th>{{__('Description Ar')}}</th>
                                <th>{{__('Price')}}</th>
                       
                                <th>{{__('Sub Provider')}}</th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Activation')}}</th>
                            <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
     var datatable = null;
     var tableId = null;
     var formData = {
         sub_provider_id : null,
         provider_id:null,
         category_id:null
     }
    $(function(){
      tableId =   $("table").attr("id");
        if(localStorage.getItem(tableId+"_state")){
            var json = localStorage.getItem(tableId+"_state");
            json = JSON.parse(json);
            formData = json;
            $("#category_id").val(formData.category_id);
            loadProvider(formData.category_id).then(function(response){
                $("#provider_id").val(formData.provider_id).trigger("change");
                loadSubProvider(formData.provider_id).then(function(response){
             
             $("#sub_provider_id").val(formData.sub_provider_id).trigger("change")
         });
            })
           
           
           
        }
        
dt();
$("#category_id").change(function(){
   
    var categoryId = $(this).val();
    loadProvider(categoryId)
  
})

$("#provider_id").change(function(){
    var providerId = $(this).val();
    loadSubProvider(providerId)

})








  

    })


    $(document).on('click', '.delete', function(){ 
        var id = $(this).data('id');
        $.ajax({
            url:"/admin/cardsTypes/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
       
   
       
    })

    function loadProvider(categoryId){
        return new Promise((resolve, reject) => {
        $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
    }
           }
           resolve(response);
       }
   })
});
    }

    function loadSubProvider(providerId){
        return new Promise((resolve, reject) => {
        $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_en+"</option>");
    }
           }
           resolve(response)
       }
     
   })
});
    }
    function dt(){
              
              $('#cardsTypesDataTable').DataTable({
            "processing":true,
            order:[],
            responsive: true,
            autoWidth:true,
            fixedColumns: true,
        "serverSide": true,
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
            'print',
            {
                text:"Save State",
                action: function ( e, dt, node, config ) {
                   
                    localStorage.setItem(tableId+"_state",JSON.stringify(formData));
                    toast("Success","State Saved Successfully","State Saved Successfully")
                    
                }
            },
            {
                text:"Clear State",
                action: function ( e, dt, node, config ) {

                    localStorage.removeItem(tableId+"_state");
                    toast("Success","State Cleard Successfully","State Cleard Successfully")


                     setTimeout(() => {
                        location.reload();  
                     }, 1000);
                   
                }
            }
            
        ],
        "pageLength":-1,
        dom: 'Blfrtip',
        "ajax": {
            "url": "{{route('cardsTypes.data')}}",
            "type": "GET",
            "data":formData
		
        },
    
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},
{"data":"title_en",render:function(data,type,row){
    return trimStringEn(15,row.title_en)
}},


{"data":"description_ar",render:function(data,type,row){
    return trimStringAr(15,row.description_ar)
}},
{"data":"price"},

{"data":"subprovider_title_ar"},
{"data":"image",render:function(data,type,row){
    return "<img style='width:50px;height:auto;' class='img-responsive' src="+row.image+">";
}},
{"data":"is_active",render:function(data,type,row){
if(row.is_active)
return "<span class='badge badge-success'>Active</span";
    return "<span class='badge badge-danger'>InActive</span";
}},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
    return "<a href='/admin/cardsTypes/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
        ]
        });
    }
    function search(){
    

       formData.category_id =  $("#category_id").val();
       formData.provider_id =  $("#provider_id").val();
       formData.sub_provider_id =  $("#sub_provider_id").val();
        $('#cardsTypesDataTable').DataTable().destroy();
	 dt();
       
   
        
        

    }
</script>
@stop