@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{Helper::getTitle($cardLimit->cardType)}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'cardslimit.update',"id"=>"editForm"]) !!}
                        {!! Form::hidden('id',$cardLimit->id) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $lookup, $cardLimit->cardType->subProvider
                                    ->provider->category->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',$providersLookup, $cardLimit->cardType->subProvider
                                    ->provider->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',$subProvidersLookup, $cardLimit->cardType->subProvider
                                    ->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                    
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Cards Types'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id',$cardTypeLookup, $cardLimit->cardType
                                    ->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                      
                            <div class="col-md-4">
                                {!! Form::label('value', __('Value'))!!}
                                <div class="form-group">
                                    {!! Form::text('value', $cardLimit->value,['class'=>'form-control','placeholder'=>__('Value')]) !!}
                                    {!! $errors->first('value', '<p class="error">:message</p>') !!}

                                </div>
                               
                            </div> 
                            
                        </div>



                        {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
        $(function(){
$("#editForm").validate({
    rules: {
        category_id:"required",
        provider_id:"required",
        sub_provider_id:"required",
        card_type_id:"required",
        value:"required"
    },
    messages: {
        category_id: "{{__('This field is required')}}",
        provider_id: "{{__('This field is required')}}",
        sub_provider_id:"{{__('This field is required')}}",
        card_type_id:"{{__('This field is required')}}",
        value:"{{__('This field is required')}}"
    
       
    },
    submitHandler: function(form) {
      form.submit();
    }
})

        
        $("#category_id").change(function(){
            $("#provider_id").empty().append("<option value=''>Select</option>");
               $("#sub_provider_id").empty().append("<option value=''>Select</option>");
               $("#card_type_id").empty().append("<option value=''>Select</option>");
            var categoryId = $(this).val();
           $.ajax({
               type:"GET",
               url:"{{route('common.providersbycategory')}}",
               data:{category_id:categoryId},
               dataType:"json",
               success:function(response){
                $("#provider_id").empty();
                $("#provider_id").append("<option value=''>Select</option>");
                   if(response.status == CONSTANTS.SUCCESS){
                    for (var provider in response.data) {
              var p = response.data[provider]
        
               $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
            }
                   }
               }
           })
        })
        $("#provider_id").change(function(){
            $("#sub_provider_id").empty().append("<option value=''>Select</option>");
               $("#card_type_id").empty().append("<option value=''>Select</option>");
            var providerId = $(this).val();
           $.ajax({
               type:"GET",
               url:"{{route('common.subprovidersbyprovider')}}",
               data:{provider_id:providerId},
               dataType:"json",
               success:function(response){
                $("#sub_provider_id").empty();
                $("#sub_provider_id").append("<option value=''>Select</option>");
                   if(response.status == CONSTANTS.SUCCESS){
                    for (var subProvider in response.data) {
              var s = response.data[subProvider]
           
               $("#sub_provider_id").append("<option value="+s.id+">"+s.title_en+"</option>");
            }
                   }
               }
           })
        })
        $("#sub_provider_id").change(function(){
            $("#card_type_id").empty().append("<option value=''>Select</option>");
            var subProviderId = $(this).val();
           $.ajax({
               type:"GET",
               url:"{{route('common.cardtypesbysubprovider')}}",
               data:{sub_provider_id:subProviderId},
               dataType:"json",
               success:function(response){
                $("#card_type_id").empty();
                $("#card_type_id").append("<option value=''>Select</option>");
                   if(response.status == CONSTANTS.SUCCESS){
                    for (var cardType in response.data) {
              var c = response.data[cardType]
           
               $("#card_type_id").append("<option value="+c.id+">"+c.title_en+"</option>");
            }
                   }
               }
           })
        })
       
        
        
                 
        
        
            })
    </script>
@stop