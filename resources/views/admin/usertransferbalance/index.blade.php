@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('User Transfered Balance')}}</h3>
				</div>
				<br/><br/>
				<div class="container">
                <div class="row">
					
					
                    <div class="col-md-3">
						{!! Form::label('user_id', __('Users'))!!}
						<div class="form-group">
							{!! Form::select('user_id', $usersLookup, old('user_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
						</div>
						
					</div>
                    <div class="col-md-3">
                        {!! Form::label('from_date', __('From Date'))!!}
                        <div class="form-group">
                            {!! Form::text('from_date', old('from_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('from_date')]) !!}
                        </div>
                       
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('to_date', __('To Date'))!!}
                        <div class="form-group">
                            {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                        <p class="error" id="to_date_error" style="display:none">{{__('Both of from date and to date are requireds')}}</p>
                        
                        </div>
                       
                    </div>
                    @if(Auth::guard('admin')->user()->accessType() != null && Auth::guard('admin')->user()->accessType() != "created_users")
                    <div class="col-md-3">
						{!! Form::label('admin_id', __('Admins'))!!}
						<div class="form-group">
							{!! Form::select('admin_id', $adminsLookup, old('admin_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
						</div>
						
					</div>
                  
                    @else
                    <input type="hidden" id="admin_id" value="0"/>
                    @endif
					
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
						<input type="button" onclick="search()" value="{{__('Search')}}" class="btn btn-info"/>
						</div>
					</div>
				</div>
				
				</div>
				<!-- /.box-header -->
				<div class="box-body">
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="userBalanceTransferDateTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                            <th>{{__('From User')}}</th>
                            <th>{{__('To User')}}</th>
                            <th>{{__('Amount')}}</th>
                           
                                <th>{{__('Created date')}}</th>
                                
                                
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
                            <tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('From User')}}</th>
                                <th>{{__('To User')}}</th>
                                <th>{{__('Amount')}}</th>
                               
                                    <th>{{__('Created date')}}</th>
                                    
                                    
                                </tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
<script>
    var formData = {
		user_id:null,
		
        from_date:null,
        to_date:null,
        admin_id:null
        
	}
    $(function(){
        dt();
    })
    function dt(){
 
   
 var dt =    $('#userBalanceTransferDateTable').DataTable({
         "processing":true,
     "serverSide": true,
     "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
    order:[],
     responsive: true,
          autoWidth:true,
          fixedColumns: true,
     buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
         'print'
         
     ],
     "pageLength":-1,
     dom: 'Blfrtip',
     "ajax": {
          "url": "{{route('transferbalance.index')}}",
          "type": "GET",
          "data":formData,
         
      },

      "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        intVal = function(i) {
                            var num = i;
                            if (typeof i === 'string') {

                                num = parseFloat(i)

                            }
                            return num;
                        },




                        total = api
                        .column(3)
                        .data()
                        .reduce(function(a, b) {
                            return ((intVal(a) + intVal(b)));
                        }, 0);

                    $(api.column(3).footer()).html(
                        "<h3>Total " + toFixed(total,3) + "</h3>"
                    );
                },
      
    
     columns:[
         {"data": "#",
 render: function (data, type, row, meta) {
     
     return meta.row + meta.settings._iDisplayStart + 1;

 }
},
{"data":"from_user"},
{"data":"to_user"},
{"data":"amount"},
{"data":"created_date"},




     ]
     });
  
  }

  function search(){
    $("#to_date_error").hide();
    var fromDate = $("#from_date").val();
        var toDate = $("#to_date").val();
        if((!fromDate && toDate) || (fromDate && !toDate))
        {
            $("#to_date_error").show();
            return ;
        }
	 formData.user_id = $("#user_id").val();

     formData.from_date = $("#from_date").val();
     formData.to_date = $("#to_date").val();
     formData.admin_id = $("#admin_id").val();
	 $('#userBalanceTransferDateTable').DataTable().destroy();
	 dt();
 }
    </script>
@stop