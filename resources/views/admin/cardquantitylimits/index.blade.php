@extends('layouts.default')
@section('content')

<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Cards Quantity Limit')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('quantitylimits.create')}}" style="color:white;">{{__('Add / Edit Cards Quantity Limit')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="cardQuantityLimits" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Card Quantity Limit')}}</th>
                         
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Title En')}}</th>
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Card Quantity Limit')}}</th>
                           
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
<script>
  var datatable = null;
 $(function(){
     datatable =  $('#cardQuantityLimits').DataTable({
         "processing":true,
     "serverSide": true,
     order:[],
     "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
     responsive: true,
            autoWidth:true,
            fixedColumns: true,
     buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
         'print'
         
     ],
    
     "pageLength":-1,
     dom: 'Blfrtip',
     "ajax":"{{route('quantitylimits.index')}}",
     columns:[
         {"data": "#",
 render: function (data, type, row, meta) {
     
     return meta.row + meta.settings._iDisplayStart + 1;

 }
},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"quantity_limit"},

{"data":"created_at"},

{"data":"options",render:function(data,type,row){
 return "<i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
     ]
     });

 })


 $(document).on('click', '.delete', function(){ 
    var id = $(this).data('id');
        $.ajax({
            url:"/admin/notifications/quantitylimit/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
  
 })

</script>
@stop