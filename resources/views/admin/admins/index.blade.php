@extends('layouts.default')
@section('content')

<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Admins')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('admins.create')}}" style="color:white;">{{__('Add New Admin')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="adminsDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Username')}}</th>
                                <th>{{__('Balance Quota')}}</th>
                                <th>{{__('Role')}}</th>
                                <th>{{__('Created at')}}</th>
                                <th>{{__('Options')}}</th>
                             
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Username')}}</th>
                                <th>{{__('Balance Quota')}}</th>
                                <th>{{__('Role')}}</th>
                                <th>{{__('Created at')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>

<script>
    var datatable = null;
   $(function(){
       datatable =  $('#adminsDataTable').DataTable({
           "processing":false,
       "serverSide": false,
       order:[],
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       responsive: true,
            autoWidth:true,
            fixedColumns: true,
            buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
         
     ],
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":"{{route('admins.index')}}",
       "footerCallback": function (row, data, start, end, display) {
        var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total = api
            .column(2)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(2).footer()).html(
            "<span>Total "+toFixed(intVal(total),3)+"</span>"
            );
     },
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},

{"data":"username"},
{"data":"balance_quota"},
{"data":"role"},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
   var options =  "<a href='/admin/admins/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
   if(!row.is_super_admin)
   options+=" | <a href='/admin/balances/balancelimit/transactions?id="+row.id+"' ><i class='fas fa-eye cursor-pointer'></i></a>";
   return options;
}}


       ]
       });

   })

  

   $(document).on('click', '.delete', function(){ 
       var id = $(this).data('id');
       

       $.ajax({
            url:"/admin/admins/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })

       
   })

    </script>
@stop