@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Admins')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'admins.update']) !!}
                        {!! Form::hidden('id',$admin->id) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('username', __('Username'))!!}
                                <div class="form-group">
                                    
                                    {!! Form::text('username', $admin->username,['class'=>'form-control','placeholder'=>__('Username')]) !!}
                                    {!! $errors->first('username', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('password', __('Password'))!!}
                                <div class="form-group">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>__('Password')]) !!}
                                    {!! $errors->first('password', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                          
                            <div class="col-md-4">
                                {!! Form::label('offical_price_package_id', __('Offical Price Packages')) !!}
                                <div class="form-group">
                                    {!! Form::select('offical_price_package_id', $packages, $admin->offical_price_package_id, ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                    {!! $errors->first('offical_price_package_id', '<p class="error">:message</p>') !!}
                                </div>

                            </div>
                          
                            
                        </div>
                      
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
            
              
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop