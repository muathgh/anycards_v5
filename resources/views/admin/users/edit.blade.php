{{-- V_5 UPDATES 26/12/2021 --}}
@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{$user->name}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'users.update','files' => true]) !!}
                        {!! Form::hidden('id',$user->id) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('username', __('User Name'))!!}
                                <div class="form-group">
                                    {!! Form::text('username', $user->username,['class'=>'form-control','placeholder'=>__('User Name')]) !!}
                                    {!! $errors->first('username', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('full_name', __('Full Name'))!!}
                                <div class="form-group">
                                    {!! Form::text('full_name', $user->name,['class'=>'form-control','placeholder'=>__('Full Name')]) !!}
                                    {!! $errors->first('full_name', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                       
                            <div class="col-md-4">
                                {!! Form::label('password', __('Password'))!!}
                                <div class="form-group">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>__('Password')]) !!}
                                    {!! $errors->first('password', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                          
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('status', __('Status'))!!}
                                <div class="form-group">
                                    {!! Form::select('status', ["active"=>"Active","inactive"=>"InActive"], $user->status,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('status', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('package_id', __('Packages'))!!}
                                <div class="form-group">
                                    {!! Form::select('package_id', $lookup, $user->package_id,["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('package_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('email', __('Email'))!!}
                                <div class="form-group">
                                    {!! Form::text('email', $user->email,['class'=>'form-control','placeholder'=>__('Email')]) !!}
                                    {!! $errors->first('email', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('mobile_number', __('Mobile Number'))!!}
                                <div class="form-group">
                                    {!! Form::text('mobile_number', $user->mobile_number,['class'=>'form-control','placeholder'=>__('Mobile Number')]) !!}
                                    {!! $errors->first('mobile_number', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('logo', __('Logo'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control file-upload-text" disabled placeholder="{{__('Select Image')}}" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default file-upload-btn">
                                            {{__('Browse...')}}
                                            <input name="logo" type="file" class="file-upload"/>
                                        </button>
                                    </span>
                                </div>
                                {!! $errors->first('logo', '<p class="error">:message</p>') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('created_by_id', __('Created By'))!!}
                                <div class="form-group">
                                    {!! Form::select('created_by_id', $admins, $user->created_by_id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('created_by_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>

                        </div>
                        <br/>
                        {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

@stop
{{-- END V_5 UPDATES 26/12/2021 --}}