{{-- V_5 UPDATES 26/12/2021 --}}
@extends('layouts.default')
@section('content')
<style>
    .form-control[readonly]{
        background-color: #fff
    }
    </style>
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{__('Add New User')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'users.store','files' => true,"autocomplete"=>"off"]) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('username', __('User Name'))!!}
                                <div class="form-group">
                                    {!! Form::text('username', old('username'),['class'=>'form-control readonly','placeholder'=>__('User Name'),'readonly'=>true , "onfocus"=>"this.removeAttribute('readonly');"]) !!}
                                    {!! $errors->first('username', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('full_name', __('Full Name'))!!}
                                <div class="form-group">
                                    {!! Form::text('full_name', old('full_name'),['class'=>'form-control','placeholder'=>__('Full Name'), "onfocus"=>"this.removeAttribute('readonly');"]) !!}
                                    {!! $errors->first('full_name', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                       
                            <div class="col-md-4">
                                {!! Form::label('password', __('Password'))!!}
                                <div class="form-group">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>__('Password'), "onfocus"=>"this.removeAttribute('readonly');"]) !!}
                                    {!! $errors->first('password', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                          
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('email', __('Email'))!!}
                                <div class="form-group">
                                    {!! Form::text('email', old('full_name'),['class'=>'form-control','placeholder'=>__('Email'), "onfocus"=>"this.removeAttribute('readonly');"]) !!}
                                    {!! $errors->first('email', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('mobile_number', __('Mobile Number'))!!}
                                <div class="form-group">
                                    {!! Form::text('mobile_number', old('full_name'),['class'=>'form-control','placeholder'=>__('Mobile Number'), "onfocus"=>"this.removeAttribute('readonly');"]) !!}
                                    {!! $errors->first('mobile_number', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('balance', __('Balance'))!!}
                                <div class="form-group">
                                    {!! Form::number('balance', old('balance'),['class'=>'form-control','placeholder'=>__('Balance'), "onfocus"=>"this.removeAttribute('readonly');"]) !!}
                                </div>
                               
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('package_id', __('Packages'))!!}
                                <div class="form-group">
                                    {!! Form::select('package_id', $lookup, old('package_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('package_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('logo', __('Logo'))!!}
                                <div class="input-group">
                                    <input type="text" class="form-control file-upload-text" disabled placeholder="{{__('Select Image')}}" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default file-upload-btn">
                                            {{__('Browse...')}}
                                            <input name="logo" type="file" class="file-upload"/>
                                        </button>
                                    </span>
                                </div>
                                {!! $errors->first('logo', '<p class="error">:message</p>') !!}
                            </div>
                        </div>
                        <br/>
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
                 
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>

@stop
{{-- END V_5 UPDATES 26/12/2021 --}}