@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Users Management')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('users.create')}}" style="color:white;">{{__('Add New User')}}</a>
                    <br/><br/>
                    <div class="row">
                        <div class="col-md-3">
                            {!! Form::label('package_id', __('Packages'))!!}
                            <div class="form-group">
                                {!! Form::select('package_id', $packagesLookup, old('package_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                            </div>
                            
                        </div>

                        @if(Auth::guard('admin')->user()->accessType() != null && Auth::guard('admin')->user()->accessType() != "created_users")
                        <div class="col-md-3">
                            {!! Form::label('admin_id', __('Admins'))!!}
                            <div class="form-group">
                                {!! Form::select('admin_id', $adminsLookup, old('admin_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
                            </div>
                            
                        </div>
                        @else
                        <input type="hidden" id="admin_id" value="0"/>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <input type="button" id="search" onclick="search()" value="{{__("Search")}}" class="btn btn-primary"/>                                
                        </div>
                        
                    </div>
					<div class="table-responsive">
					  <table  id="usersDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Full Name')}}</th>
                            
                         
                         
                                <th>{{__('Current Balance')}}</th>
                                <th>{{__('Package')}}</th>
                                <th>{{__('Status')}}</th>
                           
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Created By')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Full Name')}}</th>
                            
                         
                         
                                <th>{{__('Current Balance')}}</th>
                                <th>{{__('Package')}}</th>
                                <th>{{__('Status')}}</th>
                           
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Created By')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
     var datatable = null;
     var formData = {
        package : null,
        admin_id : null
     }

     function dt(){
        $('#usersDataTable').DataTable({
            "processing":true,
        "serverSide": true,
        order:[],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        responsive: true,
            autoWidth:true,
            fixedColumns: true,
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
            'print'
            
        ],
        "footerCallback": function (row, data, start, end, display) {
        var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total = api
            .column(2)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(2).footer()).html(
            "<h3>Total "+toFixed(intVal(total),3)+"</h3>"
            );
     },
        "pageLength":-1,
        dom: 'Blfrtip',
        "ajax":{
            "url":"/admin/users/data/",
            "type":"GET",
            "data":formData
        },
     
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},
{"data":"name"},


{"data":"balance"},
{"data":"package_title"},
{"data":"status",render:function(data,type,row){
    return row.status == CONSTANTS.ACTIVE ? "<span  class='badge badge-success'>{{__('Active')}}</span>"
    : "<span  class='badge badge-danger'>{{__('InActive')}}</span>"
}},

{"data":"created_at"},
{"data":"created_by"},
{"data":"options",render:function(data,type,row){
    return "<a href='/admin/users/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}

        ]
        });
     }
     
    $(function(){
        dt();

    })

 
    $(document).on('click', '.delete', function(){ 
        var id = $(this).data('id');
  

        $.ajax({
            url:"/admin/users/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
       
    })

    function search(){
        formData.package = null;
        if($("#package_id").val())
        formData.package = $("#package_id").val();
        formData.admin_id = $("#admin_id").val();
        $("#usersDataTable").DataTable().destroy();
        dt();
    }
</script>
@stop