@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{__('Scanned Cards')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'cards.store']) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $lookup, old('category_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',[], old('provider_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',[], old('sub_provider_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                    
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Cards Types'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id',[], old('card_type_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('expiry_date', __('Expiry Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('expiry_date', old('expiry_date'),['class'=>'form-control' ,"autocomplete"=>"off",'placeholder'=>__('Expiry Date')]) !!}
                                    <p style="display:none" id="expiry_date_error" class="error">{{__('This field is required')}}</p>
                                </div>
                               
                            </div>
                           
                         
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group text-left">
                                    <input type="button" onclick="startScan()" style="display: none"  id="startScanBtn" class="btn btn-success" value="{{__('Start Scan')}}"/>
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group text-right">
                                <input type="button" onclick="getData()" style="display:none" id="getScannedDataBtn" class="btn btn-warning" value="{{__('Get Data')}}"/>
                                <input type="button" onclick="clearData()"  id="clearDataBtn" class="btn btn-info" value="{{__('Clear Data')}}"/>
   
                            </div>
                            </div>
                        </div>

<br/>
<form id="tblForm" method="POST">
                        <div style="display:none" id="tblContainer" class="container">
                            
                            <table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                    <tr>
                                        <th>
                                            {{__("No")}}
                                        </th>
                                        <th>
                                            {{__('Code')}}
                                        </th>
                                        <th>
                                            {{__('Serial')}}
                                        </th>
                                    <th>{{__('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>
                                            {{__("No")}}
                                        </th>
                                        <th>
                                            {{__('Code')}}
                                        </th>
                                        <th>
                                            {{__('Serial')}}
                                        </th>
                                        <th>{{__('Options')}}</th>
                                    </tr>
                                </tfoot>
</table>
<p class="error" id="no_data_error" style="display:none">{{__('There is no data')}}</p>
                          
                        </div>
                        <br/>
                        <input type="button" id="saveBtn" onclick="save()" value="{{__('Save')}}" style="display:none" class="btn btn-primary"/>

                    </form>
                        {{-- {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!} --}}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){

        $("#expiry_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    var todate = moment();
var fromdate = moment().add(1, "y");
$("#expiry_date").datepicker("setDate", fromdate.toDate());


        $('#modal').on('hidden.bs.modal', function () {
 location.href="{{route('cards.index')}}"
})

        if("{{$isScanStarted}}" == 0){
            $("#startScanBtn").val("{{__('Start Scan')}}").removeClass('btn-danger').addClass('btn-success');

        }else{
            $("#startScanBtn").val("{{__('Stop Scan')}}").removeClass('btn-success').addClass('btn-danger');

        }
      
        $("#category_id").change(function(){
    $("#provider_id").empty().append("<option value=''>Select</option>");
       $("#sub_provider_id").empty().append("<option value=''>Select</option>");
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var categoryId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
    }
           }
       }
   })
   generateCardsTable()
})
$("#provider_id").change(function(){
    $("#sub_provider_id").empty().append("<option value=''>Select</option>");
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var providerId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_en+"</option>");
    }
           }
       }
   })
   generateCardsTable()
})
$("#sub_provider_id").change(function(){
    $("#card_type_id").empty().append("<option value=''>Select</option>");
    var subProviderId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.cardtypesbysubprovider')}}",
       data:{sub_provider_id:subProviderId},
       dataType:"json",
       success:function(response){
        $("#card_type_id").empty();
        $("#card_type_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var cardType in response.data) {
      var c = response.data[cardType]
   
       $("#card_type_id").append("<option value="+c.id+">"+c.title_en+"</option>");
    }
           }
       }
   })
   generateCardsTable()
})
$("#card_type_id").change(function(){
    generateCardsTable()
})

$(document).on('click','.trash',function(){
                var id = $(this).data('id');
                $("#tblContainer tbody #tr_"+id).remove();
                _countTableRows()
              
            })
    })
    function generateCardsTable(){
        $("#tblContainer").hide();
        $("#saveBtn").hide();
        $("#getScannedDataBtn").hide();
        $("#startScanBtn").hide();
      
        var cardTypeId = $("#card_type_id").val()
        if(cardTypeId){
            $("#tblContainer").show();
            $("#saveBtn").show();
            $("#getScannedDataBtn").show();
            $("#startScanBtn").show();
        

            
            
        }

    }
    function getData(){
        $.LoadingOverlay('show');
        $("#tblContainer tbody").empty()
        $.ajax({
            url:"{{route('scannedcards.index')}}",
            type:"GET",
            dataType:"json",
            success:function(response){
                $.LoadingOverlay('hide');
                if(response.status == CONSTANTS.SUCCESS && response.data){

                    var counter = 1;
                    for(var d of response.data){

                        addRow(d,counter++);
                        
                    }

                }
            }
        })
    }
function startScan(){
    $.LoadingOverlay('show');
    $.ajax({
        url:"{{route('scannedcards.start')}}",
        type:"GET",
        dataType:"json",
        success:function(response){
            $.LoadingOverlay('hide');
            if(response.status == CONSTANTS.SUCCESS)
            {
                if(response.data == 0){
                    $("#startScanBtn").val("{{__('Start Scan')}}").removeClass('btn-danger').addClass('btn-success');
                }else{
                    $("#startScanBtn").val("{{__('Stop Scan')}}").removeClass('btn-success').addClass('btn-danger');
                }
            }
        }
    })

    
}

    function addRow(ele,counter){
        var _code = "code_"+ele.id;
        var _serial = "serial_"+ele.id;
        
        var _tr = "tr_"+ele.id;
        var _no = "<td>"+counter+"</td>"
        var code = "<td><input type='text' data-id="+ele.id+" class='form-control code' value="+ele.code+" id="+_code+" name="+_code+"/><p class='error' style='display:none'>{{__('This field is required')}}</p></td>";
        var serial = "<td><input type='text' data-id="+ele.id+" class='form-control serial' value="+ele.serial+" id="+_serial+" name="+_serial+"/></td>";
        var trash = "<td><i class='fas fa-trash delete cursor-pointer trash' data-id="+ele.id+"/></td>"
        var tr = "<tr id="+_tr+">"+_no+code+serial+trash+"</tr>"
        $("#tblContainer tbody").append(tr);



    }

    function _countTableRows(){
        var _count = 1;
        $("table tbody tr").each(function(){
            $(this).find("td:first").text(_count++)
        })

    }
    function save(){
        var formData = [];
        if(validate())
        {
            
            $.LoadingOverlay('show');
            $("input.code").each(function(index,value){
var id = $(this).data('id');
var code = $("#code_"+id).val();
var serial = $("#serial_"+id).val() ? $("#serial_"+id).val() : null;
formData.push({
    code:code,
    serial:serial
})
            });
            var expiryDate = $("#expiry_date").val();

            var cardTypeId =$("#card_type_id").val()
      

            $.ajax({

                url :"{{route('scannedcards.store')}}",
                type:"POST",
                dataType:"json",
                data:{expiry_date:expiryDate,card_type_id:cardTypeId,data:JSON.stringify(formData),"_token": "{{ csrf_token() }}"},
                success:function(response){
                    $.LoadingOverlay('hide');
                    if(response.status==CONSTANTS.SUCCESS){
                        if(response.data)
                        {
                            var data = response.data;
                            $("#modal .modal-title").text(data.title);
                            $("#modal .modal-body").html(data.html);
                            $("#modal").modal('toggle');
                        }
                        else{
                            toast("Success","Cards Saved Successfully","Cards Saved Successfully");
                            setTimeout(() => {
                                location.reload();
                            }, 1500);
                           
                        }
                    }
                }

            })
        }
    }
    
function clearData(){
    $.LoadingOverlay('show');
    $.ajax({
        type:"POST",
        url :"{{route('scannedcards.clear')}}",
        data:{"_token": "{{ csrf_token() }}"},
        dataType:"json",
        success:function(response){
            $.LoadingOverlay('hide');
            if(response.status==CONSTANTS.SUCCESS)
            {
                toast("Success","Data Cleared Successfully","Data Cleared Successfully");
                setTimeout(() => {
                    location.reload();
                }, 1000);
            }

        }
    })
}

    function validate(){
        isFormValid=true;
        $(".error").hide();
        var tbody = $("#tblContainer tbody");

if (tbody.children().length == 0) {
    $("#no_data_error").show();
    isFormValid = false;
}
        if(!$("#expiry_date").val())
        {
            isFormValid = false;
            $("#expiry_date_error").show();
        }
        $("input.code").each(function(index,value){
            $(value).parent().find(".error").hide()
            if($(value).val() == undefined || $(value).val()=="")
            {
                
            $(value).parent().find(".error").show()
            isFormValid = false;
            }
        })

        return isFormValid;

    }
    </script>
@stop