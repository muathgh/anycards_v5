@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Add New Offical Price Discount')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'officalpricediscount.update']) !!}
                        {!! Form::hidden('id', $discount->id) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Provider'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id', $providers,$discount->provider_id,['class'=>'form-control','placeholder'=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('package_id', __('Package'))!!}
                                <div class="form-group">
                                    {!! Form::select('package_id', $packages,$discount->offical_price_package_id,['class'=>'form-control','placeholder'=>__('Select')]) !!}
                                    {!! $errors->first('package_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('discount', __('Discount'))!!}
                                <div class="form-group">
                                    {!! Form::text('discount', $discount->discount,['class'=>'form-control numeric','placeholder'=>__('Discount')]) !!}
                                    {!! $errors->first('discount', '<p class="error">:message</p>') !!}
                                </div>
                               
                            </div>
                          

                          
                            
                        </div>
 
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
            
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop