@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{__('Add/Edit Price Of Sale')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'priceofsale.store',"id"=>"addForm"]) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $lookup, old('category_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',[], old('provider_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',[], old('sub_provider_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                    
                        {{-- <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Cards Types'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id',[], old('card_type_id'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('price', __('Price'))!!}
                                <div class="form-group">
                                    {!! Form::text('price', old('price'),['class'=>'form-control numeric','placeholder'=>__('Price')]) !!}
                                <p style="display:none" id="expiry_date_error" class="error">{{__('This field is required')}}</p>
                                </div>
                               
                            </div>
                          
                            
                         
                            
                        </div> --}}

                        <div id="tblContainer" class="container">

                        </div>

                        <input style="display:none;" onclick="save()" id="submit" type="button" class="btn btn-primary" value="{{__('Save')}}"/>


                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){

    
    $("#category_id").change(function(){
    $("#provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
       $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
       $("#card_type_id").empty().append("<option value=''>{{__('Select')}}</option>");
    var categoryId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
    }
           }
       }
   })
   
})
$("#provider_id").change(function(){
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
       $("#card_type_id").empty().append("<option value=''>{{__('Select')}}</option>");
    var providerId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_en+"</option>");
    }
           }
       }
   })
   
})
$("#sub_provider_id").change(function(){
    $("#card_type_id").empty().append("<option value=''>{{__('Select')}}</option>");
    var subProviderId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.cardtypesbysubprovider')}}",
       data:{sub_provider_id:subProviderId},
       dataType:"json",
       success:function(response){
        $("#card_type_id").empty();
        $("#card_type_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var cardType in response.data) {
      var c = response.data[cardType]
   
       $("#card_type_id").append("<option value="+c.id+">"+c.title_en+"</option>");
    }
           }
       }
   })
   
})

$("#card_type_id").change(function(){
    var id = $(this).val();
    $.ajax({
        type:"GET",
        url:"{{route('priceofsale.price')}}",
        data:{card_type_id:id},
        dataType:"json",
        success:function(response){
            if(response.status==CONSTANTS.SUCCESS && response.data){
                $("#price").val(response.data);
            }
        }
    })
})

$("#category_id").change(function(){
    $("#tblContainer").html("");
    var categoryId = $("#category_id").val();
    if(categoryId)
    generatePriceOfSaleTable("category_id",categoryId);

})

$("#provider_id").change(function(){
    $("#tblContainer").html("");
    var providerId = $("#provider_id").val();
    if(providerId)
    generatePriceOfSaleTable("provider_id",providerId);

})
$("#sub_provider_id").change(function(){
    $("#tblContainer").html("");
    var subProviderId = $("#sub_provider_id").val();
    if(subProviderId)
    generatePriceOfSaleTable("sub_provider_id",subProviderId);

})
    })
    function generatePriceOfSaleTable(key,value){
        $.LoadingOverlay('show')
       $("#submit").hide();

            $.ajax({
                url:"{{route('priceofsale.form')}}",
                data:{[key]:value},
                type:"GET",
                dataType:"json",
                success:function(response){
                    $.LoadingOverlay('hide')
                    if(response.status == CONSTANTS.SUCCESS){
                        if(response.data)
                        {
                        $("#tblContainer").html(response.data);
                        $("#submit").show();
                        }
                    }
                }
            })
        
    }

    function save(){

        if($(".price_error:visible").length > 0)
        {
            $('html, body').animate({
        scrollTop: $(".price_error:visible").offset().top -500
    }, 10);
       return;

        }

        $.LoadingOverlay('show')
        var formData = [];

      
        
        $("table tbody .price_input input").each(function(index,value){
          var price =   $(value).val()
          var cardTypeId = $(value).data('id');
          formData.push({
              card_type_id:cardTypeId,
              price:price
          });

        });
      
        $.ajax({
            url:"{{route('priceofsale.store')}}",
            type:"POST",
            dataType:"JSON",
            data:{data:JSON.stringify(formData), "_token": "{{ csrf_token() }}"},
            success:function(response){
                $.LoadingOverlay('hide')
                if(response.status == CONSTANTS.SUCCESS)
                toast("Success","{{__('Saved Successfully')}}","{{__('Saved Successfully')}}")

                setTimeout(() => {
                    location.href="{{route('priceofsale.index')}}"
                }, 1500);
                
            }
        })
    }
</script>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
                  var subProviderId = $("#sub_provider_id").val();
    var providerId = $("#provider_id").val();
    var categoryId = $("#category_id").val();

    if(subProviderId || providerId || categoryId )
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>
@stop