@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{__('Add New Price Of Sale')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'priceofsale.update',"id"=>"editForm"]) !!}
                        {!! Form::hidden('id',$priceOfSale->id) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $lookup, $priceOfSale->cardType->subProvider->provider->category->id,["class"=>"form-control","disabled"=>true,"placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',$providersLookup, $priceOfSale->cardType->subProvider->provider->id,["class"=>"form-control","disabled"=>true,"placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',$subProvidersLookup, $priceOfSale->cardType->subProvider->id,["class"=>"form-control","disabled"=>true,"placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                    
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Cards Types'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id',$cardTypeLookup, $priceOfSale->cardType->id,["class"=>"form-control","disabled"=>true,"placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('price', __('Price'))!!}
                                <div class="form-group">
                                    {!! Form::text('price', $priceOfSale->price,['class'=>'form-control numeric','placeholder'=>__('Price')]) !!}
                                <p style="display:none" id="expiry_date_error" class="error">{{__('This field is required')}}</p>
                                </div>
                               
                            </div>
                          
                            
                         
                            
                        </div>



                        {!! Form::submit(__('Save'),['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){




$("#editForm").validate({
    rules:{
      
        price:"required"
    },
    messages:{
   
        price:{
            required:"{{__('This field is required')}}"
        },
        
    },
    submitHandler:function(form){
        $("#editForm")[0].submit();
        return false;

    }
})
    })
    </script>
@stop