@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
                <h3 class="box-title">{{__('Add New Card')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'cards.store']) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $categories, $cardType->subProvider->provider->category->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',$providers, $cardType->subProvider->provider->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',$subProviders, $cardType->subProvider->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                    
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Cards Types'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id',$cardTypes, $cardType->id,["class"=>"form-control","placeholder"=>__('Select')]) !!}
                                    {!! $errors->first('card_type_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('expiry_date', __('Expiry Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('expiry_date', old('expiry_date'),['class'=>'form-control','autocomplete'=>"off",'placeholder'=>__('Expiry Date')]) !!}
                                <p style="display:none" id="expiry_date_error" class="error">{{__('This field is required')}}</p>
                                </div>
                               
                            </div>
                       
                         
                            
                        </div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group text-right">
        <input type="button" onclick="addField()" style="display:block" id="addFieldBtn" class="btn btn-info" value="{{__('Add Field')}}"/>
        </div>
    </div>
</div>
<br/>
<form id="tblForm" method="POST">
                        <div style="display:block" id="tblContainer" class="container">
                            
                            <table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                    <tr>
                                        <th>
                                            {{__("No")}}
                                        </th>
                                        <th>
                                            {{__('Code')}}
                                        </th>
                                        <th>
                                            {{__('Serial')}}
                                        </th>
                                    <th>{{__('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="tr_1">
                                        <td class="no" id="no_1" data-num="1">
                                            1
                                        </td>
                                        <td>
                                        <input type="text" required class="form-control code" id="code_1" name="code_1" placeholder="{{__('Code')}}" data-num="1"/>
                                        <p style="display:none" class="error">{{__('This field is required')}}</p>
                                        <p style="display:none" class="error length" >{{__('PinCode must be 14 digit')}}</p>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control serial" id="serial_1" name="serial_1" placeholder="{{__('Serial')}}" data-num="1"/>
                                           
                                        </td>
                                            <td>
                                                <i class='fas fa-trash delete cursor-pointer trash' name="trash_1" id="trash_1" data-num="1"></i>
                                            </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>
                                            {{__("No")}}
                                        </th>
                                        <th>
                                            {{__('Code')}}
                                        </th>
                                        <th>
                                            {{__('Serial')}}
                                        </th>
                                        <th>{{__('Options')}}</th>
                                    </tr>
                                </tfoot>
</table>
<p class="error" id="no_data_error" style="display:none">{{__('There is no data')}}</p>
                        </div>
                        <br/>
                        <input type="button" id="saveBtn" onclick="save()" value="{{__('Save')}}" style="display:block" class="btn btn-primary"/>

                    </form>
                        {{-- {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!} --}}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
    $(function(){
        $("#expiry_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    var todate = moment();
var fromdate = moment().add(1, "y");
$("#expiry_date").datepicker("setDate", fromdate.toDate());
$("#category_id").change(function(){
    $("#provider_id").empty().append("<option value=''>Select</option>");
       $("#sub_provider_id").empty().append("<option value=''>Select</option>");
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var categoryId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_en+"</option>");
    }
           }
       }
   })
   generateCardsTable()
})
$("#provider_id").change(function(){
    $("#sub_provider_id").empty().append("<option value=''>Select</option>");
       $("#card_type_id").empty().append("<option value=''>Select</option>");
    var providerId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_en+"</option>");
    }
           }
       }
   })
   generateCardsTable()
})
$("#sub_provider_id").change(function(){
    $("#card_type_id").empty().append("<option value=''>Select</option>");
    var subProviderId = $(this).val();
   $.ajax({
       type:"GET",
       url:"{{route('common.cardtypesbysubprovider')}}",
       data:{sub_provider_id:subProviderId},
       dataType:"json",
       success:function(response){
        $("#card_type_id").empty();
        $("#card_type_id").append("<option value=''>Select</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var cardType in response.data) {
      var c = response.data[cardType]
   
       $("#card_type_id").append("<option value="+c.id+">"+c.title_en+"</option>");
    }
           }
       }
   })
   generateCardsTable()
})
$("#card_type_id").change(function(){
    generateCardsTable()
})


            $(document).on('click','.trash',function(){
                var num = $(this).data('num');
                $("#tblContainer tbody #tr_"+num).remove();
                _countTableRows();
              
            })

            $('#modal').on('hidden.bs.modal', function () {
 location.href="{{route('cards.index')}}"
})

    })

    function generateCardsTable(){
        $("#tblContainer").hide();
        $("#saveBtn").hide();
        $("#addFieldBtn").hide();
        var cardTypeId = $("#card_type_id").val()
        if(cardTypeId){
            $("#tblContainer").show();
            $("#saveBtn").show();
            $("#addFieldBtn").show();
        }

    }
    function addField(){
        var lastNoNum = $("tr.no:last").data("num") != undefined ?$("tr.no:last").data("num") :0
        var lastCodeFieldNum= $("input.code:last").data('num') != undefined ? $("input.code:last").data('num')  : 0 ;
        var lastSerialFieldNum= $("input.serial:last").data('num') != undefined ? $("input.serial:last").data('num') : 0;
        var lastTrashNum =$('.trash:last').data('num') != undefined ? $('.trash:last').data('num') : 0;
       var _no = "no_"+(lastNoNum+1);
        var _code = "code_"+(lastCodeFieldNum+1);
        var _serial = "serial_"+(lastSerialFieldNum+1);
        var _trash = "trash_"+(lastTrashNum+1);
        var _tr = "tr_"+(lastTrashNum+1);
        var code = "<input type='text' data-num="+(lastCodeFieldNum+1)+" id="+_code+" name="+_code+" class='form-control code' placeholder='{{__('Code')}}'/> <p class='error' style='display:none'>{{__('This field is required')}}</p>";
        var serial = "<input type='text' data-num="+(lastSerialFieldNum+1)+" id="+_serial+" name="+_serial+" class='form-control serial' placeholder='{{__('Serial')}}'/>";
        var trash = "<i class='fas fa-trash delete cursor-pointer trash' name="+_trash+" id="+_trash+" data-num="+(lastTrashNum+1)+"></i>"
        var tr = "<tr id="+_tr+"><td></td><td>"+code+"</td><td>"+serial+"</td><td>"+trash+"</td></tr>";
      
        $("#tblContainer tbody").append(tr);
        _countTableRows();

    }
    function _countTableRows(){
        var _count = 1;
        $("table tbody tr").each(function(){
            $(this).find("td:first").text(_count++)
        })

    }
    function save(){
        var formData = [];
        if(validate())
        {
            $("input.code").each(function(index,value){
var num = $(this).data('num');
var code = $("#code_"+num).val();
var serial = $("#serial_"+num).val() ? $("#serial_"+num).val() : null;
formData.push({
    code:code,
    serial:serial
})
            });
            var expiryDate = $("#expiry_date").val();
           

            var cardTypeId =$("#card_type_id").val()
      

            $.ajax({

                url :"{{route('cards.store')}}",
                type:"POST",
                dataType:"json",
                data:{expiry_date:expiryDate,card_type_id:cardTypeId,data:JSON.stringify(formData),"_token": "{{ csrf_token() }}"},
                success:function(response){
                    if(response.status==CONSTANTS.SUCCESS){
                        if(response.data)
                        {
                            var data = response.data;
                            $("#modal .modal-title").text(data.title);
                            $("#modal .modal-body").html(data.html);
                            $("#modal").modal('toggle');
                        }
                        else{
                            toast("Success","Cards Saved Successfully","Cards Saved Successfully");
                            setTimeout(() => {
                                location.href = "{{route('cards.index')}}"
                            }, 1500);
                           
                        }
                    }
                }

            })
        }
    }

    function validate(){
        isFormValid=true;
        $(".error").hide();
        var tbody = $("#tblContainer tbody");

if (tbody.children().length == 0) {
    $("#no_data_error").show();
    isFormValid = false;
}
        if(!$("#expiry_date").val())
        {
            isFormValid = false;
            $("#expiry_date_error").show();
        }
        $("input.code").each(function(index,value){
            $(value).parent().find(".error").hide()
            if($(value).val() == undefined || $(value).val()=="")
            {
                
            $(value).parent().find(".error").show()
            isFormValid = false;
            } else if($("#category_id").val()==1  && removeDash($(value).val()).length != 14 ){
                $(value).parent().find(".length").show()
                isFormValid = false;
            }
        })

        return isFormValid;

    }
    function removeDash(str){
        return str.split('-').join('');
    }
    </script>
    <script>
  
        $(function(){
          $('html').on("keypress", function(e) {
              
                 e.preventDefault();
                  if (e.keyCode == 13) {
                      var elements = $("form textarea");
                     
                      for(var i = 0 ; i <elements.length ; i ++){
                         
                        if($(elements[i]).is(":focus"))
                        {
                            console.log("focus");
                        return;
                        }
                      }
                      var cardTypeId = $("#card_type_id").val()

                      if(cardTypeId)
                      document.getElementById("saveBtn").click();
                     
                  }
              });
        })
          </script>
@stop