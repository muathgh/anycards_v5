{{-- V_5 UPDATS 27/12/2021 --}}
@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Uploaded Cards') }}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            @if (session()->has('success_message'))
                                <div class="alert alert-success">
                                    {{ session()->get('success_message') }}
                                </div>
                            @endif
                            <a class="btn btn-success" href="{{ route('cards.create') }}"
                                style="color:white;">{{ __('Add New Card') }}</a>
                            <br /><br />

                            <div class="row">
                             
                                <div class="col-md-3">
                                    {!! Form::label('category_id', __('Categories')) !!}
                                    <div class="form-group">
                                        {!! Form::select('category_id', $categoriesLookup, old('category_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('provider_id', __('Providers')) !!}
                                    <div class="form-group">
                                        {!! Form::select('provider_id', [], old('provider_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}

                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('sub_provider_id', __('SubProviders')) !!}
                                    <div class="form-group">
                                        {!! Form::select('sub_provider_id', [], old('sub_provider_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}

                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('card_type_id', __('Card Type')) !!}
                                    <div class="form-group">
                                        {!! Form::select('card_type_id', [], old('card_type_id'), ['class' => 'form-control select2', 'placeholder' => __('Select')]) !!}

                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('from_date', __('From Date')) !!}
                                    <div class="form-group">
                                        {!! Form::text('from_date', old('from_date'), ['class' => 'form-control ui-date', 'autocomplete' => 'off', 'placeholder' => __('From Date')]) !!}
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('to_date', __('To Date')) !!}
                                    <div class="form-group">
                                        {!! Form::text('to_date', old('to_date'), ['class' => 'form-control ui-date', 'autocomplete' => 'off', 'placeholder' => __('To Date')]) !!}
                                        <p class="error" id="to_date_error" style="display:none">
                                            {{ __('Both of from date and to date are requireds') }}</p>

                                    </div>

                                </div>

                                <div class="col-md-3">
                                    {!! Form::label('from_time', __('From Time')) !!}
                                    <div class="form-group">
                                        {!! Form::text('from_time', old('from_time'), ['class' => 'form-control ui-date', 'autocomplete' => 'off', 'placeholder' => __('From Time')]) !!}
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('to_time', __('To Time')) !!}
                                    <div class="form-group">
                                        {!! Form::text('to_time', old('to_time'), ['class' => 'form-control ui-date', 'autocomplete' => 'off', 'placeholder' => __('To Time')]) !!}
                                        <p class="error" id="to_time_error" style="display:none">
                                            {{ __('Both of from time and to time are requireds') }}</p>

                                    </div>

                                </div>
                                @if(Auth::guard('admin')->user()->accessType() != null && Auth::guard('admin')->user()->accessType() != "created_users")
                                <div class="col-md-3">
                                    {!! Form::label('admin_id', __('Admins'))!!}
                                    <div class="form-group">
                                        {!! Form::select('admin_id', $adminsLookup, old('admin_id'),["class"=>"form-control select2","placeholder"=>__('All')]) !!}
                                    </div>
                                    
                                </div>
                                @else
                                <input type="hidden" id="admin_id" value="0"/>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="button" id="search" onclick="search()" value="{{ __('Search') }}"
                                        class="btn btn-primary" />
                                </div>

                            </div>
                        
                            <br />
                            <div class="table-responsive">
                                <table id="cardsDataTable"
                                    class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                        <tr>
                                            <th>{{ __('#') }}</th>

                                            <th>{{ __('Title En') }}</th>
                                            <th>{{ __('Pin Code') }}</th>
                                            <th>{{ __('Serial') }}</th>
                                            <th>{{__('Offical Price')}}</th>


                                            <th>{{ __('Expiry Date') }}</th>
                                            <th>{{ __('Status') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Created By') }}</th>
                                            <th>{{ __('Options') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{ __('#') }}</th>
                                            <th>{{ __('Title En') }}</th>

                                            <th>{{ __('Pin Code') }}</th>
                                            <th>{{ __('Serial') }}</th>
                                            <th>{{__('Offical Price')}}</th>
                                            <th>{{ __('Expiry Date') }}</th>
                                            <th>{{ __('Status') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Created By') }}</th>
                                            <th>{{ __('Options') }}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
    </div>
    {{-- datatable --}}
    <script>
        var datatable = null;
        var formData = {
            admin_id : null,
            card_type_id: null,
            sub_provider_id: null,
            provider_id: null,
            category_id: null,
            from_date: null,
            to_date: null,
            from_time: null,
            to_time: null
        }
        $(function() {
            $('#from_time').timepicker({

                defaultTime: null,
                minuteStep: 1,
                showMeridian: false
            })
            $('#to_time').timepicker({

                defaultTime: null,
                minuteStep: 1,
                showMeridian: false
            })

            tableId = $("table").attr("id");
            if (localStorage.getItem(tableId + "_state")) {
                var json = localStorage.getItem(tableId + "_state");
                json = JSON.parse(json);
                formData = json;
                $("#category_id").val(formData.category_id);
                loadProviders(formData.category_id).then(function(response) {
                    $("#provider_id").val(formData.provider_id).trigger("change");
                    loadSubProviders(formData.provider_id).then(function(response) {
                        $("#sub_provider_id").val(formData.sub_provider_id).trigger("change");
                        loadCardsTypes(formData.sub_provider_id).then(function(response) {
                            $("#card_type_id").val(formData.card_type_id).trigger("change");

                        })
                    })
                });
                $("#from_date").val(formData.from_date);
                $("#to_date").val(formData.to_date);
            }
        })

        function loadProviders(categoryId) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.providersbycategory') }}",
                    data: {
                        category_id: categoryId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#provider_id").empty();
                        $("#provider_id").append("<option value=''>{{ __('All') }}</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var provider in response.data) {
                                var p = response.data[provider]

                                $("#provider_id").append("<option value=" + p.id + ">" + p.title_en +
                                    "</option>");
                            }
                        }
                        resolve(response);
                    }
                })

            });
        }


        function loadSubProviders(providerId) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.subprovidersbyprovider') }}",
                    data: {
                        provider_id: providerId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#sub_provider_id").empty();
                        $("#sub_provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var subProvider in response.data) {
                                var s = response.data[subProvider]

                                $("#sub_provider_id").append("<option value=" + s.id + ">" + s
                                    .title_en + "</option>");
                            }
                        }
                        resolve(response);
                    }
                })

            });
        }

        function loadCardsTypes(subProviderId) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.cardtypesbysubprovider') }}",
                    data: {
                        sub_provider_id: subProviderId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#card_type_id").empty();
                        $("#card_type_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var cardType in response.data) {
                                var c = response.data[cardType]

                                $("#card_type_id").append("<option value=" + c.id + ">" + c.title_en +
                                    "</option>");
                            }
                        }
                        resolve(response);
                    }
                })

            });
        }

        $(function() {
            dt();
            $("#category_id").change(function() {
                $("#provider_id").empty().append("<option value=''>{{ __('Select') }}</option>");
                $("#sub_provider_id").empty().append("<option value=''>{{ __('Select') }}</option>");
                $("#card_type_id").empty().append("<option value=''>{{ __('Select') }}</option>");
                var categoryId = $(this).val();
                loadProviders(categoryId)
            })

            $("#provider_id").change(function() {
                $("#sub_provider_id").empty().append("<option value=''>{{ __('Select') }}</option>");
                $("#card_type_id").empty().append("<option value=''>{{ __('Select') }}</option>");
                var providerId = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.subprovidersbyprovider') }}",
                    data: {
                        provider_id: providerId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#sub_provider_id").empty();
                        $("#sub_provider_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var subProvider in response.data) {
                                var s = response.data[subProvider]

                                $("#sub_provider_id").append("<option value=" + s.id + ">" + s
                                    .title_en + "</option>");
                            }
                        }
                    }
                })
            })
            $("#sub_provider_id").change(function() {
                $("#card_type_id").empty().append("<option value=''>{{ __('Select') }}</option>");
                var subProviderId = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('common.cardtypesbysubprovider') }}",
                    data: {
                        sub_provider_id: subProviderId
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#card_type_id").empty();
                        $("#card_type_id").append("<option value=''>Select</option>");
                        if (response.status == CONSTANTS.SUCCESS) {
                            for (var cardType in response.data) {
                                var c = response.data[cardType]

                                $("#card_type_id").append("<option value=" + c.id + ">" + c
                                    .title_en + "</option>");
                            }
                        }
                    }
                })
            })

            $("#card_type_id").change(function() {
                cardType = $(this).val();
            })

        })

        // $(document).on('dblclick','.edit',function(e){

        // })

        $(document).on('click', '.edit', function() {
            var id = $(this).data('id');
            var url = "/admin/cards/edit/" + id;
            location.href = url;


        })
        $(document).on('click', '.delete', function() {
            var id = $(this).data('id');


            $.ajax({
                url: "/admin/cards/destroy",
                type: "GET",
                dataType: "JSON",
                data: {
                    id: id
                },
                success: function(response) {
                    if (response.data) {
                        $("#delete-popup .modal-body").html(response.data.html)
                        $("#delete-popup").modal('toggle');
                    }
                }
            })

        })

        function dt() {
            var dt = $('#cardsDataTable').DataTable({
   
                order: [],
                "processing": true,
                "serverSide": true,
                "lengthMenu": [
                    [100, 150, 200, -1],
                    [100, 150, 200, "All"]
                ],

                responsive: true,
                autoWidth: true,
                fixedColumns: true,
                
                buttons: [
                    { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
                    {
                        text: "Save State",
                        action: function(e, dt, node, config) {

                            toast("Success", "State Saved Successfully", "State Saved Successfully")


                            localStorage.setItem(tableId + "_state", JSON.stringify(formData));


                        }
                    }, {
                        text: "Clear State",
                        action: function(e, dt, node, config) {
                            localStorage.removeItem(tableId + "_state");
                            toast("Success", "State Cleard Successfully", "State Cleard Successfully")



                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    }

                ],

                "pageLength": 100,
                dom: 'Blfrtip',
                "ajax": {
                    "url": "{{ route('cards.data') }}",
                    "type": "GET",
                    "data": formData
                },

                columns: [

                    {
                        "data": "#",
                        render: function(data, type, row, meta) {

                            return meta.row + meta.settings._iDisplayStart + 1;

                        }
                    },
                    {
                        "data": "title_en",render:function(data,type,row){
                            // return trimStringEn(20,row.title_en)
                            return row.title_en;
                        }
                    },
                    {
                        "data": "code"
                       
                    },
                    {
                        "data": "serial",
                        render:function(data,type,row){
                            return "<span style='font-size:12px;'>"+row.serial+"</span>"
                        }
                    },
{
"data":"offical_price"
},

                    {
                        "data": "expiry_date"
                    },
                    {
                        "data": "is_used",
                        render: function(data, type, row) {
                            return !row.is_used ?
                                "<span  class='badge badge-success'>{{ __('New') }}</span>" :
                                "<span  class='badge badge-danger'>{{ __('Used') }}</span>"
                        }
                    },
                    {
                        "data": "created_at",
                        render: function(data, type, row) {
                            return row.created_at
                            //  return "<p>"+moment(row.created_at.date.toString()).format('DD/MM/YYYY HH:mm:ss');+"</p>"
                        }
                    },
                    {
                        "data": "created_by"
                    },
                    {
                        "data": "options",
                        render: function(data, type, row) {
                            return "<a href='/admin/cards/edit/" + row.id +
                                "'><i class='fas fa-edit cursor-pointer' ></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id=" +
                                row.id + "></i>"
                        }
                    }
                ],
                "footerCallback": function (row, data, start, end, display) {
        var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total = api
            .column(4)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(4).footer()).html(
            "<span>Total "+toFixed(intVal(total),3)+"</span>"
            );
     },
            });
        }

        function search() {
            $("#to_date_error").hide();
            var fromDate = $("#from_date").val();
            var toDate = $("#to_date").val();
            var fromTime = $("#from_time").val();
            var toTime = $("#to_time").val();
            var adminId = $("#admin_id").val();
            if ((!fromDate && toDate) || (fromDate && !toDate)) {
                $("#to_date_error").show();
                return;
            }

            if ((!fromTime && toTime) || (fromTime && !toTime)) {
                $("#to_time_error").show();
                return;
            }
            formData.card_type_id = $("#card_type_id").val();
            formData.sub_provider_id = $("#sub_provider_id").val();
            formData.provider_id = $("#provider_id").val();
            formData.category_id = $("#category_id").val();
            formData.from_date = fromDate;
            formData.to_date = toDate;
            formData.from_time = fromTime;
            formData.to_time = toTime;
            formData.admin_id  = adminId;
            $("#cardsDataTable").DataTable().destroy();
            dt();




        }

     
    </script>

@stop
{{--END V_5 UPDATS 27/12/2021 --}}