<div class="row">
    <div class="col-md-12" style="text-align:center">
        <h4>{{__('Some of codes are duplicates and didnt save')}}</h4>
        <ul style="text-align:left;font-size:18px">
        @foreach($duplicates as $duplicate)
        <li>{{$duplicate}}</li>
        @endforeach
        </ul>
    </div>
</div>