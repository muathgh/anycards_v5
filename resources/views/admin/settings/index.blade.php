@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Settings')}}</h3>
                </div>
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'settings.store']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('max_notification_title', __('Max Notification Title Length'))!!}
                                <div class="form-group">
                                    {!! Form::number('max_notification_title', $maxNotificationTitle,['class'=>'form-control','placeholder'=>__('Max Notification Title Length')]) !!}
                                    {!! $errors->first('max_notification_title', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('max_notification_description', __('Max Notification Description Length'))!!}
                                <div class="form-group">
                                    {!! Form::number('max_notification_description', $maxNotificationDescription,['class'=>'form-control','placeholder'=>__('Max Notification Description Length')]) !!}
                                    {!! $errors->first('max_notification_description', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            <div class="col-md-4">
                                {!! Form::label('user_balance_min_limit', __('User Balance Min Limit'))!!}
                                <div class="form-group">
                                    {!! Form::number('user_balance_min_limit', $userBalanceMinLimit,['class'=>'form-control','placeholder'=>__('User Balance Min Limit')]) !!}
                                    {!! $errors->first('user_balance_min_limit', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>

                          
                            
                        </div>
                    
                        {!! Form::submit('Save',['class'=>"btn btn-primary","id"=>"submit"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

<script>
  
    $(function(){
      $('html').on("keypress", function(e) {
          
           
              if (e.keyCode == 13) {
               
                  var elements = $("form textarea");
                 
                  for(var i = 0 ; i <elements.length ; i ++){
                     
                    if($(elements[i]).is(":focus"))
                    {
                        
                    return;
                    }
                  }
                 
                  
                  document.getElementById("submit").click();
              }
          });
    })
      </script>

@stop