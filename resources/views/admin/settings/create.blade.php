@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Settings')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'settings.store']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('cards_zero_value', __('Cards Zero Value'))!!}
                                <div class="form-group">
                                    {!! Form::text('cards_zero_value', $cardsZeroValue,['class'=>'form-control','placeholder'=>__('Title En')]) !!}
                                    {!! $errors->first('cards_zero_value', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            
                          

                          
                            
                        </div>
                    
                        {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

@stop