@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Settings')}}</h3>
                </div>
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'settings.userbalancelimit.store']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('notification_ar', __('Notification Arabic'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('notification_ar', $userBalanceNotificationAr,['class'=>'form-control','cols'=>10,'rows'=>5,'placeholder'=>__('Notification In Arabic')]) !!}
                                    {!! $errors->first('notification_ar', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                            
                            
                            <div class="col-md-4">
                                {!! Form::label('notification_en', __('Notification Arabic'))!!}
                                <div class="form-group">
                                    {!! Form::textarea('notification_en', $userBalanceNotificationEn,['class'=>'form-control','cols'=>10,'rows'=>5,'placeholder'=>__('Notification In English')]) !!}
                                    {!! $errors->first('notification_en', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>

                          
                            
                        </div>
                    
                        {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>

@stop