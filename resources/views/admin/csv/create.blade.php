@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Attach CSV File') }}</h3>
                        </div>
                        <div class="box-body">
                            @if (session()->has('success_message'))
                                <div class="alert alert-success">
                                    {{ session()->get('success_message') }}
                                </div>
                            @endif
                            @if (session()->has('error_message'))
                            <div class="alert alert-danger">
                                {{ session()->get('error_message') }}
                            </div>
                        @endif
                            @if (session()->has('warning_message'))

                                @if (in_array('large_number_of_digits', (array)session()->get('warning_message')))
                                    <div class="alert alert-danger">
                                        {{ __('Cards with more or less than 14 digits have not been uploaded') }}
                                    </div>
                                @endif
                                @if (in_array('duplicate_cards', (array)session()->get('warning_message')))
                                    <div class="alert alert-danger">
{{__('Duplicate cards have not been uploaded')}}

                                        @if (session()->has('duplicated_cards'))
                                            @foreach (session()->get('duplicated_cards') as $card)
                                                <li>{{ $card }}</li>
                                            @endforeach
                                        @endif
                                    </div>
                                @endif
                            @endif
                            <br /><br />
                            <div id="content">

                                <div class="container-fluid">
                                    {!! Form::open(['route' => 'csv.store', 'files' => true, 'id' => 'addForm']) !!}
                                    <div class="row">

                                     
                                   


                                        <div class="col-md-4">
                                            {!! Form::label('csv_type', __('Csv Type')) !!}
                                            <div class="form-group">
                                                {!! Form::select('csv_type', $lookup, old('csv_type'), ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
                                                {!! $errors->first('csv_type', '<p class="error">:message</p>') !!}
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            {!! Form::label('csv', __('Csv')) !!}
                                            <div class="input-group">
                                                <input type="text" class="form-control file-upload-text" disabled
                                                    placeholder="{{ __('Select Csv') }}" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default file-upload-btn">
                                                        {{ __('Browse...') }}
                                                        <input name="csv" id="file" type="file" class="file-upload" />
                                                    </button>
                                                </span>
                                            </div>
                                            <p style="display:none" id="file_error" class="error">
                                                {{ __('This field is required') }}</p>
                                            {!! $errors->first('csv', '<p class="error">:message</p>') !!}
                                            @if (\Session::has('csv_error'))
                                                <p class="error">{!! \Session::get('csv_error') !!}</p>
                                            @endif
                                        </div>

                                    </div>

                                  
                                 
                                    <br />

                                    {!! Form::submit(__('Upload'), ['class' => 'btn btn-primary', 'id' => 'submit']) !!}
                                   
                                    {!! Form::close() !!}








                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        </div>
    </div>
    <script>
        function initFromExcelFilters() {
            var excelType = "{{ session('excel_group_filters') ? session('excel_group_filters')['excel_type'] : '' }}"

            if (excelType != "") {
                $("#excel_type").val(excelType);
            }

          

        }

       

 
        $(function() {



        

    

            $("#addForm").validate({
                rules: {
                    category_id: "required",
             
          
                    csv_type: "required",


                },

                messages: {
                    category_id: "{{ __('This field is required') }}",
                    csv_type: "{{ __('This field is required') }}",
                  



                },
                submitHandler: function(form) {

                
                    if (document.getElementById("file").files.length == 0) {
                        $("#file_error").show();
                        return false;
                    }


                    form.submit();
                }
            })



          
           
           

        })

   
    </script>

    <script>
        $(function() {
            $('html').on("keypress", function(e) {


                if (e.keyCode == 13) {

                    var elements = $("form textarea");

                    for (var i = 0; i < elements.length; i++) {

                        if ($(elements[i]).is(":focus")) {

                            return;
                        }
                    }


                    document.getElementById("submit").click();
                }
            });
        })
    </script>
@stop
