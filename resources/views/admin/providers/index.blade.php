@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Providers')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                    @if(session()->has('error_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('error_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('providers.create')}}" style="color:white;">{{__('Add New Provider')}}</a>
                    <br/><br/>
					<div class="table-responsive">
					  <table  id="providersDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                            
                                <th>{{__('Title Ar')}}</th>
                               
                                <th>{{__('Description Ar')}}</th>
                               
                                <th>{{__('Category Title Ar')}}</th>
                           
                                <th>{{__('Image')}}</th>
                                <th>{{__('Activation')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Title Ar')}}</th>
                                <th>{{__('Description Ar')}}</th>
                                <th>{{__('Category Title Ar')}}</th>
                         
                                <th>{{__('Image')}}</th>
                                <th>{{__('Activation')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
     var datatable = null;
     
    $(function(){
        
        datatable =  $('#providersDataTable').DataTable({
            "processing":true,
        "serverSide": true,
        order:[],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        responsive: true,
            autoWidth:true,
            fixedColumns: true,
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true },
            'print'
            
        ],
        "pageLength":100,
        dom: 'Blfrtip',
        "ajax":"/admin/providers/data/",
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},

{"data":"title_ar"},
{"data":"description_ar",render:function(data,type,row){
    return trimStringAr(20,row.description_ar)
}},
{"data":"category_title_ar"},

{"data":"image",render:function(data,type,row){
return "<img style='width:50px;height:auto;' class='img-responsive' src="+row.image_or_default+">";
}},
{"data":"is_active",render:function(data,type,row){
if(row.is_active)
return "<span class='badge badge-success'>Active</span";
    return "<span class='badge badge-danger'>InActive</span";
}},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
    return "<a href='/admin/providers/edit/"+row.id+"'><i class='fas fa-edit cursor-pointer'></i></a> | <i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"
}}
        ]
        });
      
    })

 
    $(document).on('click', '.delete', function(){ 
        var id = $(this).data('id');
        $.ajax({
            url:"/admin/providers/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
      
    })
    // $(document).on('click', '.add-sub-provider', function(){ 
    //     var id = $(this).data('id');
    //     var url = "/admin/subProviders/show/"+id;
    //     location.href=url;
    // })
</script>
@stop