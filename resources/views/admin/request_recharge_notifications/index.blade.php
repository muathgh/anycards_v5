
@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container" style="margin-top:100px;">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Notifications')}}</h3>
                </div>
                <div class="box-body">
                    <div class="col-12">
                        
                                <div class="table-responsive">
                                    <table  id="notificationDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                                  
                                        <thead>
                                            <tr>
                                            <th>{{__('#')}}</th>
                                            <th>{{__('Description En')}}</th>
                                          
                                                  <th>{{__('Description Ar')}}</th>
                                               
                                              <th>{{__('Status')}}</th>
                                            
                                              <th>{{__('Time')}}</th>
                                           
                                             
                                            </tr>
                                        </thead>
<tbody>
    @foreach($notifications as $key=>$notification)
    <tr @if($notification->is_cleared) style="background-color:#81C784" @else style="background-color:#E57373" @endif class="cursor-pointer spaceUnder @if(!$notification->is_cleared) notification_read @endif" onclick="redirect('{{$notification->href}}','{{$notification->id}}')">										
            <td>
                <div style="color:white !important;" class="h-50 w-50 l-h-50 rounded text-center">
                {{++$key}}
                </div>
            </td>
            <td>
            <a style="color:white !important;" href="javascript:void(0);" onclick="redirect('{{$notification->href}}','{{$notification->id}}')" class="text-dark font-weight-600 hover-primary font-size-16">{{$notification->title_en}}</a>
                <span style="color:white !important;" class="text-fade d-block">{{$notification->description_en}}</span>
            </td>
            <td style="direction: rtl">
                <a style="color:white !important;" href="javascript:void(0);" onclick="redirect('{{$notification->href}}','{{$notification->id}}')" class="text-dark font-weight-600 hover-primary font-size-16">{{$notification->title_ar}}</a>
                    <span style="color:white !important;" class="text-fade d-block">{{$notification->description_ar}}</span>
                </td>
         <td>
             @if(!$notification->is_cleared)
         <span style="color:white !important;" class="badge badge-warning">{{__('UnSeen')}}</span>
         @else
         <span style="color:white !important;" class="badge badge-success">{{__('Seen')}}</span>
         @endif
         </td>
         <td><span style="color:white !important;" class="text-fade d-block">{{$notification->created_at}}</span></td>
     
        </tr>
     
    
     @endforeach
</tbody>
<tfoot>

    <tr>
        <th>{{__('#')}}</th>
        <th>{{__('Description En')}}</th>
      
              <th>{{__('Description Ar')}}</th>
           
          <th>{{__('Status')}}</th>
          <th>{{__('Time')}}</th>
      
        
       
         
        </tr>
</tfoot>

                                    </table>
                         
                                </div>
                        
                    </div>
                </div>
            </div>
          </div>
      </section>
    </div>
</div>

<script>
    $(function(){
        $("#notificationDataTable").DataTable({ "pageLength":150,
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],})
    })
    function redirect(href,notificationId){

     
   
        $.ajax({
            url:"{{route('request-recharge-notifications.clearnotification')}}",
            type:"GET",
            data:{notification_id:notificationId},
            dataType:"json",
            success:function(response){
if(response.status == CONSTANTS.SUCCESS){
    location.href = href;
}
            }
        })
      
    }
    </script>
@stop

