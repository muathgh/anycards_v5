@extends('layouts.default')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ __('Balances Requests') }}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">


                            <br /><br />
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::label('from_date', __('From Date')) !!}
                                        <div class="form-group">
                                            {!! Form::text('from_date', old('from_date'), ['class' => 'form-control ui-date', 'autocomplete' => 'off', 'placeholder' => __('from_date')]) !!}
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('to_date', __('To Date')) !!}
                                        <div class="form-group">
                                            {!! Form::text('to_date', old('to_date'), ['class' => 'form-control ui-date', 'autocomplete' => 'off', 'placeholder' => __('To Date')]) !!}
                                            <p class="error" id="to_date_error" style="display:none">
                                                {{ __('Both of from date and to date are requireds') }}</p>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="button" onclick="search()" value="{{ __('Search') }}"
                                                class="btn btn-info" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="balancesRequestDataTable"
                                    class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                        <tr>
                                            <th>{{ __('#') }}</th>
                                            <th>{{ __('Full Name') }}</th>
                                            <th>{{ __('Amount') }}</th>

                                            <th>{{ __('Received Amount') }}</th>

                                            <th>{{ __('Status') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Options') }}</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>{{ __('#') }}</th>
                                            <th>{{ __('Full Name') }}</th>
                                            <th>{{ __('Amount') }}</th>

                                            <th>{{ __('Received Amount') }}</th>
                                            <th>{{ __('Status') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Options') }}</th>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
    </div>
    {{-- datatable --}}
    <script>
        var datatable = null;
        var formData = {

            from_date: null,
            to_date: null

        }
        $(function() {

            dt();
        })

        function search() {
            $("#to_date_error").hide();
            var fromDate = $("#from_date").val();
            var toDate = $("#to_date").val();
            if ((!fromDate && toDate) || (fromDate && !toDate)) {
                $("#to_date_error").show();
                return;
            }

            formData.from_date = $("#from_date").val();
            formData.to_date = $("#to_date").val();
            $('#balancesRequestDataTable').DataTable().destroy();
            dt();
        }

        function dt() {
            $('#balancesRequestDataTable').DataTable({
                "processing": true,
                "serverSide": true,
                responsive: true,
                autoWidth: true,
                fixedColumns: true,


                "lengthMenu": [
                    [100, 150, 200, -1],
                    [100, 150, 200, "All"]
                ],
                buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
         
     ],

                order: [],
                "pageLength": 100,
                dom: 'Blfrtip',
                "ajax": {
                    "url": "{{ route('balancesrequests.index') }}",
                    "type": "GET",
                    "data": formData,

                },
                "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        intVal = function(i) {
                            var num = i;
                            if (typeof i === 'string') {

                                num = parseFloat(i)

                            }
                            return num;
                        },




                        total = api
                        .column(2)
                        .data()
                        .reduce(function(a, b) {
                            return ((intVal(a) + intVal(b)));
                        }, 0);

                    $(api.column(2).footer()).html(
                        "<h3>Total " + toFixed(total,3) + "</h3>"
                    );

                    total2 = api
                        .column(3)
                        .data()
                        .reduce(function(a, b) {
                            return ((intVal(a) + intVal(b)));
                        }, 0);

                    $(api.column(3).footer()).html(
                        "<h3>Total " + toFixed(total2,3) + "</h3>"
                    );
                },

                columns: [{
                        "width": "5%",
                        "data": "#",
                        render: function(data, type, row, meta) {

                            return meta.row + meta.settings._iDisplayStart + 1;

                        }
                    },
                    {

                        "data": "full_name"
                    },
                    {

                        "data": "amount"
                    },
                    {
"data":"transaction_amount"
                    },
                    // {

                    //     "data": "text"
                    // },
                    // {

                    //     "data": "image",
                    //     render: function(data, type, row) {
                    //         console.log(row.image)
                    //         if (!row.image)
                    //             return "";
                    //         if(row.image_source =="web")
                    //         return "<a target='_blank' href=" + row.image + ">Image</a>"
                    //         else if(row.image_source =="mobile")
                    //         return "<a href='javascript:void(0)' data-image="+row.image+" class='img'>Image</a>";
                    //     }
                    // },
                    {

                        "data": "status",
                        render: function(data, type, row) {
                            if (row.status == "approved")
                                return "<span class='badge badge-success'>Approved</span>";
                            else if (row.status == "pending")
                                return "<span class='badge badge-warning'>Pending</span>";

                            return "<span class='badge badge-danger'>Rejected</span>";
                        }
                    },
                    {
                        "width": "10%",
                        "data": "created_at"
                    },
                    {
                        "data": "options",
                        render: function(data, type, row) {
                            return "<i class='fas fa-eye view cursor-pointer' data-id=" + row.id + "></i>"
                        }
                    }








                ]
            });
        }

        $(document).on('click', '.view', function() {
            var id = $(this).data("id")
            $.ajax({
                url: "{{ route('balances.balancesrequestspopup') }}",
                type: "POST",
                data: {
                    id: id,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    if (response.status == "success") {
                        var data = response.data;
                        var html = data.html

                        $("#modal .modal-body").html(html);
                        $("#modal").modal('toggle');
                    }
                }
            })

        })

        //      $(document).on('click','.img',function(){
        //         var image = $(this).data('image')
        //         var newTab = window.open();
        // newTab.document.body.innerHTML = '<img style="width:500px;height:auto;" src="data:image/jpg;base64,'+image+'">';
        //      })

    </script>

    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Request Balance Document</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  
                </div>
                <div class="modal-footer">
                </div>

            </div>
        </div>
    </div>
@stop
