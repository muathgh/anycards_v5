<table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>

    
            <th>{{__('Card Type')}}</th>
        <th>{{__('Price')}}</th>
           
        </tr>
    </thead>
    <tbody>
        
        @foreach($pricingLists as $key=>$pricingList)
        <tr>
    <td>{{++$key}}</td>
    <td>{{Helper::getTitle($pricingList->cardType)}}</td>
        <td class="price_input"><input data-id="{{$pricingList->id}}" style="width:200px;" value="{{$pricingList->price}}" name="price_{{$key}}" type="number" class="form-control" placeholder="{{__('Price')}}"/></td>
        </tr>    
    @endforeach
    </tbody>
    <tfoot>
        <th>{{__('#')}}</th>

    
        <th>{{__('Card Type')}}</th>
    <th>{{__('Price')}}</th>
    </tfoot>
</table>