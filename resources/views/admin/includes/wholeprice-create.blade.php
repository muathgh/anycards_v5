<table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>

    
            <th>{{__('Card Type')}}</th>
        <th>{{__('Price')}}</th>
        <th>{{__('Price Of Sale')}}</th>
        </tr>
    </thead>
    <tbody>
        
        @foreach($cardsTypes as $key=>$cardType)
        <tr>
    <td>{{++$key}}</td>
    <td>{{Helper::getTitle($cardType)}}</td>
   
        <td class="price_input"><input data-id="{{$cardType->id}}" style="width:200px;" value="{{$cardType->wholePrice ? 
        $cardType->wholePrice->price : 0}}" name="price_{{$key}}" type="number" class="form-control" placeholder="{{__('Price')}}"/></td>
       <td>{{$cardType->priceOfSale ? $cardType->priceOfSale->price : ""}}</td>    
    </tr> 
      
    @endforeach
    </tbody>
    <tfoot>
        <th>{{__('#')}}</th>

    
        <th>{{__('Card Type')}}</th>
    <th>{{__('Price')}}</th>
    <th>{{__('Price Of Sale')}}</th>
    </tfoot>
</table>