<table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
    <thead>
        <tr>
            <th>{{__('#')}}</th>

    
            <th>{{__('Card Type')}}</th>
        <th>{{__('Quantity Limit')}}</th>
        </tr>
    </thead>
    <tbody>
        
        @foreach($cardsTypes as $key=>$cardType)
        <tr>
    <td>{{++$key}}</td>
    <td>{{Helper::getTitle($cardType)}}</td>
   
        <td class="quantity_input"><input data-id="{{$cardType->id}}" style="width:200px;" value="{{$cardType->quantityLimit ? 
        $cardType->quantityLimit->quantity_limit : 0}}" name="quantity_{{$key}}" type="number" class="form-control input" placeholder="{{__('Quantity Limit')}}"/>
    </td>
        </tr>    
    @endforeach
    </tbody>
    <tfoot>
        <th>{{__('#')}}</th>

    
        <th>{{__('Card Type')}}</th>
    <th>{{__('Quantity Limit')}}</th>
    </tfoot>
</table>


{{-- <script>
    $(function(){
        $(".input").keyup(function(){
          
            var cardTypeId = $(this).data('id');
            $("#error_"+cardTypeId).hide()
            var officalPrice = $(".offical_price_"+cardTypeId).text();
            if(parseFloat($(this).val()) < parseFloat(officalPrice))
            $("#error_"+cardTypeId).show()
           
           
        })
    })
    </script> --}}