<table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>

    
            <th>{{__('Card Type')}}</th>
        <th>{{__('Price')}}</th>
        <th>{{__('Price Of Sale')}}</th>
        <th>{{__('Offical Price')}}</th>
           
        </tr>
    </thead>
    <tbody>
        
        @foreach($cardsTypes as $key=>$cardType)
        <tr>
    <td>{{++$key}}</td>
    <td>{{Helper::getTitle($cardType)}}</td>
   
        <td class="price_input"><input data-id="{{$cardType->id}}" style="width:200px;" value="{{$cardType->pricingLisits()->where('package_id',$packageId)->first() ? 
        $cardType->pricingLisits()->where('package_id',$packageId)->first()->price : 0}}" name="price_{{$key}}" type="number" class="form-control input" placeholder="{{__('Price')}}"/>
    <p style="display:none" id="error_{{$cardType->id}}" class="error price_error">Price should be greater than offical price</p>    
    </td>
    <td class="price_of_sale_{{$cardType->id}}">{{$cardType->priceOfSale ? $cardType->priceOfSale->price : "No Price Of Sale"}}</td>
    <td class="offical_price_{{$cardType->id}}">{{$cardType->adminOfficalPrice()->where('offical_price_package_id',$officalPricePackageId)->first() ? 
        $cardType->adminOfficalPrice()->where('offical_price_package_id',$officalPricePackageId)->first()->price : "No Offical Price"}}</td>
        </tr>    
    @endforeach
    </tbody>
    <tfoot>
        <th>{{__('#')}}</th>

    
        <th>{{__('Card Type')}}</th>
    <th>{{__('Price')}}</th>
    <th>{{__('Price Of Sale')}}</th>
    <th>{{__('Offical Price')}}</th>
    </tfoot>
</table>

<script>
    $(function(){
        $(".input").keyup(function(){
          
            var cardTypeId = $(this).data('id');
            $("#error_"+cardTypeId).hide()
            var officalPrice = $(".offical_price_"+cardTypeId).text();
            if(parseFloat($(this).val()) < parseFloat(officalPrice))
            $("#error_"+cardTypeId).show()
           
           
        })
    })
    </script>