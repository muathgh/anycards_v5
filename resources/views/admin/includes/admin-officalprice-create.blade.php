<table  class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>

    
            <th>{{__('Card Type')}}</th>
        <th>{{__('Price')}}</th>
        @if(!$copiedFrom)
        <th>{{__('Price After Discount')}}</th>
      @endif
        </tr>
    </thead>
    <tbody>
        
        @foreach($cardsTypes as $key=>$cardType)
        <tr>
    <td>{{++$key}}</td>
    <td>{{Helper::getTitle($cardType)}}</td>
    
   
        <td class="price_input"><input data-id="{{$cardType->id}}" style="width:200px;" 
            value="{{$cardType->adminOfficalPrice()->where('offical_price_package_id',$packageId)->first() ? 
            $cardType->adminOfficalPrice()->where('offical_price_package_id',$packageId)->first()->price : 0}}"
            name="price_{{$key}}" type="number" class="form-control" placeholder="{{__('Price')}}"/></td>
            @if(!$copiedFrom)
            <td>{{$cardType->adminOfficalPrice()->where('offical_price_package_id',$packageId)->first()? 
            $cardType->adminOfficalPrice()->where('offical_price_package_id',$packageId)->first()->price_after_discount : 0}}</td>
          @endif
    </tr> 
      
    @endforeach
    </tbody>
    <tfoot>
        <th>{{__('#')}}</th>

    
        <th>{{__('Card Type')}}</th>
    <th>{{__('Price')}}</th>
    @if(!$copiedFrom)
    <th>{{__('Price After Discount')}}</th>
    @endif
    </tfoot>
</table>