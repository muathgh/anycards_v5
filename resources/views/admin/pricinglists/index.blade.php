@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Pricing Lists')}}</h3>
                </div>
            
				<!-- /.box-header -->
				<div class="box-body">
                    @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                    @endif
                <a class="btn btn-success" href="{{route('pricinglists.create')}}" style="color:white;">{{__('Add / Edit Pricing List')}}</a>
                    <br/><br/>
                    @if (Auth::guard('admin')->user()->accessType() != null &&
                    Auth::guard('admin')->user()->accessType() != 'created_users')
                                            <div class="row">
            
                                                <div class="col-md-3">
                                                    {!! Form::label('admin_id', __('Admins')) !!}
                                                    <div class="form-group">
                                                        {!! Form::select('admin_id', $adminsLookup, old('admin_id'), ['class' => 'form-control select2', 'placeholder' => __('All')]) !!}
                                                    </div>
            
                                                </div>
            
            
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="button" id="search" onclick="search()" value="{{ __('Search') }}"
                                                            class="btn btn-primary" />
                                                    </div>
            
                                                </div>
                                            </div>
                                        @else
                                            <input type="hidden" id="admin_id" value="0" />
                                        @endif
					<div class="table-responsive">
					  <table  id="pricingListsDataTable" class="table  table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                            <th>{{__('#')}}</th>
                   
                                <th>{{__('Package Title En')}}</th>
                                <th>{{__('Package Title Ar')}}</th>
                            
              <th>{{__('Number of cards type')}}</th>
              <th>{{__('Created By')}}</th>
              <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
						<tfoot>
							<tr>
                                <th>{{__('#')}}</th>
                   
                                <th>{{__('Package Title En')}}</th>
                                <th>{{__('Package Title Ar')}}</th>
                               
                                <th>{{__('Number of cards type')}}</th>
                                <th>{{__('Created By')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th>{{__('Options')}}</th>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
          </div>
      </section>
      <!-- /.content -->
    </div>
</div>
{{-- datatable --}}
<script>
    var formData = {
        admin_id:null
    }
  var datatable = null;
 $(function(){
     dt();

 })

 function dt(){
    $('#pricingListsDataTable').DataTable({
         "processing":true,
     "serverSide": true,
     "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
     order:[],
     responsive: true,
            autoWidth:true,
            fixedColumns: true,
     buttons: [
        { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
         
     ],
     "footerCallback": function (row, data, start, end, display) {
        var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total = api
            .column(3)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(3).footer()).html(
            "<h3>Total "+toFixed(intVal(total),3)+"</h3>"
            );
     },
     "pageLength":100,
     dom: 'Blfrtip',
  

     "ajax": {
                    "url": "{{route('pricinglists.index')}}",
                    "type": "GET",
                    "data": formData
                },
     columns:[
         {"data": "#",
 render: function (data, type, row, meta) {
     
     return meta.row + meta.settings._iDisplayStart + 1;

 }
},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"card_types_number"},
{"data":"created_by"},
{"data":"created_at"},
{"data":"options",render:function(data,type,row){
    
    if(row.can_delete)
 return "<i class='fas fa-trash delete cursor-pointer' data-id="+row.id+"></i>"

 return "";
 // END  
}}
     ]
     });
 }


 $(document).on('click', '.delete', function(){ 
    var id = $(this).data('id');
        $.ajax({
            url:"/admin/pricinglists/destroy",
            type:"GET",
            dataType:"JSON",
            data:{id:id},
            success:function(response){
                if(response.data){
                $("#delete-popup .modal-body").html(response.data.html)
                $("#delete-popup").modal('toggle');
                }
            }
        })
 
 })
 function search(){
            var adminId = $("#admin_id").val();
            formData.admin_id = adminId;
            $("#pricingListsDataTable").DataTable().destroy();
            dt();
        }
</script>


@stop