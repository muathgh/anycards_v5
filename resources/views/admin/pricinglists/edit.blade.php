@extends('layouts.default')
@section('content')
<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{__('Packages')}}</h3>
                </div>
				<div class="box-body">
                    
                    <br/><br/>
                    <div id="content">
     
                        <div class="container-fluid">
                        {!! Form::open(['route' => 'pricinglists.store']) !!}
                        {!! Form::hidden('id',$pricingList->id) !!}
                        <div class="row">
                          
                            <div class="col-md-4">
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $categoriesLookup, $pricingList->cardType->subProvider
                                    ->provider->category->id,["class"=>"form-control select2","placeholder"=>__('Select'),"disabled"=>true]) !!}
                                    {!! $errors->first('category_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>  
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id',$providersLookup, $pricingList->cardType->subProvider
                                    ->provider->id,["class"=>"form-control select2","placeholder"=>__('Select'),"disabled"=>true]) !!}
                                    {!! $errors->first('provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                       
                         
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id',$subProvidersLookup, $pricingList->cardType->subProvider
                                    ->id,["class"=>"form-control select2","placeholder"=>__('Select'),"disabled"=>true]) !!}
                                    {!! $errors->first('sub_provider_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>
                           
                          
                        </div>
                        <div class="row">
                            
                            <div class="col-md-4">
                                {!! Form::label('package_id', __('Packages'))!!}
                                <div class="form-group">
                                    {!! Form::select('package_id', $packagesLookup, $pricingList->package_id,["class"=>"form-control select2","placeholder"=>__('Select'),"disabled"=>true]) !!}
                                    {!! $errors->first('package_id', '<p class="error">:message</p>') !!}
                                </div>
                                
                            </div>

                         
                      
                          

                          
                            
                        </div>

                        <div id="tblContainer" class="container">
@include('admin.includes.pricinglist-edit')
                        </div>

                    <input  onclick="save()" id="submit" type="button" class="btn btn-primary" value="{{__('Save')}}"/>
                        {{-- {!! Form::submit('Save',['class'=>"btn btn-primary"]) !!} --}}
                        {!! Form::close() !!}
                        </div>
                    </div>

            </div>
          </div>
      </section>
    </div>
</div>
<script>
      function save(){
        var formData = [];
        var packageId = $("#package_id").val();
        $("table tbody .price_input input").each(function(index,value){
          var price =   $(value).val()
          var id = $(value).data('id');
          formData.push({
              id:id,
              price:price
          });

        });
        $.ajax({
            url:"{{route('pricinglists.update')}}",
            type:"POST",
            dataType:"JSON",
            data:{data:JSON.stringify(formData), "_token": "{{ csrf_token() }}"},
            success:function(response){
                if(response.status == CONSTANTS.SUCCESS)
                location.href="{{route('pricinglists.index')}}"
            }
        })
    }
    </script>
@stop