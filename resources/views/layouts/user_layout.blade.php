<!doctype html>
<html lang="ar" dir="rtl">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
   <title>any-cards</title>
  
   @include('layouts.includes.user_head')
</head>
<body class="faq-bg">
 
      @include('layouts.includes.user_header')
  

   

           @yield('content')
         
 
   @include('layouts.includes.user_footer')
   @include('layouts.includes.popups')

</body>
