<!doctype html>
<html lang="ar" dir="rtl">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<head>
   <title>any-cards</title>
  
   @include('layouts.includes.user_head')
   <script type="text/javascript">
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      </script>
<link rel="stylesheet" href="{{asset('user/assets/css/sb-admin.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor_components/datatable/datatables.min.css')}}">

</head>
<body>

      @include('layouts.includes.user_header')
      @include('layouts.includes.user_nav')

   

           @yield('content')
         
 
 
   @include('layouts.includes.popups')

   <script src="{{asset('js/moment.min.js')}}"></script>
<script  src="{{asset('user/assets/js/sticky-sidebar.js')}}"></script>
<script  src="{{asset('user/assets/js/YouTubePopUp.jquery.js')}}"></script>
<script  src="{{asset('user/assets/js/owl.carousel.min.js')}}"></script>
<script  src="{{asset('user/assets/js/imagesloaded.min.js')}}"></script>
<script  src="{{asset('user/assets/js/wow.min.js')}}"></script>
<script  src="{{asset('user/assets/js/custom.js')}}"></script>
<script  src="{{asset('user/assets/js/popper.min.js')}}"></script>
<script  src="{{asset('user/assets/js/bootstrap.min.js')}}"></script>

<script src="{{asset('js/toastr.min.js')}}"></script>
<script src="{{asset('js/constants.js')}}"></script>

<script src="{{asset('assets/vendor_components/datatable/datatables.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script src="{{asset('assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/pickday.js')}}"></script>

</body>
