<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" href="../images/favicon.ico">


<!-- Vendors Style-->
<link rel="stylesheet" href="{{asset('css/vendors_css.css')}}">
  
<!-- Style-->  
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/skin_color.css')}}">	
<link rel="stylesheet" href="{{asset('css/main.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">	
<link rel="stylesheet" href="{{asset('css/timepicker.css')}}">
<link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
<link rel="stylesheet" href="{{asset('css/pickday.css')}}">
<link rel="stylesheet" href="{{asset('css/jquery.datetimepicker.css')}}">
<script src="{{asset('js/vendors.min.js')}}"></script>

