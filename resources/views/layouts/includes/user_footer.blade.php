<div  class=" footer padding-tb-30px background-main-color">
    <div class="container">
        {{-- <div class="row">
            <div class="col-lg-2">
                <a class="d-inline-block margin-tb-15px"><img src="assets/img/logo-2.png" alt=""></a>
            </div>
           
            <div class="col-lg-6">
                <ul class="footer-menu margin-tb-15px margin-lr-0px padding-0px list-unstyled float-lg-right">
                    <li><a href="#" class="text-white">{{__('Home')}}</a></li>
                    <li><a href="#" class="text-white">{{__('Home')}}</a></li>
                    <li><a href="#" class="text-white">{{__('Home')}}</a></li>
                    <li><a href="#" class="text-white">{{__('Home')}}</a></li>
                </ul>
            </div>
        </div> --}}
        {{-- <hr class="border-white opacity-4 margin-tb-45px"> --}}
        <div class="row">
           
            <div class="col-lg-6 text-right">
                <ul class="social-icon style-2 float-lg-right">
                    <li class="list-inline-item"><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="list-inline-item"><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                    <li class="list-inline-item"><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                    <li class="list-inline-item"><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                    <li class="list-inline-item"><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-6 text-left">
                <p class="margin-0px text-white opacity-7 sm-mb-15px">All Right Reserved | Algharabli Groups  © 2020   </p>
            </div>
        </div>
    </div>
</div>
<script  src="{{asset('user/assets/js/sticky-sidebar.js')}}"></script>
<script  src="{{asset('user/assets/js/YouTubePopUp.jquery.js')}}"></script>
<script  src="{{asset('user/assets/js/owl.carousel.min.js')}}"></script>
<script  src="{{asset('user/assets/js/imagesloaded.min.js')}}"></script>
<script  src="{{asset('user/assets/js/wow.min.js')}}"></script>
<script  src="{{asset('user/assets/js/custom.js')}}"></script>
<script  src="{{asset('user/assets/js/popper.min.js')}}"></script>
<script  src="{{asset('user/assets/js/bootstrap.min.js')}}"></script>

<script src="{{asset('js/toastr.min.js')}}"></script>
<script src="{{asset('js/constants.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('user/assets/js/clipboard.min.js')}}"></script>



<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js')}}"></script>

<script>
    $(function(){

if($(window).scrollTop() >=50)
{
    $("#fixed-header-dark").addClass("fixed-header-dark");
}
   
   
    });
    </script>