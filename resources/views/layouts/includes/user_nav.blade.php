@if (app()->getLocale() == "ar")

<nav class="navbar navbar-expand-lg navbar-dark z-index-9  fixed-top" id="mainNav">

    <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav navbar-sidenav background-main-color admin-nav" id="admin-nav">
            <li class="nav-item">
            <span class="nav-title-text">{{__('User Area')}}</span>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('My Profile')}}">
            <a class="nav-link @if(request()->segment(count(request()->segments()))=='profile')active @endif" href="{{route('profile.index')}}">
                    <i class="fa fa-fw fa-user-circle"></i>
                    <span class="nav-link-text">{{__('My Profile')}}</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('My Orders')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='orders')active @endif" href="{{route('orders.index')}}">
                    <i class="fa fa-fw fa-table"></i>
                        <span class="nav-link-text">{{__('My Orders')}}</span>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Price Of Sale Transactions Report')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='priceofsaletransactions')active @endif" href="{{route('orders.priceofsaletransactions')}}">
                    <i class="fa fa-fw fa-table"></i>
                        <span class="nav-link-text">{{__('Price Of Sale Transactions Report')}}</span>
                    </a>
            </li>
           
          
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Request Recharge')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='requestrecharges')active @endif" href="{{route('requestrecharges.index')}}">
                    <i class="far fa-fw fa-list-alt"></i>
                        <span class="nav-link-text">{{__('Request Recharge')}}</span>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='balancetransactions')active @endif" href="{{route('balancetransactions.index')}}">
                    <i class="far fa-fw fa-list-alt"></i>
                        <span class="nav-link-text">{{__('Balance Transactions')}}</span>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='deposittransactions')active @endif" href="{{route('deposittransactions.index')}}">
                    <i class="far fa-fw fa-list-alt"></i>
                        <span class="nav-link-text">{{__('Transfer Balance')}}</span>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='priceofsale')active @endif" href="{{route('cardtypespoints.index')}}">
                    <i class="far fa-fw fa-list-alt"></i>
                        <span class="nav-link-text">{{__('Cards Prices')}}</span>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Users Price Of Sale')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='addpriceofsale')active @endif" href="{{route('userspriceofsale.create')}}">
                    <i class="far fa-fw fa-list-alt"></i>
                        <span class="nav-link-text">{{__('Users Price Of Sale')}}</span>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='notifications')active @endif" href="{{route('usernotifications.index')}}">
                    <i class="far fa-fw fa-bell"></i>
                        <span class="nav-link-text">{{__('Notifications')}}</span> ({{Helper::getUserNotificationsNumber()}}) 
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='settings')active @endif" href="{{route('user.settings.index')}}">
                    <i class="fas fa-fw fa-cogs"></i>
                        <span class="nav-link-text">{{__('Settings')}}</span> 
                    </a>
            </li>
        

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Software')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='plugin')active @endif" href="{{route('user.settings.plugin')}}">
                    <i class="fas fa-fw fa-cogs"></i>
                        <span class="nav-link-text">{{__('Download Software')}}</span> 
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Application')}}">
                <a class="nav-link @if(request()->segment(count(request()->segments()))=='application')active @endif" href="{{route('user.settings.application')}}">
                    <i class="fas fa-fw fa-cogs"></i>
                        <span class="nav-link-text">{{__('Download Application')}}</span> 
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('logout')}}">
                <a class="nav-link" href="{{route('user.logout')}}">
                    <i class="far fa-fw  fa fa-sign-out-alt"></i>
                        <span class="nav-link-text">{{__('logout')}}</span>
                    </a>
            </li>
           
        </ul>

    </div>
</nav>
@else
<nav class="navbar navbar-expand-lg navbar-dark z-index-9  fixed-top" id="mainNav">

    <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav navbar-sidenav background-main-color admin-nav" id="admin-nav">
            <li class="nav-item">
            <span class="nav-title-text">{{__('User Area')}}</span>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('My Profile')}}">
            <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='profile')active @endif" href="{{route('profile.index')}}">
                    
                    <span class="nav-link-text">{{__('My Profile')}}</span>
                    <i class="fa fa-fw fa-user-circle"></i>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('My Orders')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='orders')active @endif" href="{{route('orders.index')}}">
                    
                        <span class="nav-link-text">{{__('My Orders')}}</span>
                        <i class="fa fa-fw fa-table"></i>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Price Of Sale Transactions Report')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='priceofsaletransactions')active @endif" href="{{route('orders.priceofsaletransactions')}}">
                    
                        <span class="nav-link-text">{{__('Price Of Sale Transactions Report')}}</span>
                        <i class="fa fa-fw fa-table"></i>
                    </a>
            </li>
           
          
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Request Recharge')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='requestrecharges')active @endif" href="{{route('requestrecharges.index')}}">
                    
                        <span class="nav-link-text">{{__('Request Recharge')}}</span>
                        <i class="far fa-fw fa-list-alt"></i>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='balancetransactions')active @endif" href="{{route('balancetransactions.index')}}">
                   
                        <span class="nav-link-text">{{__('Balance Transactions')}}</span>
                        <i class="far fa-fw fa-list-alt"></i>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='deposittransactions')active @endif" href="{{route('deposittransactions.index')}}">
                   
                        <span class="nav-link-text">{{__('Transfer Balance')}}</span>
                        <i class="far fa-fw fa-list-alt"></i>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='priceofsale')active @endif" href="{{route('cardtypespoints.index')}}">
                    
                        <span class="nav-link-text">{{__('Cards Prices')}}</span>
                        <i class="far fa-fw fa-list-alt"></i>
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='notifications')active @endif" href="{{route('usernotifications.index')}}">
                
                        <span class="nav-link-text">{{__('Notifications')}}</span> ({{Helper::getUserNotificationsNumber()}}) 
                        <i class="far fa-fw fa-bell"></i>
                  
                    </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Balance Transactions')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='settings')active @endif" href="{{route('user.settings.index')}}">
                 
                        <span class="nav-link-text">{{__('Settings')}}</span> 
                        <i class="fas fa-fw fa-cogs"></i>
                    </a>
            </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('Software')}}">
                <a class="nav-link text-left @if(request()->segment(count(request()->segments()))=='plugin')active @endif" href="{{route('user.settings.plugin')}}">
                  
                        <span class="nav-link-text">{{__('Download Software')}}</span> 
                        <i class="fas fa-fw fa-cogs"></i>
                    </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{__('logout')}}">
                <a class="nav-link text-left" href="{{route('user.logout')}}">
                 
                        <span class="nav-link-text">{{__('logout')}}</span>
                        <i class="far fa-fw  fa fa-sign-out-alt"></i>
                    </a>
            </li>
           
        </ul>

    </div>
</nav>
@endif