
{{-- V_5 UPDATS 27/12/2021 --}}
<style>
	.page-active{
		color:red !important;
		font-weight: bold !important;
	}
	.fa-edit{
		color:#000 !important;
	}

	</style>

<header class="main-header">
	<div class="d-flex align-items-center logo-box justify-content-between">
		<a href="#" class="waves-effect waves-light nav-link rounded d-none d-md-inline-block mx-10 push-btn" data-toggle="push-menu" role="button">
			<span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
		</a>	
		<!-- Logo -->
		<a href="index.html" class="logo">
		  <!-- logo-->
		  <div class="logo-lg">
        {{-- <span class="light-logo"><img src="{{asset('custom/images/logo-light-text.png')}}" alt="logo"></span>
			  <span class="dark-logo"><img src="{{asset('custom/images/logo-light-text.png')}}" alt="logo"></span> --}}
		  </div>
		</a>	
	</div>  
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top pl-10">
		<div class="container">
		  <!-- Sidebar toggle button-->
		  <div class="app-menu">
			<ul class="header-megamenu nav">
				<li class="btn-group nav-item d-md-none">
					<a href="#" class="waves-effect waves-light nav-link rounded push-btn" data-toggle="push-menu" role="button">
						<span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
					</a>
				</li>
				<li class="btn-group nav-item d-none d-xl-inline-block">
					<a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded full-screen" title="Full Screen">
						<i class="icon-Expand-arrows"><span class="path1"></span><span class="path2"></span></i>
					</a>
				</li>
			</ul> 
		  </div>

		  <div class="navbar-custom-menu r-side">
			<ul class="nav navbar-nav">		  
				<li class="dropdown user user-menu">
					<a href="/" target="_blank" class="waves-effect waves-light dropdown-toggle"  title="User">
						<i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
					</a>
					
				  </li>	
			  <!-- Notifications -->
			  @if(Auth::guard('admin')->user()->isSuperAdmin())
			@if(Helper::getNotificationNumber() >0)<li class="dropdown notifications-menu" data-badge="{{Helper::getNotificationNumber()}}">@else<li class="dropdown notifications-menu">@endif
				<a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="Notifications">
				  <i class="icon-Notifications"><span class="path1"></span><span class="path2"></span></i>
				</a>
				<ul class="dropdown-menu animated bounceIn">

				  <li class="header">
					<div class="p-20">
						<div class="flexbox">
							<div>
								<h4 class="mb-0 mt-0">Notifications</h4>
							</div>
							<div>
								<a href="javascript:void(0);" id="clear-all" class="text-danger">Clear All</a>
							</div>
						</div>
					</div>
				  </li>

				  <li>
					<!-- inner menu: contains the actual data -->
					<ul class="menu sm-scrol">
						@foreach(Helper::getLastNotifications() as $notification)
					  <li>
					  <a data-id="{{$notification->id}}" href="javascript:void(0);" onclick="clearNotification(this)">
						  <i class="fa fa-users text-info"></i> {{Helper::getTitle($notification)}}
						</a>
					  </li>
					
				@endforeach
					 
					</ul>
				  </li>
				  <li class="footer">
				  <a href="{{route('notifications.index')}}">View all</a>
				  </li>
				</ul>
			  </li>	
			  @endif

			  <!-- User Account-->
			

			  <!-- Control Sidebar Toggle Button -->
			  <li>
				  <a href="#" data-toggle="control-sidebar" title="Setting" class="waves-effect waves-light hide">
					<i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
				  </a>
			  </li>

			  {{-- Request recharge --}}

			  	  <!-- Notifications -->
					@if(Helper::getRequestRechargeNotificationNumber() >0)<li class="dropdown notifications-menu" data-badge="{{Helper::getRequestRechargeNotificationNumber()}}">@else<li class="dropdown notifications-menu">@endif									
							<a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="Notifications">
				  <i class="icon-Dollar"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
				</a>
				<ul class="dropdown-menu animated bounceIn">

				  <li class="header">
					<div class="p-20">
						<div class="flexbox">
							<div>
								<h4 class="mb-0 mt-0">Notifications</h4>
							</div>
							<div>
								<a href="javascript:void(0);" id="clear-all" class="text-danger">Clear All</a>
							</div>
						</div>
					</div>
				  </li>

				  <li>
					<!-- inner menu: contains the actual data -->
					<ul class="menu sm-scrol">
						@foreach(Helper::getLastRequestRechargeNotifications() as $notification)
					  <li>
					  <a data-id="{{$notification->id}}" href="javascript:void(0);" onclick="clearRequestRechargeNotification(this)">
						  <i class="fa fa-users text-info"></i> {{Helper::getTitle($notification)}}
						</a>
					  </li>
					
				@endforeach
					 
					</ul>
				  </li>
				  <li class="footer">
				  <a href="{{route('request-recharge-notification.index')}}">View all</a>
				  </li>
				</ul>
			  </li>	

			

			  <!-- Control Sidebar Toggle Button -->
			  <li>
				  <a href="#" data-toggle="control-sidebar" title="Setting" class="waves-effect waves-light hide">
					<i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
				  </a>
			  </li>

			</ul>
		  </div>
		</div>
    </nav>
  </header>
  
  <aside class="main-sidebar" id="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">
		<div class="user-profile px-40 py-15">
			<div class="d-flex align-items-center">			
				<div class="image">
				  <img src="{{asset('images/avatar/avatar-13.png')}}" class="avatar avatar-lg bg-primary-light" alt="User Image">
				</div>
				<div class="info">
					<div>
					<a class="px-20"  href="#">{{Auth::guard('admin')->user()->username}}</a>
					
					</div>
					@if(!Auth::guard('admin')->user()->isSuperAdmin() && Auth::guard('admin')->user()->currentBalance())
					<div>
						<a class="px-20">Balance: {{Auth::guard('admin')->user()->currentBalance()}}</a>
					</div>
					@endif
					
				</div>
			
			</div>
		
			
        </div>
		
	  <!-- sidebar menu-->
	  @if(Auth::guard('admin')->check())
      <ul class="sidebar-menu" data-widget="tree">
		@can('dashboard screen')
		<li class="header">{{__('Dashboard')}}</li>	
		
		<li>
          <a href="/admin">
           <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>{{__('Dashboard')}}</span>
            <span class="pull-right-container">
              {{-- <i class="fa fa-angle-right pull-right"></i> --}}
            </span>
          </a>
         
		</li>
		@endcan
		
	
		  @can('categories screen')
		<li class="treeview @if(request()->segment(count(request()->segments()) )=='categories' || request()->segment(count(request()->segments()) -1 )=='categories') menu-open @endif">
          <a href="#">
           <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>{{__('Categories')}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='categories' || request()->segment(count(request()->segments()) -1 )=='categories') block @else none @endif">
		  <li><a class="@if(request()->segment(count(request()->segments()) )=='categories') page-active @endif" href="{{route('categories.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Categories')}}</a></li>
		  <li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='categories') page-active @endif" href="{{route('categories.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Category')}}</a></li>
          </ul>
		</li>	
		@endcan
		@can('providers screen')
		<li class="treeview @if(request()->segment(count(request()->segments()) )=='providers' || request()->segment(count(request()->segments()) -1 )=='providers') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Providers')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='providers' || request()->segment(count(request()->segments()) -1 )=='providers') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='providers') page-active @endif" href="{{route('providers.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Providers')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='providers') page-active @endif" href="{{route('providers.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Provider')}}</a></li>
			</ul>
		  </li>	
		  @endcan
		  @can('subproviders screen')
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='subProviders' || request()->segment(count(request()->segments()) -1 )=='subProviders') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('SubProviders')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='subProviders' || request()->segment(count(request()->segments()) -1 )=='subProviders') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='subProviders') page-active @endif" href="{{route('subProviders.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('SubProviders')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='subProviders') page-active @endif" href="{{route('subProviders.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add SubProvider')}}</a></li>
			</ul>
		  </li>	
		  @endcan
		  @can('cards types screen')
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='cardsTypes' || request()->segment(count(request()->segments()) -1 )=='cardsTypes') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Cards Types')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='cardsTypes' || request()->segment(count(request()->segments()) -1 )=='cardsTypes') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='cardsTypes') page-active @endif" href="{{route('cardsTypes.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Cards Types')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='cardsTypes') page-active @endif" href="{{route('cardsTypes.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Card Type')}}</a></li>
			
		</ul>
		  </li>	
		  @endcan
		  @can('cards screen')
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='cards' || request()->segment(count(request()->segments()) -1)=='cards') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Uploaded Cards')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()))=='cards' || request()->segment(count(request()->segments()) -1)=='cards') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='cards') page-active @endif" href="{{route('cards.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Uploaded Cards')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1)=='cards') page-active @endif" href="{{route('cards.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Card')}}</a></li>
			
		
		
		
		
		</ul>
		  </li>
		  @endcan
		
		  @can("packages screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='packages' || request()->segment(count(request()->segments())-1 )=='packages') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Packages')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='packages' || request()->segment(count(request()->segments())-1 )=='packages') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='packages') page-active @endif" href="{{route('packages.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Packages')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments())-1 )=='packages') page-active @endif" href="{{route('packages.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Package')}}</a></li>
			</ul>
		  </li>
		  @endcan
		  @can("pricing lists screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='pricinglists' || request()->segment(count(request()->segments())-1 )=='pricinglists') menu-open @endif" >
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Pricing Lists')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='pricinglists' || request()->segment(count(request()->segments()) -1 )=='pricinglists') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='pricinglists') page-active @endif" href="{{route('pricinglists.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Pricing Lists')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments())-1 )=='pricinglists') page-active @endif" href="{{route('pricinglists.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Pricing List')}}</a></li>
			</ul>
		  </li>
		  @endcan
		  @if(Auth::guard('admin')->user()->isSuperAdmin())
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='officalpricepackages' || request()->segment(count(request()->segments()) )=='officalpricediscounts'  || request()->segment(count(request()->segments()) )=='adminofficalprice' || request()->segment(count(request()->segments())-1 )=='officalpricepackages' || request()->segment(count(request()->segments())-1 )=='adminofficalprice' || request()->segment(count(request()->segments())-1 )=='officalpricediscounts') menu-open @endif" >
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Admin Offical Prices')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='officalpricepackages' || request()->segment(count(request()->segments()) )=='officalpricediscounts' || request()->segment(count(request()->segments()) )=='adminofficalprice' || request()->segment(count(request()->segments())-1 )=='officalpricepackages' || request()->segment(count(request()->segments())-1 )=='adminofficalprice' || request()->segment(count(request()->segments())-1 )=='officalpricediscounts') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='officalpricepackages') page-active @endif" href="{{route('officalpricepackages.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Offical Price Packages')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='adminofficalprice') page-active @endif" href="{{route('adminofficalprice.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Admins Offical Price')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='officalpricediscounts') page-active @endif" href="{{route('officalpricediscount.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Offical Price Discounts')}}</a></li>

		</ul>
		  </li>
		  @endif
		  @can("price of sale screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='priceofsale' || request()->segment(count(request()->segments()) -1 )=='priceofsale') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Price Of Sale')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='priceofsale' || request()->segment(count(request()->segments()) -1 )=='priceofsale') block @else none @endif">
				<li><a class="@if(request()->segment(count(request()->segments()) )=='priceofsale') page-active @endif" href="{{route('priceofsale.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Price Of Sale')}}</a></li>

			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='priceofsale') page-active @endif" href="{{route('priceofsale.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Price Of Sale')}}</a></li>
			</ul>
		  </li>
		  @endcan
		  @can('users screen')
		 
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='users' || request()->segment(count(request()->segments()) -1)=='users') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Users')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='users' || request()->segment(count(request()->segments()) -1 )=='users') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='users') page-active @endif" href="{{route('users.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Users')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1)=='users') page-active @endif" href="{{route('users.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add User')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='otps' && request()->segment(count(request()->segments()) -1)=='users') page-active @endif" href="{{route('users.otps')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Users Verficiation Code')}}</a></li>

		</ul>
		  </li>
		  @endcan
		
		
		  @can("balances screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='balances' || request()->segment(count(request()->segments()) -1)=='balances') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Balances')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='balances' || request()->segment(count(request()->segments()) -1 )=='balances' || request()->segment(count(request()->segments()) -2 )=='balances') block @else none @endif">
			
				@if(Helper::checkAdminPermission("Balances")) <li><a class="@if(request()->segment(count(request()->segments()) )=='balances') page-active @endif" href="{{route('balances.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Balances')}}</a></li>@endif
				@if(Helper::checkAdminPermission("Balances"))<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1)=='balances') page-active @endif" href="{{route('balances.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Charge / WithDraw')}}</a></li>@endif
				@if(Helper::checkAdminPermission("User Transferd Balance"))<li><a class="@if(request()->segment(count(request()->segments()) )=='transferbalance') page-active @endif" href="{{route('transferbalance.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('User Transferd Balance')}}</a></li>@endif
				@if(Helper::checkAdminPermission("User Balances Requests"))<li><a class="@if(request()->segment(count(request()->segments()) )=='balancesrequests') page-active @endif" href="{{route('balancesrequests.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('User Balances Requests')}}</a></li>@endif
				<li><a class="@if(request()->segment(count(request()->segments()) )=='mytransactions') page-active @endif" href="{{route('balancelimit.mytransactions')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('My Balance Transactions')}}</a></li>
				@if(Auth::guard('admin')->user()->isSuperAdmin())<li><a class="@if(request()->segment(count(request()->segments()) )=='balancelimit') page-active @endif" href="{{route('balancelimit.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Admin Balance Limit')}}</a></li>@endif
		</ul>
		  </li>
		  @endcan
		  @can("card transactions screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='cardstransactions' || request()->segment(count(request()->segments()) -1)=='cardstransactions') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Card Transaction')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='cardstransactions' || request()->segment(count(request()->segments()) -1)=='cardstransactions') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='cardstransactions') page-active @endif" href="{{route('cardstransactions.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Card Transaction')}}</a></li>
			
	
		</ul>
		  </li>
		  @endcan
		  @can("reports screen")

		
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='reports') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Reports')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='reports' || request()->segment(count(request()->segments()) -1 )=='reports') block @else none @endif">
		@if(Helper::checkAdminPermission("Report"))	<li><a class="@if(request()->segment(count(request()->segments()) )=='reports') page-active @endif"  href="{{route('reports.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Report')}}</a></li>@endif
		@if(Helper::checkAdminPermission("Cards Stock"))	<li><a class="@if(request()->segment(count(request()->segments()) )=='cardsreports') page-active @endif" href="{{route('reports.cardsreports')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Cards Stock')}}</a></li>@endif
		@if(Helper::checkAdminPermission("UnReceivables Amounts Report"))<li><a class="@if(request()->segment(count(request()->segments()) )=='usersreports') page-active @endif" href="{{route('reports.userreports')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('UnReceivables Amounts Report')}}</a></li>@endif
		@if(Helper::checkAdminPermission("Balance Transactions Report"))<li><a class="@if(request()->segment(count(request()->segments()) )=='balancetransactionsreport') page-active @endif" href="{{route('reports.balancetransactionsreports')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Balance Transactions Report')}}</a></li>@endif
		@if(Helper::checkAdminPermission("Net Profit Report"))	<li><a class="@if(request()->segment(count(request()->segments()) )=='netprofit') page-active @endif" href="{{route('reports.netprofitreport')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Net Profit Report')}}</a></li>@endif

			
		</ul>
		  </li>
		  @endcan
		  @can('permissions screen')
		
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='role' || request()->segment(count(request()->segments()) -1 )=='role') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Role')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='role' || request()->segment(count(request()->segments())-1 )=='role') block @else none @endif">
				<li><a class="@if(request()->segment(count(request()->segments()) )=='role') page-active @endif" href="{{route('role.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Roles')}}</a></li>

			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='role') page-active @endif" href="{{route('role.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Create Role')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='assign') page-active @endif"  href="{{route('role.assign')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Assign Role To User')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='permissions') page-active @endif"  href="{{route('role.permissions')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Assign Sub Permissions To Admin')}}</a></li>
		</ul>
		  </li>
		  @endcan

		  {{-- @can("settings screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='quantitylimit' || request()->segment(count(request()->segments()) -1 )=='quantitylimit') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Cards Quantity Limits')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='quantitylimit' || request()->segment(count(request()->segments()) -1 )=='quantitylimit') block @else none @endif">

			</ul>
		  </li>
		  @endcan --}}
	
		  @can("settings screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='settings' || request()->segment(count(request()->segments())-1 )=='scannedcards' || request()->segment(count(request()->segments()) -1 )=='settings') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Settings')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='settings' || request()->segment(count(request()->segments())-1 )=='scannedcards' || request()->segment(count(request()->segments())-1 )=='settings') block @else none @endif">
				@if(Helper::checkAdminPermission("General Settings"))<li><a class="@if(request()->segment(count(request()->segments()) )=='settings') page-active @endif" href="{{route('settings.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('General Settings')}}</a></li>@endif

				@if(Helper::checkAdminPermission("Scanned Cards"))<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='scannedcards') page-active @endif" href="{{route('scannedcards.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Scanned Cards')}}</a></li>@endif
				@if(Helper::checkAdminPermission("Download Software"))<li><a  href="{{route('settings.plugin')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Download Software')}}</a></li>@endif
				@if(Helper::checkAdminPermission("Download Application"))<li><a  href="{{route('settings.application')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Download Application')}}</a></li>@endif
				<li><a class="@if(request()->segment(count(request()->segments()) )=='userbalancelimitnotification') page-active @endif" href="{{route('settings.userbalancelimitnotification')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('User Balance Limit Notification')}}</a></li>
		
			</ul>
		  </li>
		  @endcan
		  {{-- @can("card limits screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='cardslimit' || request()->segment(count(request()->segments()) -1 )=='cardslimit') menu-open @endif" >
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Cards Limit')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='cardslimit' || request()->segment(count(request()->segments())-1 )=='cardslimit') block @else none @endif">


		</ul>
		  </li>
		  @endcan --}}
		  {{-- @can("card expiry screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='cardsexpiry' || request()->segment(count(request()->segments()) -1 )=='cardsexpiry') menu-open @endif" >
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Cards Expiry')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='cardsexpiry' || request()->segment(count(request()->segments())-1 )=='cardsexpiry') block @else none @endif">
	

		</ul>
		  </li>
		  @endcan --}}
		  @can("notifications screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='notifications' || request()->segment(count(request()->segments()) -1 )=='notifications') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Notifications')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='notifications' || request()->segment(count(request()->segments())-1 )=='notifications' || request()->segment(count(request()->segments())-2 )=='notifications') block @else none @endif">
				
				@if(Helper::checkAdminPermission("Notifications"))<li><a class="@if(request()->segment(count(request()->segments()) )=='notifications') page-active @endif" href="{{route('notifications.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Notifications')}}</a></li>@endif
				@if(Helper::checkAdminPermission("Request Balance Notifications"))<li><a class="@if(request()->segment(count(request()->segments()) )=='requestRechargeNotifications') page-active @endif" href="{{route('request-recharge-notification.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Request Recharge Notifications')}}</a></li>@endif

				@if(Helper::checkAdminPermission("Push Notifications"))<li><a class="@if(request()->segment(count(request()->segments()) )=='pushnotifications') page-active @endif" href="{{route('pushnotifications.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Push Notifications')}}</a></li>@endif
				@if(Helper::checkAdminPermission("Cards Limit"))
				
				<li><a class="@if(request()->segment(count(request()->segments()) )=='cardslimit' && request()->segment(count(request()->segments()) -1 )=='notifications') page-active @endif" href="{{route('cardslimit.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Stock Notify Limit')}}</a></li>
				<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='cardslimit' &&  request()->segment(count(request()->segments()) -2 )=='notifications') page-active @endif" href="{{route('cardslimit.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Stock Notify Limit')}}</a></li>
		@endif
		@if(Helper::checkAdminPermission("Quantity Limit"))
				<li><a class="@if(request()->segment(count(request()->segments()) )=='quantitylimit' && request()->segment(count(request()->segments()) -1 )=='notifications' ) page-active @endif" href="{{route('quantitylimits.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('User daily Limit')}}</a></li>

				<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='quantitylimit' && request()->segment(count(request()->segments()) -2 )=='notifications') page-active @endif" href="{{route('quantitylimits.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add User daily Limit')}}</a></li>
@endif
@if(Helper::checkAdminPermission("Cards Expiry"))
				<li><a class="@if(request()->segment(count(request()->segments()) )=='cardsexpiry' && request()->segment(count(request()->segments()) -1 )=='notifications') page-active @endif" href="{{route('cardsexpiry.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Cards Expiry')}}</a></li>
				<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='cardsexpiry' && request()->segment(count(request()->segments()) -2 )=='notifications') page-active @endif" href="{{route('cardsexpiry.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Card Expiry')}}</a></li>
			@endif
			
			</ul>
		  </li>
		  @endcan
		  @can("whole prices screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='wholeprice' || request()->segment(count(request()->segments()) -1 )=='wholeprice') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Official Price')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='wholeprice' || request()->segment(count(request()->segments())-1 )=='wholeprice') block @else none @endif">
				<li><a class="@if(request()->segment(count(request()->segments()) )=='wholeprice') page-active @endif" href="{{route('wholeprice.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Official Price')}}</a></li>

			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments()) -1 )=='wholeprice') page-active @endif" href="{{route('wholeprice.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Official Price')}}</a></li>

		</ul>
		  </li>
		  @endcan
		  @can("excel screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='attach' || request()->segment(count(request()->segments()) -1 )=='attach' || request()->segment(count(request()->segments()) -2 )=='attach' ) menu-open @endif" >
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Attach File')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='attach' || request()->segment(count(request()->segments())-1 )=='attach' || request()->segment(count(request()->segments())-2 )=='attach') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='attach') page-active @endif" href="{{route('excel.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Download Template')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='import' && request()->segment(count(request()->segments()) -1 )=='excel') page-active @endif" href="{{route('excel.import')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Upload Excel')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='import' && request()->segment(count(request()->segments()) -1 )=='csv') page-active @endif" href="{{route('csv.import')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Upload CSV')}}</a></li>
	
		</ul>
		  </li>
	
	
	
		  @can("admins screen")
		  <li class="treeview @if(request()->segment(count(request()->segments()) )=='admins' || request()->segment(count(request()->segments()) -1 )=='admins') menu-open @endif">
			<a href="#">
			 <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Admins')}}</span>
			  <span class="pull-right-container">
				<i class="fa fa-angle-right pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu" style="display:@if(request()->segment(count(request()->segments()) )=='admins' || request()->segment(count(request()->segments())-1 )=='admins') block @else none @endif">
			<li><a class="@if(request()->segment(count(request()->segments()) )=='admins') page-active @endif" href="{{route('admins.index')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Admins')}}</a></li>
			<li><a class="@if(request()->segment(count(request()->segments()) )=='create' && request()->segment(count(request()->segments())-1 )=='admins') page-active @endif" href="{{route('admins.create')}}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>{{__('Add Admin')}}</a></li>
		</ul>
		  </li>
		 
		  @endcan
		  @endcan
		  <li class="treeview">
			<a href="javascript:void(0)" onclick="logout()">
			 <i><span class="path1"></span><span class="path2"></span></i>
			  <span>{{__('Logout')}}</span>
			  
			</a>
			
		  </li>
  
         
        </li>   	     
	  </ul>
	  @endif
    </section>
  </aside>


  
  <div class="control-sidebar-bg"></div>
 <script>
	 function logout(){
		 location.href="{{route('admin.logout')}}"
	 }
	 $(function(){
		 $(document).on('click',"#clear-all",function(){
			$.ajax({
			 url:"{{route('notifications.clearall')}}",
			 dataType:"json",
			 type:"GET",
			 success:function(){
				 location.reload();
			 }
		 })
		 })
	 })

	 function clearRequestRechargeNotification(ele){
		 var id = $(ele).data('id');
		 //{{route('request-recharge-notification.index')}}

		 $.ajax({
            url:"{{route('request-recharge-notifications.clearnotificationheader')}}",
            type:"GET",
            data:{notification_id:id},
            dataType:"json",
            success:function(response){
if(response.status == CONSTANTS.SUCCESS){
    var href = response.data.href;
	location.href = href;
}
            }
        })
	 }

	 function clearNotification(ele){
		 var id = $(ele).data('id');
		 //{{route('request-recharge-notification.index')}}

		 $.ajax({
            url:"{{route('notifications.clearnotificationheader')}}",
            type:"GET",
            data:{notification_id:id},
            dataType:"json",
            success:function(response){
if(response.status == CONSTANTS.SUCCESS){
    var href = response.data.href;
	location.href = href;
}
            }
        })
	 }

	 </script>

	 {{-- END V_5 UPDATS 27/12/2021 --}}