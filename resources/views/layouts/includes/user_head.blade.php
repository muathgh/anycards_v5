
<meta name="author" content="Nile-Theme">
<meta name="robots" content="index follow">
<meta name="googlebot" content="index follow">
<link rel="icon" href="{{asset('images/favicon.ico')}}">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="keywords" content="آيتونز , جوجل بلاي , بطاقات الألعاب , بطاقات الإتصالات , بلاي ستيشن , إكس بوكس , شاهد VIP,سيرفرات البث المباشر,ستيم,فيس بوك,نيتفليكس">
<meta name="description" content="آيتونز , جوجل بلاي , بطاقات الألعاب , بطاقات الإتصالات , بلاي ستيشن , إكس بوكس , شاهد VIP,سيرفرات البث المباشر,ستيم,فيس بوك,نيتفليكس">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,300,400,700,400i,500%7CDancing+Script:700%7CDancing+Script:700" rel="stylesheet">
    <!-- animate -->
<link rel="stylesheet" href="{{asset('user/assets/css/animate.css')}}" />
    <!-- owl Carousel assets -->
<link href="{{asset('user/assets/css/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('user/assets/css/owl.theme.css')}}" rel="stylesheet">
    <!-- bootstrap -->
<link rel="stylesheet" href="{{asset('user/assets/css/bootstrap.min.css')}}">

    <!-- hover anmation -->
<link rel="stylesheet" href="{{asset('user/assets/css/hover-min.css')}}">
    <!-- flag icon -->
<link rel="stylesheet" href="{{asset('user/assets/css/flag-icon.min.css')}}">
    <!-- main style -->
<link rel="stylesheet" href="{{asset('user/assets/css/style.css')}}">
    <!-- colors -->
<link rel="stylesheet" href="{{asset('user/assets/css/colors/main.css')}}">
    <!-- elegant icon -->
<link rel="stylesheet" href="{{asset('user/assets/css/elegant_icon.css')}}">
<link rel="stylesheet" href="{{asset('user/assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('css/util.css')}}">
<link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">

<script  src="{{asset('user/assets/js/jquery-3.2.1.min.js')}}"></script>

    <!-- Maps library  -->
    <script  src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script  src="{{asset('user/assets/js/jquery.gomap-1.3.3.min.js')}}"></script>

    <!-- jquery library  -->
    <link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
    <!-- fontawesome  -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="{{asset('js/main.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/pickday.css')}}">

    <script src="{{asset('js/Impresora.js')}}"></script>  

    @if (app()->getLocale() == "en")
    <link rel="stylesheet" href="{{asset('css/style_en.css')}}">
    @else
    <link rel="stylesheet" href="{{asset('css/style_ar.css')}}">
    @endif
    