
<style>
   button.close-order-complete{
    padding: 0;
    background: 0 0;
    border: 0;
    -webkit-appearance: none;
  }
  .close-order-complete{float: right;
    font-size: 1.5rem;
    font-weight: 700;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    opacity: .5;
  }
  
  .modal-header .close-order-complete{
    padding:15px;
  }
  .modal-header .close-order-complete{
  margin:  -15px -15px -15px 0;
  }
 
  
  </style>



<div id="popup" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header flex-column">
      
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
			</div>
		
		</div>
	</div>
</div> 
<div id="order-popup" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header flex-column">
        <div class="icon-box">
				<img src="{{asset('user/assets/img/0724884440e8ddd0896ff557b75a222a.gif')}}">
        </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
		
		</div>
	</div>
</div> 

<div id="order-view-popup" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content" style="width:700px;">
      <div class="modal-header">
        <button type="button" class="close-order-complete" id="exit-view-order">&times;</button>
        <h4 class="modal-title">{{__('Order Complete')}}</h4>
      </div>
      <div class="modal-body" style="width:700px;">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
      <button type="button" onclick="exit2()" class="btn btn-default">{{__('Close')}}</button>
      </div>
    </div>

  </div>
</div>

  <div id="order-complete-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content" style="width:700px;">
        <div class="modal-header">
          <button type="button" class="close-order-complete" id="exit-order-complete">&times;</button>
          <h4 class="modal-title">{{__('Order Complete')}}</h4>
        </div>
        <div class="modal-body" style="width:700px;">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
        <button type="button" onclick="exit()" class="btn btn-default">{{__('Close')}}</button>
        </div>
      </div>
  
    </div>
  </div>

  <div id="sendcode-byemail" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="exit()" >&times;</button>
          <h4 class="modal-title">{{__('Send Code By Email')}}</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
        <button type="button" onclick="exit()" class="btn btn-default">{{__('Close')}}</button>
        </div>
      </div>
  
    </div>
  </div>

  
  <div id="request-recharges-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{__('Request Recharge')}}</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
        </div>
      </div>
  
    </div>
  </div>

  <div id="receipt-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{__('Receipt')}}</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
        </div>
      </div>
  
    </div>
  </div>

  <script>
    $(document).on("click","#exit-order-complete",function(e){
      location.href="/"
    })
    $(document).on("click","#exit-view-order",function(e){
      $("#order-view-popup").modal('hide')
    })
    function exit(){
      location.href="/"
    }
    function exit2(){
      $("#order-view-popup").modal('hide')
    }
    </script>

