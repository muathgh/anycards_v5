<style>
    .notification {

  color: white;
  text-decoration: none;
  padding: 15px 26px;
  position: relative !important;
  display: inline-block;
  border-radius: 2px;
width:30px;
background-color: initial;
  left:initial;
  margin-top:-10px;
  margin-left: 5px;

}

.fa-bell{
    color: #007cff;
    font-size: 30px;
}


.notification .badge {
  position: absolute;
  top: 3px;
  right: 10px;
  padding: 5px 10px;
  border-radius: 50%;
  background: red;
  color: white;
}
</style>
<header style="height:65px" id="header" class="background-white box-shadow fixed-top z-index-99">
    <div id="fixed-header-dark" class="header-output fixed-header">
        <div class="container-fluid header-in">
            

            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <a id="logo" href="/" style="width: 300px;margin-top:10px;"  class="d-inline-block"><img src="{{asset('user/assets/img/logo.svg')}}" alt=""></a>
                    <a class="mobile-toggle padding-13px background-main-color" href="javascript:void(0);"><i class="fas fa-bars"></i></a>
                </div>
                <div class="col-lg-5 col-md-12 position-inherit">
                    <ul id="menu-main" class="nav-menu float-lg-right link-padding-tb-20px">

                    
                    <li ><a href="/">{{__('Home')}}</a>
                           
                        </li>
                        @if(Auth::check())
                    <li><a href="{{route('profile.index')}}">{{__('myaccount')}}</a></li>
                        @endif
                        
                        <li><a href="{{route('users.termsandconditions')}}">{{__('Terms')}}</a></li>

                     
                        <li><a href="{{route('users.contactus')}}">{{__('contactus')}}</a></li>

                        <li><a href="{{route('users.language')}}">{{app()->getLocale() == "en" ? "العربية" :"English"}}</a></li>


                    </ul>
                </div>
                
                <div class="col-lg-4 col-md-12">
                  
                    <hr class="margin-bottom-0px d-block d-sm-none">
                    <div class="row">
<div class="col-md-2">
                    @if(Auth::check())
                    
                    <a href="{{route('usernotifications.index')}}" class="notification float-lg-right">
                      
                        <i  class="fas fa-bell"></i>
                        @if(Helper::getUserNotificationsNumber() >0)
                        <span class="badge">{{Helper::getUserNotificationsNumber()}}</span>
                        @endif
                      </a>
                      @endif
</div>
<div class="col-md-6">
                    @if(Auth::check())
                    <a href="#" class="btn btn-sm border-radius-30 margin-tb-15px text-white background-second-color  box-shadow float-right padding-lr-20px margin-left-15px">
                        <span id="user-balance">  {{Auth::user()->username}}  /</span> 
                      <span id="user-balance"> {{__('Balance')}} {{Helper::currentUserBalance()}}  </span> 
                   
                    </a>
                   
                    @endif
                </div>
                <div class="col-md-4">
                    @if(!Auth::check())
                    <a href="{{route('user.login')}}" class="fs-16 margin-tb-20px margin-left-25px d-inline-block text-up-small float-left  "><i class="far fa-user"></i>  تسجيل دخول</a>
                    @else
                    <a href="{{route('user.logout')}}" class="fs-16 margin-tb-20px d-inline-block text-up-small float-left float-lg-right"><i class="fas fa-sign-out-alt"></i>  تسجيل خروج</a>
                    @endif
                </div>
                    </div>
                   
                </div>
            </div>
       
        </div>
    </div>
</header>