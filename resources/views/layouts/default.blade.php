<!doctype html>
<html lang="en" dir="ltr">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>

  <title>any-cards</title>
   @include('layouts.includes.head')
</head>
<body class="hold-transition light-skin sidebar-mini theme-primary">
   <header>
      @include('layouts.includes.nav')
   </header>

   
   <div class="wrapper">
           @yield('content')
           @include('layouts.includes.footer')
   </div>
   

</body>
