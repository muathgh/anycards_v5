<!doctype html>
<html lang="en" dir="ltr">

<head>
   <title>any-cards</title>
  
   @include('layouts.includes.head')
</head>
<body class="hold-transition theme-primary bg-img" style="background-image: url('{{asset('images/auth-bg/bg-1.jpg')}}')">

   
   
           @yield('content')
   
   <footer>
      
    <script src="{{asset('js/vendors.min.js')}}"></script>
    <script src="{{asset('assets/icons/feather-icons/feather.min.js')}}"></script>	
   </footer>

</body>
