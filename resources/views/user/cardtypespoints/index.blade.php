@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid overflow-hidden">
        <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
           
            <div class="row margin-tb-45px background-white full-width border-radius">
                <div class="col-md-12">
                    <form id="searchForm">
                    <div class="row">
                      
                        <div class="col-md-4">
                      
                            {!! Form::label('category_id', __('Categories'))!!}
                            <div class="form-group">
                                {!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('provider_id', __('Providers'))!!}
                            <div class="form-group">
                                {!! Form::select('provider_id', [], old('provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}

                            </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                            <div class="form-group">
                                {!! Form::select('sub_provider_id', [], old('sub_provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}

                            </div>
                        </div>
                    </div>
                    <input class="btn btn-success m-b-30" type="button" id="search" value="{{__('Search')}}" id="search" style="margin-top: 30px;" style="color:white;"/>
                </form>
                    <br/>
                   
<table  id="pointsDataTable" class="table  table-bordered table-hover display nowrap m-t-30 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>

        <th>{{__('Title En')}}</th>
        <th>{{__('Title Ar')}}</th>
        <th>{{__('Points')}}</th>
       
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      
            <tr>
            <th>{{__('#')}}</th>
            <th>{{__('Title En')}}</th>
            <th>{{__('Title Ar')}}</th>
            <th>{{__('Points')}}</th>
          
            </tr>
        
    </tfoot>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
   var formData = {
       sub_provider_id:null,
       provider_id:null,
       category_id:null
   }
    $(function(){
        dt();
        $("#category_id").change(function(){
    var categoryId = $(this).val();
    $("#provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_ar+"</option>");
    }
           }
       }
   })
})

$("#provider_id").change(function(){
    var providerId = $(this).val();
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");

   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_ar+"</option>");
    }
           }
       }
   })
})

$("#sub_provider_id").change(function(){
    subProvider = $(this).val();
})


$(document).on('click',"#search",function(){
    formData.sub_provider_id= $("#sub_provider_id").val();
        formData.provider_id= $("#provider_id").val();
        formData.category_id = $("#category_id").val();
        console.log(formData)
        $("#pointsDataTable").DataTable().destroy();
        dt();
})



    })

    

    function dt(){
              // 
              $('#pointsDataTable').DataTable({
            "processing":true,
        "serverSide": true,
        order:[],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'print'
            
        ],
        language: {
            emptyTable:"{{__('EmptyTable')}}",
        search: "{{__('Search')}}",
        "lengthMenu": "{{__('Display')}} _MENU_ {{__('Records per page')}}",
        "info":           "{{__('Showing')}} _START_ {{__('To')}} _END_ {{__('Of')}} _TOTAL_ {{__('Entries')}}",
        buttons: {
            copy: "{{__('Copy')}}",
            csv:"{{__('Csv')}}",
            print:"{{__('Print')}}",
            excel:"{{__('Excel')}}"
        },
        paginate: {
              
                previous:   "{{__('Previous')}}",
                next:       "{{__('Next')}}",
             
            },
    },
        "pageLength":100,
        dom: 'Blfrtip',
        "ajax": {
            "url": "{{route('cardtypespoints.index')}}",
            "type": "GET",
            "data":formData
		
        },
    
        columns:[
            {"data": "#",
    render: function (data, type, row, meta) {
        
        return meta.row + meta.settings._iDisplayStart + 1;
 
    }
},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"points"},


        ]
        });
    }
</script>
@stop