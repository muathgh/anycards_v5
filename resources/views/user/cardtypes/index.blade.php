@extends('layouts.user_layout')
@section('content')
<section class="banner p-t-5 p-b-5 sm-ptb-80px background-overlay"> 

<!-- <section class="banner p-t-60 p-b-5 sm-ptb-80px background-overlay"> -->
    <div class="container z-index-2 position-relative">
        <!-- <div class="title text-center">
            <h1 class="text-title-large text-main-color font-weight-300 margin-top-60px margin-bottom-15px">بطاقاتي لايف</h1>
            <h4 class="font-weight-300 text-main-color text-up-small p-t-25">شراء جميع أنواع البطاقات والخدمات </h4>
        </div> -->
        <div class="row justify-content-center margin-tb-5px">
            {{-- <div class="col-lg-8">
                <div class="listing-search">
                    <form class="row no-gutters">
                         <div class="col-md-3">
                            <div class="categories dropdown">
                                <a class="listing-form d-block text-nowrap" id="categoryDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Category')}}</a>
                                <div class="dropdown-menu" aria-labelledby="categoryDropDown">
                                    @foreach($categories as $category)
                                   
                                <button class="dropdown-item text-up-small categoryItem" data-id="{{$category->id}}" type="button">{{Helper::getTitle($category)}}</button>
                                  
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div id="providers" class="categories dropdown">
                            <a class="listing-form d-block text-nowrap" id="providerDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Provider')}}</a>
                           
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div id="subproviders" class="categories dropdown">
                            <a class="listing-form d-block text-nowrap" id="subProviderDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Sub Provider')}}</a>
                               
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <a id="search" class="listing-bottom background-second-color box-shadow" href="javascript:void(0);">{{__('Search')}}</a>
                        </div>
                    </form>
                </div>
            </div> --}}
        </div>
    
    </div>
</section>
<section class="padding-tb-100px" style="min-height: 900px;" >
    <div class="container">
     


        <div class="row" id="items">

            @foreach($cardTypes as $item)
            @include('user.includes.item')
            @endforeach
          

          
       

        </div>

    </div>
</section>

<script>
  var subProviderId = "{{ Request::get('sub_provider_id') }}"
    function loadMoreData(page){
$.ajax({
    url:"?sub_provider_id="+subProviderId+"&page="+page,
    type:"get",
   
    success:function(response){
       
       
       
        $("#items").append(response.data);
    }
})
    }
    
    var formData = {
        category : null,
        provider:null,
        sub_provider : null
    }
    $(function(){
        
        var page = 1;
$(window).scroll(function(){
    if($(window).scrollTop()+$(window).height() >= $(document).height()){
        page++;
        loadMoreData(page);
    }
})
       
        $('#receipt-popup').on('hidden.bs.modal', function () {

            location.reload();
         
            // getMyBalance()
       

})

        $(document).one('click','.order-now,.img-thumbnail',function(event){
            var id = $(this).data("id");
            
             event.preventDefault();
       
          $(this).prop('disabled', true);
        
            $.ajax({
                url:"{{route('users.showorder')}}",
                data:{id:id},
                dataType:"json",
                type:"GET",
                success:function(response){
                    
                  
                    if(response.status == CONSTANTS.SUCCESS){
                        if(response.message == MESSAGES.NO_CARDS)
                        {
                            toast('Warning',"{{__('No Cards')}}","{{__('No Cards Title')}}")


                        }else if(response.data){
                      
                        $("#order-popup .modal-body").html(response.data);
                      
                        $("#order-popup").modal({
            backdrop: 'static',
            keyboard: false
        });
                        $("#order-popup").modal("toggle");
                       
                        }
                    }
                },
                error:function(response){
                    location.href = "{{route('user.login')}}"
                    
                }
            })
            
        })

$(document).on('click',"#search",function(){
    let isNull = Object.values(formData).every(o => o === null);
   if(!isNull){
       $.ajax({
url : "{{route('users.search')}}",
data:{category:formData.category,provider:formData.provider,sub_provider:formData.sub_provider,"_token": "{{ csrf_token() }}"},
type:"POST",
dataType:"json",
success:function(response){
    if(response.status == CONSTANTS.SUCCESS && response.data){
        $("#items").html(response.data);
    }
}
       })
   }
   

    

    
    
})

        $(".categoryItem").on('click',function(){
            formData.provider= null;
            formData.sub_provider = null;
            $("#categoryDropDown").text($(this).text())
            var id = $(this).data('id');
            formData.category = id;
            $.ajax({
                url:"users/providers/"+id,
                dataType:"json",
                type:"GET",
                success:function(response){
                   
                    $("#providerDropDown").text("{{__('Provider')}}")
                    $("#subProviderDropDown").text("{{__('SubProvider')}}")
                    if(response.status == CONSTANTS.SUCCESS && response.data){
                      
                        $("#providers").html(response.data);

                    }
                    
                }
            })

        })
        $(document).on('click','.providerItem',function(){
            $("#providerDropDown").text($(this).text())
            formData.sub_provider = null;
            var id = $(this).data('id');
            formData.provider = id;
            
            $.ajax({
                url:"users/subproviders/"+id,
                dataType:"json",
                type:"GET",
                success:function(response){
                    $("#subProviderDropDown").text("{{__('SubProvider')}}")
                    if(response.status == CONSTANTS.SUCCESS && response.data){
                      
                        $("#subproviders").html(response.data);

                    }
                    
                }
            })
        })
        $(document).on('click','.subProviderItem',function(){
            $("#subProviderDropDown").text($(this).text())
            var id = $(this).data('id');
            formData.sub_provider = id;

        });
     
    
    })
    
    $('#order-popup').on('hidden.bs.modal', function () {
      location.reload();
});
  
</script>

@stop