@extends('layouts.user_layout')
@section('content')
<div class="container margin-top-100px margin-bottom-100px">
    @if(session()->has('success_message'))
    <div class="alert alert-success">
        {{ session()->get('success_message') }}
    </div>
    @endif
    <div class="row">
        
        <div class="col-lg-6">
           
<form method="POST" id="contact_us_form" action="{{route('users.contactus.ajax')}}"  class="margin-top-90px">
    @csrf
<div class="row">
    <div class="col-md-12">
        <input type="text" name="full_name" class="form-control text-align" placeholder="{{__('Full Name')}}"/>
    </div>
    
    <div class="col-md-12">
        <input type="text" name="email" class="form-control margin-top-30px text-align" placeholder="{{__('Email')}}"/>
    </div>
    <div class="col-md-12">
        <input type="text" name="mobile_number" class="form-control margin-top-30px text-align" placeholder="{{__('Mobile Number')}}"/>
    </div>
    <div class="col-md-12">
<textarea class="form-control margin-top-30px" name="content text-align" rows="8"></textarea>
    </div>
</div>
<div class="col-md-12 m-t-10">
    <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
      
        <div class="col-md-12">
            {!! app('captcha')->display() !!}
            
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong class="error">{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
<div class="col-md-12">
<input type="submit" class="btn btn-primary  margin-top-10px" style="width:100%" value="{{__('Send')}}"/>
</div>
</form>
         
        </div>

        <div class="col-lg-6 margin-top-90px">
            <div class="row">
               
                <div class="col-lg-6 col-md-6 margin-bottom-45px">
                    <div class="background-white text-center padding-30px box-shadow border-radius-10">
                        <i class="icon_mail_alt icon-large text-grey-2"></i>
                        <h6 class="font-weight-300 margin-top-15px">{{__('Email')}}</h6>
                        <h5 class="font-2 ">info@any-cards.com</h5>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 margin-bottom-45px">
                    <div class="background-white text-center padding-30px box-shadow border-radius-10">
                        <i class="icon_map_alt icon-large text-grey-2"></i>
                        <h6 class="font-weight-300 margin-top-15px">{{__('Address')}}</h6>
                        <h5 class="font-2 ">Amman-Jordan</h5>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 sm-mb-30px">
                    <div class="background-white text-center padding-30px box-shadow border-radius-10">
                        <i class="icon_link icon-large text-grey-2"></i>
                        <h6 class="font-weight-300 margin-top-15px">{{__('Website')}}</h6>
                        <h5 class="font-2">www.any-cards.com</h5>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="background-white text-center padding-30px box-shadow border-radius-10">
                        <i class="icon_phone icon-large text-grey-2"></i>
                        <h6 class="font-weight-300 margin-top-15px">{{__('Telphone')}}</h6>
                        <h5 class="font-2">962796384878+</h5>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
</div>
{!! NoCaptcha::renderJs() !!}
<script>
    $(function(){
        $("#contact_us_form").validate({
            rules:{
                "full_name":{
                    required:true
                },
                "email":{
                    required:true
                },
                "mobile_number":{
                    required:true
                },
                "content":{
                    required:true
                },
                messages:{
                    "full_name":{
                        required:"{{__('This field is required')}}"
                    },
                    "email":{
                        required:"{{__('This field is required')}}"
                    },
                    "mobile_number":{
                        required:"{{__('This field is required')}}"
                    },
                    "content":{
                        required:"{{__('This field is required')}}"
                    },
                },
                submitHandler:function(form){
                   $("#contact_us_form").submit();
                }
            }
        })
    })
    </script>

@stop


