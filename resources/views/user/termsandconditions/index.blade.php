@extends('layouts.user_layout')
@section('content')
<style>
    section{
        background-color: #ffffff;
    }
    h3,p,li{
        direction: ltr;
        
    }
    p,li{
        font-size: 20px;
        line-height: 35px;
    }
    .arabic li{
        direction: rtl;
    }
    </style>
<section class="padding-tb-100px section-height" style="min-height: 900px;" >
    <div class="container">
@if(app()->getLocale() == "en")

        <div class="row">
            <div class="col-md-12">
                <h2>Any-Cards application Serves wide variety of Cards types</h2>
                <p>This app is for both retail point of sale and end user. 
                    Our application offers card services which let buying process so esyer. 
                    We are gathering differnet brands from famous cards in one place </p>
                    <ul>
                        <li>The application provides a 24-hour card sales service</li>
                        <li>The Any-Cards application provides you with the following cards:

<ul>
    <li>ITunes cards.</li>
    <li>Google Play.</li>
    <li>Online games like PUBG & FreeFire.</li>
    <li>Communications cards (Zain, Orange, Umniah).</li>
    <li>Playstation Cards with different regions (Saudi, UAE, USA).</li>
    <li>XBOX Cards.</li>
    <li>IPTV Cards (HAHA, MY HD) .</li>
    <li>Amazon Cards.</li>
    <li>Steam cards.</li>
    <li>Facebook cards.. Etc.</li>
</ul>

                        </li>
                        <li>The application of Any-Cards is the main source for Business owners, providing lowest prices for merchants and ease of dealing.</li>
                        <li>Any-Cards is the exclusive agent in Jordan for selling open market cards.</li>
                        <p>We offer direct technical support to traders and individuals to save time and effort by solve any issues may occurs while the cards selling to customers.</p>
                        <p>For Any inquiries 962796384878</p>

                    
                    
                    </ul>


            </div>
        </div>
        @else
        {{-- arabic --}}
        <div class="row" style="text-align: right;">
            <div class="col-md-12">
                <h2>صُمم أني كاردز ليلبي خدمات مُعظم المُستخدمين بمُختلف نشاطهم، سواءاً كانوا تجاراً أو أصحاب المحلات أو حتى المتسخدمين العاديين.</h2>
                <p>نتميز في أني كاردز من خلال تجميع أكبر قدر من أنواع وأصناف البطاقات المتنوعة في مكان واحد بشكل يُسهل عملية الوصول إليها والقيام بعمليات الشراء بكل سهولة مع توفير خدمة الدعم الفني وحل المشكلات التي قد تواجه أي مُستخدم.
                </p>
                    <ul class="arabic">
                        <li>خدمة 24 ساعة لمتابعة أي ملاحظة وحلها على وجه السرعة</li>
                        <li>نوفر في أني كاردز الأصناف التالية:

<ul class="arabic">
    <li>آيتونز</li>
    <li>جوجل بلاي</li>
    <li>بطاقات الألعاب بمختلف أنواعها مثل فري فاير وببجي وغيرها    </li>
    <li>بطاقات الإتصالات (زين، أمنية وأورانج)
    </li>
    <li>بلاي ستيشن بمختلف المتاجر (أمريكي، إماراتي، سعودي، كويتي، بريطاني وغيرها)
    </li>
    <li>إكس بوكس
    </li>
    <li>شاهد VIP
    </li>
    <li>سيرفرات البث المباشر مثل (هاها وماي أتش دي وغيرها)
    </li>
    <li>ستيم</li>
    <li>فيس بوك
    </li>
    <li>نيتفليكس</li>
    <li>والكثير الكثير
    </li>
</ul>

                        </li>
                        <li>يعتبر أني كاردز مصدراً للكثير من أصحاب العمل في توفير خدمة البطاقات المدفوعة مسبقاً، وذلك نظراً للأسعار المنافسة التي نقدمها وسهولة التعامل التي نتميز بها بالإضافة إلى الخيارات الكثيرة التي نوفرها في بيئة وطبيعة العمل.</li>
                        <li>أني كاردز هو الوكيل الحصري لتنصيب نظام الكاش في بيع البطاقات بمختلف أنواعها.
                        </li>
                        <p>نوفر خدمة الدعم الفني للأفراد والتجار لتوفير الوقت والجهد في بيع البطاقات للزبائن.
                        </p>
                        <p>للتواصل والاستفسار : 962796384878</p>

                    
                    
                    </ul>


            </div>
        </div>
        @endif
    </div>
</section>

@stop