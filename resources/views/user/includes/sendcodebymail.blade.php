<div class="container">

        <div class="row">
            <input type="hidden" id="card-type-id" value="{{$cardTypeId}}"/>

            <h2 class="col-md-12 text-center">
                {{__('Email Address')}}
            </h2>
            <input type="text" id="email" class="form-control" placeholder="@example.com"/>
            <p class="error"  style="display:none">{{__('This field is required')}}</p>

    
           
            
        </div>
        <hr class="border-white opacity-2 margin-tb-30px">
        <div class="justify-content-center text-center">
            <button type="button" id="send" onclick="send()" class="btn btn-primary" >{{__('Send')}}</button>
    
        </div>
    
    </div>

    <script>
        function send(){
            $(".error").hide();
            var value = $("#email").val();
            if(value ==null || value.trim() =="")
            {
                $(".error").show();
                return;
            }

         
         var url = "{{route('users.sendcodebyemail')}}"
         var cardTypeId = $("#card-type-id").val()
         var codes = @json($codes);
        
         $.LoadingOverlay("show")
            $.ajax({
                url : url,
           type:"POST",
           dataType:"json",
           data:{codes:codes,card_type_id:cardTypeId,email:value,"_token": "{{ csrf_token() }}"},
           success:function(response){
            $.LoadingOverlay("hide")
            if(response.status == CONSTANTS.SUCCESS){
                toast("Success","{{__('Email Send Successfully')}}","{{__('Email Send Successfully')}}");
                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
           }
            })
        }
        </script>
