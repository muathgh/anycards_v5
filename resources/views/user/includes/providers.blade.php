<a class="listing-form d-block text-nowrap" id="providerDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Provider')}}</a>
<div class="dropdown-menu" aria-labelledby="providerDropDown">
  @foreach($providers as $provider)
                                   
  <button class="dropdown-item text-up-small providerItem" data-id="{{$provider->id}}" type="button">{{Helper::getTitle($provider)}}</button>
    
      @endforeach                            
</div>