
<div class="container">
    <input type="hidden" value="{{$card->id}}"/>
<div style="text-align:center">

    <label class="checkbox">
    <strong> {{__('Print to Default printer')}} </strong>  <input type="checkbox" id="useDefaultPrinter" />
    </label>
    <p>{{__('Or')}}</p>
    <div id="installedPrinters">
    <label for="installedPrinterName">{{__('Select an installed Printer:')}}</label>
        <select class="form-control" name="installedPrinterName" id="installedPrinterName"></select>
    </div>
    <br /><br />
<button type="button" class="btn btn-primary" onclick="doPrinting();">{{__('Print')}}</button>
</div>
</div>
<script src="{{asset('js/cptable.js')}}"></script>
<script src="{{asset('js/cputils.js')}}"></script>
<script src="{{asset('js/zip.js')}}"></script>
<script src="{{asset('js/zip-ext.js')}}"></script>
<script src="{{asset('js/deflate.js')}}"></script>

<script src="{{asset('js/JSPrintManager.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
<script>

    $(function(){
        $("#useDefaultPrinter").attr('checked',localStorage.getItem('is_default_printer')!=undefined)
        $("#installedPrinterName").attr('disabled',localStorage.getItem('is_default_printer')!=undefined)
        $("#useDefaultPrinter").change(function(){
            $('#installedPrinterName').attr('disabled',false);
            deleteValueFromStorage('is_default_printer')
            if($(this).is(":checked"))
            {
            $('#installedPrinterName').attr('disabled',true);
            setValueToStorage('is_default_printer',JSON.stringify(true));
            }
            
        })
    })
    JSPM.JSPrintManager.auto_reconnect = true;
    JSPM.JSPrintManager.start();
    JSPM.JSPrintManager.WS.onStatusChanged = function () {
        if (jspmWSStatus()) {
            //get client installed printers
            JSPM.JSPrintManager.getPrinters().then(function (myPrinters) {
                var options = '';
                for (var i = 0; i < myPrinters.length; i++) {
                    options += '<option>' + myPrinters[i] + '</option>';
                }
                $('#installedPrinterName').html(options);
            });
        }
    };
 
    //Check JSPM WebSocket status
    function jspmWSStatus() {
        
        if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Open)
            return true;
        else if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Closed) {
            alert('JSPrintManager (JSPM) is not installed or not running! Download JSPM Client App from https://neodynamic.com/downloads/jspm');
            return false;
        }
        else if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Blocked) {
            alert('JSPM has blocked this website!');
            return false;
        }
    }

   

  
    function print(o) {
        var template = "{{$card->cardType->subProvider->provider->template}}";
      
        var category = "{{$card->cardType->subProvider->provider->category->title_en}}";
        var provider = "{{$card->cardType->subProvider->provider->title_en}}";
        var subProvider = "{{$card->cardType->subProvider->title_en}}";
        var cardType = "{{$card->cardType->title_en}}";
        if (jspmWSStatus()) {
            //Create a ClientPrintJob
            var cpj = new JSPM.ClientPrintJob();
            //Set Printer type (Refer to the help, there many of them!)
            if ($('#useDefaultPrinter').prop('checked')) {
                
                cpj.clientPrinter = new JSPM.DefaultPrinter();
            } else {
                cpj.clientPrinter = new JSPM.InstalledPrinter($('#installedPrinterName').val());
            }
            //Set content to print...
            //Create ESP/POS commands for sample label
            var esc = '\x1B'; //ESC byte in hex notation
            var newLine = '\x0A'; //LF byte in hex notation
         
            
            var cmds = esc + "@";
            cmds+="TEL : 0797211511"
            cmds+=newLine;
            cmds+="DATE : 12/11/2020 TIME:22:13:11"
            cmds+=newLine;
            cmds+="Serial: {{$card->serial}}";
            cmds+=newLine+newLine;
            cmds += category;
            cmds+='\x1B' + '\x61' + '\x30' // left align
            cmds+=newLine;
            cmds+=provider;
            cmds+=newLine;
            cmds+=subProvider;
            cmds+=newLine
            
            cmds += 'Evoucher '+cardType; //text to print
            cmds += newLine+newLine;
            cmds+='\x1B' + '\x61' + '\x31' // center align
            cmds += esc + '!' + '\x18'; //Character font A selected (ESC ! 0)
            cmds+="PIN"
           
            cmds+=newLine
            cmds += '        {{Helper::hyphenate($card->code)}}       '; 
            cmds += esc + '!' + '\x00';
            cmds+='\x1B' + '\x61' + '\x30' // left align
            cmds += newLine+newLine;
            cmds+="EXP DATE: {{$card->expiry_date}}"
            cmds+=newLine + '\x00';
            cmds+="Cashier ID: {{Auth::check() ? Auth::user()->username : ""}}";


            cmds += newLine+newLine;
            if(template == CONSTANTS.ZAIN_TEMPLATE)
            {
                cmds += esc + '!' + '\x00';
                cmds+="Press *114* 14 digit code# then callbutton";
                cmds+=newLine + newLine +'\x00';
                
            }else if(template == CONSTANTS.UMNIAH_TEMPLATE){
                
                cmds+='\x1B' + '\x4D' + '\x31';
                cmds+="To recharge call 1331 then press 1 and enter the 14 digit recharge code above" + '\x0A'
                cmds+=newLine + newLine;
              
                // cmds+=newLine;
                // cmds+="followed by #"
            }else if(template == "orange_template"){
                cmds += esc + '!' + '\x00';
                // cmds+="Key in *150* recharge code #";
                cmds+="Key in *150* recharge code #";
                cmds+=newLine +'\x00';
                
            }
            cmds += esc + '!' + '\x18'; //Emphasized + Double-height mode selected (ESC ! (16 + 8)) 24 dec => 18 hex

            
 
            cpj.printerCommands = cmds;
            //Send print job to printer!
            cpj.sendToClient();
        }
    }
    
    </script>

