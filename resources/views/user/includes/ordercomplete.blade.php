
       
                <div class="cardbox">
                    <input type="hidden" value="{{$cardTypeId}}" id="card-type-id"/>
                    <div class="row justify-content-center">
                        <div class="col-md-11 col-11">

                            
                                <div class="row text-center">
                                    
                                <img  src="{{asset('user/assets/img/yes.png')}}" class="center-block m-l-r-auto" style="width:100px;height:auto;">
                                    </div>
                              
                                    <div class="row text-right">
                                    <div class="col-sm-6 col-md-8">
                      
                   
                        <p class="fs-18">
                        <span class="font-weight-bold"> {{__('Mobile Number')}}: </span> {{$user->mobile_number}}
                            <br />
                            <span class="font-weight-bold">  {{__('User Name')}}:</span> {{$user->username}}
                            <br />
                          
                        </p>

                                    </div>
                                   
                                    </div>
                                 
                          
                        
                   
                            <br/>
                            <div class="row mt-0">
                                <div class="col-md-12 ">
                                    <h4 class="text-center heading">{{$cardTypeTitle}}</h4>
                                    <h4 class=" text-center text-green fs-25"> {{$totalPrice}} </h4> 
                                </div>
                            </div>
                            <div class="row mt-0">
                                <div class="col-md-12 ">
                                    <p class="text-center sub-heading2 text-green fs-25" style="text-align: right;"> {{__('cutCost')}}   </p> 
                                </div>
                            </div>
                            <div>
                            @foreach($cards as $card)
                            <div class="form-group row mb-3">
                               
                            <div class="col-9 mb-0 px-0 pr-2"> <input class="code" id="code_{{$card['id']}}" data-clipboard-target="#copy" type="text" style="font-size: 35px;background-color:#fff;width: 500px;text-align: center;" name="code" value="{{Helper::hyphenate($card['code'])}}" readonly class="form-control input-box rm-border text-left"> </div>
                            <div class="col-3 px-0"> <button data-id="{{$card['id']}}" onclick="copyFunction(this)" id="copy" style="line-height: 35px;height:100%;margin-right: 30px;width: 100px;"   value="{{__('COPYCODE')}}" class="btn btn-primary btn-block">{{__('COPYCODE')}}</button> </div>
                            @if($card['serial'] != null)

                            <div class="col-4 px-0 text-right pr-2"> <label for="serial">{{__('Serial')}}: {{$card['serial']}}</label></div>
                            @endif
                        </div>
                         @endforeach
                            </div>
                         <div class="panel-footer text-right">
                            <div class="exp">{{__('Expires')}}: {{$cardExpiryDate}}</div>
                            
                          
                        </div> 
                            <hr class="border-white opacity-4 margin-tb-15px">
                            
                            <div class="row text-center">
                                <div class="col-md-12">
                                <span class="print"> <a href="javascript:void(0);" id="print" class="btn btn-link"><i class="fa fa-lg fa-print"></i> {{__('Print Code')}}</a> </span>
                                {{-- <span class="print"> <a href="#" class="btn btn-link"><i class="fa fa-lg fa-mobile"></i> {{__('Send by mobile')}}</a> </span> --}}
                                <span class="print"> <a href="javascript:void(0);" onclick="sendByMailPopup()"  class="btn btn-link"><i class="fa fa-lg fa-envelope"></i> {{__('Send by Email')}}</a> </span>
                            </div> </div>
                         
                        </div>
                    </div>
                </div>
               
                <div id="hidden-div" style="display:none">
                    <table>
                        <thead>
                            <tr>
                                <th class="quantity">Q.</th>
                                <th class="description">Description</th>
                                <th class="price">$$</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="quantity">1.00</td>
                                <td class="description">ARDUINO UNO R3</td>
                                <td class="price">$25.00</td>
                            </tr>
                            <tr>
                                <td class="quantity">2.00</td>
                                <td class="description">JAVASCRIPT BOOK</td>
                                <td class="price">$10.00</td>
                            </tr>
                            <tr>
                                <td class="quantity">1.00</td>
                                <td class="description">STICKER PACK</td>
                                <td class="price">$10.00</td>
                            </tr>
                            <tr>
                                <td class="quantity"></td>
                                <td class="description">TOTAL</td>
                                <td class="price">$55.00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <script src="{{asset('js/zip.js')}}"></script>
<script src="{{asset('js/zip-ext.js')}}"></script>
<script src="{{asset('js/deflate.js')}}"></script>
<script src="{{asset('js/JSPrintManager.js')}}"></script>
<script src="https://cdn.rawgit.com/kjur/jsrsasign/c057d3447b194fa0a3fdcea110579454898e093d/jsrsasign-all-min.js"></script>
<script src="{{asset('js/qz-tray.js')}}"></script>
<script src="{{asset('js/sign-message.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>

<script src="{{asset('js/Impresora.js')}}"></script>  

<script>
    function sendByMailPopup(){
        $("#order-complete-popup").modal('toggle')
         $.LoadingOverlay("show")
         var url = "{{route('users.showsendbymailpopup')}}";
         var codes = [];
         $(".code").each(function(index,cr){

             codes.push($(cr).val())

         })

        var cardTypeId = $("#card-type-id").val();
         
       $.ajax({
           url : url,
           type:"POST",
           dataType:"json",
           data:{codes:JSON.stringify(codes),card_type_id:cardTypeId,"_token": "{{ csrf_token() }}"},
           success:function(response){
            $.LoadingOverlay("hide")
               if(response.status == CONSTANTS.SUCCESS && response.data){
                $("#sendcode-byemail").modal({
            backdrop: 'static',
            keyboard: false
        });
$("#sendcode-byemail .modal-body").html(response.data);
$("#sendcode-byemail").modal('toggle');


               }
           }

       })
    }
    var RUTA_API = "http://localhost:8000"
    function copyFunction(ele) {
        var id = $(ele).data('id');
  /* Get the text field */
  var copyText = document.getElementById("code_"+id);
  

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
}
    </script>




<script>

    
    
    $(function(){

        
        
       
        $(document).on('click','#print',function(){
            var printerName = getValueFromStorage('default-printer');

            if(!printerName){
                toast('Error',"{{__('Select Default Printer')}}","{{__('Select Default Printer')}}")
                return ;
            }
            
$("#order-complete-popup").modal('hide');
var isPrinting = localStorage.getItem('is_printing');

if(isPrinting == "1")
{
    
    var cards = @json($cards);
   for(var card in cards){
    connectAndPrint(printerName,cards[card]);
   }

localStorage.setItem('is_printing',"0")
}


          
        
            })

       

   
    })

        function connectAndPrint(printerName,card){



            let impresora = new Impresora(RUTA_API);
            var template = `${card.template}`;
      
      var category = card.category;
      var provider =card.provider ;
      var subProvider = card.sub_provider;
      var cardType = card.card_type;
      var pinCode = card.code;
      pinCode = pinCode.trim();
      

      impresora.setFontSize(1, 1);
   

    impresora.setAlign("left");
    impresora.write("TEL : "+card.mobile_number+"\n")
    impresora.write("DATE : "+moment().format('DD/MM/yyyy')+" TIME:"+moment().format('HH:ss:mm')+"\n")

    if(card.serial)
    impresora.write("Serial : "+card.serial+"\n")

    if(card.order_number)
    impresora.write("Order Number : "+card.order_number+"\n")

    impresora.setEmphasize(1);
    impresora.write(category+"\n")
    impresora.write(provider+"\n")
    impresora.write('Evoucher '+cardType+"\n")
    impresora.setAlign("center");
    impresora.write('----------------------------\n')
    impresora.write('PIN\n')
    impresora.write(pinCode+'\n')
    impresora.write('----------------------------\n\n')
    impresora.setEmphasize(0);
    impresora.setAlign("left");
    impresora.write("EXP DATE: "+card.expiry_date+"\n")
    impresora.write("Cashier ID: "+card.username+"\n\n")

    if(template){
        impresora.write(template+"\n")
    }

   
        impresora.write("\n\n")
    impresora.cut();
   impresora.end();
  impresora.imprimirEnImpresora(printerName);
  setTimeout(() => {
      location.href="/"
  }, 300);






    }



   

  
</script>

<style>
    @media print and (width: 21cm) and (height: 29.7cm) {
     @page {
        margin: 3cm;
     }
}

/* style sheet for "letter" printing */
@media print and (width: 8.5in) and (height: 11in) {
    @page {
        margin: 1in;
    }
}

/* A4 Landscape*/
@page {
    size: A4 landscape;
    margin: 10%;
}
    </style>