<style>

#quantity{
    width: 45px !important;  
}

.counter {
  width: 45px;  
  border-radius: 0px !important;
  text-align: center;
}
.up_count {
  margin-bottom: 10px;  
  margin-left: -4px;
  border-top-left-radius: 0px;
  border-bottom-left-radius: 0px;
  min-width: 50px !important;
} 
.down_count {
  margin-bottom: 10px;  
  margin-right: -4px;
  border-top-right-radius: 0px;
  border-bottom-right-radius: 0px;
  min-width: 50px !important;
}
#toast-container > div {
width: 600px;
height: 100px;
}
.jq-toast-single {
  font-size: 1000px;
}
.toast-error{

color:#FFFFFF;

font-size: 18px;
}
</style>
<div class="container">
    <input type="hidden" id="cardtype-id" value="{{encrypt($cardType->id)}}"/>
        <div class="row">
           
            <h2 class="col-md-12 text-center">
                {{__('Confirm Order')}}
            </h2>
        <h3 class="m-l-r-auto">{{Helper::getTitle($cardType)}}</h5>
    
            <h5 id="price" class="col-md-12 text-center p-t-30 text-black">
                {{sprintf(__('The Amount'),$price,Helper::getPointsFormat($price))}}
            </h3>

          
           
            <div class='main m-l-r-auto'>
                <div><label> {{__('Quantity')}}</label></div>
                <button class='up_count btn btn-info' title='Up'><i class="fas fa-plus"></i></button>

                <input class='counter' type="text" id="quantity" placeholder="value..." value='1' />    
                <button class='down_count btn btn-info' title='Down'><i class="fas fa-minus"></i></button>
  
            </div>
        
              
               
               <div class="col-md-12"> 
              
                <div class="form-group">
                    
                   
               {{-- <input style="width:100px;"  onkeypress="return false;" min="1"  value="1" type="number" class="form-control m-l-r-auto" id="quantity" placeholder="{{__('Quantity')}}"/>  --}}
                </div>
            </div>
   
            
        </div>
        <hr class="border-white opacity-2 margin-tb-30px">
        <div class="modal-footer justify-content-center">
            <button type="button" id="decline" class="btn btn-danger">{{__('Decline')}}</button>
            <button type="button" id="accept" class="btn btn-primary" data-dismiss="modal">{{__('Accept')}}</button>
    
        </div>
    
    </div>
    
    <script>
    
//        $('#order-popup').on('hidden.bs.modal', function () {
//     location.reload();
// });
$(function(){

    $('.up_count,.down_count').click(function(e){
        var button_classes, value = +$('.counter').val();
        button_classes = $(e.currentTarget).prop('class');        
        if(button_classes.indexOf('up_count') !== -1){
            value = (value) + 1;            
        } else {
            value = (value) - 1;            
        }
        value = value < 1 ? 1 : value;
       
        $('.counter').val(value);
    });  
    $('.counter').click(function(){
        $(this).focus().select();
    });

    $(document).on('click','.up_count,.down_count',function(){
        $.ajax({
            url:"{{route('users.newprice')}}",
            type:"GET",
            data:{id:$("#cardtype-id").val(),quantity:$("#quantity").val()},
            success:function(response){
                if(response.data){
                    var price = response.data.price;
                    var points = price > 10 ? "{{__('Point')}}" :"{{__('Points')}}"
                    var text = "{{__('Will Deduct')}}"
                   
                    $("#price").html(text+" "+price+" "+points);
                }
            }
        })
    })
})

        $(document).on('click','#decline,.close',function(){
            $("#order-popup").modal('toggle');
            location.reload();
        })
        $(document).on('click','#accept',function(){
            var id = $('input[type=hidden]').val();

            
            $.LoadingOverlay('show');
            var isBuying = localStorage.getItem('is_buying')
          
            $.ajax({
                url :"{{route('users.order')}}",
                data:{card_type_id:id,quantity:$("#quantity").val(),"_token": "{{ csrf_token() }}"},
                type:"POST",
                dataType:"json",
                success:function(response){
                    $.LoadingOverlay('hide');
                    if(response.message && response.message ==MESSAGES.QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY){
                        toast("Error","{{__('quantity exceeds')}}","{{__('quantity exceeds title')}}",300)
                        setTimeout(() => {
                            location.reload();
                        }, 2800);
                        return;
                    }
                   else if(response.message && response.message ==MESSAGES.NOT_ENOUGH_PRICE){
    
                        toast("Error","{{__('Not Enough Price')}}","{{__('Not Enough Price Title')}}")
                        setTimeout(() => {
                            location.reload();
                        }, 1200);
                        return;
                    }

                    else if(response.message && response.message ==MESSAGES.QUANTITY_LIMIT){
    
    toast("Error","{{__('Quantity Limit')}}","{{__('Quantity Limit Title')}}")
    setTimeout(() => {
        location.reload();
    }, 2000);
    return;
}
    
                    if(response.status == CONSTANTS.SUCCESS && response.data){
                        $("#order-complete-popup").modal({
                backdrop: 'static',
                keyboard: false
            });
    $("#order-complete-popup .modal-body").html(response.data);
    $("#order-popup").modal('hide');
    $("#order-complete-popup").modal('toggle');
    localStorage.setItem('is_printing',"1");
                        
                    };
                },
                error:function(){
                    $.LoadingOverlay('hide');
                   
                }
            })
           
        
    
        })
      
    </script>