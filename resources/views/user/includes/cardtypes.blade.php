<a class="listing-form d-block text-nowrap" id="cardTypeDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Card Type')}}</a>
<div class="dropdown-menu" aria-labelledby="cardTypeDropDown">
  @foreach($cardTypes as $cardType)
                                   
  <button class="dropdown-item text-up-small cardTypeItem" data-id="{{$cardType->id}}" type="button">{{Helper::getTitle($cardType)}}</button>
    
      @endforeach                            
</div>