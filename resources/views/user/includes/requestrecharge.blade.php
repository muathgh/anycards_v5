{!! Form::open(['route' => 'requestrecharges.store', 'files' => true]) !!}
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            {!! Form::label('amount', __('Amount')) !!}
            <div class="form-group">
                {!! Form::text('amount', old('amount'), ['class' => 'form-control', 'placeholder' => __('Amount')]) !!}
            </div>

        </div>

        <div class="col-md-12 text-center">
            {!! Form::label('type', __('Type of invoice')) !!}
            <div class="form-group">
                {!! Form::select(
    'type',
    [
        'text' => __('Text'),
        'image' => __('Image'),
    ],
    old('type') ?? 'deposit',
    ['class' => 'form-control', 'placeholder' => __('Select')],
) !!}

            </div>

        </div>
        <div style="display:none" id="image-container" class="col-md-12 text-center">
            {!! Form::label('Image', __('Image')) !!}
            <div class="form-group">
                <input name="image" id="image" style="width:100%" type="file" class="file-upload" />
                <p style="display:none;" class="error" id="image-error">{{ __('This field is required') }}</p>
            </div>



        </div>
        <div style="display:none" id="text-container" class="col-md-12">
            {!! Form::label('text', __('Text')) !!}
            <div class="form-group">
                {!! Form::textarea('text', null, ['class' => 'form-control', 'placeholder' => __('Text'), 'cols' => '20', 'rows' => '5']) !!}
                <p style="display:none;" class="error text-center" id="text-error">{{ __('This field is required') }}
                </p>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                {!! Form::submit(__('Submit'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
    </div>
    </form>
    {!! Form::close() !!}

    <script>
        $(function() {

            $("#type").change(function() {
                $("#image-container").hide();
                $("#text-container").hide();
                var type = $(this).val();
                if (type == "image")
                    $("#image-container").show();
                else if (type == "text")
                    $("#text-container").show();
            })
            $("form").validate({
                rules: {
                    "amount": "required",
                    "type": "required"
                },
                messages: {
                    "amount": "{{ __('This field is required') }}",
                    "type": "{{ __('This field is required') }}"
                },
                submitHandler: function() {
                    $('#image-error').hide();
                    $('#text-error').hide();
                    var type = $("#type").val();
                    if (type == "text") {
                        var text = $("#text").val();
                        if (!text) {
                            $('#text-error').show();
                            return false;
                        }
                    } else if (type == "image") {
                        var file = document.getElementById('image');
                        if (!file.files[0]) {
                            $('#image-error').show();
                            return false;
                        }
                    }
                    var formData = new FormData;
                    var files = $('#image')[0].files;
                  
                  formData.append("amount",$("#amount").val())
                  formData.append('type',type);
                  if(files.length)
                  formData.append('image',files[0]);
                  if($("#text").val())
                  formData.append("text",$("#text").val());
                  
                   
                 
                    $.LoadingOverlay('show')
                    $.ajax({
                        url: "{{ route('requestrecharges.store') }}",
                        contentType: false,
                        processData: false,
                        data: formData,
                        type: "POST",
                        dataType: "JSON",
                        success: function(response) {
                            $.LoadingOverlay('hide');

                            toast('Success', response.message, "{{ __('Success') }}")
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        },
                        error: function(response) {
                            $.LoadingOverlay('hide');
                            toast('Error', response.responseJSON.message,
                                "{{ __('There is an error') }}")

                        }
                    })
                    return false;
                }
            })
        })

    </script>
