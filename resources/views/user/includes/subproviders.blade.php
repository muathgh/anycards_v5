<a class="listing-form d-block text-nowrap" id="subProviderDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('Sub Provider')}}</a>
<div class="dropdown-menu" aria-labelledby="subProviderDropDown">
  @foreach($subProviders as $subProvoider)
                                   
  <button class="dropdown-item text-up-small subProviderItem" data-id="{{$subProvoider->id}}" type="button">{{Helper::getTitle($subProvoider)}}</button>
    
      @endforeach                            
</div>