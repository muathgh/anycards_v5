<div class="col-lg-3 col-md-6 sm-mb-45px margin-bottom-30px">
    <div class="background-white full-width thum-hover box-shadow hvr-float wow" data-wow-delay="0.2s">
        <div class="item-thumbnail thum background-white">
        <a href="javascript:Void(0);"><img data-id="{{encrypt($item->id)}}" class="img-thumbnail rounded" style="width: 100%;height:400" src="{{$item->image}}" alt=""></a>
        </div>
        <div class="padding-30px">
        <h5 class="margin-bottom-20px text-center"><a class="text-dark fs-20" href="#">{{Helper::getTitle($item)}}</a></h5>
            <div class="rating clearfix">
            <span class="badge badge-primary">{{Helper::getTitle($item->subProvider->provider)}}</span>

                <!-- <span class="float-left text-grey-2">  </span> -->
                <span class="float-right text-amber fs-20"> {{$item->getPriceOfSale()}}</span>
                
            <span class="float-right text-grey-2 fs-12 m-r-5">{{__('JOD')}}</span>
            </div>
        </div>
        <div class="p-r-30 padding-tb-15px background-light-grey">
            <div class="row no-gutters">
                <div class="col-5 text-right"><a href="javascript:void(0);" class="text-lime fs-12">@if($item->cards()->where('is_used',false)->count()>0)<i class="fa fa-check text-green" aria-hidden="true"></i> <span class="text-green checkItem">{{__('Available')}}<span>@else <span class="text-red checkItem">{{__('UnAvailable')}}<span> @endif</a></div>
            <div class="offset-md-2 col-5"><a href="javascript:void(0)" data-id="{{encrypt($item->id)}}" class="text-lime order-now fs-12"><i class="fas fa-plus-circle"></i> {{__('Order Now')}}</a></div>
               
            </div>
        </div>
    </div>
</div>