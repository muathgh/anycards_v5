
						{{-- <div class="col-12 col-sm-6 col-md-4 image-grid-item  rounded ">
							<div style="background-image: url({{$provider->image}});" class="image-grid-cover img-thumbnail rounded">
								<a href="/subproviders/{{encrypt($provider->id)}}" class="image-grid-clickbox img-thumbnail rounded" ></a>
								<a href="/subproviders/{{encrypt($provider->id)}}" class="cover-wrapper">{{Helper::getTitle($provider)}}</a>
							</div>
                        </div> --}}
                        @if(!$provider->skip_subproviders)
                        <div class="col-md-4">
                            <a href="/subproviders/{{encrypt($provider->id)}}">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-header-grey">
                                       <h1 class="card-heading m-t-5 fs-20 text-center p-r-10">{{Helper::getTitle($provider)}}</h1>
                                       </div>
                                       <div class="card-body">
                                       <img src="{{$provider->image}}" />
                                     </div>
                                     
                                    
                                         
                                </div>
                            </div>
                            </a>
                           </div>
                           @elseif($provider->skip_subproviders && $provider->subProviders()->count()>0)
                           <div class="col-md-4">
                            <a href="{{route('user.cardtypes.index',['sub_provider_id'=>encrypt($provider->subProviders()->first()->id)])}}">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-header-grey">
                                       <h1 class="card-heading m-t-5 fs-20 text-center p-r-10">{{Helper::getTitle($provider->subProviders()->first())}}</h1>
                                       </div>
                                       <div class="card-body">
                                       <img src="{{$provider->subProviders()->first()->image}}" />
                                     </div>
                                     
                                    
                                         
                                </div>
                            </div>
                            </a>
                           </div>
                           @endif

<!-- <div class="col-lg-3 col-md-6 sm-mb-45px margin-bottom-30px">
    <div class="background-white full-width thum-hover box-shadow hvr-float wow" data-wow-delay="0.2s">
        <div class="item-thumbnail thum background-white">
        <a href="/subproviders/{{encrypt($provider->id)}}"><img data-id="{{encrypt($provider->id)}}" class="img-thumbnail rounded" style="width: 100%;height:400" src="{{$provider->image}}" alt=""></a>
        </div>
        <div class="padding-30px">
        <h5 class="margin-bottom-20px text-center"><a class="text-dark" href="#"><span class="badge badge-primary">{{Helper::getTitle($provider)}}</span></a></h5>
            <div class="rating clearfix">
          

       
            </div>
        </div>
        <div class="padding-lr-30px padding-tb-15px background-light-grey">
           
        </div>
    </div>
</div> -->