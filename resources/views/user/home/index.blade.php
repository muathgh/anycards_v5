@extends('layouts.user_layout')
@section('content')

<section class="padding-tb-100px section-height">
    <div class="container">
     


        <div class="row">

            @foreach($providers as $provider)
            @include('user.includes.provider')
            @endforeach
          

          
       

        </div>

    </div>
</section>
@stop