@extends('layouts.user_layout')
@section('content')
<section class="padding-tb-100px section-height" style="min-height: 900px;" >
    <div class="container">
     


        <div class="row">

            @foreach($subProviders as $subProvider)
            @include('user.includes.subprovider')
            @endforeach
          

          
       

        </div>

    </div>
</section>
@stop