@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid overflow-hidden">
        <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
           
            <div class="row margin-tb-45px background-white full-width border-radius" >
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="row">
                          
                            <div class="col-md-4">
                          
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id', [], old('provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id', [], old('sub_provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Card Type'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id', [], old('card_type_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('from_date', __('From Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('from_date', old('expiry_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('From Date')]) !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('to_date', __('To Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                                </div>
                               
                            </div>
                        </div>
                        <input  class="btn btn-success m-b-30" type="button" value="{{__('Search')}}" id="search" style="margin-top: 30px;" style="color:white;"/>
                    </form>

<table  id="priceOfSaleTransactionsDataTable" class="table  table-bordered table-hover display nowrap m-t-30 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>
        <th>{{__('Transaction Number')}}</th>
        <th>{{__('Title En')}}</th>
        <th>{{__('Title Ar')}}</th>
        <th>{{__('Order Date')}}</th>
        <th>{{__('Price Of Sale')}}</th>
        <th>{{__('WholeSale Price')}}</th>
        <th>{{__('Profit')}}</th>
           

        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      
            <tr>
            <th>{{__('#')}}</th>
            <th>{{__('Transaction Number')}}</th>
            <th>{{__('Title En')}}</th>
            <th>{{__('Title Ar')}}</th>
            <th>{{__('Order Date')}}</th>
            <th>{{__('Price Of Sale')}}</th>
            <th>{{__('WholeSale Price')}}</th>
            <th>{{__('Profit')}}</th>
            </tr>
        
    </tfoot>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     var formData = {
        category_id:null,
        provider_id:null,
        sub_provider_id:null,
        card_type_id:null,
        from_date:null,
        to_date:null
    }
    $(function(){
        $("#category_id").change(function(){
    var categoryId = $(this).val();
    $("#provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_ar+"</option>");
    }
           }
       }
   })
})
$("#provider_id").change(function(){
    var providerId = $(this).val();
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");

   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_ar+"</option>");
    }
           }
       }
   })
})
$("#sub_provider_id").change(function(){
    var subProviderId = $(this).val();
    $("#card_type_id").empty().append("<option value=''>{{__('Select')}}</option>");

   $.ajax({
       type:"GET",
       url:"{{route('common.cardtypesbysubprovider')}}",
       data:{sub_provider_id:subProviderId},
       dataType:"json",
       success:function(response){
        $("#card_type_id").empty();
        $("#card_type_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var cardType in response.data) {
      var s = response.data[cardType]
   
       $("#card_type_id").append("<option value="+s.id+">"+s.title_ar+"</option>");
    }
           }
       }
   })
})


    $("#from_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    $("#to_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });

    dt();
    });

    $(document).on("click","#search",function(){
    formData.category_id = $("#category_id").val();
    formData.provider_id = $("#provider_id").val();
    formData.sub_provider_id = $("#sub_provider_id").val();
    formData.card_type_id = $("#card_type_id").val();
    formData.from_date = $("#from_date").val();
    formData.to_date=$("#to_date").val();
    
    $("#priceOfSaleTransactionsDataTable").DataTable().destroy();
    dt();
})

    function dt(){
    $('#priceOfSaleTransactionsDataTable').DataTable({
           "processing":true,
       "serverSide": true,
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       buttons: [
           'copyHtml5',
           'excelHtml5',
           'csvHtml5',
           'print'
           
       ],
       order:[],
       language: {
        emptyTable:"{{__('EmptyTable')}}",
        search: "{{__('Search')}}",
        "lengthMenu": "{{__('Display')}} _MENU_ {{__('Records per page')}}",
        "info":           "{{__('Showing')}} _START_ {{__('To')}} _END_ {{__('Of')}} _TOTAL_ {{__('Entries')}}",
        buttons: {
            copy: "{{__('Copy')}}",
            csv:"{{__('Csv')}}",
            print:"{{__('Print')}}",
            excel:"{{__('Excel')}}"
        },
        paginate: {
              
                previous:   "{{__('Previous')}}",
                next:       "{{__('Next')}}",
             
            },
    },
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":{
           url:"{{route('orders.priceofsaletransactions')}}",
           data:formData
       },
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"transaction_number"},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"order_date"},
{"data":"price_of_sale"},
{"data":"wholeprice"},
{"data":"profit"},


       ],
       "footerCallback": function (row, data, start, end, display) {
          var api = this.api(),
        intVal = function (i) {
            var num = i;
             if( typeof i=== 'string')
             {
                
            num = parseFloat(i)

             }
     return num;
        },
        total2 = api
            .column(5)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(5).footer()).html(
            "<h3>{{__('Total')}} "+toFixed(total2,3)+"</h3>"
            );
            total3 = api
            .column(7)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
    $(api.column(7).footer()).html(
            "<h3>{{__('Total')}} "+toFixed(total3,3)+"</h3>"
            );
            total4 = api
            .column(6)
            .data()
            .reduce(function (a, b) {
                return (intVal(a) + intVal(b));
            }, 0);
  
            $(api.column(6).footer()).html(
            "<h3>{{__('Total')}} "+toFixed(total4,3)+"</h3>"
            );
         
  

},
       });
   }

    </script>
@stop