@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
   
        <div class="container-fluid overflow-hidden">
            <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
               
                <div class="row margin-tb-45px background-white full-width border-radius" >
                    <div class="col-12">
                        
                                <div class="table-responsive">
                                    <table id="notificationsDataTable" class="table no-border">
                                        <thead>
                                            <tr class="text-left">
                                                <th class="p-0" style="width: 50px"></th>
                                                <th class="p-0" style="min-width: 200px"></th>
                                                <th class="p-0" style="min-width: 150px"></th>
                                                <th class="p-0" style="min-width: 100px"></th>
                                                <th class="p-0" style="min-width: 130px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($notifications as $key=>$notification)
                                           
                                        <tr class="cursor-pointer spaceUnder">										
                                                <td>
                                                    <div class="h-50 w-50 l-h-50 rounded text-center">
                                                    {{++$key}}
                                                    </div>
                                                </td>
                                               
                                                <td>
                                                    <a href="javascript:void(0);" class="text-dark font-weight-600 hover-primary font-size-16">{{$notification['title_ar']}}</a>
                                                        <span class="text-fade d-block">{{$notification['description_ar']}}</span>
                                                    </td>
                                          
                                              <td>
                                                <span style="font-size:13px;" class="text-fade d-block">{{date('d-m-Y H:i:s', $notification['created_at']) }}</span>
                                              </td>
                                            </tr>
                                           
                                        
                                         @endforeach
                                     
                                         
                                        </tbody>
                                    </table>
                                </div>
                        
                    </div>
                </div>
      
        </div>
    </div>
</div>
<script>
    $(function(){
        $("#notificationsDataTable").DataTable({
            language: {
            emptyTable:"{{__('EmptyTable')}}",
        search: "{{__('Search')}}",
        "lengthMenu": "{{__('Display')}} _MENU_ {{__('Records per page')}}",
        "info":           "{{__('Showing')}} _START_ {{__('To')}} _END_ {{__('Of')}} _TOTAL_ {{__('Entries')}}",
        buttons: {
            copy: "{{__('Copy')}}",
            csv:"{{__('Csv')}}",
            print:"{{__('Print')}}",
            excel:"{{__('Excel')}}"
        },
        paginate: {
              
                previous:   "{{__('Previous')}}",
                next:       "{{__('Next')}}",
             
            },
    },
    "pageLength":100,
    order:[],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
        });
    })
    </script>
              
@stop

