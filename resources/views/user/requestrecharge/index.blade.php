@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid overflow-hidden">
        <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
           
            <div class="row margin-tb-45px background-white full-width border-radius">
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('from_date', __('From Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('from_date', old('expiry_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('From Date')]) !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('to_date', __('To Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                               
                                </div>
                                <p style="display:none" id="date-required-error" class="error">{{__('From Date And To Date Are Required')}}</p>
                            </div>
                            <div class="col-md-4 text-left">
                                <a class="btn btn-success m-b-30" href="javascript:void(0);" id="create" style="margin-top: 30px;" style="color:white;">{{__('Request Recharge')}}</a>
                            </div>
                        </div>
                        <input  class="btn btn-success m-b-30" type="button" value="{{__('Search')}}" id="search" style="margin-top: 30px;" style="color:white;"/>

                    </form>
                </div>
                <div class="col-md-12">
                
                    <br/>
                   
<table  id="requestsDataTable" class="table  table-bordered table-hover display nowrap m-t-30 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>

        <th>{{__('Amount')}}</th>
        <th>{{__('Status')}}</th>
        <th>{{__('Received Amount')}}</th>
        <th>{{__('Request Date')}}</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      
            <tr>
            <th>{{__('#')}}</th>
            <th>{{__('Amount')}}</th>
        <th>{{__('Status')}}</th>
        <th>{{__('Received Amount')}}</th>
        <th>{{__('Request Date')}}</th>
            </tr>
        
    </tfoot>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var formData = {
    
    from_date:null,
    to_date:null
}
    $(function(){

        $("#from_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    $("#to_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });

dt()
  

$(document).on('click',"#create",function(){

$.ajax({
    url:"{{route('requestrecharges.create')}}",
    type:"GET",
    dataType:"JSON",
    success:function(response){
        if(response.status == CONSTANTS.SUCCESS && response.data){
            $("#request-recharges-popup .modal-body").html(response.data);
$("#request-recharges-popup").modal('toggle');
        }
    }
})

})
    });

    function dt(){
        $('#requestsDataTable').DataTable({
           "processing":true,
       "serverSide": true,
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        intVal = function(i) {
                            var num = i;
                            if (typeof i === 'string') {

                                num = parseFloat(i)

                            }
                            return num;
                        },




                        total = api
                        .column(1)
                        .data()
                        .reduce(function(a, b) {
                            return ((intVal(a) + intVal(b)));
                        }, 0);

                    $(api.column(1).footer()).html(
                        "<h3>{{__('Total')}} " + toFixed(total,3) + "</h3>"
                    );

                    
                    total2 = api
                        .column(3)
                        .data()
                        .reduce(function(a, b) {
                            return ((intVal(a) + intVal(b)));
                        }, 0);

                    $(api.column(3).footer()).html(
                        "<h3>{{__('Total')}} " + toFixed(total2,3) + "</h3>"
                    );
                },
       buttons: [
           'copyHtml5',
           'excelHtml5',
           'csvHtml5',
           'print'
           
       ],
       language: {
        emptyTable:"{{__('EmptyTable')}}",
        search: "{{__('Search')}}",
        "lengthMenu": "{{__('Display')}} _MENU_ {{__('Records per page')}}",
        "info":           "{{__('Showing')}} _START_ {{__('To')}} _END_ {{__('Of')}} _TOTAL_ {{__('Entries')}}",
        buttons: {
            copy: "{{__('Copy')}}",
            csv:"{{__('Csv')}}",
            print:"{{__('Print')}}",
            excel:"{{__('Excel')}}"
        },
        paginate: {
              
                previous:   "{{__('Previous')}}",
                next:       "{{__('Next')}}",
             
            },
    },
       "pageLength":100,
       dom: 'Blfrtip',
     
       "ajax":{
           url:"{{route('requestrecharges.index')}}",
           data:formData
       },
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"amount"},
{"data":"status",render:function(data,type,row){
    
    if(row.status == CONSTANTS.PENDING)
    return '<span class="badge badge-warning">{{__("Pending")}}</span>'
    if(row.status == CONSTANTS.APPROVED)
    return '<span class="badge badge-success">{{__("Approved")}}</span>'

    return '<span class="badge badge-danger">{{__("Rejected")}}</span>'
}},
{"data":"transaction_amount"},
{"data":"created_at"}


       ]
       });
    }

    $(document).on("click","#search",function(e){
        $("#date-required-error").hide();
        e.preventDefault();
        if($("#from_date").val() && !$("#to_date").val())
        {
           
            $("#date-required-error").show();
            return;
        }
        if(!$("#from_date").val() && $("#to_date").val())
        {
            $("#date-required-error").show();
            return;
        }

        formData.from_date = $("#from_date").val();
    formData.to_date=$("#to_date").val();
    $('#requestsDataTable').DataTable().destroy();
    dt();
    })
    </script>
{{-- data table --}}
@stop
