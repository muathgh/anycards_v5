@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid overflow-hidden">
        <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
           
            <div class="row margin-tb-45px background-white full-width border-radius" >
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="row">
                          
                            <div class="col-md-4">
                          
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id', [], old('provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id', [], old('sub_provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('card_type_id', __('Card Type'))!!}
                                <div class="form-group">
                                    {!! Form::select('card_type_id', [], old('card_type_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('from_date', __('From Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('from_date', old('expiry_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('From Date')]) !!}
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('to_date', __('To Date'))!!}
                                <div class="form-group">
                                    {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                                </div>
                               
                            </div>
                        </div>
                        <input  class="btn btn-success m-b-30" type="button" value="{{__('Search')}}" id="search" style="margin-top: 30px;" style="color:white;"/>
                    </form>

    {{-- <h3 class="text-right">{{__("Total Price Of Sale")}} : {{$totalPriceOfSale}}</h3> --}}
<table  id="ordersDataTable" class="table  table-bordered table-hover display nowrap m-t-30 w-p100">
    <thead>
        <tr>
        <th>{{__('#')}}</th>
        <th>{{__('Order Number')}}</th>
        <th>{{__('Title En')}}</th>
        <th>{{__('Title Ar')}}</th>
        <th>{{__('Serial')}}</th>
            <th>{{__('Order Date')}}</th>
           
            <th>{{__('Options')}}</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      
            <tr>
            <th>{{__('#')}}</th>
    <th>{{__('Order Number')}}</th>
                <th>{{__('Title En')}}</th>
                <th>{{__('Title Ar')}}</th>
            <th>{{__('Serial')}}</th>
                <th>{{__('Order Date')}}</th>
               
                <th>{{__('Options')}}</th>
            </tr>
        
    </tfoot>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  
    var formData = {
        category_id:null,
        provider_id:null,
        sub_provider_id:null,
        card_type_id:null,
        from_date:null,
        to_date:null
    }
   $(function(){

    $("#category_id").change(function(){
    var categoryId = $(this).val();
    $("#provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_ar+"</option>");
    }
           }
       }
   })
})

$("#provider_id").change(function(){
    var providerId = $(this).val();
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");

   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_ar+"</option>");
    }
           }
       }
   })
})

$("#sub_provider_id").change(function(){
    var subProviderId = $(this).val();
    $("#card_type_id").empty().append("<option value=''>{{__('Select')}}</option>");

   $.ajax({
       type:"GET",
       url:"{{route('common.cardtypesbysubprovider')}}",
       data:{sub_provider_id:subProviderId},
       dataType:"json",
       success:function(response){
        $("#card_type_id").empty();
        $("#card_type_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var cardType in response.data) {
      var s = response.data[cardType]
   
       $("#card_type_id").append("<option value="+s.id+">"+s.title_ar+"</option>");
    }
           }
       }
   })
})

// 

    
var picker2 = new Pikaday({
        field: document.getElementById('from_date'),
        format: 'DD-MM-yy',
       
    });
    var picker2 = new Pikaday({
        field: document.getElementById('to_date'),
        format: 'DD-MM-yy',
       
    });
 
dt();
   })


$(document).on("click","#search",function(){
    formData.category_id = $("#category_id").val();
    formData.provider_id = $("#provider_id").val();
    formData.sub_provider_id = $("#sub_provider_id").val();
    formData.card_type_id = $("#card_type_id").val();
    formData.from_date = $("#from_date").val();
    formData.to_date=$("#to_date").val();
    
    $("#ordersDataTable").DataTable().destroy();
    dt();
})

   function dt(){
    $('#ordersDataTable').DataTable({
           "processing":true,
       "serverSide": true,
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       buttons: [
           'copyHtml5',
           'excelHtml5',
           'csvHtml5',
           'print'
           
       ],
       order:[],
       language: {
        emptyTable:"{{__('EmptyTable')}}",
        search: "{{__('Search')}}",
        "lengthMenu": "{{__('Display')}} _MENU_ {{__('Records per page')}}",
        "info":           "{{__('Showing')}} _START_ {{__('To')}} _END_ {{__('Of')}} _TOTAL_ {{__('Entries')}}",
        buttons: {
            copy: "{{__('Copy')}}",
            csv:"{{__('Csv')}}",
            print:"{{__('Print')}}",
            excel:"{{__('Excel')}}"
        },
        paginate: {
              
                previous:   "{{__('Previous')}}",
                next:       "{{__('Next')}}",
             
            },
    },
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":{
           url:"{{route('orders.index')}}",
           data:formData
       },
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"transaction_number"},
{"data":"title_en"},
{"data":"title_ar"},
{"data":"serial"},
{"data":"order_date"},


{"data":"options",render:function(data,type,row){
   return "<i class='fas fa-eye view cursor-pointer' data-id="+row.card_transaction_id+"></i>"
}}
       ]
       });
   }

   $(document).on('click', '.view', function(){ 
       var id = $(this).data('id');
       console.log(id);
       var url = "/users/orders/show/"+id;
       $.LoadingOverlay("show")
       $.ajax({
           url : url,
           type:"GET",
           dataType:"json",
           success:function(response){
            $.LoadingOverlay("hide")
               if(response.status == CONSTANTS.SUCCESS && response.data){
                $("#order-view-popup").modal({
            backdrop: 'static',
            keyboard: false
        });
$("#order-view-popup .modal-body").html(response.data);
$("#order-view-popup").modal('toggle');

localStorage.setItem('is_printing',"1")
               }
           }

       })
   })
   

    </script>
    
    @stop