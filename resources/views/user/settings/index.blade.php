@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
   
        <div class="container-fluid overflow-hidden">
            <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
               
                <div class="row margin-tb-45px background-white full-width border-radius" style="min-height: 500px" >
                    <div class="col-12">
                        <form id="defaultPrintersForm" name="defaultPrinterForm" action="">
                        <div class="row">
                        <div class="col-md-4">
<label>{{__('Select Default Printer')}}</label>
<select class="form-control" name="default-printer" id="default-printer">


</select>


                        </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-4">
    <label>{{__('Type Printer Name')}}</label>
    <input type="text" class="form-control"  id="printer-name" />
  
    
    
                            </div>
                            </div>
                            <br/>
                            <p class="error" style="display:none;" id="error">{{__('You should select a printer')}}</p>
                        <div class="row m-t-10">
                          
                            <div class="col-md-12">
                                <input type="button"  value="{{__('Save')}}" id="submit" class="btn btn-primary" style="width:150px;"/>
                            </div>

                            <div class="col-md-12 m-t-20">
                                <input type="button" onclick="printSample()" id="print-sample" value="{{__('Print Test')}}" class="btn btn-info" style="width:150px;"/>
                            </div>
                        </div>
                          
                    </form>
                    </div>
                </div>
      
        </div>
    </div>
</div>
<script src="https://cdn.rawgit.com/kjur/jsrsasign/c057d3447b194fa0a3fdcea110579454898e093d/jsrsasign-all-min.js"></script>
<script src="{{asset('js/qz-tray.js')}}"></script>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script>
var RUTA_API = "http://localhost:8000"

var listPrinters = (defaultPrinter=null) => {
    Impresora.getImpresoras()
        .then(listaDeImpresoras => {
            $("#default-printer").empty();
            $("#default-printer").append("<option value='' selected>{{__('Select')}}</option>")
            listaDeImpresoras.forEach(printer => {
                if(defaultPrinter && defaultPrinter == printer)
              $("#default-printer").append("<option value="+printer+" selected>"+printer+"</option>")
              else
              $("#default-printer").append("<option value="+printer+">"+printer+"</option>")
            })
        });
}

$(document).on("#print-sample","click",function(){
    alert("done");
})
function printSample(){

    
 
            var printer = localStorage.getItem("default-printer");
       
            if(printer){
            var username = "{{Auth::user()->username}}"

            var impresora = new Impresora(RUTA_API);

            impresora.setFontSize(2, 2);
    impresora.setEmphasize(0);
    impresora.setAlign("center");
    impresora.write("Any-Cards\n");
    impresora.write("User :"+username+"\n");
    impresora.write("TEL :07*********\n")
    impresora.write("DATE :"+moment().format('DD/MM/YYYY')+"\n")
    impresora.write("TIME :"+moment().format('HH:mm:ss')+"\n")
     impresora.setFontSize(1, 1);
    impresora.write('----------------------------\n\n')
     impresora.setFontSize(2, 2);
    impresora.write("TEST PAGE\n\n")
    impresora.cut();
  
    impresora.end();
    impresora.imprimirEnImpresora(printer);

            }

    
        }

        function strip(key) {
    if (key.indexOf('-----') !== -1) {
        return key.split('-----')[2].replace(/\r?\n|\r/g, '');
    }
}

    $(function(){

        if(getValueFromStorage("default-printer") == null){
            $("#print-sample").attr('disabled',true);
        }

        var getDefaultPrinter = getValueFromStorage('default-printer')

        listPrinters(getDefaultPrinter)
   $("#printer-name").val(getDefaultPrinter);


        $("#submit").click(function(){
            $("#error").hide();
           
            var printer = $("#default-printer").val()

if((printer == null || printer == "") && ($("#printer-name").val()==null || $("#printer-name").val()=="")){
   
    $("#error").show();
    return;
}
if($("#printer-name").val())
{
    setValueToStorage("default-printer",$("#printer-name").val())
}
else if(printer){
    setValueToStorage("default-printer",printer)
}


location.reload()
        })
        
       
       
    })


    </script>


              
@stop

