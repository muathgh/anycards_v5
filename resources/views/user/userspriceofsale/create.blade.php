@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
    
    <div class="container-fluid overflow-hidden">
        <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
            <div class="row margin-tb-45px background-white full-width border-radius" style="min-height: 500px" >
                <div class="col-12">
                    <form id="usersPriceOfSalesForm" method="POST" name="usersPriceOfSalesForm">
                        <div class="row">
                          
                            <div class="col-md-4">
                          
                                {!! Form::label('category_id', __('Categories'))!!}
                                <div class="form-group">
                                    {!! Form::select('category_id', $categoriesLookup, old('category_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('provider_id', __('Providers'))!!}
                                <div class="form-group">
                                    {!! Form::select('provider_id', [], old('provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('sub_provider_id', __('SubProviders'))!!}
                                <div class="form-group">
                                    {!! Form::select('sub_provider_id', [], old('sub_provider_id'),["class"=>"form-control select2","placeholder"=>__('Select')]) !!}
    
                                </div>
                            </div>
                         
                        </div>

                        <div id="tblContainer" class="container-fluid">

                        </div>

                        <input style="display:none;margin:20px;" onclick="save()" id="submit" type="button" class="btn btn-primary" value="{{__('Save')}}"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        $(function(){
        $("#category_id").change(function(){
    var categoryId = $(this).val();
    $("#provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");
   $.ajax({
       type:"GET",
       url:"{{route('common.providersbycategory')}}",
       data:{category_id:categoryId},
       dataType:"json",
       success:function(response){
        $("#provider_id").empty();
        $("#provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var provider in response.data) {
      var p = response.data[provider]

       $("#provider_id").append("<option value="+p.id+">"+p.title_ar+"</option>");
    }
           }
       }
   })
})

$("#category_id").change(function(){
    $("#tblContainer").html("");
    var categoryId = $("#category_id").val();
    if(categoryId)
    generatePriceOfSaleTable("category_id",categoryId);

})

$("#provider_id").change(function(){
    var providerId = $(this).val();
    $("#sub_provider_id").empty().append("<option value=''>{{__('Select')}}</option>");

   $.ajax({
       type:"GET",
       url:"{{route('common.subprovidersbyprovider')}}",
       data:{provider_id:providerId},
       dataType:"json",
       success:function(response){
        $("#sub_provider_id").empty();
        $("#sub_provider_id").append("<option value=''>{{__('Select')}}</option>");
           if(response.status == CONSTANTS.SUCCESS){
            for (var subProvider in response.data) {
      var s = response.data[subProvider]
   
       $("#sub_provider_id").append("<option value="+s.id+">"+s.title_ar+"</option>");
    }
           }
       }
   })
})

$("#provider_id").change(function(){
    $("#tblContainer").html("");
    var providerId = $("#provider_id").val();
    if(providerId)
    generatePriceOfSaleTable("provider_id",providerId);

})

$("#sub_provider_id").change(function(){
    $("#tblContainer").html("");
    var subProviderId = $("#sub_provider_id").val();
    if(subProviderId)
    generatePriceOfSaleTable("sub_provider_id",subProviderId);

})

        });

        function generatePriceOfSaleTable(key,value){
        $.LoadingOverlay('show')
       $("#submit").hide();

            $.ajax({
                url:"{{route('userspriceofsale.form')}}",
                data:{[key]:value},
                type:"GET",
                dataType:"json",
                success:function(response){
                    $.LoadingOverlay('hide')
                    if(response.status == CONSTANTS.SUCCESS){
                        if(response.data)
                        {
                        $("#tblContainer").html(response.data);
                        $("#submit").show();
                        }
                    }
                }
            })
        
    }

    function save(){

if($(".price_error:visible").length > 0)
{
    $('html, body').animate({
scrollTop: $(".price_error:visible").offset().top -500
}, 10);
return;

}

$.LoadingOverlay('show')
var formData = [];



$("table tbody .price_input input").each(function(index,value){
  var price =   $(value).val()
  var cardTypeId = $(value).data('id');
  formData.push({
      card_type_id:cardTypeId,
      price:price
  });

});

$.ajax({
    url:"{{route('userspriceofsale.store')}}",
    type:"POST",
    dataType:"JSON",
    data:{data:JSON.stringify(formData), "_token": "{{ csrf_token() }}"},
    success:function(response){
        $.LoadingOverlay('hide')
        if(response.status == CONSTANTS.SUCCESS)
        toast("Success","{{__('Saved Successfully')}}","{{__('Saved Successfully')}}")

        setTimeout(() => {
            location.href="{{route('orders.priceofsaletransactions')}}"
        }, 1500);
        
    }
})
}
</script>
@stop