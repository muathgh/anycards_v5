@extends('layouts.user_dashboard_layout')
@section('content')
<script src="{{asset('js/main.js')}}"></script>
<div class="content-wrapper">
   
        <div class="container-fluid overflow-hidden">
            <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">
               
                <div class="row margin-tb-45px background-white full-width border-radius" style="min-height: 500px" >
                    <div class="col-12">
                        
                        <form id="depositBalanceTransactionForm" method="POST" name="depositBalanceTransactionForm" action="{{route('deposittransactions.store')}}">
                            @if(session()->has('success_message'))
                            <div class="alert alert-success">
                                {{ session()->get('success_message') }}
                            </div>
                            @endif
                            @if(session()->has('error_message'))
                            <div class="alert alert-danger">
                                {{ session()->get('error_message') }}
                            </div>
                            @endif
                            @csrf
                            <div class="row">
                        <div class="col-md-4">
                            {!! Form::label('amount', __('Amount'))!!}
                            <div class="form-group">
                                
                                {!! Form::text('amount', old('amount'),['class'=>'form-control numeric','placeholder'=>__('Amount')]) !!}
                                {!! $errors->first('amount', '<p class="error">:message</p>') !!}
                            </div>



                        </div>
                    
                      
                        <div class="col-md-4">
                            {!! Form::label('username', __('User Name'))!!}
                            <div class="form-group">
                                
                                {!! Form::text('username', old('username'),['class'=>'form-control','placeholder'=>__('User Name')]) !!}
                                {!! $errors->first('username', '<p class="error">:message</p>') !!}
                            </div>



                        </div>
                        <div class="col-md-4">
                            
                            <div class="form-group">
                        {!! Form::submit(__('Transfer'),['class'=>"btn btn-primary vertical-center","id"=>"submit"]) !!}
                            </div>    
                    </div>
                            </div>

                      
                      
                          
                          
                                     </form>
                                     <form>  
                                     <div class="row">

                    <div class="col-md-4">
                        {!! Form::label('from_date', __('From Date'))!!}
                        <div class="form-group">
                            {!! Form::text('from_date', old('expiry_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('From Date')]) !!}
                        </div>
                       
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('to_date', __('To Date'))!!}
                        <div class="form-group">
                            {!! Form::text('to_date', old('to_date'),['class'=>'form-control ui-date','autocomplete'=>"off",'placeholder'=>__('To Date')]) !!}
                        </div>
                        <p style="display:none" id="date-required-error" class="error">{{__('From Date And To Date Are Required')}}</p>
                        <p style="display:none" id="date-invalid-error" class="error">{{__('From Date Must Be Less Than To Date')}}</p>

                    </div>
                  

                    <div class="col-md-4">
                        {!! Form::label('type', __('Type'))!!}
                        <div class="form-group">
                            {!! Form::select('type', $typeLookup,old('type'),["class"=>"form-control","placeholder"=>__('Select')]) !!}
                            {!! $errors->first('type', '<p class="error">:message</p>') !!}
                        </div>
                        
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <input type="button" id="search" value="{{__('Search')}}" class="btn btn-info"/>
                        </div>
                    </div>
                  
                   
                    
</div>
                                     </form>
                    <table  id="depositBalanceTransactionDataTable" class="table  table-bordered table-hover display nowrap m-t-30 w-p100">
                        <thead>
                            <tr>
                            <th>{{__('#')}}</th>
                    
                            <th>{{__('User')}}</th>
                            
                            <th>{{__("Amount")}}</th>
                            <th>{{__("Type")}}</th>
                            <th>{{__("Created at")}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                          
                                <tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('User')}}</th>
                                <th>{{__("Amount")}}</th>
                                <th>{{__("Type")}}</th>
                                <th>{{__("Created at")}}</th>
                                </tr>
                            
                        </tfoot>
                    </table>
                    </div>
                </div>
      
        </div>
    </div>
</div>
<script>
      var formData = {
       type:null,
        from_date:null,
        to_date:null
    }
    $(function(){
        $(".numeric").keypress(function (event) {
       
       return isNumber(event, this)
});
        $(document).on('click',"#yes",function(){
            var amount = $("#amount").val();
        var username = $("#username").val();
            $.ajax({
                url:"{{route('deposittransactions.store')}}",
                dataType:"json",
                type:"POST",
            data:{amount:amount,username:username,"_token": "{{ csrf_token() }}"},
            success:function(response){
                $("#popup").modal('toggle');
                toast("Success",'{{__("Transfered Successfully")}}',"{{__('Success')}}");
                setTimeout(() => {
                    location.reload()
                }, 3000);
            },
            error:function(response){
                $("#popup").modal('toggle');
                var resp = response.responseJSON;
                var message = "";
                if(resp.message === "You Must Select Another User")
                message = "{{__('You Must Select Another User')}}"
                else  if(resp.message === "Your balance is less than amount")
                message = "{{__('Your balance is less than amount')}}"
                else if (resp.message ==="User does not exist")
                message = "{{__('User does not exist')}}"
                else if(resp.message ==='Next transaction must be after 5 min')
                message = "{{__('Next transaction must be after 5 min')}}"
                toast("Error",message,"{{__('Faliure')}}");
            }
            })
        })
      $("#depositBalanceTransactionForm").validate({
        rules:{
  "amount":"required",
  "username":"required"
     
        
    },
    messages:{
"amount":{
    "required":"{{__('This field is required')}}"
},
"username":{
    "required":"{{__('This field is required')}}"
}
    },
    submitHandler: function(form) {
        var amount = $("#amount").val();
        var username = $("#username").val();
        $.ajax({
            url:"{{route('deposittransactions.popup')}}",
            type:"POST",
            dataType:"json",
            data:{amount:amount,username:username,"_token": "{{ csrf_token() }}"},
            success:function(response){
                if(response.data){
                var data = response.data
                $("#popup .modal-body").html(data.html);
                $("#popup").modal('toggle');
                }
            }
        })
     return false;
    }
      })
        $("#from_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    $("#to_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });

    $(document).on("click","#search",function(e){
        $("#date-required-error").hide();
        $("#date-invalid-error").hide();
        if($("#from_date").val() && !$("#to_date").val())
        {
            e.preventDefault();
            $("#date-required-error").show();
            return;
        }
        if(!$("#from_date").val() && $("#to_date").val())
        {
            e.preventDefault();
            $("#date-required-error").show();
            return;
        }
      
       var fdate = generateDate($("#from_date").val());
   
       var tdate = generateDate($("#to_date").val());
   
        if(fdate > tdate){
            e.preventDefault();
            $("#date-invalid-error").show();
            return;
        }
     
        formData.from_date = $("#from_date").val();
    formData.to_date=$("#to_date").val();
    formData.type = $("#type").val();
    
    $('#depositBalanceTransactionDataTable').DataTable().destroy();
    dt();
    });
       dt();
    })

    function dt(){
        $('#depositBalanceTransactionDataTable').DataTable({
           "processing":true,
       "serverSide": true,
       order:[],
       "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
       buttons: [
           'copyHtml5',
           'excelHtml5',
           'csvHtml5',
           'print'
           
       ],
       "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        intVal = function(i) {
                            var num = i;
                            if (typeof i === 'string') {

                                num = parseFloat(i)

                            }
                            return num;
                        },




                        total = api
                        .column(2)
                        .data()
                        .reduce(function(a, b) {
                            return ((intVal(a) + intVal(b)));
                        }, 0);

                    $(api.column(2).footer()).html(
                        "<h3>{{__('Total')}} " + toFixed(total,3) + "</h3>"
                    );
                },
       language: {
        emptyTable:"{{__('EmptyTable')}}",
        search: "{{__('Search')}}",
        "lengthMenu": "{{__('Display')}} _MENU_ {{__('Records per page')}}",
        "info":           "{{__('Showing')}} _START_ {{__('To')}} _END_ {{__('Of')}} _TOTAL_ {{__('Entries')}}",
        buttons: {
            copy: "{{__('Copy')}}",
            csv:"{{__('Csv')}}",
            print:"{{__('Print')}}",
            excel:"{{__('Excel')}}"
        },
        paginate: {
              
                previous:   "{{__('Previous')}}",
                next:       "{{__('Next')}}",
             
            },
    },
       "pageLength":100,
       dom: 'Blfrtip',
       "ajax":{
           url:"{{route('deposittransactions.index')}}",
           data:formData
       },
       columns:[
           {"data": "#",
   render: function (data, type, row, meta) {
       
       return meta.row + meta.settings._iDisplayStart + 1;

   }
},
{"data":"user"},

{"data":"amount"},
{"data":"type",render:function(data,type,row){
    return "<span class='badge badge-success'>"+row.type+"</span>"
    
}},
{"data":"created_at"}


       ]
       });
    }
    function isNumber(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (            
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;
        return true;
}
    </script>
@stop