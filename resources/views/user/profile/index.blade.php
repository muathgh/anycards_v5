@extends('layouts.user_dashboard_layout')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid overflow-hidden">
        <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px background-white full-width border-radius">
            <!-- Page Title -->
         
            <!-- // Page Title -->
        <form method="POST" action="{{route('profile.updatepassword')}}">
            @csrf
   
            <div class="row margin-tb-45px full-width">
                <div class="col-md-12">
                    @if (session()->has('success_message'))
                    <div class="alert-success text-right" id="popup_notification">
                    {{ session('success_message') }}
                    </div>
                @endif
                @if (session()->has('warning_message'))
                <div class="alert-warning text-right" id="popup_notification">
                {{ session('warning_message') }}
                </div>
            @endif
                </div>
                <div class="col-md-4">
                    <div class="padding-15px background-white">
                        <a href="#" class="d-block margin-bottom-10px text-center"><img class="image-grid-item rounded" src="{{$user->logo_or_default}}" alt=""></a>
                        {{-- <a href="javascript:void(0);" class="btn btn-sm  text-white background-main-color btn-block">{{__('Logo')}}</a> --}}
                    </div>
                </div>
                <div class="col-md-8">
                  <div class="row">
                      <div class="col-md-6 margin-bottom-20px">
                          <label><i class="far fa-user margin-left-10px"></i> {{__('User Name')}}</label>
                          <input type="text" class="form-control form-control-sm" value="{{$user->username}}" disabled="disabled" placeholder="">
                      </div>
                    
                      <div class="col-md-6 margin-bottom-20px">
                          <label><i class="far fa-envelope-open margin-left-10px"></i> {{__('Email')}}</label>
                          <input type="text" class="form-control form-control-sm" value="{{$user->email}}" disabled="disabled" placeholder="">
                      </div>
                      <div class="col-md-6 margin-bottom-20px">
                          <label><i class="fas fa-mobile-alt margin-left-10px"></i> {{__('Mobile Number')}}</label>
                          <input type="text" class="form-control form-control-sm" value="{{$user->mobile_number}}" disabled="disabled" placeholder="">
                      </div>
                    
                    
                  </div>
                  <hr class="margin-tb-40px">
                  <div class="row">
                    <div class="col-md-6 margin-bottom-20px">
                        <label><i class="fas fa-lock margin-left-10px"></i>{{__('Current Password')}} </label>
                        <input type="password" name="current_password" class="form-control form-control-sm">
                        {!! $errors->first('current_password', '<p class="error">:message</p>') !!}
                        @if(\Session::has('error_message'))
                        <p class="error">{!! \Session::get('error_message') !!}</p>
                        @endif
                    </div>
                    

                    <div class="col-md-6 margin-bottom-20px">
                        <label><i class="fas fa-lock margin-left-10px"></i>{{__('New Password')}} </label>
                        <input type="password" name="new_password" class="form-control form-control-sm">
                        {!! $errors->first('new_password', '<p class="error">:message</p>') !!}
                       
                    </div>
                    


                  </div>
                  <input type="submit" class="btn btn-md padding-lr-25px  text-white background-main-color btn-inline-block" value="{{__('Update Pssword')}}"/>
                   

                </div>
            
            </div>
        </form>

        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="page-login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
</div>

@stop