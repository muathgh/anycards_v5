@extends('layouts.user_layout')

@section('content')	



<div class="container margin-bottom-100px margin-top-100px m-b-250">
	<!--======= log_in_page =======-->
	<div id="log-in" class="site-form log-in-form box-shadow border-radius-10">

		<div class="form-output">
		<form method="POST" action="{{route('user.login')}}">
			@csrf
				<div class="form-group label-floating text-align">
					<label class="control-label">{{__('User Name')}}</label>
					<input name="username" value="{{old('username')}}" class="form-control" placeholder="{{__('User Name')}}" type="text">
					{!! $errors->first('username', '<p class="error">:message</p>') !!}
				</div>
				<div class="form-group label-floating text-align">
					<label class="control-label">{{__('Password')}}</label>
					<input name="password" class="form-control" placeholder="{{__('Password')}}" type="password">
					{!! $errors->first('password', '<p class="error">:message</p>') !!}
				    @if(\Session::has('error_message'))
				    <p class="error">{!! \Session::get('error_message') !!}</p>
					@endif
					@if(\Session::has('inactive_error_login'))
				    <p class="error">{!! \Session::get('inactive_error_login') !!}</p>
					@endif
				</div>

				<div class="remember">
					{{-- <div class="checkbox">
						<label>
						<input name="optionsCheckboxes" type="checkbox">
							{{__('Remember Me')}}
					</label>
					</div> --}}
					<a href="#" class="forgot">{{__('Forgot my Password')}}</a>
				</div>

				{{-- <a href="javascript:void(0);" onclick="submit()" class="btn btn-md btn-primary full-width">{{__('Sign In')}}</a> --}}
<input type="submit" class="btn btn-md btn-primary full-width" value="{{__('Sign In')}}"/>
			



				
			</form>
		</div>
	</div>
	<!--======= // log_in_page =======-->

</div>
<script>
	// function submit(){
		
	// 	$("form").submit();
	// }

	$(function(){
		$('form').keypress(function (e) {
  if (e.which == 13) {
	 
	$("form").submit();
  }
});
	})
	</script>
@stop