@extends('layouts.login_layout')

@section('content')	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">	
			
			<div class="col-12">
				<div class="row justify-content-center no-gutters">
					<div class="col-lg-5 col-md-5 col-12">
						<div class="bg-white rounded30 shadow-lg">
							<div class="content-top-agile p-20 pb-0">
								<img src="{{asset('user/assets/img/logo.svg')}}" style="width: 300px"/>
								<p class="mb-0" style="margin-top:10px">{{__('Sign in to continue to Any Cards')}}.</p>							
							</div>
							<div class="p-40">
                            <form action="{{route('admin.login')}}" method="POST">
                      
                                @csrf
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
											</div>
											<input type="text" name="username" value="{{old('username')}}" class="form-control pl-15 bg-transparent" placeholder="{{__('User Name')}}">
                                         
                                           
                                   
                                        </div>
                                       
                                        {!! $errors->first('username', '<p class="error">:message</p>') !!}
                                        
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
											</div>
											<input type="password" name="password" class="form-control pl-15 bg-transparent" placeholder="{{__('Password')}}">
                                        </div>
										{!! $errors->first('password', '<p class="error">:message</p>') !!}
										@if(\Session::has('error_message'))
										<p class="error">{!! \Session::get('error_message') !!}</p>
										@endif
										
									</div>
									  <div class="row">
										
										<!-- /.col -->
										<!-- <div class="col-6">
										 <div class="fog-pwd text-right">
											<a href="javascript:void(0)" class="hover-warning"><i class="ion ion-locked"></i> {{__('Forgot pwd?')}}</a><br>
										  </div>
										</div> -->
										<!-- /.col -->
										<div class="col-12 text-center">
										  <button type="submit" class="btn btn-primary btn-lg mt-10">{{__('Sign In')}}</button>
										</div>
										<!-- /.col -->
									  </div>
								</form>	
								
							</div>						
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>

@stop