<?php
namespace Ichtrojan\Otp;

use Illuminate\Support\Facades\Facade;
class OtpFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Otp';
    }

}