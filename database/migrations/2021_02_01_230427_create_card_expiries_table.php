<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardExpiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_expiries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('card_type_id')->unsigned();
            $table->integer('value');
            $table->foreign('card_type_id')->references('id')->on('cards_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_expiries');
    }
}
