<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameNameToTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ar');
            $table->string('title_en');
            $table->string('title_ar');
        });
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ar');
            $table->string('title_en');
            $table->string('title_ar');
        });
        Schema::table('sub_providers', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ar');
            $table->string('title_en');
            $table->string('title_ar');
        });
        Schema::table('cards_types', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ar');
            $table->string('title_en');
            $table->string('title_ar');
        });
        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ar');
            $table->string('title_en');
            $table->string('title_ar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
