<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropCardPriceForeignAndAddNewOne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('card_prices', function (Blueprint $table) {
            $table->dropForeign(['card_id']);
            $table->dropColumn('card_id');
        });

        Schema::table('card_prices', function (Blueprint $table) {
        
            $table->bigInteger('card_type_id')->unsigned();
            $table->foreign('card_type_id')->references('id')->on('cards_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('card_prices', function (Blueprint $table) {
            //
        });
    }
}
