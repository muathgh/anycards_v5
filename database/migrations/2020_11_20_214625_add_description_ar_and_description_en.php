<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionArAndDescriptionEn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->text('description_en');
            $table->text('description_ar');
        });
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->text('description_en');
            $table->text('description_ar');
        });
        Schema::table('sub_providers', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->text('description_en');
            $table->text('description_ar');
        });
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->text('description_en');
            $table->text('description_ar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
