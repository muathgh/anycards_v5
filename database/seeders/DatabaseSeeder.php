<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\CategoryModels\CardType;
use App\Models\CategoryModels\Category;
use App\Models\CategoryModels\Provider;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['guard_name' => 'admin', 'name' => 'super admin']);
        $p1 = Permission::create(['guard_name' => 'admin','name' => "dashboard screen"]);
        $p2 = Permission::create(['guard_name' => 'admin','name' => "categories screen"]);
        $p3 = Permission::create(['guard_name' => 'admin','name' => "providers screen"]);
        $p4 = Permission::create(['guard_name' => 'admin','name' => "cards types screen"]);
        $p5 = Permission::create(['guard_name' => 'admin','name' => "cards screen"]);
        $p6 = Permission::create(['guard_name' => 'admin','name' => "packages screen"]);
        $p7 = Permission::create(['guard_name' => 'admin','name' => "pricing lists screen"]);
        $p8 = Permission::create(['guard_name' => 'admin','name' => "price of sale screen"]);
        $p9 = Permission::create(['guard_name' => 'admin','name' => "users screen"]);
        $p10 = Permission::create(['guard_name' => 'admin','name' => "balances screen"]);
        $p11 = Permission::create(['guard_name' => 'admin','name' => "card transactions screen"]);
        $p12 = Permission::create(['guard_name' => 'admin','name' => "reports screen"]);
        $p13 = Permission::create(['guard_name' => 'admin','name' => "settings screen"]);
        $p14 = Permission::create(['guard_name' => 'admin','name' => "card limits screen"]);
        $p15 = Permission::create(['guard_name' => 'admin','name' => "notifications screen"]);
        $p16 = Permission::create(['guard_name' => 'admin','name' => "whole prices screen"]);
        $p17 = Permission::create(['guard_name' => 'admin','name' => "excel screen"]);
        $p18 = Permission::create(['guard_name' => 'admin','name' => "permissions screen"]);
     $p19 = Permission::create(['guard_name' => 'admin','name' => "subproviders screen"]);
     $p20 = Permission::create(['guard_name' => 'admin','name' => "admins screen"]);
     $p21 = Permission::create(['guard_name'=>"admin","name"=>"card expiry screen"]);
       $permeissions = [$p1 , $p2,$p3,$p4,$p5,$p6,$p7,$p8,
       $p9,$p10,$p11,$p12,$p13,$p14,$p15,$p16,$p17,$p18,$p19,$p20,$p21];
        $role->syncPermissions($permeissions);
        $admin = Admin::find(1);
        $admin->assignRole('super admin');
      
    


    
        
    }
}
