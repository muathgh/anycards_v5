

function initializeFileUploads() {
    $('.file-upload').change(function () {
        var file = $(this).val();
        $(this).closest('.input-group').find('.file-upload-text').val(file);
    });
    $('.file-upload-btn').click(function () {
        $(this).find('.file-upload').trigger('click');
    });
    $('.file-upload').click(function (e) {
        e.stopPropagation();
    });
}





// On document load:
$(function() {
    initializeFileUploads();
   
    // $(".ui-date").datepicker({
    //     dateFormat: "dd-m-yy",
    //     changeYear: true
    // });
    // var picker = new Pikaday({
    //     field: document.getElementById('ui-date'),
    //     format: 'DD-MM-yy',
       
    // });
    // var picker2 = new Pikaday({
    //     field: document.getElementById('from_date'),
    //     format: 'DD-MM-yy',
       
    // });
    // var picker2 = new Pikaday({
    //     field: document.getElementById('to_date'),
    //     format: 'DD-MM-yy',
       
    // });
    $("#from_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    $("#to_date").datepicker({
        changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy' ,
   
    });
    $('.select2').select2();
    $(".numeric").keypress(function (event) {
       
        return isNumber(event, this)
});

});

function isNumber(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (            
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;
        return true;
}