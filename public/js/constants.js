var CONSTANTS = {
    ACTIVE:"active",
    INACTIVE:"inactive",
    SUCCESS:"success",
    PENDING:"pending",
    REJECTED:"rejected",
    APPROVED:"approved",
    DEPOSIT:"deposit",
    WITHDRAW:"withdraw",
    CARD_LIMIT:"cards_limit",
    RECHARGE_REQUEST:"recharge_request",
    ZAIN_TEMPLATE:"zain_template",
    UMNIAH_TEMPLATE:"umniah_template",
    ORANGE_TEMPLATE:"orange_template",
    WITHOUT_TEMPLATE:"without_template"
    }

var MESSAGES = {
    NO_CARDS :"NO_CARDS",
    NOT_ENOUGH_PRICE:"NOT_ENOUGH_PRICE",
    QUANTITY_LIMIT:"QUANTITY_LIMIT",
    UNAUTHENTICATED:"Unauthenticated.",
    QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY:"QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY"
}