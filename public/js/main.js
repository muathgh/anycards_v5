

function toFixed(num, fixed) {
    var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
    return num.toString().match(re)[0];
}

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
        rtn = rtn.replace(/\?/g, '');
    }
    return rtn;
}

function setValueToStorage(key, value) {
    localStorage.setItem(key, value);

}

function getValueFromStorage(key) {
    return localStorage.getItem(key);
}
function deleteValueFromStorage(key) {
    localStorage.removeItem(key);
}

function toast(type,title,message,duration = 100){
    var options = {showDuration: duration,
        preventDuplicates:true
    }
  
    if(type =="Success"){
        toastr.success(title,message,options);
    }
   else if(type == "Warning")
    {
    toastr.warning(title,message,options)
    }
    else if(type=="Error"){
    toastr.error(title,message,options);
    }


}

function isNumber(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (            
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;
        return true;
}

function getMyBalance(){
    $.ajax({
        url:"/users/currentbalance",
        type:"GET",
        dataType:"json",
        success:function(response){
            $("#user-balance").text(response.data);
        }
    })
}

function formatDate(date)
{
  
 
   return moment(date).format('DD/MM/YYYY');
}

function PrintElem() {
    var mywindow = window.open('', 'PRINT');
    mywindow.document.write('<html><head><title>Payment Slip</title>');
    mywindow.document.write('</head><body style="text-align:center;font: Georgia, "Times New Roman", Times, serif;background: #fff;font-size: 22pt;margin:20px auto auto 50px;" >');
    mywindow.document.write('<header style="text-align:center; white-space:nowrap;overflow:hidden;line-height: 1em;">' +
    '<p  style="font-size:16pt;white-space:nowrap;overflow:hidden;line-height: 12pt;">Payment Slip</p>' +
    '<p style="font-size:16pt;white-space:nowrap;overflow:hidden;line-height: 1em;">' + $('.mylabelpaymentheader').html() + '</p>' +
'</header>');
    mywindow.document.write('<content style="text-align:center;">' +
    '<table style="margin-left: auto;margin-right: auto;border-collapse: collapse;font-size:16pt;">' +
        '<tr  style="border:1px solid black"><td  style="border:1px solid black">Name:</td><td  style="border:1px solid black">' + $('.lblName').html() + '</td></tr>' +
        '<tr style="border:1px solid black"><td style="border:1px solid black">Address:</td><td style="border:1px solid black">' + $('.lblAddress').html() + '</td></tr>' +
        '<tr  style="border:1px solid black"><td  style="border:1px solid black">Meter No:</td><td  style="border:1px solid black">' + $('.lblMeterNo').html() + '</td></tr>' +
        '<tr  style="border:1px solid black"><td  style="border:1px solid black">Token:</td><td  style="border:1px solid black">' + $('.lblToken').html() + '</td></tr>' +
    '</table>'+

'</content>' +
'<footer>' +
'<hr style="margin-top:30pt;margin-bottom:30pt;">' +
    '<p style="text-align:right;">&copy pdb</p>' +
'</footer>'+
'');
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
}

function generateDate(dateObject) {
    var d = dateObject.split('-');

   return new Date(d[2],d[1],d[0]);
};

function trimStringAr(max,value){
   return value.length > max ? "..."+value.substring(0,max)  : value;
    
}
function trimStringEn(max,value){
    return value.length > max ? value.substring(0,max) +"..." : value;
}