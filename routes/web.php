<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// V_5 UPDATES 28/12/2021

Route::get('/', function () {
    //   Log::channel('mysql')->error('Something happened!');
    // return ResponseWrapper::success();
    return redirect('/login/admin');
    // return view('welcome');
});

Route::get('/login/admin', 'App\Http\Controllers\AuthControllers\LoginController@showAdminLoginForm')->middleware(['guest:admin', 'admin.language']);
Route::get('/login/user', 'App\Http\Controllers\AuthControllers\LoginController@showUserLoginForm')->middleware(['guest', 'user.language']);
Route::post('/login/admin', 'App\Http\Controllers\AuthControllers\LoginController@adminLogin')->name('admin.login')->middleware('admin.language');
Route::post('/login/user', 'App\Http\Controllers\AuthControllers\LoginController@userLogin')->name('user.login')->middleware(['user.language', "first_time_login"]);
Route::get('/logout/admin', 'App\Http\Controllers\AuthControllers\LoginController@adminLogout')->name('admin.logout');
Route::get('/logout/user', 'App\Http\Controllers\AuthControllers\LoginController@userLogout')->name('user.logout');
Route::get('/common/getPrivdersByCategory', 'App\Http\Controllers\AdminControllers\GlobalControllers\CommonController@getProvidersByCategoryId')->name('common.providersbycategory');
Route::get('/common/getSubProvidersByProvider', 'App\Http\Controllers\AdminControllers\GlobalControllers\CommonController@getSubProvidersByProviderId')->name('common.subprovidersbyprovider');
Route::get('/common/getCardTypesBySubProvider', 'App\Http\Controllers\AdminControllers\GlobalControllers\CommonController@getCardTypeBySubProviderId')->name('common.cardtypesbysubprovider');
Route::get('/common/getAdminRole/{id}', 'App\Http\Controllers\AdminControllers\GlobalControllers\CommonController@getAdminRole')->name('common.adminRole');
Route::get('/common/getAdminSubPermissions/{id}', 'App\Http\Controllers\AdminControllers\GlobalControllers\CommonController@getAdminSubPermissions')->name('common.adminpermission');

Route::get('/common/getIncludedPackages', 'App\Http\Controllers\AdminControllers\GlobalControllers\CommonController@getIncludedPackages')->name('common.includedpackages');
Route::group(['namespace' => 'App\Http\Controllers\AdminControllers', 'prefix' => 'admin', 'middleware' => ['auth:admin', 'admin.language']], function () {
    Route::resource('/', 'AdminController');
    Route::get('/getstatisticsdata', 'AdminController@getStatisticsData')->name('dashboard.getstatisticsdata');
    //categories
    Route::get('/categories', 'CategoriesControllers\CategoryController@index')->middleware(["permission:categories screen"])->name('categories.index');
    Route::get('/categories/create', 'CategoriesControllers\CategoryController@create')->middleware(["permission:categories screen"])->name('categories.create');
    Route::post('/categories/store', 'CategoriesControllers\CategoryController@store')->middleware(["permission:categories screen"])->name('categories.store');
    Route::get('/categories/edit/{id}', 'CategoriesControllers\CategoryController@edit')->middleware(["permission:categories screen"]);
    Route::post('/categories/update', 'CategoriesControllers\CategoryController@update')->middleware(["permission:categories screen"])->name('categories.update');
    Route::get('categories/destroy', 'CategoriesControllers\CategoryController@destroy')->middleware(["permission:categories screen"]);
    Route::get('/categories/data', 'CategoriesControllers\CategoryController@getData')->middleware(["permission:categories screen"]);


    // providers
    Route::get('/providers', 'CategoriesControllers\ProviderController@index')->middleware(["permission:providers screen"])->name('providers.index');
    Route::get('/providers/data', 'CategoriesControllers\ProviderController@getData')->middleware(["permission:providers screen"]);
    Route::get('/providers/create', 'CategoriesControllers\ProviderController@create')->middleware(["permission:providers screen"])->name('providers.create');
    Route::post('/providers/store', 'CategoriesControllers\ProviderController@store')->middleware(["permission:providers screen"])->name('providers.store');
    Route::get('/providers/edit/{id}', 'CategoriesControllers\ProviderController@edit')->middleware(["permission:providers screen"]);
    Route::get('/providers/destroy', 'CategoriesControllers\ProviderController@destroy')->middleware(["permission:providers screen"]);
    Route::post('/providers/update', 'CategoriesControllers\ProviderController@update')->middleware(["permission:providers screen"])->name('providers.update');

    // subProviders
    Route::get('/subProviders', 'CategoriesControllers\SubProviderController@index')->middleware(["permission:subproviders screen"])->name('subProviders.index');
    Route::get('/subProviders/data', 'CategoriesControllers\SubProviderController@getData')->middleware(["permission:subproviders screen"]);
    Route::get('/subProviders/create', 'CategoriesControllers\SubProviderController@create')->middleware(["permission:subproviders screen"])->name('subProviders.create');
    Route::post('/subProviders/store', 'CategoriesControllers\SubProviderController@store')->middleware(["permission:subproviders screen"])->name('subProviders.store');
    Route::get('/subProviders/edit/{id}', 'CategoriesControllers\SubProviderController@edit')->middleware(["permission:subproviders screen"]);
    Route::get('/subProviders/destroy/{id}', 'CategoriesControllers\SubProviderController@destroy')->middleware(["permission:subproviders screen"]);
    Route::post('/subProviders/update', 'CategoriesControllers\SubProviderController@update')->middleware(["permission:subproviders screen"])->name('subProviders.update');

    // cardsTypes
    Route::get('/cardsTypes', 'CategoriesControllers\CardTypeController@index')->middleware(["permission:cards types screen"])->name('cardsTypes.index');
    Route::get('/cardsTypes/data', 'CategoriesControllers\CardTypeController@getData')->middleware(["permission:cards types screen"])->name('cardsTypes.data');
    Route::get('/cardsTypes/create', 'CategoriesControllers\CardTypeController@create')->middleware(["permission:cards types screen"])->name('cardsTypes.create');
    Route::post('/cardsTypes/store', 'CategoriesControllers\CardTypeController@store')->middleware(["permission:cards types screen"])->name('cardsTypes.store');
    Route::get('/cardsTypes/edit/{id}', 'CategoriesControllers\CardTypeController@edit')->middleware(["permission:cards types screen"]);
    Route::get('/cardsTypes/destroy', 'CategoriesControllers\CardTypeController@destroy')->middleware(["permission:cards types screen"]);
    Route::post('/cardsTypes/update', 'CategoriesControllers\CardTypeController@update')->middleware(["permission:cards types screen"])->name('cardsTypes.update');


    //balance limit
    Route::get('/balances/balancelimit','SettingsControllers\BalanceLimitController@create')->middleware('is_super_admin')->name('balancelimit.create');
    Route::post('/balances/balancelimit/store','SettingsControllers\BalanceLimitController@store')->middleware('is_super_admin')->name('balancelimit.store');
    Route::get('/balances/balancelimit/amount','SettingsControllers\BalanceLimitController@getAmount')->name('balancelimit.amount');
    Route::get('/balances/balancelimit/transactions','SettingsControllers\BalanceLimitController@transactions')->middleware('is_super_admin');
    Route::get('/balances/balancelimit/mytransactions','SettingsControllers\BalanceLimitReportController@index')->middleware(["permission:balances screen"])->name('balancelimit.mytransactions');
    Route::post('/balances/balancelimit/note','SettingsControllers\BalanceLimitController@storeNote')->name('balancelimit.storenote');
    Route::get('/balances/balancelimit/note','SettingsControllers\BalanceLimitController@notePopup')->name('balancelimit.notepopup');
   
    // cards
    Route::get('/cards', 'CardsControllers\CardController@index')->middleware(["permission:cards screen"])->name('cards.index');
    Route::get('/cards/data', 'CardsControllers\CardController@getData')->middleware(["permission:cards screen"])->name('cards.data');
    Route::get('/cards/create', 'CardsControllers\CardController@create')->middleware(["permission:cards screen"])->name('cards.create');
    Route::post('/cards/store', 'CardsControllers\CardController@store')->middleware(["permission:cards screen"])->name('cards.store');
    Route::get('/cards/edit/{id}', 'CardsControllers\CardController@edit')->middleware(["permission:cards screen"]);
    Route::get('/cards/destroy', 'CardsControllers\CardController@destroy')->middleware(["permission:cards screen"]);
    Route::post('/cards/update', 'CardsControllers\CardController@update')->middleware(["permission:cards screen"])->name('cards.update');
Route::post('/cards/remove-filters','CardsControllers\CardController@removeFilters')->middleware(["permission:cards screen"])->name('cards.remove-filters');

    // users
    Route::get('/users', 'UsersControllers\UserController@index')->middleware(["permission:users screen"])->name('users.index');
    Route::get('/users/data', 'UsersControllers\UserController@getData')->middleware(["permission:users screen"]);
    Route::get('/users/create', 'UsersControllers\UserController@create')->middleware(["permission:users screen"])->name('users.create');
    Route::post('/users/store', 'UsersControllers\UserController@store')->middleware(["permission:users screen"])->name('users.store');
    Route::get('/users/edit/{id}', 'UsersControllers\UserController@edit')->middleware(["permission:users screen"]);
    Route::get('/users/destroy', 'UsersControllers\UserController@destroy')->middleware(["permission:users screen"]);
    Route::post('/users/update', 'UsersControllers\UserController@update')->middleware(["permission:users screen"])->name('users.update');

    //balances
    Route::get('/balances', 'TransactionsController\BalanceController@index')->middleware(["permission:balances screen"])->name('balances.index');
    Route::get('/balances/data', 'TransactionsController\BalanceController@getData')->middleware(["permission:balances screen"]);
    Route::get('/balances/create', 'TransactionsController\BalanceController@create')->middleware(["permission:balances screen"])->name('balances.create');
    Route::post('/balances/store', 'TransactionsController\BalanceController@store')->middleware(["permission:balances screen"])->name('balances.store');
    Route::get('/balances/transactions/{id}', 'TransactionsController\BalanceController@transactions')->middleware(["permission:balances screen"])->name('balances.transactions');
    Route::get('/balances/transactionData', 'TransactionsController\BalanceController@getTransactionData')->middleware(["permission:balances screen"]);


    //cardtransactions
    Route::get('/cardstransactions', 'TransactionsController\CardTransactionController@index')->middleware(["permission:card transactions screen"])->name('cardstransactions.index');

    //packages
    Route::get('/packages', 'PricingListsControllers\PackageController@index')->middleware(["permission:packages screen"])->name('packages.index');
    Route::get('/packages/create', 'PricingListsControllers\PackageController@create')->middleware(["permission:packages screen"])->name('packages.create');
    Route::post('/packages/store', 'PricingListsControllers\PackageController@store')->middleware(["permission:packages screen"])->name('packages.store');
    Route::get('/packages/edit/{id}', 'PricingListsControllers\PackageController@edit')->middleware(["permission:packages screen"]);
    Route::post('/packages/update', 'PricingListsControllers\PackageController@update')->middleware(["permission:packages screen"])->name('packages.update');
    Route::get('/packages/destroy', 'PricingListsControllers\PackageController@destroy')->middleware(["permission:packages screen"]);

    //pricinglist
    Route::get('/pricinglists', 'PricingListsControllers\PricingListController@index')->middleware(["permission:pricing lists screen"])->name('pricinglists.index');
    Route::get('/pricinglists/create', 'PricingListsControllers\PricingListController@create')->middleware(["permission:pricing lists screen"])->name('pricinglists.create');
    Route::post('/pricinglists/store', 'PricingListsControllers\PricingListController@store')->middleware(["permission:pricing lists screen"])->name('pricinglists.store');
    Route::get('/pricinglists/edit/{id}', 'PricingListsControllers\PricingListController@edit')->middleware(["permission:pricing lists screen"]);
    Route::post('/pricinglists/update', 'PricingListsControllers\PricingListController@update')->middleware(["permission:pricing lists screen"])->name('pricinglists.update');
    Route::get('/pricinglists/destroy', 'PricingListsControllers\PricingListController@destroy')->middleware(["permission:pricing lists screen"]);
    Route::get('/pricinglists/form', 'PricingListsControllers\PricingListController@getPricingListsForm')->middleware(["permission:pricing lists screen"])->name('pricinglists.form');
    Route::get('/pricinglists/form-all', 'PricingListsControllers\PricingListController@getPricingListsFormAll')->middleware(["permission:pricing lists screen"])->name('pricinglists.form-all');

    //priceofsale
    Route::get('/priceofsale', 'PricingListsControllers\PriceOfSaleController@index')->middleware(["permission:price of sale screen"])->name('priceofsale.index');
    Route::get('/priceofsale/create', 'PricingListsControllers\PriceOfSaleController@create')->middleware(["permission:price of sale screen"])->name('priceofsale.create');
    Route::post('/priceofsale/store', 'PricingListsControllers\PriceOfSaleController@store')->middleware(["permission:price of sale screen"])->name('priceofsale.store');
    Route::get('/priceofsale/price', 'PricingListsControllers\PriceOfSaleController@getPrice')->middleware(["permission:price of sale screen"])->name('priceofsale.price');
    Route::get('/priceofsale/edit/{id}', 'PricingListsControllers\PriceOfSaleController@edit')->middleware(["permission:price of sale screen"]);
    Route::post('/priceofsale/update', 'PricingListsControllers\PriceOfSaleController@update')->middleware(["permission:price of sale screen"])->name('priceofsale.update');
    Route::get('/priceofsale/destroy', 'PricingListsControllers\PriceOfSaleController@destroy')->middleware(["permission:price of sale screen"]);
    Route::get('/priceofsale/form', 'PricingListsControllers\PriceOfSaleController@getPriceOfSaleForm')->middleware(["permission:price of sale screen"])->name('priceofsale.form');

    //notifications
    Route::get('/notifications/requestRechargeNotifications','SettingsControllers\RequestRechargeNotificationController@index')->name('request-recharge-notification.index');
    Route::get('/notifications/requestRechargeNotifications/clearall', 'SettingsControllers\RequestRechargeNotificationController@clearAll')->middleware(["permission:notifications screen"])->name('request-recharge-notifications.clearall');
    Route::get('/notifications/requestRechargeNotifications/clearnotificationheader','SettingsControllers\RequestRechargeNotificationController@clearHeaderNotification')->name('request-recharge-notifications.clearnotificationheader');

    Route::get('/notifications/requestRechargeNotifications/clearnotification', 'SettingsControllers\RequestRechargeNotificationController@clearNotification')->name('request-recharge-notifications.clearnotification');
   
    Route::get('notifications', 'SettingsControllers\NotificationController@index')->middleware(["permission:notifications screen"])->name('notifications.index');
    Route::get('/notifications/clearall', 'SettingsControllers\NotificationController@clearAll')->middleware(["permission:notifications screen"])->name('notifications.clearall');
    Route::get('/notifications/clearnotification', 'SettingsControllers\NotificationController@clearNotification')->middleware(["permission:notifications screen"])->name('notifications.clearnotification');
    Route::get('/notifications/clearnotificationheader','SettingsControllers\NotificationController@clearHeaderNotification')->middleware(["permission:notifications screen"])->name('notifications.clearnotificationheader');

Route::get('notifications/quantitylimit','SettingsControllers\CardsQuantityLimitController@index')->middleware(["permission:notifications screen"])->name('quantitylimits.index');
Route::get('notifications/quantitylimit/create', 'SettingsControllers\CardsQuantityLimitController@create')->middleware(["permission:notifications screen"])->name('quantitylimits.create');
Route::post('notifications/quantitylimit/store', 'SettingsControllers\CardsQuantityLimitController@store')->middleware(["permission:notifications screen"])->name('quantitylimits.store');
Route::get('notifications/quantitylimit/form', 'SettingsControllers\CardsQuantityLimitController@getQuantityLimitForm')->middleware(["permission:notifications screen"])->name('quantitylimits.form');
Route::get('notifications/quantitylimit/destroy', 'SettingsControllers\CardsQuantityLimitController@destroy')->middleware(["permission:notifications screen"])->name('quantitylimits.destroy');
    

//pushnotifications
    Route::get('/notifications/pushnotifications', 'SettingsControllers\PushNotificationController@index')->middleware(["permission:notifications screen"])->name('pushnotifications.index');
    Route::post('/notifications/pushnotifications/store', 'SettingsControllers\PushNotificationController@store')->middleware(["permission:notifications screen"])->name('pushnotifications.store');



    //scannedCards
    Route::get('/scannedcards', 'SettingsControllers\ScannedCardsController@index')->name('scannedcards.index');
    Route::get('/scannedcards/create', 'SettingsControllers\ScannedCardsController@create')->name('scannedcards.create');
    Route::post('/scannedcards/store', 'SettingsControllers\ScannedCardsController@store')->name('scannedcards.store');
    Route::get('/scannedcards/start', 'SettingsControllers\ScannedCardsController@startScan')->name('scannedcards.start');
    Route::post('/scannedcards/clear', 'SettingsControllers\ScannedCardsController@clearData')->name('scannedcards.clear');
    Route::get('settings', 'SettingsControllers\SettingController@index')->middleware(["permission:settings screen"])->name('settings.index');
   //V_6 UPDATES 04/01/2022
    Route::get('settings/userbalancelimitnotification', 'SettingsControllers\SettingController@userBalanceLimitNotification')->middleware(["permission:settings screen"])->name('settings.userbalancelimitnotification');
//END V_6 UPDATES 04/01/2022
    Route::post('settings/store', 'SettingsControllers\SettingController@store')->middleware(["permission:settings screen"])->name('settings.store');
     //V_6 UPDATES 04/01/2022
    Route::post('settings/userbalancelimit/store', 'SettingsControllers\SettingController@userBalanceLimitStore')->middleware(["permission:settings screen"])->name('settings.userbalancelimit.store');
     //END V_6 UPDATES 04/01/2022
    //cardsLimit
    Route::get('/notifications/cardslimit', 'SettingsControllers\CardsLimitController@index')->middleware(["permission:notifications screen"])->name('cardslimit.index');
    Route::get('/notifications/cardslimit/create', 'SettingsControllers\CardsLimitController@create')->middleware(["permission:notifications screen"])->name('cardslimit.create');
    Route::post('/notifications/cardslimit/store', 'SettingsControllers\CardsLimitController@store')->middleware(["permission:notifications screen"])->middleware(["permission:card limits screen"])->name('cardslimit.store');
    Route::get('/notifications/cardslimit/edit/{id}', 'SettingsControllers\CardsLimitController@edit')->middleware(["permission:notifications screen"]);
    Route::post('/notifications/cardslimit/update', 'SettingsControllers\CardsLimitController@update')->middleware(["permission:notifications screen"])->name('cardslimit.update');
    Route::get('/notifications/cardslimit/destroy', 'SettingsControllers\CardsLimitController@destroy')->middleware(["permission:notifications screen"]);
    Route::get('/notifications/cardslimit/value', 'SettingsControllers\CardsLimitController@getValue')->middleware(["permission:notifications screen"])->name('cardslimit.value');
    Route::get('/notifications/cardslimit/form', 'SettingsControllers\CardsLimitController@getCardsLimitForm')->middleware(["permission:notifications screen"])->name('cardslimit.form');


    // cardexpiry

    Route::get('notifications/cardsexpiry', 'SettingsControllers\CardExpiryController@index')->middleware(["permission:notifications screen"])->name('cardsexpiry.index');
    Route::get('notifications/cardsexpiry/create', 'SettingsControllers\CardExpiryController@create')->middleware(["permission:notifications screen"])->name('cardsexpiry.create');
    Route::post('notifications/cardsexpiry/store', 'SettingsControllers\CardExpiryController@store')->middleware(["permission:notifications screen"])->middleware(["permission:card limits screen"])->name('cardsexpiry.store');
    Route::get('notifications/cardsexpiry/edit/{id}', 'SettingsControllers\CardExpiryController@edit')->middleware(["permission:notifications screen"]);
    Route::post('notifications/cardsexpiry/update', 'SettingsControllers\CardExpiryController@update')->middleware(["permission:notifications screen"])->name('cardsexpiry.update');
    Route::get('notifications/cardsexpiry/destroy', 'SettingsControllers\CardExpiryController@destroy')->middleware(["permission:notifications screen"]);
    Route::get('notifications/cardsexpiry/value', 'SettingsControllers\CardExpiryController@getValue')->middleware(["permission:notifications screen"])->name('cardsexpiry.value');
    Route::get('notifications/cardsexpiry/form', 'SettingsControllers\CardExpiryController@getCardsExpiryForm')->middleware(["permission:notifications screen"])->name('cardsexpiry.form');


    Route::get('/wholeprice/create', 'SettingsControllers\WholesalePriceController@create')->middleware(["permission:whole prices screen"])->name('wholeprice.create');
    Route::get('/wholeprice', 'SettingsControllers\WholesalePriceController@index')->middleware(["permission:whole prices screen"])->name('wholeprice.index');
    Route::post('/wholeprice/store', 'SettingsControllers\WholesalePriceController@store')->middleware(["permission:whole prices screen"])->name('wholeprice.store');
    Route::get('wholeprice/getprice', 'SettingsControllers\WholesalePriceController@getPrice')->middleware(["permission:whole prices screen"])->name('wholeprice.getprice');
    Route::get('/wholeprice/destroy', 'SettingsControllers\WholesalePriceController@destroy')->middleware(["permission:whole prices screen"]);
    Route::get('/wholeprice/form', 'SettingsControllers\WholesalePriceController@getWholePriceForm')->middleware(["permission:whole prices screen"])->name('wholeprice.form');

    Route::get('/reports', 'ReportsControllers\ReportController@index')->middleware(["permission:reports screen"])->name('reports.index');
    Route::get('reports/cardsreports', 'ReportsControllers\ReportController@cardsReport')->middleware(["permission:reports screen"])->name('reports.cardsreports');
    Route::get('reports/usersreports', 'ReportsControllers\ReportController@userReport')->middleware(["permission:reports screen"])->name('reports.userreports');
    Route::get('/reports/balancetransactionsreport', 'ReportsControllers\ReportController@balanceTransactionsReport')->middleware(["permission:reports screen"])->name('reports.balancetransactionsreports');
    Route::post('/receivables', 'ReportsControllers\ReportController@storeReceivables')->middleware(["permission:reports screen"])->name('reports.receivables');
    Route::post('/unreceived', 'ReportsControllers\ReportController@storeUnreceived')->middleware(["permission:reports screen"])->name('reports.unreceived');
    
    
    
    Route::get('/reports/notespopup', 'ReportsControllers\ReportController@notesPopup')->name('reports.notespopup');
    Route::get('/reports/notespopupusers', 'ReportsControllers\ReportController@notesUserPopup')->name('reports.notes2popup');
    Route::get('/reports/deletenotespopupuser', 'ReportsControllers\ReportController@deletenotesUserPopup')->name('reports.deletenotesuser');
    Route::get('/reports/deletenotespopup', 'ReportsControllers\ReportController@deletenotesPopup')->name('reports.deletenotes');




    Route::post('/reports/deletenotes', 'ReportsControllers\ReportController@deletenotes')->name('reports.deletenotes.post');
    Route::post('/reports/deletenotesuser', 'ReportsControllers\ReportController@deletenotesUser')->name('reports.deletenotes2.post');
    Route::get('/reports/netprofit','ReportsControllers\ReportController@netProfitReport')->middleware(["permission:reports screen"])->name('reports.netprofitreport');
    Route::post('/reports/netprofit','ReportsControllers\ReportController@saveNetProfit')->middleware(["permission:reports screen"])->name('reports.savenetprofit');
    Route::post('/reports/deletenetprofit','ReportsControllers\ReportController@deleteNetProfit')->middleware(["permission:reports screen"])->name('reports.deletenetprofit');
    Route::post('/reports/exportNetProfit','ReportsControllers\ReportController@exportNetProfit')->middleware(["permission:reports screen"])->name('reports.exportnetprofit');
    //    Route::post('/reports/receivables','ReportsControllers\ReportController@storeReceivables')->middleware(["permission:reports screen"])->name('reports.receivables');

// csv
Route::get('/attach/csv/import','SettingsControllers\CsvController@create')->middleware(["permission:excel screen"])->name('csv.import');
Route::post('/attach/csv/store', 'SettingsControllers\CsvController@store')->middleware(["permission:excel screen"])->name('csv.store');

    //excel
    Route::get('/excelgroup','SettingsControllers\ExcelController@index')->middleware(["permission:excel screen"])->name('excelgroup.index');
    Route::get('/excelgroup/import', 'SettingsControllers\ExcelController@excelGroupCreate')->middleware(["permission:excel screen"])->name('excelgroup.import');
    Route::post('/excelgroup/store', 'SettingsControllers\ExcelController@storeGroupExcel')->middleware(["permission:excel screen"])->name('excelgroup.store');
    Route::get('/attach', 'SettingsControllers\ExcelController@index')->middleware(["permission:excel screen"])->name('excel.index');
    Route::get('/attach/excel/import', 'SettingsControllers\ExcelController@create')->middleware(["permission:excel screen"])->name('excel.import');
    Route::post('/attach/excel/store', 'SettingsControllers\ExcelController@store')->middleware(["permission:excel screen"])->name('excel.store');
    Route::post('/attach/excel/remove-filters','SettingsControllers\ExcelController@removeFilters')->middleware(["permission:excel screen"])->name('excel.remove-filter');
    //global

    //permissions
    Route::get('/role', 'PermissionsControllers\RoleController@index')->name('role.index');
    Route::get('/role/create', 'PermissionsControllers\RoleController@create')->middleware("is_super_admin")->name('role.create');
    Route::get('/role/edit/{id}', 'PermissionsControllers\RoleController@edit')->middleware("is_super_admin")->name('role.edit');
    Route::post('/role/store', 'PermissionsControllers\RoleController@store')->middleware("is_super_admin")->name('role.store');
    Route::post('/role/update', 'PermissionsControllers\RoleController@update')->middleware("is_super_admin")->name('role.update');
    Route::get('/role/assign', 'PermissionsControllers\RoleController@assign')->middleware("is_super_admin")->name('role.assign');
    Route::post('/role/assign', 'PermissionsControllers\RoleController@assignPost')->middleware("is_super_admin")->name('role.assign.post');
    Route::post('/role/assign/permissions', 'PermissionsControllers\RoleController@assignPermissions')->middleware("is_super_admin")->name('role.assign.permissions.post');
    Route::get('/role/destroy', 'PermissionsControllers\RoleController@destroy')->middleware("is_super_admin");
    Route::get('/role/permissions', 'PermissionsControllers\RoleController@permissions')->middleware("is_super_admin")->name('role.permissions');
    Route::get('/admins', 'AdminsControllers\AdminController@index')->middleware("is_super_admin")->name('admins.index');
    Route::get('/admins/create', 'AdminsControllers\AdminController@create')->middleware("is_super_admin")->name('admins.create');
    Route::get('/admins/edit/{id}', 'AdminsControllers\AdminController@edit')->middleware("is_super_admin")->name('admins.edit');
    Route::post('/admins/update', 'AdminsControllers\AdminController@update')->middleware("is_super_admin")->name('admins.update');
    Route::post('/admins/store', 'AdminsControllers\AdminController@store')->middleware("is_super_admin")->name('admins.store');
    Route::get('/admins/destroy', 'AdminsControllers\AdminController@destroy')->middleware("is_super_admin");

    // Route::get('settings/plugin64', 'SettingsControllers\SettingController@pluginDownload64')->middleware("is_super_admin")->name('settings.plugin64');
    // Route::get('settings/plugin32', 'SettingsControllers\SettingController@pluginDownload32')->middleware("is_super_admin")->name('settings.plugin32');
Route::get('settings/plugin','SettingsControllers\SettingController@pluginDownload')->middleware("is_super_admin")->name('settings.plugin');
Route::get('settings/application','SettingsControllers\SettingController@applicationDownload')->name('settings.application');

    Route::get('/balances/transferbalance', 'TransactionsController\UserTransferBalanceController@index')->name('transferbalance.index');

    Route::get('/balances/balancesrequests', 'TransactionsController\RequestBalanceController@index')->name('balancesrequests.index');

    Route::post('/balances/balancesrequestspopup','TransactionsController\RequestBalanceController@balancesRequestPopup')->name('balances.balancesrequestspopup');


    Route::get('/users/otps','UsersControllers\OtpsController@index')->middleware(["permission:users screen"])->name('users.otps');
     
Route::get('/officalpricepackages','OfficalPriceControllers\OfficalPricePackagesController@index')->middleware('is_super_admin')->name('officalpricepackages.index');
Route::get('/officalpricepackages/create','OfficalPriceControllers\OfficalPricePackagesController@create')->middleware('is_super_admin')->name('officalpricepackages.create');
Route::post('/officalpricepackages/store','OfficalPriceControllers\OfficalPricePackagesController@store')->middleware('is_super_admin')->name('officalpricepackages.store');
Route::get('/officalpricepackages/edit/{id}','OfficalPriceControllers\OfficalPricePackagesController@edit')->middleware('is_super_admin')->name('officalpricepackages.edit');
Route::post('/officalpricepackages/update','OfficalPriceControllers\OfficalPricePackagesController@update')->middleware('is_super_admin')->name('officalpricepackages.update');
Route::get('/officalpricepackages/destroy', 'OfficalPriceControllers\OfficalPricePackagesController@destroy')->middleware('is_super_admin');


Route::get('/adminofficalprice','OfficalPriceControllers\AdminOfficalPricesController@index')->middleware('is_super_admin')->name('adminofficalprice.index');
Route::get('/adminofficalprice/create','OfficalPriceControllers\AdminOfficalPricesController@create')->middleware('is_super_admin')->name('adminofficalprice.create');
Route::post('/adminofficalprice/store','OfficalPriceControllers\AdminOfficalPricesController@store')->middleware('is_super_admin')->name('adminofficalprice.store');
Route::get('/adminofficalprice/form','OfficalPriceControllers\AdminOfficalPricesController@getOfficalPriceForm')->middleware('is_super_admin')->name('adminofficalprice.form');
Route::get('/adminofficalprice/form-all','OfficalPriceControllers\AdminOfficalPricesController@getOfficalPriceFormAll')->middleware('is_super_admin')->name('adminofficalprice.form-all');
Route::get('/adminofficalprice/discount','OfficalPriceControllers\AdminOfficalPricesController@discount')->middleware('is_super_admin')->name('adminofficalprice.discount');
Route::get('/adminofficalprice/destroy', 'OfficalPriceControllers\AdminOfficalPricesController@destroy')->middleware('is_super_admin');



Route::get('/officalpricediscounts','OfficalPriceControllers\OfficalPriceDiscountController@index')->middleware('is_super_admin')->name('officalpricediscount.index');
Route::get('/officalpricediscounts/create','OfficalPriceControllers\OfficalPriceDiscountController@create')->middleware('is_super_admin')->name('officalpricediscount.create');
Route::get('/officalpricediscounts/edit/{id}','OfficalPriceControllers\OfficalPriceDiscountController@edit')->middleware('is_super_admin')->name('officalpricediscount.edit');
Route::post('/officalpricediscounts/store','OfficalPriceControllers\OfficalPriceDiscountController@store')->middleware('is_super_admin')->name('officalpricediscount.store');
Route::post('/officalpricediscounts/update','OfficalPriceControllers\OfficalPriceDiscountController@update')->middleware('is_super_admin')->name('officalpricediscount.update');
Route::get('/officalpricediscounts/destroy', 'OfficalPriceControllers\OfficalPriceDiscountController@destroy')->middleware('is_super_admin');


});

Route::resource('/', 'App\Http\Controllers\UserControllers\ProviderController')->middleware('user.language');
Route::get('/subproviders/{id}', 'App\Http\Controllers\UserControllers\SubProviderController@index')->middleware('user.language');
Route::get('/cardtypes', 'App\Http\Controllers\UserControllers\UserController@index')->middleware('user.language')->name('user.cardtypes.index');
Route::get("/users/providers/{categoryid}", 'App\Http\Controllers\UserControllers\UserController@getProviderByCategoryId')->middleware('user.language');
Route::get("/users/subproviders/{providerid}", 'App\Http\Controllers\UserControllers\UserController@getSubProviderByProviderId')->middleware('user.language');;
Route::get("/users/cardtypes/{subproviderid}", 'App\Http\Controllers\UserControllers\UserController@getCardTypesBySubProviderId')->middleware('user.language');;
Route::post("/search", 'App\Http\Controllers\UserControllers\UserController@search')->name('users.search')->middleware('user.language');;
Route::view('/users/termsandconditions', 'user.termsandconditions.index')->name('users.termsandconditions')->middleware('user.language');
Route::get('/users/contactus', 'App\Http\Controllers\UserControllers\UserController@contactus')->name('users.contactus')->middleware('user.language');
Route::post('/users/contactus', 'App\Http\Controllers\UserControllers\UserController@contactusAjAX')->name('users.contactus.ajax');
Route::get('/users/language', 'App\Http\Controllers\UserControllers\UserController@language')->name('users.language')->middleware('user.language');
Route::group(['namespace' => 'App\Http\Controllers\UserControllers', 'prefix' => 'users', 'middleware' => ['auth.user', 'web', 'user.language', 'check_web_user_status']], function () {

    Route::get('/currentbalance', 'UserController@getMyBalance');



    Route::get('/order', 'UserController@showOrderPopup')->name('users.showorder');
    Route::post('/order', 'UserController@order')->name('users.order');

    Route::get('/order/newprice', 'UserController@newPrice')->name('users.newprice');
    //profile
    Route::get('/profile', 'ProfileControllers\ProfileController@index')->name('profile.index');
    Route::post('/updatepassword', 'ProfileControllers\ProfileController@updatePassword')->name('profile.updatepassword');

    //orders
    Route::get('/orders', 'OrdersControllers\OrderController@index')->name('orders.index');
    Route::get('/orders/show/{id}', 'OrdersControllers\OrderController@show');
    Route::get('/orders/receipt', 'OrdersControllers\OrderController@showReceipt')->name('orders.receipt');
    Route::get('/orders/priceofsaletransactions', 'OrdersControllers\OrderController@priceOfSaleTransactions')->name('orders.priceofsaletransactions');

    //RequestRecharge
    Route::get('/requestrecharges', 'RequestRechargesControllers\RequestRechargeController@index')->name('requestrecharges.index');
    Route::get('/requestrecharges/create', 'RequestRechargesControllers\RequestRechargeController@create')->name('requestrecharges.create');
    Route::post('/requestrecharges/store', 'RequestRechargesControllers\RequestRechargeController@store')->name('requestrecharges.store');

    //balanceTransactions
    Route::get('/balancetransactions', 'BalanceTransactionsController\BalanceTransactionController@index')->name('balancetransactions.index');

    //depositbalancetransaction
    Route::get('/transferbalance', 'BalanceTransactionsController\TransferBalanceTransactionController@index')->name('deposittransactions.index');
    Route::post('/transferbalance/store', 'BalanceTransactionsController\TransferBalanceTransactionController@store')->name('deposittransactions.store');
    Route::post('/transferbalance/popup', 'BalanceTransactionsController\TransferBalanceTransactionController@showPopup')->name('deposittransactions.popup');


    Route::get('/cardtypespoints', 'CardTypePointsControllers\CardTypePointsController@index')->name('cardtypespoints.index');

    Route::get('/notifications', 'NotificationControllers\NotificationController@index')->name('usernotifications.index');



    //settings
    Route::get('/settings', 'SettingsControllers\SettingsController@index')->name('user.settings.index');

    Route::get('/settings/plugin', 'SettingsControllers\SettingsController@pluginDownload')->name('user.settings.plugin');
    Route::get('/settings/application', 'SettingsControllers\SettingsController@applicationDownload')->name('user.settings.application');
    Route::post('/sendbymail', 'UserController@showSendByMailPopup')->name('users.showsendbymailpopup');
    Route::post('/sendcodebyemail', 'UserController@sendCodeByEmail')->name('users.sendcodebyemail');

    Route::get('/addpriceofsale','UsersPriceOfSaleControllers\UsersPriceOfSaleController@create')->name('userspriceofsale.create');
    Route::post('/addpriceofsale','UsersPriceOfSaleControllers\UsersPriceOfSaleController@store')->name('userspriceofsale.store');
    Route::get('/userspriceofsaleform','UsersPriceOfSaleControllers\UsersPriceOfSaleController@getPriceOfSaleForm')->name('userspriceofsale.form');
});


// END V_5 UPDATES 28/12/2021