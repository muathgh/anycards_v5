<?php

use App\Facades\SmsSenderFacade;
use Ichtrojan\Otp\OtpFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'App\Http\Controllers\ApiControllers'], function () {
    Route::resource('/scanning', 'SettingsControllers\ScanningCardController');
    Route::resource('/login', 'UserControllers\LoginController');
    Route::get('/refreshToken', 'UserControllers\LoginController@refresh');
    Route::get('/appversion', 'SettingsControllers\SettingsController@index');
    Route::post('/validateOtp','UserControllers\LoginController@validateOtp');
    Route::post('/reSendOtp','UserControllers\LoginController@reSendOtp');
});

Route::group(['namespace' => 'App\Http\Controllers\ApiControllers', "middleware" => ["auth:api",'check_mobile_user_status']], function () {

    Route::resource('/account', 'UserControllers\AccountController');
    Route::post('/fullname','UserControllers\AccountController@fullname');
    Route::post('/updateFirstTimeLogin', 'UserControllers\AccountController@updateFirstTimeLogin');
    Route::get('/userNotificationsCount', 'UserControllers\AccountController@userNotificationsCount');
    Route::get('/userNotifications', 'UserControllers\AccountController@userNotifications');
    Route::post('/updateNotificationsToSeen', 'UserControllers\AccountController@updateNotificationsToSeen');
    Route::post('/updateFcmToken', 'UserControllers\AccountController@updateFcmToken');
    Route::resource('/providers', 'CategoriesControllers\ProviderController');
    Route::resource('/categories', 'CategoriesControllers\CategoryController');
    Route::get('/subproviders/{id}', 'CategoriesControllers\SubProviderController@index');
    Route::get('/cardstypes/{id}', 'CategoriesControllers\CardsTypesController@index');
    Route::get('/cardtype/show/{id}', 'CategoriesControllers\CardsTypesController@show');
    Route::resource('/balances', 'TransactionsControllers\BalancesController');
    Route::resource('/transactions', 'TransactionsControllers\TransactionsController');
    Route::resource('/requestbalance', 'TransactionsControllers\RequestBalanceController');
    Route::resource('/transfers', 'TransactionsControllers\TransferBalanceTransactionController');
    Route::post('/getmyorders', 'TransactionsControllers\TransactionsController@getMyOrders');
    Route::get('/getmyorder/{id}', 'TransactionsControllers\TransactionsController@getMyOrder');
    Route::get('/getProviderByCategoryId/{id}', 'CategoriesControllers\ProviderController@getProviderByCategoryId');
    Route::get('/getSubProviderByProviderId/{id}', 'CategoriesControllers\SubProviderController@getSubProviderByProviderId');
    Route::get('/getCardTypesBySubProviderId/{id}', 'CategoriesControllers\CardsTypesController@getCardTypesBySubProviderId');
    Route::post('/sendCodeByEmail', 'TransactionsControllers\TransactionsController@sendCodeByEmail');
    Route::get('/balanceTransaction', "TransactionsControllers\BalancesController@balanceTransaction");
    Route::get('/getCardsTypePoints', 'CategoriesControllers\CardsTypesController@getCardsTypePoints');
    Route::get('/getUserPriceOfSale','CategoriesControllers\CardsTypesController@getUserPriceOfSale');
    Route::post('/updateUserPriceOfSale','CategoriesControllers\CardsTypesController@updateUserPriceOfSale');
    Route::post('/contactus/store', 'ContactUsControllers\ContactUsController@store');
    Route::post('/updateProfile', 'UserControllers\AccountController@updateProfile');
});



