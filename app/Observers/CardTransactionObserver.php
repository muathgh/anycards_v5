<?php

namespace App\Observers;

use App\Facades\SettingFacade;
use App\Models\SettingModels\Notification;
use App\Models\SettingModels\UserNotification;
use App\Models\TransactionModels\CardTransaction;
use Illuminate\Support\Facades\Config;

class CardTransactionObserver
{
    public function created(CardTransaction $cardTransacttion){
        $card = $cardTransacttion->card;
        $transaction = $cardTransacttion->transaction;
        $user = $transaction->user; 
        $currentBalance= $user->currentBalance();
        $cardType = $card->cardType;
        $userMinBalanceLimit = SettingFacade::getValueByKey("USER_BALANCE_MIN_LIMIT");
        if($cardType->is_active){
        $cardLimit = $cardType->cardLimit;
        if($cardLimit){
        if($cardType->cards()->where('is_used',false)->count()<=$cardLimit->value){
            $notification = new Notification([
                "title_en" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_EN'),$cardType->title_en),
                "title_ar"=>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_AR'),$cardType->title_ar),
                "description_en" =>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_EN'),$cardType->title_en),
                "description_ar"=>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_AR'),$cardType->title_ar),
                "type"=>Config::get('constants.NOTIFICATION_TYPE.CARD_LIMIT'),
                "href"=>route("cards.create",["card_type_id"=>$cardType->id])

            ]);
            $notification->save();
        }
        }
//V_6 UPDATES 04/01/2022
        if($currentBalance <= $userMinBalanceLimit){
            $notificationAr = SettingFacade::getValueByKey("USER_BALANCE_MIN_LIMIT_NOTIFICATION_AR");
            $notificationEn =  SettingFacade::getValueByKey("USER_BALANCE_MIN_LIMIT_NOTIFICATION_EN");
            $notification = new UserNotification([
                "title_en" => $notificationEn != null  ? $notificationEn: Config::get('constants.NOTIFICATION_MESSAGES.BALANCE_IS_ABOUT_TO_BE_EMPTY_TITLE_EN'),
                "title_ar"=>$notificationAr != null ? $notificationAr : Config::get('constants.NOTIFICATION_MESSAGES.BALANCE_IS_ABOUT_TO_BE_EMPTY_TITLE_AR'),
                "description_en" =>$notificationEn != null ? $notificationEn : Config::get('constants.NOTIFICATION_MESSAGES.BALANCE_IS_ABOUT_TO_BE_EMPTY_EN'),
                "description_ar"=>$notificationAr != null ? $notificationAr : Config::get('constants.NOTIFICATION_MESSAGES.BALANCE_IS_ABOUT_TO_BE_EMPTY_AR'),
                "type"=>"",
                "user_id"=>$user->id
               

            ]);
            $notification->save();
            $user->sendFcmNotification(Config::get('constants.NOTIFICATION_MESSAGES.BALANCE_IS_ABOUT_TO_BE_EMPTY_TITLE_AR'),Config::get('constants.NOTIFICATION_MESSAGES.BALANCE_IS_ABOUT_TO_BE_EMPTY_AR'));
        }
        //END V_6 UPDATES 04/01/2022
    }

    }
}
