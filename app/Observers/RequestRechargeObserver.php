<?php

namespace App\Observers;
use App\Models\RequestRechargeModels\RequestRecharge;
use App\Models\SettingModels\Notification;
use App\Models\SettingModels\RequestRechargeNotification;
use Illuminate\Support\Facades\Config;

class RequestRechargeObserver

{
    
    public function created(RequestRecharge $requestRecharge)
    {
        $notification = new RequestRechargeNotification([
            "title_en"=>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.NEW_REQUEST_RECHARGE_EN'),$requestRecharge->user->username,$requestRecharge->amount),
            "title_ar"=>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.NEW_REQUEST_RECHARGE_AR'),$requestRecharge->user->username,$requestRecharge->amount),
            "description_en"=>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.NEW_REQUEST_RECHARGE_EN'),$requestRecharge->user->username,$requestRecharge->amount),
            "description_ar"=>sprintf(Config::get('constants.NOTIFICATION_MESSAGES.NEW_REQUEST_RECHARGE_AR'),$requestRecharge->user->username,$requestRecharge->amount),
            "request_recharge_id"=>$requestRecharge->id,
            "user_id"=>$requestRecharge->user->id,
            "href"=>route("balances.create",["user_id"=>$requestRecharge->user->id,"request_id"=>$requestRecharge->id])
        ]);
        $notification->save();
    }
    
}
