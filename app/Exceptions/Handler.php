<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Mail;
use Exception;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured as ExceptionMail;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
       /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    // public function report(Throwable $exception)
    // {
    //     if ($this->shouldReport($exception)) {
    //         $this->sendExceptionEmail($exception);
    //     }
    //     parent::report($exception);
    // }
    // public function sendExceptionEmail(Exception $exception)
    // {
    //     try {
    //         $e = FlattenException::create($exception);
    //         $handler = new SymfonyExceptionHandler();
    //         $html = $handler->getHtml($e);
    //         Mail::queue(new ExceptionMail($html));
    //     } catch (Exception $e) {
    //         //
    //     }
    // }
    // public function render($request, Throwable $exception)
    // {
    //     /**
    //      * Had to put this in because it was throwing an exception if the user wasn't unauthenticated
    //      */
    //     if ( ! $this->shouldReport($exception)) {
    //         return parent::render($request, $exception);
    //     }
    //     if(config('app.debug')) {
    //         return parent::render($request, $exception);
    //     }
    //     return response()->view('errors.custom', [], 500);
    // }


    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    
}
