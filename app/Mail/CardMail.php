<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CardMail extends Mailable
{
    use Queueable, SerializesModels;
    public $codes,$cardTypeTitle,$subProviderTitle,$providerTitle,$categoryTitle;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cardTypeTitle,$subProviderTitle,$providerTitle,$categoryTitle,$codes)
    {
        $this->codes = $codes;
        $this->cardTypeTitle = $cardTypeTitle;
        $this->subProviderTitle = $subProviderTitle;
        $this->providerTitle=$providerTitle;
        $this->categoryTitle = $categoryTitle;
      
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.card');
    }
}
