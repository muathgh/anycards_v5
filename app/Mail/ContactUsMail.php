<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name,$mobileNumber,$email,$body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$mobileNumber,$body,$email = null)
    {
        $this->name= $name;
        $this->mobileNumber = $mobileNumber;
        $this->body = $body;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.contactus');
    }
}
