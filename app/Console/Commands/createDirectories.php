<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File as FileManager;
class createDirectories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_directories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!FileManager::exists(public_path('storage/Categories')))
        FileManager::makeDirectory(public_path('storage/Categories'));
        if(!FileManager::exists(public_path('storage/Providers')))
        FileManager::makeDirectory(public_path('storage/Providers'));
        if(!FileManager::exists(public_path('storage/SubProviders')))
        FileManager::makeDirectory(public_path('storage/SubProviders'));
        if(!FileManager::exists(public_path('storage/CardsTypes')))
        FileManager::makeDirectory(public_path('storage/CardsTypes'));
        if(!FileManager::exists(public_path('storage/UsersLogo')))
        FileManager::makeDirectory(public_path('storage/UsersLogo'));
        return 0;
    }
}
