<?php

namespace App\Exports;

use App\Models\ReportsModels\NetProfit;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class NetProfitExport implements FromCollection, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return NetProfit::all();
    }

    public function map($netProfit): array
    {
        return [
            
            $netProfit->stock_amount,
            $netProfit->stock_notes,
            $netProfit->current_balance_amount,
            $netProfit->current_balance_notes,
            $netProfit->receivables_amount,
            $netProfit->receivables_notes,
            $netProfit->unreceived_amount,
            $netProfit->unreceived_notes,
            $netProfit->other_amount,
            $netProfit->other_notes,
            $netProfit->net_profit_amount,
            $netProfit->net_profit_notes


        ];
    }

    public function headings(): array
    {
        return [
             "Total Of Stock", "Stock Notes", "Total Of Current Balance",
            "Current Balance Notes",
            "Receivables", "Receivables Notes", "Unreceived", "Unreceived Notes",
            "Other", "Other Notes", "Net Profit", "Net Profit Notes"
        ];
    }
}
