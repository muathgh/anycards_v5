<?php

namespace App\Exports;

use App\Models\CategoryModels\CardType;
use App\Models\ReportsModels\NetProfit;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class NetProfitExportFromView implements FromView
{
    public function view(): View
    {
        return view('exports.netprofit', [
            'netProfits' => NetProfit::all(),
            "receivables_amount_sum" => NetProfit::query()->sum("receivables_amount"),
            "unreceived_amount_sum" => NetProfit::query()->sum("unreceived_amount"),
            "other_amount_sum" => NetProfit::query()->sum("other_amount"),
            "current_balance_amount_sum" => $this->getTotalCurrentBalanceAmount(),
            "net_profit_amount_sum" => $this->netProfit(),
            "stock_amount_sum" => $this->getStockAmountSum(),
            "monthly_profit_sum" => $this->monthlyNetProfit(),

        ]);
    }

    private function getStockAmountSum()
    {
        $cardTypes = CardType::all();
        $totalStock = NetProfit::query()->sum("stock_amount");
        foreach ($cardTypes as $c) {
            $totalNumberOfCards = $c->cards()->where('is_used', false)->count();
            $officalPrice = $c->wholePrice->price;
            $totalStock += $officalPrice * $totalNumberOfCards;
        }

        return $totalStock;
    }

    private function getTotalCurrentBalanceAmount()
    {
        $users = User::all();
        $totalBalance = NetProfit::query()->sum("current_balance_amount");
        foreach ($users as $user) {
            $balance = $user->currentBalance();
            $totalBalance += $balance;
        }
        return $totalBalance;
    }


    private function netProfit()
    {
        $totalStock = $this->getStockAmountSum();
        $totalReceivables = NetProfit::query()->sum("receivables_amount");
        $totalBalance = $this->getTotalCurrentBalanceAmount();
        $totalOthers = NetProfit::query()->sum("other_amount");
        $totalNetProfit = NetProfit::query()->sum("net_profit_amount");
        $totalUnreceived = NetProfit::query()->sum("unreceived_amount");
        $monthlyProfit = $this->monthlyNetProfit();



        $netProfit = ($totalStock - $totalReceivables - $totalBalance  - $totalOthers - $totalNetProfit) + $totalUnreceived;
        $netProfit = $netProfit - $monthlyProfit;
        return $netProfit;
    }

    private function monthlyNetProfit()
    {
        $currentMonth = \Carbon\Carbon::now()->format("m");
        $currentYear =  \Carbon\Carbon::now()->year;


        $firstDateOfCurrentMonth = \Carbon\Carbon::createFromFormat('Y-m-d', "$currentYear-$currentMonth-01");
        $monthlyProfit = DB::table('card_transactions')
            ->join('transactions', 'transactions.id', '=', 'card_transactions.transaction_id')
            ->join('cards', 'card_transactions.card_id', '=', 'cards.id')
            ->join('cards_types', 'cards.card_type_id', '=', 'cards_types.id')
            ->join('whole_sale_prices', 'whole_sale_prices.card_type_id', '=', 'cards_types.id')
            ->selectRaw("SUM(transactions.amount) - SUM(whole_sale_prices.price) as monthlyProfit ,
        card_transactions.created_at as created_at")
            ->whereDate("card_transactions.created_at", ">=", $firstDateOfCurrentMonth)
            ->whereDate("card_transactions.created_at", "<=", \Carbon\Carbon::now())
            ->get();

        $monthlyProfit =   $monthlyProfit[0]->monthlyProfit;
        $monthlyProfitAmount = NetProfit::query()->sum("monthly_profit_amount");
        $monthlyProfitAmount += $monthlyProfit;
        return $monthlyProfitAmount;
    }
}
