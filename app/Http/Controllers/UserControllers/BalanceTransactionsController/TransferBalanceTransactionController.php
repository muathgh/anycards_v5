<?php

namespace App\Http\Controllers\UserControllers\BalanceTransactionsController;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest\TransferBalanceRequest;
use App\ServicesInterface\TransferBalanceServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransferBalanceTransactionController extends Controller
{
    

    private $_transferBalanceService,$_userService;
    public function __construct(TransferBalanceServiceInterface $transferBalanceService,UsersServiceInterface $userService)
    {
        $this->_transferBalanceService = $transferBalanceService;
        $this->_userService= $userService;
    }
    
    public function index(Request $request)
    {
        try{

            

            $types = [
                "transfer"=>trans("Transfer"),
                "recepion"=>trans('Recepion')
            ];

            if(request()->ajax()){
               
$data = $this->_transferBalanceService->getDtData($request->all());
return datatables()->of($data)->toJson();

            }

            return view('user.balancetransfer.index',["typeLookup"=>$types]);

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function showPopup(Request $request){
        $username = $request->username;
        $user = $this->_userService->getUserByUserName($username);
        $amount = $request->amount;
        if(request()->ajax()){
               
            $view = view('user.includes.transfer-popup',["fullname"=>$user->name,"amount"=>$amount])->render();
            
            return ResponseWrapperFacade::successWithData(["html"=>$view]);
        }

     }
    public function store(TransferBalanceRequest $request)
    {
        try{
            $username = $request->username;
            $amount = $request->amount;
           
            $fromUser = auth()->user();
          
            if($fromUser->username == $username)
           return ResponseWrapperFacade::failureWithMessage("You Must Select Another User");
           $result =  $fromUser->transferBalance($username,$amount);

           if(!$result['success']){
            return ResponseWrapperFacade::failureWithMessage($result['error']);
               
           }

           return ResponseWrapperFacade::success();



        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
