<?php

namespace App\Http\Controllers\UserControllers\UsersPriceOfSaleControllers;

use App\Http\Controllers\Controller;
use App\ServicesInterface\CategoryServiceInterface;
use Illuminate\Http\Request;
use App\Facades\ResponseWrapperFacade;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\UsersPriceOfSaleServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UsersPriceOfSaleController extends Controller
{
    private $_categoryService, $_providerService, $_subProviderService, $_userPriceOfSaleService;
    public function __construct(
        CategoryServiceInterface $categoryService,
        ProviderServiceInterface $providerService,
        SubProviderServiceInterface $subProviderService,
        UsersPriceOfSaleServiceInterface $userPriceOfSaleService
    ) {
        $this->_categoryService = $categoryService;
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProviderService;
        $this->_userPriceOfSaleService = $userPriceOfSaleService;
    }
    public function index()
    {
    }

    public function getPriceOfSaleForm(Request $request)
    {

        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  : null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  : null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  : null;

        $packageId = $request->package_id;
        $cardsTypes = collect();
        if ($subProviderId) {
            $subProvider = $this->_subProviderService->findById($subProviderId);
            $cardsTypes = $subProvider->cardsTypes()->orderBy("order", "ASC")->get();
        } else if ($providerId) {
            $provider = $this->_providerService->findById($providerId);
            foreach ($provider->subProviders()->orderBy('order', 'ASC')->get() as $s) {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
            }
        } else if ($categoryId) {
            $category = $this->_categoryService->findById($categoryId);
            foreach ($category->providers()->orderBy('order', 'ASC')->get() as $p) {
                foreach ($p->subProviders()->orderBy('order', 'ASC')->get() as $s) {
                    $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
                }
            }
        }




        $html =  view('user.includes.priceofsale-create', compact('cardsTypes'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesLookup = $this->_categoryService->lookup();
        return view('user.userspriceofsale.create', ["categoriesLookup" => $categoriesLookup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = json_decode($request->data);
            $user = auth()->user();
            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $price = $d->price ? $d->price : 0;
                $priceOfSale = $this->_userPriceOfSaleService->query()->where('card_type_id', $cardTypeId)
                    ->where('user_id', $user->id)->first();
                if ($priceOfSale) {
                    $this->_userPriceOfSaleService->updateByColumn("id", $priceOfSale->id, [
                        "price" => $price,

                    ]);

                    activity()
                        ->performedOn(Auth::guard('web')->user())
                        ->causedBy(Auth::guard('web')->user())
                        ->log('User update priceofsale');
                } else if ($price != 0) {
                    $this->_userPriceOfSaleService->create([
                        "card_type_id" => $cardTypeId,
                        "price" => $price,
                        "user_id" => $user->id
                    ]);

                    activity()
                        ->performedOn(Auth::guard('web')->user())
                        ->causedBy(Auth::guard('web')->user())
                        ->log('User create priceofsale');
                }
            }

            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
