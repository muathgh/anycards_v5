<?php

namespace App\Http\Controllers\UserControllers\RequestRechargesControllers;

use App\Facades\ResponseWrapper;
use App\Facades\ResponseWrapperFacade;
use App\Facades\UploadImageFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\RequestRechargeServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class RequestRechargeController extends Controller
{


    private $_requestRechargeService;
    public function __construct(RequestRechargeServiceInterface $requestRechargeService)
    {
        $this->_requestRechargeService = $requestRechargeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            return datatables()->of($this->_requestRechargeService->getDataDt($request->all()))->toJson();
        }

        return view('user.requestrecharge.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (request()->ajax()) {
            $view = view('user.includes.requestrecharge')->render();
            return ResponseWrapperFacade::successWithData($view);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (request()->ajax()) {
            try {


                $amount = $request->amount;
                $type = $request->type;
                $url = null;
                $text = null;

                if ($this->_requestRechargeService->checkIfUserHavePendingRequests(Auth()->user()->id) > 0)
                    return ResponseWrapperFacade::failureWithMessage(trans('Pending request already exist'));

                if ($type == "image") {
                    if ($request->hasFile('image')) {
                        $image = $request->file('image');
                        $url =  UploadImageFacade::upload($image, "Attachments");
                    }
                }else if($type == "text"){
                    $text = $request->text;
                }
                if ($this->_requestRechargeService->create([
                    "amount" => $amount,
                    "status" => Config::get('constants.REQUEST_RECHARGE_STATUS.PENDING'),
                    "user_id" => Auth::user()->id,
                    "attachment_text"=>$text,
                    "attachment_image"=>$url
                ])) {
                    activity()
                        ->performedOn(Auth::user())
                        ->causedBy(Auth::user())
                        ->log(Auth::user()->username . 'request new recharge');
                    return ResponseWrapperFacade::successWithMessage(trans('Request sent successfully'));
                }
            } catch (\Exception $e) {
                Log::channel('mysql')->error($e->getMessage());
                return ResponseWrapperFacade::failureWithMessage($e->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
