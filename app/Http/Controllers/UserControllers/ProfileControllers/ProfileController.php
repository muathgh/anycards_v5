<?php

namespace App\Http\Controllers\UserControllers\ProfileControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest\UpdatePasswordRequest;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth ;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth("web")->user();
        
        return view('user.profile.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function updatePassword(UpdatePasswordRequest $request){

    try{
        $currentPassword = $request->current_password;
        $newPassword = $request->new_password;
        
        if(!Hash::check($currentPassword,Auth()->user()->password))
        {
            return back()->with('error_message', trans('Current password is invalid'));
        }
        $user = Auth::user();
        $user->password = Hash::make($newPassword);
        $user->save();

        return back()->with('success_message', trans('Password updated successfully'));

    }catch(\Exception $e)
    {
        Log::channel('mysql')->error($e->getMessage());
        return redirect()->back();
    }

   }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
