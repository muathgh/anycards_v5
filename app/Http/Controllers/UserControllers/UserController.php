<?php

namespace App\Http\Controllers\UserControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest\ContactUsRequest;
use App\Mail\CardMail;
use App\Mail\ContactUsMail;
use App\ServicesInterface\BalancesServiceInterface;
use App\ServicesInterface\CardQuantityLimitServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTransactionsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\TransactionsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    private $_categoryService, $_providerService, $_subProviderService, $_cardTypeService, $_balanceService, $_transactionService,$_quantityLimitService;

    public function __construct(
        CategoryServiceInterface $categoryService,
        ProviderServiceInterface $providerService,
        SubProviderServiceInterface $subProviderService,
        CardsTypesServiceInterface $cardTypeService,
        BalancesServiceInterface $balanceService,
        TransactionsServiceInterface $transactionService,
        CardQuantityLimitServiceInterface $quantityLimitService
    ) {
        $this->_categoryService = $categoryService;
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProviderService;
        $this->_cardTypeService = $cardTypeService;
        $this->_balanceService = $balanceService;
        $this->_transactionService = $transactionService;
        $this->_quantityLimitService = $quantityLimitService;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        

      

        $decryptedId = decrypt($request->sub_provider_id);

        $categories = $this->_categoryService->latest();
         $cardTypes = $this->_cardTypeService->paginateBySubProviderIdOrderd($decryptedId);
        
        if(request()->ajax())
        {
            $view = view('user.includes.items',compact('cardTypes'))->render();
            return ResponseWrapperFacade::successWithData($view);
        }


        return view('user.cardtypes.index', compact("categories", 'cardTypes'));
    }

 

    public function getMyBalance(){
        if(request()->ajax()){
            $userId = Auth::user()->id;
            $balance = $this->_balanceService->userCurrentBalance($userId);
            return ResponseWrapperFacade::successWithData($balance);
        }
    }

    public function getProviderByCategoryId($categoryId)
    {
        if (request()->ajax()) {
            $providers = $this->_providerService->getProvidersByCategoryId($categoryId);
            $view = view('user.includes.providers', compact('providers'))->render();
            return ResponseWrapperFacade::successWithData($view);
        }
    }

    public function getSubProviderByProviderId($providerId)
    {
        if (request()->ajax()) {
            $subProviders = $this->_subProviderService->getSubProviderByProviderId($providerId);
            $view = view('user.includes.subproviders', compact('subProviders'))->render();
            return ResponseWrapperFacade::successWithData($view);
        }
    }
    public function getCardTypesBySubProviderId($subproviderid)
    {
        if (request()->ajax()) {
            $cardTypes = $this->_cardTypeService->getCardTypeBySubProviderId($subproviderid);
            $view = view('user.includes.cardtypes', compact('cardTypes'))->render();
            return ResponseWrapperFacade::successWithData($view);
        }
    }

    public function search(Request $request)
    {
        try {

            $category = $request->category;
            $provider = $request->provider;
            $subProvider = $request->sub_provider;

            if ($subProvider) {
                $cardTypes = $this->_cardTypeService->getCardTypeBySubProviderId($subProvider);
                $view = view('user.includes.items', compact('cardTypes'))->render();
                return ResponseWrapperFacade::successWithData($view);
            }
            if ($provider) {
                $subProviders = $this->_subProviderService->getSubProviderByProviderId($provider);

                $cardTypes = [];
                foreach ($subProviders as $subProvider) {
                    foreach ($subProvider->cardsTypes as $cardType) {
                        $cardTypes[] = $cardType;
                    }
                }

                $view = view('user.includes.items', compact('cardTypes'))->render();
                return ResponseWrapperFacade::successWithData($view);
            }
            if ($category) {
                $providers = $this->_providerService->getProvidersByCategoryId($category);

                $cardTypes = [];
                foreach ($providers as $provider) {
                    foreach ($provider->subProviders as $subProvider) {
                        foreach ($subProvider->cardsTypes as $cardType) {
                            $cardTypes[] = $cardType;
                        }
                    }
                }

                $view = view('user.includes.items', compact('cardTypes'))->render();
                return ResponseWrapperFacade::successWithData($view);
            }
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function newPrice(Request $request){
        try{
            $id = $request->id;
            $quantity = $request->quantity;
            $decryptedId = decrypt($id);
            $price = $this->_cardTypeService->getUserCardTypePrice(Auth::user()->id, $decryptedId);
            $price = $price * $quantity;
           
            $price= bcdiv($price,1,2);
            return ResponseWrapperFacade::successWithData(["price"=>$price]);
        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function showOrderPopup(Request $request)
    {

        try {
           
            $id = $request->id;
            $decryptedId = decrypt($id);
            $cardType = $this->_cardTypeService->findById($decryptedId);
            if ($cardType->cards()->where('is_used', 0)->count() <= 0) {
                
                return ResponseWrapperFacade::successWithMessage(Config::get("constants.MESSAGES.NO_CARDS"));
            }
            //calculate price
            $price = $this->_cardTypeService->getUserCardTypePrice(Auth::user()->id, $decryptedId);

            $cardType = $this->_cardTypeService->findById($decryptedId);
            $view = view('user.includes.order', compact('cardType', "price"))->render();


            return ResponseWrapperFacade::successWithData($view);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    public function order(Request $request)
    {
        try {
            if (request()->ajax()) {
                //get user price
                $quantity = $request->quantity;
                $cards = [];
                $cardTypeId = $request->card_type_id;
                $decryptedId = decrypt($cardTypeId);
                $userId = Auth::user()->id;
                $price = $this->_cardTypeService->getUserCardTypePrice($userId, $decryptedId);
                $totalPrice = $price * $quantity;
                $cardTypeTitle = null;
                $cardExpiryDate = null;
                $cardType = $this->_cardTypeService->findById($decryptedId);
                $cardsCount = $cardType->cards()->where('is_used', 0)->count();
                if($cardsCount < $quantity){
                    return ResponseWrapperFacade::successWithMessage(Config::get("constants.MESSAGES.QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY"));
    
                }
               
                $balance = $this->_balanceService->userCurrentBalance($userId);
                if ($balance < $totalPrice)
                    return ResponseWrapperFacade::successWithMessage(Config::get('constants.MESSAGES.NOT_ENOUGH_PRICE'));
            // check cards quantity limit
            $todayTransactionsCount =DB::table('card_transactions')->
            join('cards','cards.id','=','card_transactions.card_id')->
            join('cards_types','cards_types.id','=','cards.card_type_id')->
            join('transactions','transactions.id','=','card_transactions.transaction_id')->
           
            whereYear('card_transactions.created_at',\Carbon\Carbon::now()->year)->
            whereMonth('card_transactions.created_at',\Carbon\Carbon::now()->month)->
            whereDay('card_transactions.created_at',\Carbon\Carbon::now()->day)
            ->where('cards_types.id',$decryptedId)
            ->where('transactions.user_id',$userId)->count();

           

            $quantityLimit = $this->_quantityLimitService->query()->where('card_type_id',$decryptedId)->first();
            if($quantityLimit && $quantityLimit->quantity_limit != 0)
            {
            if(($todayTransactionsCount+$quantity) > $quantityLimit->quantity_limit){
                return ResponseWrapperFacade::successWithMessage(Config::get('constants.MESSAGES.QUANTITY_LIMIT'));

            }
        }


            
            
            
                    for($i = 0; $i<$quantity;$i++)
               {
             

                $balanceAfterDeduction = $this->_balanceService->balanceDeduction($userId, $price);

                $transaction =   $this->_transactionService->orderCard($decryptedId, $userId, $price);

                $this->_balanceService->changeUserBalance(Auth::user(), $balanceAfterDeduction, $transaction['transaction_id']);
                $card = $transaction['card'];
                
                $cardTypeTitle = $card->cardType->title_en;
                $cardExpiryDate = $card->expiry_date;
                
                $cards[]=[
                    "id"=>$card->id,
                    "template"=>$card->cardType->subProvider->template,
                    "category"=>$card->cardType->subProvider->provider->category->title_en,
                    "provider"=>$card->cardType->subProvider->provider->title_en,
                    "sub_provider"=>$card->cardType->subProvider->title_en,
                    "card_type"=>$card->cardType->title_en,
                    "code"=>HelperFacade::hyphenate(trim($card->code)),
                    "mobile_number"=>Auth::check() ? Auth::user()->mobile_number : "",
                    "serial"=>$card->serial? $card->serial:null,
                    "order_number"=>$card->cardTransaction->order_number ? $card->cardTransaction->order_number:null,
                    "expiry_date"=>$card->expiry_date,
                    "username"=>Auth::check() ? Auth::user()->username : "",


                ];
               }
               
               
               $view = view('user.includes.ordercomplete', ["user"=>Auth::user(),"cardTypeId"=>$cardTypeId,"cards"=>$cards , "cardExpiryDate"=>$cardExpiryDate , "totalPrice"=>$totalPrice,"cardTypeTitle"=>$cardTypeTitle])->render();

                return ResponseWrapperFacade::successWithData($view);
            }
            return ResponseWrapperFacade::failure();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function print(Request $request){

    }

    public function showSendByMailPopup(Request $request){
        try{
            $codes = $request->codes;
            $cardTypeId = $request->card_type_id;
           
            $view = view('user.includes.sendcodebymail',['codes'=>$codes,"cardTypeId"=>$cardTypeId])->render();
            return ResponseWrapperFacade::successWithData($view);
        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function sendCodeByEmail(Request $request){
        try{
            
         
            $email = $request->email;
            $codes = $request->codes;
            $codes = json_decode($codes);
            
            $cardTypeId = $request->card_type_id;
            $cardTypeId = decrypt($cardTypeId);
            $cardType = $this->_cardTypeService->findById($cardTypeId);
            $subProvider = $cardType->subProvider;
            $provider = $subProvider->provider;
            $category = $provider->category;
            $cardTypeTitle = $cardType->title_en;
            $subProviderTitle = $subProvider->title_en;
            $providerTitle = $provider->title_en;
            $categoryTitle= $category->title_en;
          

            

            Mail::to($email)->send(new CardMail($cardTypeTitle,$subProviderTitle,$providerTitle,$categoryTitle,$codes));

            return ResponseWrapperFacade::success();

        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function contactus(Request $request){
       
        return view('user.contactus.index');
    }

    public function contactusAjAX(ContactUsRequest $request){
        try{
            

            $name = $request->full_name;
            $mobileNumber = $request->mobile_number;
            $email = $request->email;
            $body = $request->content;
            Mail::to("info@any-cards.com")->send(new ContactUsMail($name,$mobileNumber,$body,$email));
return redirect()->back()->with('success_message',trans('Email Send Successfully'));
        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
return redirect()->back();
        }
    }

    public function language(){
     
        if(!Session::has('user_changed_language'))
        {
        Session::put('user_changed_language', 'en');
        return redirect()->back();
        }
        if(Session::get('user_changed_language') == "ar"){
            Session::put('user_changed_language', 'en');
           
        }else{
            Session::put('user_changed_language', 'ar');
           
        }
        
    
       

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
