<?php

namespace App\Http\Controllers\UserControllers;

use App\Http\Controllers\Controller;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;

class SubProviderController extends Controller
{


    private $_subProviderService;
    public function __construct(SubProviderServiceInterface $subProviderService)
    {
        $this->_subProviderService = $subProviderService;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($providerId)
    {
        $decryptedId = decrypt($providerId);
        $subProviders = $this->_subProviderService->getSubProviderByProviderIdOrderd($decryptedId);
        return view('user.subproviders.index',compact('subProviders'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
