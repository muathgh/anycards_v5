<?php

namespace App\Http\Controllers\UserControllers\CardTypePointsControllers;

use App\Http\Controllers\Controller;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTypePointsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use Illuminate\Http\Request;

class CardTypePointsController extends Controller
{

    private $_categoryService,$_cardTypePointsService;

    public function __construct(CategoryServiceInterface $categoryService,CardTypePointsServiceInterface $cardTypePointsService)
    {
        $this->_categoryService = $categoryService;
        $this->_cardTypePointsService = $cardTypePointsService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categoriesLookup = $this->_categoryService->lookup();
        if(request()->ajax()){
            $params = $request->all();
       
           
            $cardTypes = $this->_cardTypePointsService->getDtData($params);
            
            
            

            return datatables()->of($cardTypes)->toJson();
        }
        return view('user.cardtypespoints.index',compact('categoriesLookup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
