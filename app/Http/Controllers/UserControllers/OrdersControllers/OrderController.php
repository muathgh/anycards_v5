<?php

namespace App\Http\Controllers\UserControllers\OrdersControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTransactionsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use Illuminate\Http\Request;
use App\ServicesInterface\OrdersServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{


    private $_ordersService,$_cardTransactionService,$_cardService,$_cardTypeService,$_categoryService;
    public function __construct(OrdersServiceInterface $ordersService,CardTransactionsServiceInterface $cardTransactionService,CardsServiceInterface $cardService,CardsTypesServiceInterface $cardsTypesService,
    CategoryServiceInterface $categoryService)
    {
        $this->_ordersService = $ordersService;
        $this->_cardTransactionService = $cardTransactionService;
        $this->_cardService = $cardService;
        $this->_cardTypeService = $cardsTypesService;
        $this->_categoryService= $categoryService;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $categoriesLookup = $this->_categoryService->lookup();
      
        $params = $request->all();
        if(request()->ajax()){
            $myOrders = $this->_ordersService->getDtData($params);
            // $response = array(
            //     "draw" => intval($myOrders['draw']),
            //     "iTotalRecords" =>  $myOrders['total'],
            //     "iTotalDisplayRecords" =>  $myOrders['total'],
            //     "aaData" => $myOrders['data']
            //  );

             return datatables()->of($myOrders)->make(true);

            //  return response()->json($response);
            
        }
        // $totalPriceOfSale = User::totalPriceOfSaleTransactions();

        return view('user.orders.index',["categoriesLookup"=>$categoriesLookup]);
    }

    public function showReceipt(Request $request){
        $cardId = $request->card_id;
       
        $card = $this->_cardService->findById($cardId);

        $view = view('user.includes.receipt',compact("card"))->render();
        return ResponseWrapperFacade::successWithData($view);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
        $cardTransaction = $this->_cardTransactionService->findById($id);
        
        $card = $cardTransaction->card;
        $cardTypeTitle = $card->cardType->title_en;
        $cardExpiryDate = $card->expiry_date;
        $cardTypeId = $card->card_type_id;
        $serial =$card->serial? $card->serial:null;
  
        $cards[]=[
            "id"=>$card->id,
            "template"=>$card->cardType->subProvider->template,
            "category"=>$card->cardType->subProvider->provider->category->title_en,
            "provider"=>$card->cardType->subProvider->provider->title_en,
            "sub_provider"=>$card->cardType->subProvider->title_en,
            "card_type"=>$card->cardType->title_en,
            "code"=>HelperFacade::hyphenate(trim($card->code)),
            "mobile_number"=>Auth::check() ? Auth::user()->mobile_number : "",
            "serial"=>$card->serial? $card->serial:null,
            "order_number"=>$card->cardTransaction->order_number ? $card->cardTransaction->order_number:null,
            "expiry_date"=>$card->expiry_date,
            "username"=>Auth::check() ? Auth::user()->username : "",
            


        ];
       

       
        
        $transaction = $cardTransaction->transaction;
        $totalPrice = $transaction->amount;
        $user = Auth::user();
      
        
      
        $view = view('user.includes.vieworder', compact('user','serial','cards','cardTypeId','totalPrice',"cardTypeTitle","cardExpiryDate"))->render();
        return ResponseWrapperFacade::successWithData($view);
        }
    }

    public function priceOfSaleTransactions(Request $request){
        $categoriesLookup = $this->_categoryService->lookup();
        $params = $request->all();
        if(request()->ajax()){
            $myOrders = $this->_ordersService->getPriceOfSalesDt($params);
            $response = array(
                "draw" => intval($myOrders['draw']),
                "iTotalRecords" =>  $myOrders['total'],
                "iTotalDisplayRecords" =>  $myOrders['total'],
                "aaData" => $myOrders['data']
             );

             return response()->json($response);

            
        }
        return view('user.priceofsaletransactions.index',["categoriesLookup"=>$categoriesLookup]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
