<?php

namespace App\Http\Controllers\AuthControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;
use App\Facades\ResponseWrapperFacade;
use App\Http\Requests\AuthRequests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{

    use AuthenticatesUsers;
    protected $username;
    public function __construct()

    {
        // $this->middleware('guest')->except('logout');
        // $this->middleware('guest:admin')->except('logout');
        $this->username = 'username';
    }

    public function username()
    {
        return $this->username; //or whatever field
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    public function showAdminLoginForm()
    {
       
        if (!Session::has('admin_locale')){
            Session::put('admin_locale', 'en');
        }

        return view('auth.admin.login');
    }

    public function showUserLoginForm()
    {
        if (!Session::has('user_locale'))
        {
            Session::put('user_locale', 'ar');
        }

        return view('auth.user.login');
    }



    public function userLogin(LoginRequest $request)
    {
        try {


            if (Auth::guard("web")->attempt(['username' => $request->username, 'password' => $request->password])) {
                $user = Auth::user();
                if($user->status != "active")
                {
                    Auth::logout();
                    return redirect()->route("user.login")->with('inactive_error_login', trans('Your Account In-active (Please contact with admin)'));
                }
                $user->ip_address = request()->ip();
                $user->last_login = now();
                $user->save();
                activity()
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log(Auth::user()->username . 'logged in');
                if (!Session::has('user_locale')){
                    Session::put('user_locale', 'ar');
                }

                return redirect()->back();
            }
            activity()
                ->log("incorrect username or password");
            return back()->withInput($request->only('username', 'remember'))->with('error_message', trans('Incorrect username or password'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
    public function adminLogin(LoginRequest $request)
    {
        try {


            if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log(Auth::user('admin')->username . 'logged in');

                    
              
                if (!Session::has('admin_locale'))
                {
                    Session::put('admin_locale', 'en');
                }
                return redirect()->back();
            }
            activity()
                ->log("incorrect username or password");
            return back()->withInput($request->only('username', 'remember'))->with('error_message', trans('Incorrect username or password'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function adminLogout()
    {
        try {
            Auth::guard('admin')->logout();
            // activity()
            //     ->log("user logged out successfully");
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
        }
    }
    public function userLogout()
    {
        try {
            Auth::guard('web')->logout();
            // activity()
            //     ->log("user logged out successfully");
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
