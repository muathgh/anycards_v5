<?php

namespace App\Http\Controllers\AdminControllers\CardsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapper;
use App\Http\Controllers\Controller;
use App\ServicesInterface\CardsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\Http\Requests\AdminRequests\CreateCardPriceRequest;

use App\Http\Requests\AdminRequests\UpdateCardRequest;
use App\Models\Admin;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Support\Facades\Session;

class CardController extends Controller
{

    private $_cardService, $_categoryService,$_cardTypeService,$_subProviderService,$_providerService;

    public function __construct(
        CardsServiceInterface $cardService,
        CategoryServiceInterface $categoryService,
        CardsTypesServiceInterface $cardTypeService,
        SubProviderServiceInterface $subProviderService,
        ProviderServiceInterface $providerService
    ) {
        $this->_cardService = $cardService;
        $this->_categoryService = $categoryService;
        $this->_cardTypeService = $cardTypeService;
        $this->_subProviderService =$subProviderService;
        $this->_providerService = $providerService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoriesLookup = $this->_categoryService->lookup();
        $adminsLookup = Admin::all()->pluck('username','id');
 
        return view('admin.cards.index', compact('categoriesLookup','adminsLookup'));
    }
    public function getData(Request $request)
    {
        try {
            if (request()->ajax()) {
$data = $request->all();
                $data = $this->_cardService->getDtData($data);
               return response()->json($data);
                // return datatables()->of($data)->toJson();
            }
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Session::remove('upload_cards_filters');
        $cardType = null;
        $id = $request->card_type_id;
        if($id)
        {
$cardType = $this->_cardTypeService->findById($id);


$cardTypes = $cardType->subProvider->cardsTypes()->orderBy('order','ASC')->pluck('title_en','id');
$subProviders = $cardType->subProvider->provider->subProviders()->orderBy('order','ASC')->pluck('title_en','id');;
$providers = $cardType->subProvider->provider->category->providers()->orderBy('order','ASC')->pluck('title_en','id');
$categories = $this->_categoryService->lookup();    
return view('admin.cards.createfromnotifications', ["cardType"=>$cardType,"cardTypes"=>$cardTypes,"subProviders"=>$subProviders,
"providers"=>$providers,"categories"=>$categories]);
}
        $lookup = $this->lookup();
        return view('admin.cards.create', ["lookup"=>$lookup]);
    }

    private function lookup()
    {
        $data = $this->_categoryService->lookup();
        return $data;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $categoryId = $request->category_id;
            $providerId = $request->provider_id;
            $subProviderId = $request->sub_provider_id;
            $cardTypeId = $request->card_type_id;
            $expiryDate = $request->expiry_date;
            
            $data = json_decode($request->data);
            $duplicate = [];
            Session::put('upload_cards_filters',[
"category_id"=>$categoryId,
"provider_id"=>$providerId,
"sub_provider_id"=>$subProviderId,
"card_type_id"=>$cardTypeId,
"expiry_date"=>\Carbon\Carbon::parse($expiryDate)
            ]);
            foreach ($data as $d) {
                $code = $d->code;
                $codeToCheck = HelperFacade::removeDash($code);
                $serial = $d->serial;
                $card = $this->_cardService->whereRaw('REPLACE(cards.code,"-","") = ?',[$codeToCheck])->first();
                // $card = $this->_cardService->getFirstCardByCondition("code", $codeToCheck);
                if (!$card) {
                    $this->_cardService->create([
                        "card_type_id" => $cardTypeId,
                        "expiry_date" => $expiryDate,
                        "serial" => $serial,
                        "code" => $code,
                        "created_by_id"=>Auth::guard('admin')->user()->id
                    ]);
                } else {
                    $duplicate[] = $card->code;
                }
            }



            if (sizeof($duplicate) > 0)
            {
                $view = view("admin.cards.duplicatedcodes",["duplicates"=>$duplicate])->render();
                return ResponseWrapperFacade::successWithData(["html"=>$view,"title"=>"Duplicated Cards"]);
            }

            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function cardPricesStore(CreateCardPriceRequest $request)
    {
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = $this->_cardService->findById($id);
        $lookup = $this->lookup();
        $category =   $card->cardType->subProvider->provider->category;
        $provider = $card->cardType->subProvider->provider;
        $subProvider = $card->cardType->subProvider;

        $providersLookup = $category->providers()->orderBy('order','ASC')->pluck('title_en', "id");
        $subProvidersLookup = $provider->subProviders()->orderBy('order','ASC')->pluck('title_en', "id");
        $cardTypeLookup = $subProvider->cardsTypes()->orderBy('order','ASC')->pluck('title_en', "id");


        return view('admin.cards.edit', compact('card', 'lookup', 'providersLookup', 'subProvidersLookup', 'cardTypeLookup'));
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCardRequest $request)
    {
        try {

            $id = $request->id;
            $cardTypeId = $request->card_type_id;
            $code = $request->code;
            $serial = $request->serial;
            $expiryDate = $request->expiry_date;
            if ($this->_cardService->update($id, [
                "card_type_id" => $cardTypeId,
                "code" => $code,
                "serial" => $serial,
                "expiry_date" => $expiryDate,
                "updated_by_id"=>Auth::guard('admin')->user()->id

            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update card with id' . $id);
                return redirect()->route('cards.index');
            }



            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function removeFilters(){
        Session::remove('upload_cards_filters');
        return 'success';
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {

            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/cards/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }

            if(!$this->_cardService->delete($id));
            return redirect()->back()->with('error_message',trans('Deletion Failed'));
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete card with id ' . $id);
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
