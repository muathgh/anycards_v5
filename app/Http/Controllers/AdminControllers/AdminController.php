<?php

namespace App\Http\Controllers\AdminControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTransactionsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\RedeemCodeServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    private $_userService, $_categoryService, $_providerService, $_subProviderService, $_cardTypeService, $_cardTransactionService,$_redeemCodeService;
    public function __construct(
        UsersServiceInterface $userService,
        CategoryServiceInterface $categoryService,
        ProviderServiceInterface $providerServiec,
        SubProviderServiceInterface $subProviderService,
        CardsTypesServiceInterface $cardTypeService,
        CardTransactionsServiceInterface $cardTransactionService,
        RedeemCodeServiceInterface $redeemCodeService
    ) {
        $this->_userService = $userService;
        $this->_categoryService = $categoryService;
        $this->_providerService = $providerServiec;
        $this->_subProviderService = $subProviderService;
        $this->_cardTypeService = $cardTypeService;
        $this->_cardTransactionService = $cardTransactionService;
        $this->_redeemCodeService = $redeemCodeService;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

   
        $categories = sizeof($this->_categoryService->all());
        $providers = sizeof($this->_providerService->all());
        $subProviders = sizeof($this->_subProviderService->all());
        $cardsTypes = sizeof($this->_cardTypeService->all());
        // $cardTransactions = $this->_cardTransactionService->count();
        $cardTransactions= $this->_cardTransactionService->query()->whereMonth('created_at',\Carbon\Carbon::now()->month)->count();
        if(Auth::guard("admin")->user()->accessType()=="created_users"){
            $cardTransactions = DB::table("card_transactions")
            ->join('transactions','transactions.id','=','card_transactions.transaction_id')
            ->join('users','users.id','=','transactions.user_id')
            ->where('users.created_by_id',Auth::guard('admin')->user()->id)
            ->whereMonth('card_transactions.created_at',\Carbon\Carbon::now()->month)
            ->count();
        }
        $registeredUsers = $this->_userService->count();
   
      


        

        return view('admin.home.index', compact('categories', 'providers', 'subProviders', 'cardsTypes', 'cardTransactions', 'registeredUsers'));
    }

    public function getStatisticsData(){

        $providersStatistics = $this->_providerService->getProviderStatistics();
        $providersTitles  = [];
        $providersNameAndValue = [];
        foreach ($providersStatistics as $providerStatistic) {
            array_push($providersTitles,$providerStatistic->title_en);
            array_PUSH($providersNameAndValue,[
                "value"=>$providerStatistic->count,
                "name"=>$providerStatistic->title_en
            ]);
        }
        
        $data = [
            "cardTypesTitles"=>$providersTitles,
            "cardTypesNameAndValue"=>$providersNameAndValue
        ];
        return ResponseWrapperFacade::successWithData($data);
    }

    // public function getStatisticsData(){

    //     $cardsTypeStatistics = $this->_cardTypeService->getCardsTypeStatistics();
    //     $cardTypesTitles  = [];
    //     $cardTypesNameAndValue = [];
    //     foreach ($cardsTypeStatistics as $cardTypeStatistic) {
    //         array_push($cardTypesTitles,$cardTypeStatistic->title_en);
    //         array_PUSH($cardTypesNameAndValue,[
    //             "value"=>$cardTypeStatistic->count,
    //             "name"=>$cardTypeStatistic->title_en
    //         ]);
    //     }
        
    //     $data = [
    //         "cardTypesTitles"=>$cardTypesTitles,
    //         "cardTypesNameAndValue"=>$cardTypesNameAndValue
    //     ];
    //     return ResponseWrapperFacade::successWithData($data);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
