<?php

namespace App\Http\Controllers\AdminControllers\TransactionsController;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Facades\UploadImageFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\RequestRechargeServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RequestBalanceController extends Controller
{

    private $_requestRechargeService;

    public function __construct(RequestRechargeServiceInterface $requestRechargeService)
    {
        $this->_requestRechargeService = $requestRechargeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!HelperFacade::checkAdminPermission("User Balances Requests")){
            return redirect()->back();
                    }
        if (request()->ajax()) {
            $data = $this->_requestRechargeService->getAdminDtData($request->all());
            return response()->json($data);
        }

        return view('admin.balancesRequests.index');
    }

    public function balancesRequestPopup(Request $request)
    {
        try {
            $id = $request->id;
            $requestBalance = $this->_requestRechargeService->findById($id);
            $document = null;
            if ($requestBalance) {


                if ($requestBalance->attachment_text) {
                    $document = $requestBalance->attachment_text;
                    $type = "text";
                } else  if ($requestBalance->attachment_image) {
                    $document = $requestBalance->attachment_image;
                    $type = "image";
                } else if ($requestBalance->attachment_mobile_image) {
                    $document = $requestBalance->attachment_mobile_image;
                    $type = "mobile_image";
                }

                if($type == "mobile_image")
                {
                    if($requestBalance->resized_image_path)
                    {
                        $document = $requestBalance->resized_image_path;
                    }else{
                    $result = UploadImageFacade::reszieMobileImage($document);
                    $requestBalance->resized_image_name = $result['name']; 
                    $requestBalance->resized_image_path=$result['url'];
                    $requestBalance->save();
                    $document = $result['url'];
                    }
                    
                    
                }

               
            }
            $html = view('admin.balancesRequests.request-balance-popup', ["requestBalance" => $requestBalance,'document'=>$document,'type'=>$type])->render();
            return ResponseWrapperFacade::successWithData([
                "html" => $html
            ]);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
