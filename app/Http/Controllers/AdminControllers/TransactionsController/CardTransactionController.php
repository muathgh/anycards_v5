<?php

namespace App\Http\Controllers\AdminControllers\TransactionsController;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\TransactionModels\Transaction;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTransactionsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CardTransactionController extends Controller
{


    private $_cardTransactionService,$_userService,$_categoryService,$_providerService,$_subProviderService,$_cardTypeService;
    public function __construct(CardTransactionsServiceInterface $cardTransactionService,UsersServiceInterface $usersService,CategoryServiceInterface $categoryService,
    ProviderServiceInterface $providerService,SubProviderServiceInterface $subProvderService,CardsTypesServiceInterface $cardTypeService)
    {
        $this->_cardTransactionService = $cardTransactionService;
        $this->_userService = $usersService;
        $this->_categoryService = $categoryService;
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProvderService;
        $this->_cardTypeService = $cardTypeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        // 




        $usersLookup = $this->_userService->lookupByAccessType();
        $categoriesLookup = $this->_categoryService->lookup();
        $adminsLookup = Admin::all()->pluck('username','id');
        $data = $request->all();
        
        if(request()->ajax()){
            $cardTransactions = $this->_cardTransactionService->getDtData($data);

            return response()->json($cardTransactions);
            // return datatables()->of($cardTransactions)->make(true);
        }
        return view('admin.cardstransactions.index',compact('usersLookup','categoriesLookup','adminsLookup'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

