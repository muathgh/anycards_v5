<?php

namespace App\Http\Controllers\AdminControllers\TransactionsController;

use App\Facades\HelperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\BalancesServiceInterface;
use App\ServicesInterface\TransactionsServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\Facades\UploadImageFacade;
use App\Http\Requests\AdminRequests\ChargeBalanceRequest;
use App\Models\SettingModels\BalanceLimit;
use App\Models\SettingModels\Notification;
use App\Models\TransactionModels\Transaction;
use App\ServicesInterface\RequestRechargeServiceInterface;
use Illuminate\Support\Facades\Config;

class BalanceController extends Controller
{

    private $_userService, $_transactionService, $_balancesService, $_requestRechargeService;

    public function __construct(UsersServiceInterface $userService, TransactionsServiceInterface $transactionService, BalancesServiceInterface $balancesService, RequestRechargeServiceInterface $requestRechargeService)
    {
        $this->_userService = $userService;
        $this->_transactionService = $transactionService;
        $this->_balancesService = $balancesService;
        $this->_requestRechargeService = $requestRechargeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!HelperFacade::checkAdminPermission("Balances")) {
            return redirect()->back();
        }
        return view('admin.balances.index');
    }
    public function getData()
    {
        try {
            if (request()->ajax())
                return datatables()->of($this->_balancesService->getDtData())->toJson();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create(Request $request)
    {
        if (!HelperFacade::checkAdminPermission("Balances")) {
            return redirect()->back();
        }
        $typesLookup = $this->typesLookup();
        $usersLookup = $this->usersLookup();
        $userId = $request->has('user_id') && $request->user_id ? $request->user_id : null;
        $requestId = $request->has('request_id') && $request->request_id ? $request->request_id : null;
        $document = null;
        $type = null;
        $requestAmount = null;
        $requestDate=null;
        if ($userId != null && $requestId != null) {

            $requestRecharge = $this->_requestRechargeService->query()->where('user_id', $userId)
                ->where('id', $requestId)
                ->first();

            if ($requestRecharge) {

                $requestDate = $requestRecharge->created_at;
                if ($requestRecharge->attachment_text) {
                    $document = $requestRecharge->attachment_text;
                    $type = "text";
                } else  if ($requestRecharge->attachment_image) {
                    $document = $requestRecharge->attachment_image;
                    $type = "image";
                } else if ($requestRecharge->attachment_mobile_image) {
                    $document = $requestRecharge->attachment_mobile_image;
                    $type = "mobile_image";
                }

                if($type == "mobile_image")
                {
                    if($requestRecharge->resized_image_path)
                    {
                        $document = $requestRecharge->resized_image_path;
                    }else{
                    $result = UploadImageFacade::reszieMobileImage($document);
                    $requestRecharge->resized_image_name = $result['name']; 
                    $requestRecharge->resized_image_path=$result['url'];
                    $requestRecharge->save();
                    $document = $result['url'];
                    }
                    
                    
                }

                $requestAmount = $requestRecharge->amount;
            }
        }
        return view('admin.balances.create', compact('typesLookup', 'usersLookup', 'userId', "type", "document", 'requestAmount','requestDate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChargeBalanceRequest $request)
    {
        try {
            $userId = $request->user_id;
            $amount = $request->amount;
            $type = $request->type;
            $user = $this->_userService->findById($userId);
            $admin = Auth::guard('admin')->user();
            // last balance transaction 

            $lastTransaction = Transaction::where('type',$type)
            ->where('created_by_id', Auth::guard('admin')->user()->id)->where('user_id', $userId)->where('amount', number_format((float)$amount, 2, '.', ''))->latest()->first();
            if ($lastTransaction && $lastTransaction->next_transaction) {

                if (\Carbon\Carbon::now()->lte(\Carbon\Carbon::parse($lastTransaction->next_transaction))) {
                    return redirect()->back()->with('error_message', 'You have to wait 5 minutes to do the same operation');
                }
            }
            if ($type == Config::get('constants.TRANSACTION_TYPE.DEPOSIT')) {
                $balanceLimit = BalanceLimit::where('admin_id', $admin->id)->first();
                if ($balanceLimit && !$admin->isSuperAdmin()) {

                    if ($amount > $balanceLimit->amount) {
                        return redirect()->back()->with("error_message", trans("You have exceeded your charging quota"));
                    } else {
                        BalanceLimit::where('admin_id', $admin->id)->update(['amount' => ((float)($balanceLimit->amount - $amount)) < 0.0 ? 0.0 : ((float)($balanceLimit->amount - $amount))]);
                        $balanceLimit = BalanceLimit::where('admin_id', $admin->id)->first();
                        if ($balanceLimit->amount == 0) {
                            $admin = $balanceLimit->admin->username;
                            Notification::create([
                                "title_en" => "$admin have exceeded his charging quota",
                                "title_ar" => "تجاوز حصته في الشحن $admin الأدمن",
                                "description_en" => "$admin have exceeded his charging quota",
                                "description_ar" => "تجاوز حصته في الشحن $admin الأدمن",
                                "type" => Config::get('constants.NOTIFICATION_TYPE.EXCEEDED_CHARGING_QUOTA'),
                                "is_seen" => false,
                                "href" => ""



                            ]);
                        }
                    }
                }
                $user->addBalance($amount);
            } else {
                if ($user->currentBalance() < $amount)
                    return redirect()->back()->with('error_message', 'Balance is less than amount')->withInput();
                $user->withDrawBalance($amount);
                if (!$admin->isSuperAdmin()) {
                    $balanceLimit = BalanceLimit::where('admin_id', $admin->id)->first();
                    if ($balanceLimit) {
                        $balanceLimit->update(['amount' => $balanceLimit->amount + $amount]);
                    }
                }
            }

            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin update user balance');
            return redirect()->route('balances.index')->with("success_message", trans("Saved Successfully"));
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function transactions($id)
    {
        try {
            $user = $this->_userService->findById($id);
            return view('admin.balances.transactions', compact('user'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function getTransactionData(Request $request)
    {
        try {
            if (request()->ajax()) {
                return datatables()->of($this->_transactionService->getDtBalancesTransactionsDataByUser($request->all()))->toJson();
            }
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function typesLookup()
    {
        return [
            Config::get('constants.TRANSACTION_TYPE.DEPOSIT') => trans('Deposit'),
            Config::get("constants.TRANSACTION_TYPE.WITHDRAW") => trans('WithDraw')
        ];
    }

    public function usersLookup()
    {
        $admin = Auth::guard('admin')->user();

        if ($admin->accessType() == "created_users") {
            $users = $this->_userService->query();
            $users = $users->where('users.created_by_id', $admin->id);
            return $users->get()->pluck('full_user_name', 'id');
        }
        return $this->_userService->all()->pluck('full_user_name', 'id');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
