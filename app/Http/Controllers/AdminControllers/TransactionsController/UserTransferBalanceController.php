<?php

namespace App\Http\Controllers\AdminControllers\TransactionsController;

use App\Facades\HelperFacade;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\ServicesInterface\TransferBalanceServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;

class UserTransferBalanceController extends Controller
{

    private $_userService,$_transferBalanceService;
    public function __construct(UsersServiceInterface $userService,TransferBalanceServiceInterface $transferBalanceService)
    {
        $this->_userService = $userService;
        $this->_transferBalanceService = $transferBalanceService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!HelperFacade::checkAdminPermission("User Transferd Balance")){
            return redirect()->back();
                    }
        $usersLookup  = $this->usersLookup();
$adminsLookup = Admin::all()->pluck('username','id');

        if(request()->ajax()){
$data = $this->_transferBalanceService->getAdminDtData($request->all());
return datatables()->of($data)->make(true);
        }
        return view('admin.usertransferbalance.index',['usersLookup'=>$usersLookup,'adminsLookup'=>$adminsLookup]);
    }

    
    public function usersLookup()
    {
        return $this->_userService->lookupByAccessType();
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
