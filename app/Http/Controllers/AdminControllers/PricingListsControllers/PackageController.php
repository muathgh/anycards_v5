<?php

namespace App\Http\Controllers\AdminControllers\PricingListsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\CreatePackageRequest;
use App\Http\Requests\AdminRequests\UpdatePackageRequest;
use App\Models\Admin;
use App\ServicesInterface\PackageServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
class PackageController extends Controller
{

    private $_packageService;
    public function __construct(PackageServiceInterface $packageService)
    {
        $this->_packageService = $packageService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $adminsLookup = Admin::all()->pluck('username','id');
        if(request()->ajax())
        {
            
            return response()->json($this->_packageService->getDtData($request->all()));
        }
        return view('admin.packages.index',['adminsLookup'=>$adminsLookup]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        
        return view('admin.packages.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePackageRequest $request)
    {
        try{
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            if($this->_packageService->create([
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "created_by_id"=>Auth::guard("admin")->user()->id
            ])){
                activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin add new package');
                return redirect()->route('packages.index')->with('success_message',trans('Saved Successfully'));
            }
            return redirect()->back();
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id)
    {
        $package = $this->_packageService->findById($id);
        $adminsLookup = Admin::all()->pluck('username','id');
        return view('admin.packages.edit',compact("package",'adminsLookup'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(UpdatePackageRequest $request)
    {
        try{
            $id = $request->id;
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $adminId= $request->has('admin_id') && $request->admin_id ? $request->admin_id : null;
            if($this->_packageService->update($id,[
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                'created_by_id'=>$adminId,
                "updated_by_id"=>Auth::guard("admin")->user()->id
            ])){
                activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin update package with id'.$id);
                return redirect()->route('packages.index')->with('success_message',trans('Updated Successfully'));
            }

return redirect()->back();

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/packages/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            if(!$this->_packageService->delete($id))
            return redirect()->back()->with('error_message',trans('Deletion Failed'));
            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin delete package with id ' . $id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
