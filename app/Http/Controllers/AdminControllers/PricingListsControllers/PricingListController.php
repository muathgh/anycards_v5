<?php

namespace App\Http\Controllers\AdminControllers\PricingListsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\PackageServiceInterface;
use App\ServicesInterface\PricingListServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PricingListController extends Controller
{


    private $_pricingListService, $_packageService, $_cardTypeService, $_categoryService, $_subProviderService,$_providerService;
    public function __construct(
        PricingListServiceInterface $pricingListService,
        PackageServiceInterface $packageService,
        CardsTypesServiceInterface $cardTypeService,
        CategoryServiceInterface $categoryService,
        SubProviderServiceInterface $subProviderService,
        ProviderServiceInterface $providerSerivce
    ) {
        $this->_pricingListService = $pricingListService;
        $this->_packageService = $packageService;
        $this->_cardTypeService = $cardTypeService;
        $this->_categoryService = $categoryService;
        $this->_subProviderService = $subProviderService;
        $this->_providerService = $providerSerivce;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $adminsLookup = Admin::all()->pluck('username','id');
        if (request()->ajax()) {
            return datatables()->of($this->_pricingListService->getDtData($request->all()))->toJson();
        }
        return view('admin.pricinglists.index',['adminsLookup'=>$adminsLookup]);
    }

    public function getCardTypePackagePrice(Request $request)
    {
        if (request()->ajax()) {
            $packageId = $request->package_id;
            $cardTypeId = $request->card_type_id;
            $pricingList = $this->_pricingListService->getPricingListByDoubleCondition("package_id", "card_type_id", $packageId, $cardTypeId);
            if ($pricingList) {
                $price = $pricingList->price;
                return ResponseWrapperFacade::successWithData($price);
            }
            return ResponseWrapperFacade::success();
        }
    }

    public function getPricingListsFormAll(Request $request)
    {
        

        $admin = Auth::guard('admin')->user();
       $packageId = $request->package_id;
      
        $cardsTypes = collect();
        $categories = $this->_categoryService->all();
        $officalPricePackageId = $admin->offical_price_package_id;
        foreach($categories as $category){
            $providers = $category->providers()->orderBy('order','ASC')->get();
            foreach($providers as $provider){
                $subProviders = $provider->subProviders()->orderBy('order','ASC')->get();
                foreach($subProviders as $subProvider){
                   
                    $ctypes = $subProvider->cardsTypes()->orderBy('order','ASC')->get();
                    $cardsTypes = $cardsTypes->merge($ctypes);
                   
                
                   
                }
            }

        }

      
      
      
          
        $html =  view('admin.includes.pricinglist-all-create', compact('cardsTypes', 'packageId','officalPricePackageId'))->render();
        return ResponseWrapperFacade::successWithData($html);
       
  
       

     
    }
    public function getPricingListsForm(Request $request)
    {
        $admin =Auth::guard('admin')->user();
        $accessType = $admin->accessType();
        $packageCreatedById = null;
        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  :null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  :null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  :null;
        $officalPricePackageId = $admin->offical_price_package_id;
        $packageId = $request->package_id;
        $package = $this->_packageService->findById($packageId);
        if($package){
            $packageCreatedById = $package->created_by_id;
        }
        $cardsTypes = collect();
        if($subProviderId)
        {
        $subProvider = $this->_subProviderService->findById($subProviderId);
        $cardsTypes = $subProvider->cardsTypes()->orderBy("order","ASC")->get();
        }
        else if($providerId){
            $provider = $this->_providerService->findById($providerId);
            foreach($provider->subProviders()->orderBy('order','ASC')->get() as $s)

            {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order','ASC')->get());
            }
          
        }else if($categoryId){
            $category = $this->_categoryService->findById($categoryId);
            foreach($category->providers()->orderBy('order','ASC')->get() as $p)

            {
               foreach($p->subProviders()->orderBy('order','ASC')->get() as $s){
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order','ASC')->get());
               }
            }

        }
       
  
       

        $html =  view('admin.includes.pricinglist-create', compact('cardsTypes', 'packageId',"accessType",'packageCreatedById','officalPricePackageId'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     // V_5 UPDATES 26/12/2021
    public function create(Request $request)
    {

        $packageId = null;
        $copiedFrom = null;
        $admin = Auth::guard("admin")->user();
        $packages = $this->_packageService->all();
        $packagesLookup = [];
      foreach($packages as $p){
        if($admin->accessType()=="all"){
           $packagesLookup[$p->id]= $p->title_ar.'-'.$p->title_en;
        }else{
            if($p->createdBy)
            {
            if($p->createdBy->isSuperAdmin()){
                $packagesLookup[$p->id]=  $p->title_ar.'-'.$p->title_en;
            }else if($p->createdBy->id == $admin->id){
                $packagesLookup[$p->id]=  $p->title_ar.'-'.$p->title_en;
            }
        }
        
        
        }
      }
        if($request->has('package_id') && $request->package_id)
        {
            
        $packageId = $request->package_id;
        $copiedFrom = $this->_packageService->findById($packageId);
        $packagesLookup= [];
        foreach($packages as $p){
            if($p->createdBy)
            if($p->createdBy->id == $admin->id){
                $packagesLookup[$p->id]= $p->title_ar.'-'.$p->title_en;
            }
          }
        }

        $categoriesLookup = $this->_categoryService->lookup();

        $cardTypesLookup = $this->_cardTypeService->lookup();
        // $packagesLookup = $this->_packageService->lookupByAccessType($packageId);
     
     
        return view('admin.pricinglists.create', compact('packagesLookup', 'cardTypesLookup', 'categoriesLookup','packageId',"copiedFrom"));
    }

    // END UPDATES V_5 26/12/2021

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        try {
            $data = json_decode($request->data);
            $packageId = $request->package_id;
            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $price = $d->price ? $d->price : 0;
                $priceList = $this->_pricingListService->getPricingListByDoubleCondition('package_id', 'card_type_id', $packageId, $cardTypeId);
                if ($priceList) {
                    if ($this->_pricingListService->updateByCardTypeAndPackageId($packageId, $cardTypeId, [
                        "price" => $price,
                        "created_by_id"=>Auth::guard("admin")->user()->id

                    ])) {
                        activity()
                            ->performedOn(Auth::guard('admin')->user())
                            ->causedBy(Auth::guard('admin')->user())
                            ->log('Admin update pricing list');
                    }
                } else {
                    $this->_pricingListService->create([
                        "package_id" => $packageId,
                        "card_type_id" => $cardTypeId,
                        "price" => $price,
                        "updated_by_id"=>Auth::guard("admin")->user()->id
                    ]);
                    activity()
                        ->performedOn(Auth::guard('admin')->user())
                        ->causedBy(Auth::guard('admin')->user())
                        ->log('Admin create pricing list');
                }
            }
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = $this->_packageService->findById($id);
        $pricingLists = $this->_pricingListService->getPricingListByPackageId($id);
        $pricingList = $package->pricingLists()->first();

        $category =   $pricingList->cardType->subProvider->provider->category;
        $provider = $pricingList->cardType->subProvider->provider;
        $packagesLookup = $this->_packageService->lookup();
        $categoriesLookup  = $this->_categoryService->lookup();
        $providersLookup = $category->providers()->orderBy('order','ASC')->pluck('title_en', "id");
        $subProvidersLookup = $provider->subProviders()->orderBy('order','ASC')->pluck('title_en', "id");

        return view('admin.pricinglists.edit', compact('pricingList', 'pricingLists', 'packagesLookup', 'subProvidersLookup', 'providersLookup', 'categoriesLookup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $data = json_decode($request->data);
            foreach ($data as $d) {

                $price = $d->price ? $d->price : 0;
                if ($this->_pricingListService->update($d->id, [

                    "price" => $price
                ])) {
                    activity()
                        ->performedOn(Auth::guard('admin')->user())
                        ->causedBy(Auth::guard('admin')->user())
                        ->log('Admin create pricing list');
                }
            }
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{

            
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/pricinglists/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }

            $this->_pricingListService->delete($id);

            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin delete pricing list with package id'.$id);

            return redirect()->back();

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
