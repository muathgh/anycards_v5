<?php

namespace App\Http\Controllers\AdminControllers\PricingListsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\PriceOfSaleServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PriceOfSaleController extends Controller
{

    private $_priceOfSaleService, $_categoryService, $_providerService, $_subProviderService, $_cardTypeService;

    public function __construct(PriceOfSaleServiceInterface $priceOfSaleService, CategoryServiceInterface $categoryService, ProviderServiceInterface $providerService, SubProviderServiceInterface $subProviderService, CardsTypesServiceInterface $cardTypeService)
    {
        $this->_categoryService = $categoryService;
        $this->_priceOfSaleService = $priceOfSaleService;
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProviderService;
        $this->_cardTypeService = $cardTypeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            // return datatables()->of($this->_priceOfSaleService->getDtData($request->all()))->toJson();
           $data = $this->_priceOfSaleService->getDtData($request->all());
            return response()->json($data);
    
        }

        return view('admin.priceofsale.index');
    }

    public function getPrice(Request $request)
    {
        if (request()->ajax()) {
            $cardTypeId = $request->card_type_id;
            $priceOfSale = $this->_priceOfSaleService->getPriceOfSaleByCardTypeId($cardTypeId);
            return ResponseWrapperFacade::successWithData($priceOfSale ? $priceOfSale->price : null);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookup = $this->_categoryService->lookup();
        return view('admin.priceofsale.create', compact('lookup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = json_decode($request->data);
            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $price = $d->price ? $d->price : 0;
                $priceOfSale = $this->_priceOfSaleService->getPriceOfSaleByCardTypeId($cardTypeId);
                if ($priceOfSale) {
                    $this->_priceOfSaleService->updateByColumn("card_type_id", $cardTypeId, [
                        "price" => $price,
                        "updated_by_id" => Auth::guard("admin")->user()->id
                    ]);

                    activity()
                        ->performedOn(Auth::guard('admin')->user())
                        ->causedBy(Auth::guard('admin')->user())
                        ->log('Admin update priceofsale');
                } else if ($price != 0) {
                    $this->_priceOfSaleService->create([
                        "card_type_id" => $cardTypeId,
                        "price" => $price,
                        "created_by_id" => Auth::guard("admin")->user()->id
                    ]);

                    activity()
                        ->performedOn(Auth::guard('admin')->user())
                        ->causedBy(Auth::guard('admin')->user())
                        ->log('Admin create priceofsale');
                }
            }



            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $priceOfSale = $this->_priceOfSaleService->findById($id);

        $lookup = $this->_categoryService->lookup();
        $providersLookup = $this->_providerService->lookup();
        $subProvidersLookup = $this->_subProviderService->lookup();
        $cardTypeLookup = $this->_cardTypeService->lookup();
        return view('admin.priceofsale.edit', compact('priceOfSale', 'lookup', 'providersLookup', 'subProvidersLookup', 'cardTypeLookup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = $request->id;
            $price = $request->price;
            if ($this->_priceOfSaleService->update($id, ["price" => $price])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update price of sale with id' . $id);
                return redirect()->route('priceofsale.index')->with('success_message', trans('Updated Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    // V_6 UPDATES 06/01/2022
    public function getPriceOfSaleForm(Request $request)
    {

        $admin = Auth::guard('admin')->user();
        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  : null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  : null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  : null;
        $officalPricePackageId = $admin->offical_price_package_id;
        $packageId = $request->package_id;
        $cardsTypes = collect();
        if ($subProviderId) {
            $subProvider = $this->_subProviderService->findById($subProviderId);
            $cardsTypes = $subProvider->cardsTypes()->orderBy("order", "ASC")->get();
        } else if ($providerId) {
            $provider = $this->_providerService->findById($providerId);
            foreach ($provider->subProviders()->orderBy('order', 'ASC')->get() as $s) {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
            }
        } else if ($categoryId) {
            $category = $this->_categoryService->findById($categoryId);
            foreach ($category->providers()->orderBy('order', 'ASC')->get() as $p) {
                foreach ($p->subProviders()->orderBy('order', 'ASC')->get() as $s) {
                    $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
                }
            }
        }




        $html =  view('admin.includes.priceofsale-create', compact('cardsTypes','officalPricePackageId'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    // END V_6 UPDATES 06/01/2022

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {

            $id = $request->id;
            if (request()->ajax()) {

                $view = view('layouts.includes.delete-popup', ["id" => $id, "url" => url("/admin/priceofsale/destroy?id=" . $id)])->render();

                return ResponseWrapperFacade::successWithData(["html" => $view]);
            }
            $this->_priceOfSaleService->delete($id);
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete price of sale with id' . $id);
            return redirect()->back()->with('success_message', trans('Deleted Successfully'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
