<?php

namespace App\Http\Controllers\AdminControllers\PricingListsControllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\ServicesInterface\UsersServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\Http\Requests\AdminRequests\CreateCardPriceRequest;
use App\Http\Requests\AdminRequests\UpdateCardPriceRequest;
use App\Services\SubProviderService;

class UserAssigningPriceController extends Controller
{

    private $_cardTypeService,$_userService,$_subProvider;
    public function __construct(CardsTypesServiceInterface $cardsTypesService , UsersServiceInterface $userService , SubProviderService $subProvider )
    {
        $this->_cardTypeService = $cardsTypesService;
        $this->_userService = $userService;
        $this->_subProvider =$subProvider;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = $this->_cardTypeService->getCardsTypePricesDtData();
            return datatables()->of($data)->toJson();
        }
        return view('admin.userAssigningPrice.index');
    }

 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usersLookup = $this->_userService->all()->pluck('username', 'id');
        $cardsTypeLookup = $this->lookup();
        return view('admin.userAssigningPrice.create', compact('usersLookup', 'cardsTypeLookup'));
    }

    private function lookup()
    {

        $subProviders = $this->_subProvider->all();

        $data = [];

        foreach ($subProviders as $subProvider) {
            $items = [];
            if ($subProvider->cardsTypes()->count() > 0) {
                foreach ($subProvider->cardsTypes as $cardType) {

                    $items[$cardType->id] = $cardType->title_en;
                }

                $data[$subProvider->title_en] = $items;
            }
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCardPriceRequest $request)
    {
        try {
            $userId = $request->user_id;
            $cardTypeId = $request->card_type_id;
            $price = $request->price;
            $cardType = $this->_cardTypeService->findById($cardTypeId);
            if ($price == $cardType->price)
                return back()->withInput($request->only('user_id', 'card_type_id'))->with('price_error_message', trans('Price must be not equal to default price'));

            $prevPrice = $this->_cardTypeService->findCardPriceByDoubleCondition('user_id', "card_type_id", $userId, $cardTypeId);
            if ($prevPrice)
                return back()->withInput($request->only('user_id', 'card_type_id'))->with('duplicate_value_error_message', trans('User Already has price'));

            if ($this->_cardTypeService->createCardPrice([
                "user_id" => $userId,
                "card_type_id" => $cardTypeId,
                "price" => $price
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add a new card price');
                return redirect()->route('cardsprices.index');
            }
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cardPrice = $this->_cardTypeService->findCardPriceById($id);
        $usersLookup = $this->_userService->all()->pluck('username', 'id');
        $cardsTypeLookup = $this->lookup();
        return view('admin.userAssigningPrice.edit', compact('cardPrice', 'usersLookup', 'cardsTypeLookup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCardPriceRequest $request)
    {
        try {
            $id = $request->id;
            $userId = $request->user_id;
            $cardTypeId = $request->card_type_id;
            $price = $request->price;
            $cardType = $this->_cardTypeService->findById($cardTypeId);
            if ($price == $cardType->price)
                return back()->withInput($request->only('user_id', 'card_type_id'))->with('price_error_message', trans('Price must be not equal to default price'));

            // check if user has a prev value 
            $prevPrice = $this->_cardTypeService->findCardPriceByCondition("user_id", $userId)->where('card_type_id', $cardTypeId)->where('id', '!=', $id)->first();
            if ($prevPrice)
                return back()->withInput($request->only('user_id', 'card_type_id'))->with('duplicate_value_error_message', trans('User Already has price'));


            if ($this->_cardTypeService->updateCardPrice($id, [
                "user_id" => $userId,
                "card_type_id" => $cardTypeId,
                "price" => $price
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add a update card price with id' . $id);
                return redirect()->route('cardsprices.index');
            }
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {


            $this->_cardTypeService->deleteCardPrice($id);
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete card price with id ' . $id);
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
