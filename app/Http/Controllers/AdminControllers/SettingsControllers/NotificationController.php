<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\NotificationsServiceInterface;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    private $_notificationsService;
    public function __construct(NotificationsServiceInterface $notificationsService)
    {
        $this->_notificationsService = $notificationsService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!HelperFacade::checkAdminPermission("Notifications")){
            return redirect()->back();
        }
        $notifications = $this->_notificationsService->latest();
        return view('admin.notifications.index', compact("notifications"));
    }

    public function clearAll()
    {

        if (request()->ajax()) {
            $this->_notificationsService->clearAll();
            return ResponseWrapperFacade::success();
        }
    }

    public function clearNotification(Request $request)
    {
        $notificationId = $request->notification_id;
        $this->_notificationsService->clearNotification($notificationId);
        return ResponseWrapperFacade::success();
    }
    
    public function clearHeaderNotification(Request $request){
        $notificationId = $request->notification_id;
   
        $notification  = $this->_notificationsService->findById($notificationId);
       
                $this->_notificationsService->clearNotification($notificationId);
        if (filter_var($notification->href, FILTER_VALIDATE_URL)) { 
            return ResponseWrapperFacade::successWithData(["href"=>$notification->href]);
          }

          $href = route('notifications.index');
          return ResponseWrapperFacade::successWithData(["href"=>$href]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
