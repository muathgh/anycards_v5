<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\CreateCardLimitRequest;
use App\Http\Requests\AdminRequests\UpdateCardLimitRequest;
use App\ServicesInterface\CardsLimitServiceInterface;
use App\ServicesInterface\CardTypePointsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class CardsLimitController extends Controller
{

    private $_cardsLimitService,$_categoryService,$_cardTypeService,$_subProviderService,$_providerService;
    public function __construct(CardsLimitServiceInterface $cardsLimitService,CategoryServiceInterface $categoryService,SubProviderServiceInterface $subProviderService,
    ProviderServiceInterface $providerService,CardTypePointsServiceInterface $cardTypeService)
    {
        $this->_cardsLimitService = $cardsLimitService;
        $this->_categoryService = $categoryService;
        $this->_subProviderService = $subProviderService;
        $this->_providerService = $providerService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    if(request()->ajax()){
        return datatables()->of($this->_cardsLimitService->getDtData())->toJson();
    }
    return view('admin.cardLimits.index');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookup = $this->lookup();
        return view('admin.cardLimits.create',compact('lookup'));

    }

    private function lookup()
    {
        $data = $this->_categoryService->lookup();
        return $data;
    }
    public function getValue(Request $request){
        if(request()->ajax()){
            $cardTypeId = $request->card_type_id;
            $cardLimit = $this->_cardsLimitService->getCardLimitByCardTypeId($cardTypeId);
            return ResponseWrapperFacade::successWithData($cardLimit ? $cardLimit->value : null);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $data = json_decode($request->data);
            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $value = $d->value ? $d->value : 0;
                $priceOfSale = $this->_cardsLimitService->getCardLimitByCardTypeId($cardTypeId);
                if($priceOfSale){
                    $this->_cardsLimitService->updateByColumn("card_type_id",$cardTypeId,[
                        "value"=>$value
                    ]);

                    activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update cardlimit');
                }else{
                    $this->_cardsLimitService->create([
                        "card_type_id"=>$cardTypeId,
                        "value"=>$value
                    ]);

                    activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin create cardlimit');
                }
            }
            // 
           return ResponseWrapperFacade::success();

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function getCardsLimitForm(Request $request)
    {
        
        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  :null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  :null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  :null;
       
        $packageId = $request->package_id;
        $cardsTypes = collect();
        if($subProviderId)
        {
        $subProvider = $this->_subProviderService->findById($subProviderId);
        $cardsTypes = $subProvider->cardsTypes()->orderBy("order","ASC")->get();
        }
        else if($providerId){
            $provider = $this->_providerService->findById($providerId);
            foreach($provider->subProviders()->orderBy('order','ASC')->get() as $s)

            {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy("order","ASC")->get());
            }
          
        }else if($categoryId){
            $category = $this->_categoryService->findById($categoryId);
            foreach($category->providers()->orderBy('order','ASC')->get() as $p)

            {
               foreach($p->subProviders()->orderBy('order','ASC')->get() as $s){
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy("order","ASC")->get());
               }
            }

        }
       
  
       

        $html =  view('admin.includes.cardslimit-create', compact('cardsTypes'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cardLimit = $this->_cardsLimitService->findById($id);
        $lookup = $this->lookup();
        $category =   $cardLimit->cardType->subProvider->provider->category;
        $provider = $cardLimit->cardType->subProvider->provider;
        $subProvider = $cardLimit->cardType->subProvider;

        $providersLookup = $category->providers()->orderBy('order','ASC')->pluck('title_en', "id");
        $subProvidersLookup = $provider->subProviders()->orderBy('order','ASC')->pluck('title_en', "id");
        $cardTypeLookup = $subProvider->cardsTypes()->orderBy('order','ASC')->pluck('title_en', "id");
        return view('admin.cardLimits.edit',compact('cardLimit', 'lookup', 'providersLookup', 'subProvidersLookup', 'cardTypeLookup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCardLimitRequest $request)
    {
        try{
            $id = $request->id;
            $cardTypeId = $request->card_type_id;
            $value = $request->value;
            if($this->_cardsLimitService->update($id,["
            card_type_id"=>$cardTypeId,"value"=>$value])){
                activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin add a update card limit with id '.$id);
            return redirect()->route('cardslimit.index')->with("success_message",trans("Updated Successfully"));
            }
            return redirect()->back();
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/notifications/cardslimit/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $this->_cardsLimitService->delete($id);
            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin delete card limit with id '.$id);
            return redirect()->back()->with("success_message",trans("Deleted Successfully"));
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
