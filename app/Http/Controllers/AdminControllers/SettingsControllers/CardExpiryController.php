<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;

use App\ServicesInterface\CardExpiryServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class CardExpiryController extends Controller
{

    private $_cardExpiryService,$_categoryService,$_cardTypeService,$_subProviderService,$_providerService;

    public function __construct(CardExpiryServiceInterface $cardExpiryService,CategoryServiceInterface $categoryService,SubProviderServiceInterface $subProviderService,
    ProviderServiceInterface $providerService)
    {
        $this->_cardExpiryService = $cardExpiryService;
        $this->_categoryService = $categoryService;
        $this->_subProviderService = $subProviderService;
        $this->_providerService = $providerService;
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    if(request()->ajax()){
        return datatables()->of($this->_cardExpiryService->getDtData())->toJson();
    }
    return view('admin.cardExpiry.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookup = $this->lookup();
        return view('admin.cardExpiry.create',compact('lookup'));

    }

    private function lookup()
    {
        $data = $this->_categoryService->lookup();
        return $data;
    }

    public function store(Request $request)
    {
        try{

            $data = json_decode($request->data);
            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $value = $d->value ? $d->value : 0;
                $priceOfSale = $this->_cardExpiryService->getCardExpiryByCardTypeId($cardTypeId);
                if($priceOfSale){
                    $this->_cardExpiryService->updateByColumn("card_type_id",$cardTypeId,[
                        "value"=>$value
                    ]);

                    activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update cardExpiry');
                }else if($value !=0){
                    $this->_cardExpiryService->create([
                        "card_type_id"=>$cardTypeId,
                        "value"=>$value
                    ]);

                    activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin create cardExpiry');
                }
            }
            // 
           return ResponseWrapperFacade::success();

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function getCardsExpiryForm(Request $request)
    {
        
        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  :null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  :null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  :null;
       
        $packageId = $request->package_id;
        $cardsTypes = collect();
        if($subProviderId)
        {
        $subProvider = $this->_subProviderService->findById($subProviderId);
        $cardsTypes = $subProvider->cardsTypes()->orderBy("order","ASC")->get();
        }
        else if($providerId){
            $provider = $this->_providerService->findById($providerId);
            foreach($provider->subProviders()->orderBy('order','ASC')->get() as $s)

            {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy("order","ASC")->get());
            }
          
        }else if($categoryId){
            $category = $this->_categoryService->findById($categoryId);
            foreach($category->providers()->orderBy('order','ASC')->get() as $p)

            {
               foreach($p->subProviders()->orderBy('order','ASC')->get() as $s){
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy("order","ASC")->get());
               }
            }

        }
       
  
       

        $html =  view('admin.includes.cardsexpiry-create', compact('cardsTypes'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/notifications/cardsexpiry/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $this->_cardExpiryService->delete($id);
            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin delete card expiry with id '.$id);
            return redirect()->back()->with("success_message",trans("Deleted Successfully"));
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
