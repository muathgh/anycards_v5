<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\CardQuantityLimitServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
class CardsQuantityLimitController extends Controller {
   private $_cardQuantityLimitService,$_categoriesService,$_providerService,$_subProviderService;
    public function __construct(CardQuantityLimitServiceInterface $cardQuantityLimitService,CategoryServiceInterface $categoriesService,
    ProviderServiceInterface $providerService,SubProviderServiceInterface $subProviderService)
    {
        $this->_cardQuantityLimitService= $cardQuantityLimitService;
        $this->_categoriesService = $categoriesService;
        $this->_providerService= $providerService;
        $this->_subProviderService = $subProviderService;
    }

    public function index(Request $request){
        if (request()->ajax()) {
           $data = $this->_cardQuantityLimitService->getDtData($request->all());
            return response()->json($data);
    
        }

        return view('admin.cardquantitylimits.index');
    }

    public function create(){
        $lookup = $this->_categoriesService->lookup();
        return view('admin.cardquantitylimits.create', compact('lookup'));
    }

    public function store(Request $request){
        try{
            $data = json_decode($request->data);
            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $quantity = $d->quantity ? $d->quantity : 0;
                $quantityLimit = $this->_cardQuantityLimitService->getQuantityByCardType($cardTypeId);
                if($quantityLimit){
                    $this->_cardQuantityLimitService->updateByColumn("card_type_id", $cardTypeId, [
                        "quantity_limit" => $quantity,
                      
                    ]);
                }
                else if($quantity != 0) {
                    $this->_cardQuantityLimitService->create([
                        "card_type_id" => $cardTypeId,
                        "quantity_limit" => $quantity,
                    ]);
                }

            }
            return ResponseWrapperFacade::success();
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function getQuantityLimitForm(Request $request)
    {

        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  : null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  : null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  : null;

        $packageId = $request->package_id;
        $cardsTypes = collect();
        if ($subProviderId) {
            $subProvider = $this->_subProviderService->findById($subProviderId);
            $cardsTypes = $subProvider->cardsTypes()->orderBy("order", "ASC")->get();
        } else if ($providerId) {
            $provider = $this->_providerService->findById($providerId);
            foreach ($provider->subProviders()->orderBy('order', 'ASC')->get() as $s) {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
            }
        } else if ($categoryId) {
            $category = $this->_categoriesService->findById($categoryId);
            foreach ($category->providers()->orderBy('order', 'ASC')->get() as $p) {
                foreach ($p->subProviders()->orderBy('order', 'ASC')->get() as $s) {
                    $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
                }
            }
        }




        $html =  view('admin.includes.quantitylimit-create', compact('cardsTypes'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    public function destroy(Request $request)
    {
        try {

            $id = $request->id;
            if (request()->ajax()) {

                $view = view('layouts.includes.delete-popup', ["id" => $id, "url" => url("/admin/notifications/quantitylimit/destroy?id=" . $id)])->render();

                return ResponseWrapperFacade::successWithData(["html" => $view]);
            }
            $this->_cardQuantityLimitService->delete($id);
      
            return redirect()->back()->with('success_message', trans('Deleted Successfully'));
        } catch (\Exception $e) {
           
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}