<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\HelperFacade;
use App\Facades\SettingFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\ServicesInterface\SettingsServiceInterface;
use App\Http\Requests\AdminRequests\SettingsRequest;
use App\Http\Requests\AdminRequests\UserLimitBalanceNotificationRequest;

class SettingController extends Controller
{

    private $_settingsService;
    public function __construct(SettingsServiceInterface $settingsService)
    {
        $this->_settingsService = $settingsService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!HelperFacade::checkAdminPermission("General Settings")){
            return redirect()->back();
                    }
        $maxNotificationTitle = SettingFacade::getValueByKey("MAX_NOTIFICATION_TITLE");
        $maxNotificationDescription = SettingFacade::getValueByKey("MAX_NOTIFICATION_DESCRIPTION");
        $userBalanceMinLimit = SettingFacade::getValueByKey("USER_BALANCE_MIN_LIMIT");
        return view('admin.settings.index',["maxNotificationTitle"=>$maxNotificationTitle,"maxNotificationDescription"=>$maxNotificationDescription,"userBalanceMinLimit"=>$userBalanceMinLimit]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cardsZeroValue = $this->_settingsService->getValueByKey(Config::get('constants.SETTINGS.CARDS_ZERO_VALUE'));
        return view('admin.settings.create',compact('cardsZeroValue'));
    }


public function pluginDownload(){
    $file = public_path() . "/storage/Software/plugin.zip";
    $headers = array('Content-Type: application/zip');
     
        return response()->download($file, 'printing_plugin.zip', $headers);
}

public function applicationDownload(){

    $file = public_path() . "/storage/Application/anycards.apk";
    $headers = array('Content-Type: application/vnd.android.package-archive');
 
    return response()->download($file, 'anycards.apk', $headers);

}


 public function pluginDownload64(){

        $file = public_path() . "/storage/Software/plugin_64.zip";
        $headers = array('Content-Type: application/zip');
     
        return response()->download($file, 'printing_plugin_64.zip', $headers);

    }

    public function pluginDownload32(){

        $file = public_path() . "/storage/Software/plugin_32.zip";
        $headers = array('Content-Type: application/zip');
        return response()->download($file, 'printing_plugin_32.zip', $headers);

    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingsRequest $request)
    {
        try{

            $maxNotificationTitle = $request->max_notification_title;
            $maxNotificationDescription = $request->max_notification_description;
            $userBalanceMinLimit = $request->user_balance_min_limit;
            SettingFacade::setValueByKey("MAX_NOTIFICATION_TITLE",$maxNotificationTitle);
            SettingFacade::setValueByKey("MAX_NOTIFICATION_DESCRIPTION",$maxNotificationDescription);
            SettingFacade::setValueByKey("USER_BALANCE_MIN_LIMIT",$userBalanceMinLimit);

                return redirect()->back()->with('success_message','Saved Successfully');
          



        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
 //V_6 UPDATES 04/01/2022
    public function userBalanceLimitNotification(){
        $userBalanceNotificationAr = SettingFacade::getValueByKey("USER_BALANCE_MIN_LIMIT_NOTIFICATION_AR");
        $userBalanceNotificationEn = SettingFacade::getValueByKey("USER_BALANCE_MIN_LIMIT_NOTIFICATION_EN");
return view('admin.settings.userbalancelimit',['userBalanceNotificationAr'=>$userBalanceNotificationAr,'userBalanceNotificationEn'=>$userBalanceNotificationEn]);
    }
    //END V_6 UPDATES 04/01/2022
//V_6 UPDATES 04/01/2022
    public function userBalanceLimitStore(UserLimitBalanceNotificationRequest $request){
        try{
$notificatinAr = $request->notification_ar;
$notificationEn = $request->notification_en ;

SettingFacade::setValueByKey("USER_BALANCE_MIN_LIMIT_NOTIFICATION_AR",$notificatinAr);
SettingFacade::setValueByKey("USER_BALANCE_MIN_LIMIT_NOTIFICATION_EN",$notificationEn);

    return redirect()->back()->with('success_message','Saved Successfully');

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
    //END V_6 UPDATES 04/01/2022

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
