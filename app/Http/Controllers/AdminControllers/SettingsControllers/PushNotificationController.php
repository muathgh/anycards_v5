<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\PushNotificationRequest;
use App\Models\SettingModels\UserNotification;
use App\ServicesInterface\NotificationsServiceInterface;
use Illuminate\Http\Request;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Support\Facades\Log;

class PushNotificationController extends Controller
{


    private $_userService;
    public function __construct(UsersServiceInterface $userService)
    {
        $this->_userService = $userService;
    }
    public function index()
    {
        if(!HelperFacade::checkAdminPermission("Push Notifications")){
            return redirect()->back();
        }
        $usersLookup = $this->_userService->lookUpFullNameAccessType();
        return view('admin.pushnotifications.index', ["usersLookup" => $usersLookup]);
    }

    public function store(PushNotificationRequest $request)
    {
        try {

            $title = $request->title;
            $description = $request->description;
            $user_ids = $request->has('user_id') ? $request->user_id : null;
            $users = $user_ids ? $this->_userService->whereIdsIn($user_ids) :
                $this->_userService->usersByAccessType();
         
          $rows =   $users->map(function ($u) use ( $title, $description) {
                $item = [
                    "title_en" => $title,
                    "title_ar" => $title,
                    "description_en" => $description,
                    "description_ar" => $description,
                    "user_id"=>$u->id,
                    "type" => "notification"
                ];
                return $item;
            });

            UserNotification::insert($rows->toArray());
            


            foreach ($users as $user) {
                $user->sendFcmNotification($title, $description);
            }


            return redirect()->back()->with('success_message', 'Send Successfully');
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
}


