<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\UploadFileFacade;
use App\Http\Controllers\Controller;
use App\Models\CsvGroupImport\OrangeGroupImport;
use App\Models\CsvGroupImport\OtherGroupImport;
use App\Models\CsvGroupImport\UmniahGroupImport;
use App\Models\CsvGroupImport\ZainGroupImport;
use App\ServicesInterface\CategoryServiceInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CsvController extends Controller
{

    private $_categoryService, $_zainGroupImport,$_orangeGroupImport,$_umniahGroupImport,$_otherGroupImport;
    public function __construct(CategoryServiceInterface $categoryService, ZainGroupImport $zainGroupImport
    ,OrangeGroupImport $orangeGroupImport,UmniahGroupImport $umniahGroupImport,OtherGroupImport $otherGroupImport)
    {
        $this->_categoryService = $categoryService;
        $this->_zainGroupImport = $zainGroupImport;
        $this->_orangeGroupImport = $orangeGroupImport;
        $this->_umniahGroupImport = $umniahGroupImport;
        $this->_otherGroupImport = $otherGroupImport;
    }
    // V_5 UPDATES 30/12/2021
    public function create()
    {

        $categoryLookup = $this->_categoryService->lookup();
        $lookup = [
            'zain' => 'Zain',
            'umniah' => 'Umniah',
            'orange' => 'Orange',
            "other" => "Other"
        ];
        return view('admin.csv.create', compact('lookup', 'categoryLookup'));
    }

    // END V_5 UPDATES 30/12/2021
  // V_5 UPDATES 30/12/2021
    public function store(Request $request)
    {
        try {
            $extensions = array("csv");
            $validator = Validator::make(
                [
                    'csv_type' => $request->csv_type,
                    
                    'csv'      => $request->csv,
                ],
                [
                    'csv_type' => 'required',
                   
                    'csv'          => 'required',

                ],
                [
                    "csv_type.required" => trans('This field is required'),
                    "csv.required" => trans('This field is required'),


                ]

            );

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->only('csv_type'));
            }

            $uploadedFileExtension = array($request->file('csv')->getClientOriginalExtension());

            if (!in_array($uploadedFileExtension[0], $extensions)) {
                return redirect()->back()->withInput($request->only('csv_type'))->with('csv_error', trans('File must be an csv'));
            }
            Session::remove('warning_message');
            Session::remove('duplicate_cards');
            $result = UploadFileFacade::upload($request->file('csv'), $request->csv_type);
            $path = $result['full_path'];
            $dir = $result['dir'];
            if($request->csv_type =="zain")
            $validate = $this->_zainGroupImport->upload($request->file('csv'));
            else if($request->csv_type=="orange")
            $validate = $this->_orangeGroupImport->upload($request->file('csv'));
            else if($request->csv_type =="umniah")
            $validate = $this->_umniahGroupImport->upload($request->file('csv'));
            else if($request->csv_type=="other")
            $validate = $this->_otherGroupImport->upload($request->file('csv'));

     

if($validate == "invalid file"){
    return redirect()->back()->with('error_message','Please select correct file type')->withInput();
}
            UploadFileFacade::remove($request->csv_type, $dir);
            if (Session::has('warning_message')) {
                $warnings = Session::get('warning_message');
                $warnings = array_unique($warnings);
                if (in_array("duplicate_cards", $warnings)) {
                   
                    
                    $request->session()->flash("duplicated_cards", Session::get("duplicate_cards"));
                }
                return redirect()->back()->with('warning_message', $warnings);
            }

            return redirect()->back()->with('success_message', 'Uploaded Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error_message',$e->getMessage());
        }
    }

    // END V_5 UPDATES 30/12/2021
}
