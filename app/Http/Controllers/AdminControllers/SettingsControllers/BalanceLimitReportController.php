<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use App\ServicesInterface\BalanceLimitServiceInterface;

class BalanceLimitReportController extends Controller
{

    private $_balanceLimitService;
    public function __construct(BalanceLimitServiceInterface $balanceLimitService)
    {
        $this->_balanceLimitService = $balanceLimitService;
        
    }
    public function index(Request $request){
try{
    $data = $request->all();
    if(request()->ajax()){
        $transactions = $this->_balanceLimitService->getDtData($data);

        return response()->json($transactions);
    }

return view('admin.balanceLimitReport.index');
}
catch(\Exception $e){

}
        

    }

}