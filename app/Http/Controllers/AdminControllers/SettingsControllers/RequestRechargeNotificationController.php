<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\RequestRechargeNotificationServiceInterface;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RequestRechargeNotificationController extends Controller
{

    private $_notificationsService;
    public function __construct(RequestRechargeNotificationServiceInterface $notificationsService)
    {
        $this->_notificationsService = $notificationsService;
    }
 
    //V_5 UPDATES 26/12/2021
    //V_5 UPDATES 27/12/2021
    public function index()
    {
        if(!HelperFacade::checkAdminPermission("Request Balance Notifications")){
            return redirect()->back();
        }
        if(!Auth::guard('admin')->user()->isSuperAdmin())
        $notifications = DB::table('request_recharge_notifications')->join('users','users.id','=','request_recharge_notifications.user_id')
        ->where('users.created_by_id',Auth::guard('admin')->user()->id)->select("request_recharge_notifications.is_cleared","request_recharge_notifications.created_at as created_at",'request_recharge_notifications.description_en as description_en','request_recharge_notifications.description_ar as description_ar','request_recharge_notifications.href as href','request_recharge_notifications.id as id','request_recharge_notifications.title_en as title_en','request_recharge_notifications.title_ar as title_ar')
        ->latest('request_recharge_notifications.created_at')->get();
        else 
        $notifications = DB::table('request_recharge_notifications')->select("request_recharge_notifications.is_cleared","request_recharge_notifications.created_at as created_at",'request_recharge_notifications.description_en as description_en','request_recharge_notifications.description_ar as description_ar','request_recharge_notifications.href as href','request_recharge_notifications.id as id','request_recharge_notifications.title_en as title_en','request_recharge_notifications.title_ar as title_ar')
        ->latest('request_recharge_notifications.created_at')->get();
        return view('admin.request_recharge_notifications.index', compact("notifications"));
    }
    //END V_5 UPDATES 26/12/2021
    //END V_5 UPDATES 27/12/2021
    

    public function clearAll()
    {

        if (request()->ajax()) {
            $this->_notificationsService->clearAll();
            return ResponseWrapperFacade::success();
        }
    }

    public function clearNotification(Request $request)
    {
        $notificationId = $request->notification_id;
       
        $this->_notificationsService->clearNotification($notificationId);
        return ResponseWrapperFacade::success();
    }

    public function clearHeaderNotification(Request $request){
        $notificationId = $request->notification_id;
        $notification = $this->_notificationsService->findById($notificationId);
        $this->_notificationsService->clearNotification($notificationId);
        return ResponseWrapperFacade::successWithData([
            "href"=>$notification->href,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
