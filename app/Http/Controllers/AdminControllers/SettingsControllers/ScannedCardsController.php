<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\SettingsServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ScannedCardsController extends Controller
{

    private $_cardService, $_categoryService,$_settingService;
    public function __construct(CardsServiceInterface $cardService,CategoryServiceInterface $categoryService,SettingsServiceInterface $settingService)
    {
        $this->_cardService = $cardService;
        $this->_categoryService = $categoryService;
        $this->_settingService =$settingService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(request()->ajax()){
            $data = DB::table('scanned_cards')->select("id","code","serial","created_at")->get();
            return ResponseWrapperFacade::successWithData($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!HelperFacade::checkAdminPermission("Scanned Cards")){
return redirect()->back();
        }
        $lookup = $this->lookup();
        $isScanStarted = $this->_settingService->getValueByKey('START_SCAN');
        return view('admin.scannedCards.create',compact('lookup','isScanStarted'));

    }

    private function lookup()
    {
        $data = $this->_categoryService->lookup();
        return $data;
    }

    public function startScan(){
        $prev = $this->_settingService->getValueByKey("START_SCAN");
        $new = $prev == 0 ? 1 : 0;
        $this->_settingService->setValueByKey("START_SCAN",$new);
        return ResponseWrapperFacade::successWithData($new);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // get start scan value
           
            $cardTypeId = $request->card_type_id;
            $expiryDate = $request->expiry_date;
            $data = json_decode($request->data);
            $duplicate = [];
            foreach ($data as $d) {
                $code = $d->code;
                $serial = $d->serial;
            $card = $this->_cardService->getFirstCardByCondition("code",$code);
            if(!$card)
            {
                $this->_cardService->create([
                    "card_type_id" => $cardTypeId,
                    "expiry_date" => $expiryDate,
                    "serial" => $serial,
                    "code" => $code,
                    "created_by_id"=>Auth::guard("admin")->user()->id
                ]);
            }else{
                $duplicate[] = $card->code;
            }
        }

        DB::table('scanned_cards')->delete();
        $this->_settingService->setValueByKey('START_SCAN',0);

        if (sizeof($duplicate) > 0)
        {
            $view = view("admin.cards.duplicatedcodes",["duplicates"=>$duplicate])->render();
            return ResponseWrapperFacade::successWithData(["html"=>$view,"title"=>"Duplicated Cards"]);
        }






            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function clearData(){
        try{
            DB::table('scanned_cards')->delete();
            return ResponseWrapperFacade::success();

        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
