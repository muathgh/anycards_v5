<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\BalanceLimitRequest;
use App\Models\Admin;
use App\Models\SettingModels\BalanceLimit;
use App\Models\TransactionModels\AdminBalanceLimitTransaction;
use App\Models\TransactionModels\Transaction;
use App\ServicesInterface\TransactionsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BalanceLimitController extends Controller
{

    private $_transactionsService;
    public function __construct(TransactionsServiceInterface $transactionsService)
    {
        $this->_transactionsService = $transactionsService;
    }
    public function index(Request $request)
    {
       
        if ($request()->ajax()) {
            $params = $request->all();
            $draw = $params['draw'];
            $start = $params['start'];
            $rowperpage  = $params['length'];


            $search  = $params['search'];
            $searchvalue = $search['value'];
            $records = DB::table('balance_limits')->join('admins', 'admins.id', '=', 'balance_limits.admin_id')
                ->select('admins.username as username', 'balance_limits.amount as amount', 'balance_limits.id as id');
            $totalRecords = $records->count();

            $records = $records->where(function ($query) use ($searchvalue) {
                $query->where('admins.username', 'like', '%' . $searchvalue . '%')
                    ->orWhere('balance_limits.amount', 'like', '%' . $searchvalue . '%');
            });

            $totalRecordsWithFilter = $records->count();
            if ($rowperpage != -1)
                $records = $records->skip($start)->take($rowperpage);

            return response()->json(array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordsWithFilter,
                "aaData" => $records
            ));
        }
    }


    public function create(Request $request)
    {
       
        $typesLookup = $this->typesLookup();
        $data = [];
        $admins = Admin::all();
        foreach($admins as $admin){
            if(!$admin->isSuperAdmin()){
                $data[]=$admin;
            }
        }
        $admins = collect($data)->pluck('username','id');
        return view('admin.balanceLimit.create', ['adminsLookup' => $admins,'typesLookup'=>$typesLookup]);
    }

    public function typesLookup()
    {
        return [
            Config::get('constants.TRANSACTION_TYPE.DEPOSIT_ADMIN_BALANCE') => trans('Deposit'),
            Config::get("constants.TRANSACTION_TYPE.WITHDRAW_ADMIN_BALANCE") => trans('WithDraw')
        ];
    }

    public function store(BalanceLimitRequest $request)
    {
        try {
            Config::get('constants.TRANSACTION_TYPE.DEPOSIT_ADMIN_BALANCE');
            $admin = Auth::guard('admin')->user();
            $adminId = $request->admin_id;
            $amount = $request->amount;
            $type = $request->type;
            $lastTransaction = AdminBalanceLimitTransaction::where('type',$type)->
            where('amount',number_format((float)$amount, 2, '.', ''))->where('admin_id',$adminId)->latest()->first();

            if($lastTransaction && $lastTransaction->next_transaction){
                if (\Carbon\Carbon::now()->lte(\Carbon\Carbon::parse($lastTransaction->next_transaction))) {
                    return redirect()->back()->with('error_message', 'You have to wait 5 minutes to do the same operation');
                }
            }

            $balanceLimit = BalanceLimit::where('admin_id', $adminId)->first();
            
            if ($balanceLimit) {
                $prevAmount = $balanceLimit->amount;
                if($type == Config::get('constants.TRANSACTION_TYPE.DEPOSIT_ADMIN_BALANCE') )
                $balanceLimit->update(['amount' => ($prevAmount+ $amount )]);
                else 
                $balanceLimit->update(['amount' => ($prevAmount - $amount  ) > 0 ? ($prevAmount - $amount  ) : 0]);
                AdminBalanceLimitTransaction::create([
                    "admin_id"=>$adminId,
                    "amount"=>$amount,
                    "type"=>$type,
                    "next_transaction"=>\Carbon\Carbon::now()->addMinutes(5),
                    "created_by_id"=>$admin->id
                ]);
         
                
            } else {
                if($type == Config::get('constants.TRANSACTION_TYPE.WITHDRAW_ADMIN_BALANCE') )
                return redirect()->back()->with('error_message','There is no initial balance');

                BalanceLimit::create(['admin_id' => $adminId, 'amount' => $amount]);

                AdminBalanceLimitTransaction::create([
                    "admin_id"=>$adminId,
                    "amount"=>$amount,
                    "type"=>$type,
                    "next_transaction"=>\Carbon\Carbon::now()->addMinutes(5),
                    "created_by_id"=>$admin->id
                ]);
            }
            return redirect()->back()->with('success_message', 'Saved Successfully');
        } catch (\Exception $e) {

            return redirect()->back();
        }
    }
    

    public function notePopup(Request $request)
    {
        $id = $request->id;
     
        $html = view('admin.balanceLimit.notes', ["id"=>$id])->render();
        return ResponseWrapperFacade::successWithData($html);
    }
    


    public function transactions(Request $request){
        $admin = Admin::find($request->id);
     
        if(request()->ajax()){
            $data = $request->all();
            $userId = $data['id'];
            $draw = $data['draw'];
            $start = $data['start'];
            $rowperpage  = $data['length'];
           
            
            $search  = $data['search'];

         

        
        // $transactions = AdminBalanceLimitTransaction::where('admin_id',$request->id);
        $transactions = DB::table("admin_balance_limit_transactions")->leftJoin('admins','admins.id','=','admin_balance_limit_transactions.created_by_id')
        ->select("admin_balance_limit_transactions.admin_id as admin_id","admin_balance_limit_transactions.id as id",'admin_balance_limit_transactions.type as type','admin_balance_limit_transactions.amount as amount','admin_balance_limit_transactions.created_at as created_at','admin_balance_limit_transactions.notes as notes','admins.username as username','admin_balance_limit_transactions.received_amount as received_amount');
       $transactions= $transactions->where('admin_balance_limit_transactions.admin_id',$admin->id);
        if($data['from_date'] && $data['to_date']){
            $transactions = $transactions->whereDate('admin_balance_limit_transactions.created_at','>=',\Carbon\Carbon::parse($data['from_date']))->
            whereDate('admin_balance_limit_transactions.created_at','<=',\Carbon\Carbon::parse($data['to_date']));
        }else{
            $transactions = $transactions->whereYear('admin_balance_limit_transactions.created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('admin_balance_limit_transactions.created_at',\Carbon\Carbon::now()->month)
            ->whereDay('admin_balance_limit_transactions.created_at',\Carbon\Carbon::now()->day);
        }
        $transactions = $transactions->latest('admin_balance_limit_transactions.created_at');
        $totalRecords = $transactions->count();
        $totalRecordsWithFilter = $totalRecords;
        if($rowperpage != -1)
        $transactions = $transactions->skip($start)->take($rowperpage);
        $data = $transactions->get()->map(function($t){
$item['amount'] = $t->type == "withdraw_admin_balance"  ? HelperFacade::convertNumber2Negative($t->amount) : $t->amount;
$item['type'] = $t->type;
$item['username'] = $t->username;
$item['created_at'] = $t->created_at;
$item['id'] = $t->id;
$item['notes'] = $t->notes;
$item['received_amount'] = $t->received_amount;
return $item;
        });
        return datatables()->of($data)->toJson();
    }
        return view('admin.balanceLimit.transactions',['admin'=>$admin]);

    }
  
    

      
      public function storeNote(Request $request){
          try{
          $id = $request->id;
          $notes = $request->notes;
          $received_amount = $request->received_amount;
          $balanceLimitTransaction = AdminBalanceLimitTransaction::where("id",$id)->first();
          if($balanceLimitTransaction){
              $balanceLimitTransaction->notes = $notes;
              $balanceLimitTransaction->received_amount = $received_amount;
              $balanceLimitTransaction->save();
          }
          return ResponseWrapperFacade::success();
        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
        
    }
    

    public function getAmount(Request $request)
    {
        $balanceLimit = BalanceLimit::where('admin_id', $request->id)->first();
        return ResponseWrapperFacade::successWithData($balanceLimit);
    }
}
