<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\WholePriceRequest;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\WholesalePriceServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;
class WholesalePriceController extends Controller
{

    private $_categoryService,$_providerService,$_subProviderService,$_wholeSalePriceService;
    public function __construct(CategoryServiceInterface $categoryService,ProviderServiceInterface $providerService,SubProviderServiceInterface $subProviderService,WholesalePriceServiceInterface $wholeSalePriceService)
    {
        $this->_categoryService = $categoryService;
        $this->_wholeSalePriceService = $wholeSalePriceService;
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProviderService;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(request()->ajax()){
        return response()->json($this->_wholeSalePriceService->getDtData($request->all()));
        }
        return view('admin.wholeprice.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesLookup = $this->_categoryService->lookup();
        return view('admin.wholeprice.create',["categoriesLookup"=>$categoriesLookup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function getPrice(Request $request){
        $cardTypeId = $request->card_type_id;
        $wholeSalePrice = $this->_wholeSalePriceService->getByCondition("card_type_id",$cardTypeId);
        return ResponseWrapperFacade::successWithData($wholeSalePrice ? $wholeSalePrice->price : null);

    }
    public function store(Request $request)
    {
        try{
            $data = json_decode($request->data);

            foreach ($data as $d) {
                $cardTypeId = $d->card_type_id;
                $price = $d->price ? $d->price : 0;
                $wholePrice = $this->_wholeSalePriceService->getByCondition("card_type_id",$cardTypeId);
                if ($wholePrice) {
                    if ($this->_wholeSalePriceService->updateByColumn('card_type_id', $cardTypeId, [
                        "price" => $price

                    ])) {
                        activity()
                            ->performedOn(Auth::guard('admin')->user())
                            ->causedBy(Auth::guard('admin')->user())
                            ->log('Admin update wholesaleprice');
                    }
                } else {
                    $this->_wholeSalePriceService->create([
                 
                        "card_type_id" => $cardTypeId,
                        "price" => $price
                    ]);
                    activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin add new whole sale price');
                }
            }
       
        

            return ResponseWrapperFacade::success();
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function getWholePriceForm(Request $request)
    {
        
        $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  :null;
        $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  :null;
        $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  :null;
       
        $packageId = $request->package_id;
        $cardsTypes = collect();
        if($subProviderId)
        {
        $subProvider = $this->_subProviderService->findById($subProviderId);
        $cardsTypes = $subProvider->cardsTypes()->orderBy('order','ASC')->get();
        }
        else if($providerId){
            $provider = $this->_providerService->findById($providerId);
            foreach($provider->subProviders()->orderBy('order','ASC')->get() as $s)

            {
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order','ASC')->get());
            }
          
        }else if($categoryId){
            $category = $this->_categoryService->findById($categoryId);
            foreach($category->providers()->orderBy('order','ASC')->get() as $p)

            {
               foreach($p->subProviders()->orderBy('order','ASC')->get() as $s){
                $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order','ASC')->get());
               }
            }

        }
       
  
       

        $html =  view('admin.includes.wholeprice-create', compact('cardsTypes'))->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{

            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/wholeprice/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $this->_wholeSalePriceService->delete($id);
            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin add delete whole price with id'.$id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
