<?php

namespace App\Http\Controllers\AdminControllers\SettingsControllers;

use App\Http\Controllers\Controller;
use App\Models\ExcelGroupImportModels\GroupCardsCodeImport;
use App\Models\ExcelImportModels\CardsCodeImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Excel;
use App\Models\ExcelGroupImportModels\GroupCardsCodeWithSerialImport;
use App\Models\ExcelImportModels\CardsCodeWithSerialImport;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use Illuminate\Support\Facades\Session;

class ExcelController extends Controller
{

    private $_categoryService, $_cardTypeService,$_cardsService;
    public function __construct(CategoryServiceInterface $categoryService, CardsTypesServiceInterface $cardTypeService,CardsServiceInterface $cardsService)
    {
        $this->_categoryService = $categoryService;
        $this->_cardTypeService = $cardTypeService;
        $this->_cardsService = $cardsService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // V_5 UPDATES 30/12/2021
    public function index(Request $request)
    {

        if ($request->has("excel_type") && $request->excel_type) {
            $fileType = $request->excel_type;
            $file = public_path() . "/storage/ExcelFiles/PinCodeOnly.xlsx";
            if ($fileType == Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL')) {
                $file = public_path() . "/storage/ExcelFiles/PinCodeWithSerial.xlsx";
            }else if($fileType == Config::get('constants.EXCEL_TYPES.GROUP')){
                $file = public_path() . "/storage/ExcelFiles/GroupOfCards.xlsx";
            }else if($fileType ==  Config::get('constants.EXCEL_TYPES.GROUP_OTHER'))
            {
                $file = public_path() . "/storage/ExcelFiles/GroupOfCardsOther.xlsx";
            }
            $headers = array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            return response()->download($file, 'ImportCardsTemplate.xlsx', $headers);
        }

        $lookup = [
            Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL') => trans('PinCode With Serial'),
            Config::get('constants.EXCEL_TYPES.PINCODE') => trans('PinCode Only'),
            Config::get('constants.EXCEL_TYPES.GROUP') => trans('Group Of Cards (Telecommunication)'),
            Config::get('constants.EXCEL_TYPES.GROUP_OTHER') => trans('Group Of Cards (OTHER)'),
        ];
        return view('admin.excel.index', compact('lookup'));
    }

     //END V_5 UPDATES 30/12/2021

    public function excelGroupIndex(Request $request){
        if ($request->has("excel_type") && $request->excel_type) {
            $excelType = $request->excel_type;
            $file = public_path() . "/storage/ExcelFiles/GroupPinCodeOnly.xlsx";
            if ($excelType == Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL')) {
                $file = public_path() . "/storage/ExcelFiles/GroupPinCodeWithSerial.xlsx";
            }
            $headers = array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            return response()->download($file, 'ImportCardsTemplate.xlsx', $headers);
        }

        $lookup = [
            Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL') => trans('PinCode With Serial'),
            Config::get('constants.EXCEL_TYPES.PINCODE') => trans('PinCode Only'),
        ];
        return view('admin.excelgroup.index', compact('lookup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //V_5 UPDATES 27/12/2021
    public function create()
    {
        // Session::remove('excel_filters');
        
        $categoryLookup = $this->_categoryService->lookup();
        $lookup = [
            Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL') => trans('PinCode With Serial'),
           
        ];
        return view('admin.excel.create', compact('lookup', 'categoryLookup'));
    }
    //END V_5 UPDATES 27/12/2021
    public function excelGroupCreate()
    {
        // Session::remove('excel_filters');
        
        $categoryLookup = $this->_categoryService->lookup();
        $lookup = [
            Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL') => trans('Group PinCode With Serial'),
            Config::get('constants.EXCEL_TYPES.PINCODE') => trans('Group PinCode Only'),
        ];
        return view('admin.excelgroup.create', compact('lookup', 'categoryLookup'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extensions = array("xls", "xlsx", "xlm", "xla", "xlc", "xlt", "xlw");
        $validator = Validator::make(
            [
                'excel_type' => $request->excel_type,
                'excel'      => $request->excel,
            ],
            [
                'excel_type' => 'required',
                'excel'          => 'required',

            ],
            [
                "excel_type.required" => trans('This field is required'),
                "excel.required" => trans('This field is required'),


            ]

        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('excel_type'));
        }

        $uploadedFileExtension = array($request->file('excel')->getClientOriginalExtension());

        if (!in_array($uploadedFileExtension[0], $extensions)) {
            return redirect()->back()->withInput($request->only('excel_type'))->with('excel_error', trans('File must be an excel'));
        }
        $categoryId = $request->category_id;
        $providerId = $request->provider_id;
        $subProviderId= $request->sub_provider_id;
        
        $cardTypeId = $request->card_type_id;
        // get category
        $cardType = $this->_cardTypeService->findById($cardTypeId);
        $category = $cardType->subProvider->provider->category;

        $expiryDate = $request->expiry_date;
        $excelType = $request->excel_type;
        $excel = null;
        Session::remove('warning_message');
        Session::remove('duplicate_cards');
        if ($excelType == Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL'))
            $excel = new CardsCodeWithSerialImport(["card_type_id" => $cardTypeId, "expiry_date" => $expiryDate, "title" => $category->title_en],$this->_cardsService);
        else
            $excel = new CardsCodeImport(["card_type_id" => $cardTypeId, "expiry_date" => $expiryDate, "title" => $category->title_en],$this->_cardsService);

        Excel::import($excel, request()->file('excel'));
        Session::put("excel_filters",[
            "category_id"=>$categoryId,
            "provider_id"=>$providerId,
            "sub_provider_id"=>$subProviderId,
            "card_type_id"=>$cardTypeId,
            "expiry_date"=>\Carbon\Carbon::parse($expiryDate),
            "excel_type"=>$excelType
        ]);
        if (Session::has('warning_message')) {
            $warnings = Session::get('warning_message');
$warnings = array_unique($warnings);
if(in_array("duplicate_cards", $warnings)){
    $request->session()->flash("duplicated_cards",Session::get("duplicate_cards"));
}
return redirect()->back()->with('warning_message',$warnings);
            // if ($warning == "large_number_of_digits") {
            //     return redirect()->back()->with('warning_message', 'Cards with more or less than 14 digits have not been uploaded');
            // } else if ($warning == "duplicate_cards") {
            //     $request->session()->flash("duplicated_cards",Session::get("duplicate_cards"));
            //     return redirect()->back()->with('warning_message', 'Duplicate cards have not been uploaded');
            // }
        }
        return redirect()->back()->with('success_message', 'Uploaded Successfully');



        // return redirect()->route('cards.index',["card_type_id"=>$cardTypeId])->with('success_message','Uploaded Successfully');
    }

    


    public function storeGroupExcel(Request $request)
    {
        $extensions = array("xls", "xlsx", "xlm", "xla", "xlc", "xlt", "xlw");
        $validator = Validator::make(
            [
                'excel_type' => $request->excel_type,
                'excel'      => $request->excel,
            ],
            [
                'excel_type' => 'required',
                'excel'          => 'required',

            ],
            [
                "excel_type.required" => trans('This field is required'),
                "excel.required" => trans('This field is required'),


            ]

        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('excel_type'));
        }

        $uploadedFileExtension = array($request->file('excel')->getClientOriginalExtension());

        if (!in_array($uploadedFileExtension[0], $extensions)) {
            return redirect()->back()->withInput($request->only('excel_type'))->with('excel_error', trans('File must be an excel'));
        }
       
   
        
      
$categoryId = $request->category_id;
$category = $this->_categoryService->findById($categoryId);
        $expiryDate = $request->expiry_date;
        $excelType = $request->excel_type;
        $excel = null;
        Session::remove('warning_message');
        Session::remove('duplicate_cards');
        if ($excelType == Config::get('constants.EXCEL_TYPES.PINCODE_WITH_SERIAL'))
            $excel = new GroupCardsCodeWithSerialImport([ "title" => $category->title_en],$this->_cardsService);
        else
            $excel = new GroupCardsCodeImport(["title" => $category->title_en],$this->_cardsService);

        Excel::import($excel, request()->file('excel'));
        Session::put("excel_group_filters",[
            "category_id"=>$categoryId,
          
            "excel_type"=>$excelType
        ]);
        if (Session::has('warning_message')) {
            $warnings = Session::get('warning_message');
$warnings = array_unique($warnings);
if(in_array("duplicate_cards", $warnings)){
    $request->session()->flash("duplicated_cards",Session::get("duplicate_cards"));
}
return redirect()->back()->with('warning_message',$warnings);
            // if ($warning == "large_number_of_digits") {
            //     return redirect()->back()->with('warning_message', 'Cards with more or less than 14 digits have not been uploaded');
            // } else if ($warning == "duplicate_cards") {
            //     $request->session()->flash("duplicated_cards",Session::get("duplicate_cards"));
            //     return redirect()->back()->with('warning_message', 'Duplicate cards have not been uploaded');
            // }
        }
        return redirect()->back()->with('success_message', 'Uploaded Successfully');



        // return redirect()->route('cards.index',["card_type_id"=>$cardTypeId])->with('success_message','Uploaded Successfully');
    }

    public function removeFilters(){
        Session::remove('excel_filters');
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
