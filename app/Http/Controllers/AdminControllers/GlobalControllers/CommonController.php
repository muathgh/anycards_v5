<?php

namespace App\Http\Controllers\AdminControllers\GlobalControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\Models\Admin;
use App\Models\Permissions\AdminSubPermission;
use App\Models\Permissions\SubPermission;
use App\Models\User;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\PackageServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;

class CommonController extends Controller
{

    private $_providerService,$_subProviderService,$_cardTypeService,$_packageService;
    public function __construct(ProviderServiceInterface $providerService,SubProviderServiceInterface $subProviderService,CardsTypesServiceInterface $cardTypeService,
    PackageServiceInterface $packageService)
    {
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProviderService;
        $this->_cardTypeService = $cardTypeService;
        $this->_packageService = $packageService;
    }

    public function getProvidersByCategoryId(Request $request)
    {
        if (request()->ajax()) {
            $categoryId = $request->category_id;
            $providers = $this->_providerService->getProvidersByCategoryId($categoryId);
            return ResponseWrapperFacade::successWithData($providers);
        }
    }

    public function getSubProvidersByProviderId(Request $request)
    {
        if (request()->ajax()) {
            $providerId = $request->provider_id;
            $providers = $this->_subProviderService->getSubProviderByProviderId($providerId);
            return ResponseWrapperFacade::successWithData($providers);
        }
    }

    public function getCardTypeBySubProviderId(Request $request){
        if (request()->ajax()) {
            $subProviderId = $request->sub_provider_id;
            $providers = $this->_cardTypeService->getCardTypeBySubProviderId($subProviderId);
            return ResponseWrapperFacade::successWithData($providers);
        }
    }

    public function getIncludedPackages(Request $request){
        if(request()->ajax()){
            $cardTypeId = $request->card_type_id;
            $includedPackages = $this->_packageService->getIncludedPackages($cardTypeId);
            return ResponseWrapperFacade::successWithData($includedPackages);
        }
    }

    public function getAdminRole($adminId){
        if(request()->ajax()){
        $admin = Admin::find($adminId);
        return ResponseWrapperFacade::successWithData($admin->getRoleNames());
        }
    }

    public function getAdminSubPermissions($adminId){
        if(request()->ajax()){
            $permissions = AdminSubPermission::where('admin_id',$adminId)->get();
         
            $ids=[];
            foreach($permissions as $permission){
                $screen = SubPermission::find($permission->permission_id);
                $ids[] = $screen->id;
            }
            return ResponseWrapperFacade::successWithData($ids);
            }
    }
}
