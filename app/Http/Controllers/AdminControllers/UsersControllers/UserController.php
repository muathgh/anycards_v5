<?php

namespace App\Http\Controllers\AdminControllers\UsersControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\Http\Requests\AdminRequests\CreateUserRequest;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Support\Facades\Hash;
use App\Facades\UploadImageFacade;
use App\Http\Requests\AdminRequests\UpdateUserRequest;
use App\Models\Admin;
use App\ServicesInterface\BalancesServiceInterface;
use App\ServicesInterface\PackageServiceInterface;
use App\ServicesInterface\TransactionsServiceInterface;


class UserController extends Controller
{


    private $_userService, $_transactionsService, $_balancesService, $_packageService;
    public function __construct(
        UsersServiceInterface $userService,
        TransactionsServiceInterface $transactionService,
        BalancesServiceInterface $balancesService,
        PackageServiceInterface $packageService
        
    ) {
        $this->_userService = $userService;
        $this->_transactionsService = $transactionService;
        $this->_balancesService = $balancesService;
        $this->_packageService = $packageService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packagesLookup = $this->_packageService->lookup();
        $adminsLookup = Admin::all()->pluck('username','id');
        return view('admin.users.index',["packagesLookup"=>$packagesLookup,'adminsLookup'=>$adminsLookup]);
    }

    public function getData(Request $request)
    {
        try {
            return response()->json($this->_userService->getDtData($request->all()));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookup = $this->lookup();
        return view('admin.users.create', compact('lookup'));
    }

    private function lookup()
    {

        $lookup = $this->_packageService->lookup();
        return $lookup;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        try {
            $username = $request->username;
            $fullName = $request->full_name;
            $password = $request->password;
            $email = $request->email;
            $mobileNumber = $request->mobile_number;
            $balance = $request->has('balance') && $request->balance ? $request->balance : 0;
            $packageId = $request->has('package_id') ? $request->package_id  : null;

            $logo = $request->hasFile('logo') && $request->file('logo') ? $request->file('logo') : null;
            $logoUrl = null;
            $password = Hash::make($password);
            if ($logo)
                $logoUrl = UploadImageFacade::upload($logo, 'UsersLogo');
            $user =   $this->_userService->create([
                "username" => $username,
                "name" => $fullName,
                "password" => $password,
                "logo" => $logoUrl,
                "email" => $email,
                "mobile_number" => $mobileNumber,
                "package_id" => $packageId,
                "created_by_id" => Auth::guard('admin')->user()->id
            ]);

            if ($user) {
                $user->addBalance($balance);

                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add a new user');
                return redirect()->route('users.index')->with('success_message', trans('Saved Successfully'));
            }

            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->_userService->findById($id);
        $admins = Admin::all()->pluck('username','id');
        $lookup = $this->lookup();
        return view('admin.users.edit', compact('user', 'lookup','admins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request)
    {
        try {
            $id = $request->id;
            $user = $this->_userService->findById($id);
            $username = $request->username;
            $fullName = $request->full_name;
            $email = $request->email;
            $mobileNumber = $request->mobile_number;
            $status = $request->status;
            $password = $request->has('password') && $request->password ? Hash::make($request->password) : null;

            $imageUrl = $user->logo;
            $packageId = $request->has('package_id') ? $request->package_id  : null;
            $createdById = $request->created_by_id;
            $prevDestination = null;
            $prevName = null;
            if ($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $url = $user->logo;
                if ($url != null) {
                    $url = explode('/', $url);
                    $prevDestination = $url[5];
                    $prevName = $url[6];
                }
                $imageUrl = UploadImageFacade::upload($logo, "UsersLogo", $prevDestination, $prevName);
            }
            if ($this->_userService->update($id, $password, [
                "username" => $username,
                "name" => $fullName,

                "logo" => $imageUrl,
                "email" => $email,
                "mobile_number" => $mobileNumber,
                "status" => $status,
                "package_id" => $packageId,
                "updated_by_id" => Auth::guard('admin')->user()->id,
                "created_by_id"=>$createdById
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update user with id ' . $id);
                return redirect()->route('users.index')->with('success_message', trans('Updated Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $id = $request->id;
            if (request()->ajax()) {

                $view = view('layouts.includes.delete-popup', ["id" => $id, "url" => url("/admin/users/destroy?id=" . $id)])->render();

                return ResponseWrapperFacade::successWithData(["html" => $view]);
            }
            $user = $this->_userService->findById($id);
            if (!$this->_userService->delete($id));
            return redirect()->back()->with('error_message', trans('Deletion Failed'));

            $url = $user->image;
            if ($url) {
                $url = explode('/', $url);
                $prevDestination = $url[5];
                UploadImageFacade::removeDir('UsersLogo', $prevDestination);
            }


            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete user with id ' . $id);
            return redirect()->back()->with('success_message', trans('Deleted Successfully'));
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
