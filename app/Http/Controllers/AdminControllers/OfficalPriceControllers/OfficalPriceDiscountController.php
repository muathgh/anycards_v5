<?php

namespace App\Http\Controllers\AdminControllers\OfficalPriceControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\OfficalPriceDiscountRequest;
use App\Http\Requests\AdminRequests\OfficalPricePackageRequest;
use App\Models\Admin;
use App\ServicesInterface\AdminOfficalPriceDiscountServiceInterface;
use App\ServicesInterface\AdminOfficalPriceServiceInterface;
use App\ServicesInterface\OfficalPricePackageServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OfficalPriceDiscountController extends Controller
{

    private $_officalPriceDiscount, $_officalPricePackages, $_adminOfficalPrice, $_providers;
    public function __construct(
        AdminOfficalPriceDiscountServiceInterface $officalPriceDiscount,
        OfficalPricePackageServiceInterface $officalPricePackages,
        ProviderServiceInterface $providers,
        AdminOfficalPriceServiceInterface $adminOfficalPrice
    ) {
        $this->_officalPriceDiscount = $officalPriceDiscount;
        $this->_officalPricePackages = $officalPricePackages;
        $this->_providers = $providers;
        $this->_adminOfficalPrice = $adminOfficalPrice;
    }

    public function index(Request $request)
    {
        $adminsLookup = Admin::all()->pluck('username', 'id');

        if (request()->ajax()) {

            return response()->json($this->_officalPriceDiscount->getDtData($request->all()));
        }

        return view('admin.adminOfficalPriceDiscounts.index', ['adminsLookup' => $adminsLookup]);
    }


    public function create()
    {
        $packages = $this->_officalPricePackages->all();
        $packagesLookup = [];
        foreach ($packages as $p) {
            $packagesLookup[$p->id] = $p->title_ar . '-' . $p->title_en;
        }
        $providers = $this->_providers->lookup();

        return view('admin.adminOfficalPriceDiscounts.create', ['packages' => $packagesLookup, "providers" => $providers]);
    }


    public function edit($id)
    {
        $discount = $this->_officalPriceDiscount->findById($id);

        $packages = $this->_officalPricePackages->all();
        $packagesLookup = [];
        foreach ($packages as $p) {
            $packagesLookup[$p->id] = $p->title_ar . '-' . $p->title_en;
        }
        $providers = $this->_providers->lookup();

        return view('admin.adminOfficalPriceDiscounts.edit', ['discount' => $discount, 'packages' => $packagesLookup, "providers" => $providers]);
    }

    public function store(OfficalPriceDiscountRequest $request)
    {
        try {
            $providerId = $request->provider_id;
            $packageId = $request->package_id;
            $discount = $request->discount;
            if ($this->_officalPriceDiscount->create([
                "offical_price_package_id" => $packageId,
                "provider_id" => $providerId,
                "discount" => $discount,
                "created_by_id" => Auth::guard('admin')->user()->id
            ])) {

                $provider = $this->_providers->findById($providerId);
                foreach ($provider->subProviders as $subProvider) {
                    foreach ($subProvider->cardsTypes as $cardType) {
                        $officalPrice = $this->_adminOfficalPrice->query()->where('offical_price_package_id', $packageId)
                            ->where("card_type_id", $cardType->id)->first();
                        if ($officalPrice) {
                            $originalPrice = $officalPrice->price;
                            $priceAfterDiscount = $originalPrice - $discount;
                            $officalPrice->price_after_discount = $priceAfterDiscount;
                            $officalPrice->save();
                        }
                    }
                }
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add new offical price discount');
                return redirect()->route('officalpricediscount.index')->with('success_message', trans('Saved Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function update(OfficalPriceDiscountRequest $request)
    {
        try {
            $id = $request->id;
            $providerId = $request->provider_id;
            $packageId = $request->package_id;
            $discount = $request->discount;
            if ($this->_officalPriceDiscount->update($id, [
                "offical_price_package_id" => $packageId,
                "provider_id" => $providerId,
                "discount" => $discount,
                "updated_by_id" => Auth::guard("admin")->user()->id
            ])) {
                $provider = $this->_providers->findById($providerId);
                foreach ($provider->subProviders as $subProvider) {
                    foreach ($subProvider->cardsTypes as $cardType) {
                        $officalPrice = $this->_adminOfficalPrice->query()->where('offical_price_package_id', $packageId)
                            ->where("card_type_id", $cardType->id)->first();
                        if ($officalPrice) {
                            $originalPrice = $officalPrice->price;
                            $priceAfterDiscount = $originalPrice - $discount;
                            $officalPrice->price_after_discount = $priceAfterDiscount;
                            $officalPrice->save();
                        }
                    }
                }
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add new offical price discount');
                return redirect()->route('officalpricediscount.index')->with('success_message', trans('Saved Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy(Request $request)
    {
        try{
            $id = $request->id;
        
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/officalpricediscounts/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $discount = $this->_officalPriceDiscount->findById($id);
            $providerId = $discount->provider_id;
            $packageId = $discount->offical_price_package_id;
            if(!$this->_officalPriceDiscount->delete($id))
            return redirect()->back()->with('error_message',trans('Deletion Failed'));
            $provider = $this->_providers->findById($providerId);
            foreach ($provider->subProviders as $subProvider) {
                foreach ($subProvider->cardsTypes as $cardType) {
                    $officalPrice = $this->_adminOfficalPrice->query()->where('offical_price_package_id', $packageId)
                        ->where("card_type_id", $cardType->id)->first();
                    if ($officalPrice) {
                       
                        $officalPrice->price_after_discount = $officalPrice->price;
                        $officalPrice->save();
                    }
                }
            }
            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin delete offical price discount with id ' . $id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
