<?php

namespace App\Http\Controllers\AdminControllers\OfficalPriceControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\ServicesInterface\AdminOfficalPriceDiscountServiceInterface;
use App\ServicesInterface\AdminOfficalPriceServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\OfficalPricePackageServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AdminOfficalPricesController extends Controller
{
  private $_adminOfficalPrice, $_officalPricePackages, $_categoryService, $_cardTypeService, $_subProviderService, $_providerService,$_officalPriceDiscount;
  public function __construct(
    AdminOfficalPriceServiceInterface $adminOfficalPrice,
    OfficalPricePackageServiceInterface $officalPricePackages,
    CategoryServiceInterface $categoryService,
    CardsTypesServiceInterface $cardTypeService,
    SubProviderServiceInterface $subProviderService,
    ProviderServiceInterface $providerService,AdminOfficalPriceDiscountServiceInterface $officalPriceDiscount
  ) {
    $this->_adminOfficalPrice = $adminOfficalPrice;
    $this->_officalPricePackages = $officalPricePackages;
    $this->_categoryService = $categoryService;
    $this->_cardTypeService = $cardTypeService;
    $this->_subProviderService = $subProviderService;
    $this->_providerService = $providerService;
    $this->_officalPriceDiscount = $officalPriceDiscount;
  }

  public function index(Request $request)
  {
    $adminsLookup = Admin::all()->pluck('username', 'id');
    if (request()->ajax()) {
      return datatables()->of($this->_adminOfficalPrice->getDtData($request->all()))->toJson();
    }
    return view('admin.adminOfficalPrices.index', ['adminsLookup' => $adminsLookup]);
  }

  public function getOfficalPriceForm(Request $request)
  {
    $subProviderId = $request->has("sub_provider_id") && $request->sub_provider_id ? $request->sub_provider_id  : null;
    $providerId = $request->has("provider_id") && $request->provider_id ? $request->provider_id  : null;
    $categoryId = $request->has("category_id") && $request->category_id ? $request->category_id  : null;
    $copiedFrom = $request->has("copied_from") && $request->copied_from ? $request->copied_from : null;

    $packageId = $request->package_id;
    $package = $this->_officalPricePackages->findById($packageId);
    $cardsTypes = collect();
    if ($subProviderId) {
      $subProvider = $this->_subProviderService->findById($subProviderId);
      $cardsTypes = $subProvider->cardsTypes()->orderBy("order", "ASC")->get();
    } else if ($providerId) {
      $provider = $this->_providerService->findById($providerId);
      foreach ($provider->subProviders()->orderBy('order', 'ASC')->get() as $s) {
        $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
      }
    } else if ($categoryId) {
      $category = $this->_categoryService->findById($categoryId);
      foreach ($category->providers()->orderBy('order', 'ASC')->get() as $p) {
        foreach ($p->subProviders()->orderBy('order', 'ASC')->get() as $s) {
          $cardsTypes = $cardsTypes->merge($s->cardsTypes()->orderBy('order', 'ASC')->get());
        }
      }
    }
    $html =  view('admin.includes.admin-officalprice-create', compact('cardsTypes', 'packageId','copiedFrom'))->render();
    return ResponseWrapperFacade::successWithData($html);
  }

  public function getOfficalPriceFormAll(Request $request)
  {
      

     $packageId = $request->package_id;
     $copiedFrom = $request->has("copied_from") && $request->copied_from ? $request->copied_from : null;
      $cardsTypes = collect();
      $categories = $this->_categoryService->all();

      foreach($categories as $category){
          $providers = $category->providers()->orderBy('order','ASC')->get();
          foreach($providers as $provider){
              $subProviders = $provider->subProviders()->orderBy('order','ASC')->get();
              foreach($subProviders as $subProvider){
                 
                  $ctypes = $subProvider->cardsTypes()->orderBy('order','ASC')->get();
                  $cardsTypes = $cardsTypes->merge($ctypes);
                 
              
                 
              }
          }

      }

    
    
    
        
      $html =  view('admin.includes.admin-officalprice-all-create', compact('cardsTypes', 'packageId','copiedFrom'))->render();
      return ResponseWrapperFacade::successWithData($html);
     

     

   

  }
  public function store(Request $request)
    {
        try {
            $data = json_decode($request->data);
            $packageId = $request->package_id;
            foreach ($data as $d) {

                $cardTypeId = $d->card_type_id;
                $price = $d->price ? $d->price : 0;
                $priceAfterDiscount = $price;
                $cardType = $this->_cardTypeService->findById($cardTypeId);
                $subProvider = $cardType->subProvider;
                $providerId = $subProvider->provider_id;
                $priceList = $this->_adminOfficalPrice->getAdminOfficalPriceByDoubleCondition('offical_price_package_id', 'card_type_id', $packageId, $cardTypeId);
                if ($priceList) {
                  $officalPriceDiscount = $this->_officalPriceDiscount->query()->where('provider_id',$providerId)
                  ->where('offical_price_package_id',$packageId)->first();
                  if($officalPriceDiscount){
                    $discount = $officalPriceDiscount->discount;
                    $priceAfterDiscount = $priceAfterDiscount - $discount;
                    
                  }
                    if ($this->_adminOfficalPrice->updateByCardTypeAndPackageId($packageId, $cardTypeId, [
                        "price" => $price,
                        "price_after_discount"=>$priceAfterDiscount,
                        "created_by_id"=>Auth::guard("admin")->user()->id

                    ])) {
                        activity()
                            ->performedOn(Auth::guard('admin')->user())
                            ->causedBy(Auth::guard('admin')->user())
                            ->log('Admin update pricing list');
                    }
                } else if($price != 0) {
                  // getDiscount
                  $priceAfterDiscount = $price;
                  $cardType = $this->_cardTypeService->findById($cardTypeId);
                  $subProvider = $cardType->subProvider;
                  $providerId = $subProvider->provider_id;
                  $officalPriceDiscount = $this->_officalPriceDiscount->query()->where('provider_id',$providerId)
                  ->where('offical_price_package_id',$packageId)->first();
                  if($officalPriceDiscount){
                    $discount = $officalPriceDiscount->discount;
                    $priceAfterDiscount = $priceAfterDiscount - $discount;
                    
                  }

                    $this->_adminOfficalPrice->create([
                        "offical_price_package_id" => $packageId,
                        "card_type_id" => $cardTypeId,
                        "price" => $price,
                        "price_after_discount"=>$priceAfterDiscount,
                        "updated_by_id"=>Auth::guard("admin")->user()->id
                    ]);
                    activity()
                        ->performedOn(Auth::guard('admin')->user())
                        ->causedBy(Auth::guard('admin')->user())
                        ->log('Admin create pricing list');
                }
            }
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
  public function create(Request $request)
  {

    $packageId = null;
    $copiedFrom = null;
    $admin = Auth::guard("admin")->user();
    $packages = $this->_officalPricePackages->all();
    $packagesLookup = [];
    foreach ($packages as $p) {
      $packagesLookup[$p->id] = $p->title_ar . '-' . $p->title_en;
    }
    if ($request->has('package_id') && $request->package_id) {

      $packageId = $request->package_id;
      $copiedFrom = $this->_officalPricePackages->findById($packageId);
      $packagesLookup = [];
      foreach ($packages as $p) {
        $packagesLookup[$p->id] = $p->title_ar . '-' . $p->title_en;
      }
    }

    $categoriesLookup = $this->_categoryService->lookup();

    $cardTypesLookup = $this->_cardTypeService->lookup();
    // $packagesLookup = $this->_packageService->lookupByAccessType($packageId);


    return view('admin.adminOfficalPrices.create', compact('packagesLookup', 'cardTypesLookup', 'categoriesLookup', 'packageId', "copiedFrom"));
  }

  public function destroy(Request $request)
  {
      try{

        
          $id = $request->id;
          if(request()->ajax()){
             
              $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/adminofficalprice/destroy?id=".$id)])->render();
              
              return ResponseWrapperFacade::successWithData(["html"=>$view]);
          }

         if(! $this->_adminOfficalPrice->delete($id))
         return redirect()->back()->with('error_message',trans('Deletion Failed'));
          activity()
          ->performedOn(Auth::guard('admin')->user())
          ->causedBy(Auth::guard('admin')->user())
          ->log('Admin delete offical price list with package id'.$id);

          return redirect()->back()->with('success_message',trans('Deleted Successfully'));

      }catch(\Exception $e){
        dd($e->getMessage());
          Log::channel('mysql')->error($e->getMessage());
          return redirect()->back();
      }
  }

}
