<?php

namespace App\Http\Controllers\AdminControllers\OfficalPriceControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\OfficalPricePackageRequest;
use App\Models\Admin;
use App\ServicesInterface\OfficalPricePackageServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OfficalPricePackagesController extends Controller {
    private $_officalPricePackages;
    public function __construct(OfficalPricePackageServiceInterface $officalPricePackages)
    {
     $this->_officalPricePackages = $officalPricePackages;   
    }

    public function index(Request $request){
        $adminsLookup = Admin::all()->pluck('username','id');
        if(request()->ajax())
        {
            
            return response()->json($this->_officalPricePackages->getDtData($request->all()));
        }

        return view('admin.officalPricePackages.index',['adminsLookup'=>$adminsLookup]);
    }

    public function create(){
        return view('admin.officalPricePackages.create');
    }

    public function edit($id){
        $officalPricePackage = $this->_officalPricePackages->findById($id);

        return view('admin.officalPricePackages.edit',['officalPricePackage'=>$officalPricePackage]);
        
    }

    public function store(OfficalPricePackageRequest $request){
        try{

            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;

            if($this->_officalPricePackages->create([
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "created_by_id"=>Auth::guard("admin")->user()->id
            ])){
                activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin add new offical price package');
                return redirect()->route('officalpricepackages.index')->with('success_message',trans('Saved Successfully'));
            }

            return redirect()->back();
            
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    

    public function update(OfficalPricePackageRequest $request){
        try{
            $id = $request->id;
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            if($this->_officalPricePackages->update($id,[
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
            
                "updated_by_id"=>Auth::guard("admin")->user()->id
            ])){
                activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin update offical price package with id'.$id);
                return redirect()->route('officalpricepackages.index')->with('success_message',trans('Updated Successfully'));
            }
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy(Request $request)
    {
        try{
            $id = $request->id;
        
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/officalpricepackages/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            if(!$this->_officalPricePackages->delete($id))
            return redirect()->back()->with('error_message',trans('Deletion Failed'));
            activity()
            ->performedOn(Auth::guard('admin')->user())
            ->causedBy(Auth::guard('admin')->user())
            ->log('Admin delete offical price package with id ' . $id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}