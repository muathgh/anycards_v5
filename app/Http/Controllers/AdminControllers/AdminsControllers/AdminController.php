<?php

namespace App\Http\Controllers\AdminControllers\AdminsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\ServicesInterface\OfficalPricePackageServiceInterface;
// V_6 UPDATES 06/01/2022
class AdminController extends Controller
{

    private $_officalPricePackages;
    public function __construct(OfficalPricePackageServiceInterface $officalPricePackages)
    {
        $this->_officalPricePackages = $officalPricePackages;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // V_5 UPDATES 28/12/2021
    public function index()
    {
        
        if (request()->ajax()) {
            $data = DB::table("admins")->leftJoin('balance_limits','balance_limits.admin_id','admins.id')
            ->join('model_has_roles','model_has_roles.model_id','=','admins.id')
            ->join('roles','roles.id','=','model_has_roles.role_id')
            ->select('admins.username as username','roles.name as role','admins.id as id','admins.created_at as created_at','balance_limits.amount as balance_quota')->get();
            $data = $data->map(function($d){
$item['username'] = $d->username;
$item['id']  = $d->id;
$item['created_at'] = $d->created_at;
$item['balance_quota'] = Admin::find($d->id)->isSuperAdmin() ? null : $d->balance_quota;
$item['is_super_admin'] = Admin::find($d->id)->isSuperAdmin();
$item['role'] = $d->role;
return $item;
            });
            return DataTables::of($data)->make(true);
        }
        return view('admin.admins.index');
    }
    //END V_5 UPDATES 28/12/2021

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $packages = $this->_officalPricePackages->all();
        $packagesLookup = [];
        foreach ($packages as $p) {
          $packagesLookup[$p->id] = $p->title_ar . '-' . $p->title_en;
        }
        return view('admin.admins.create',['packages'=>$packagesLookup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdminRequest $request)
    {
        try {
            $validated = $request->validated();
            $username = $validated["username"];
            $password = $validated["password"];
            $packageId = $validated['offical_price_package_id'];
            $password = Hash::make($password);
            Admin::create(["username" => $username, "password" => $password,'offical_price_package_id'=>$packageId]);
            return redirect()->route('admins.index')->with('success_message', trans('Saved Successfully'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find($id);
        $packages = $this->_officalPricePackages->all();
        $packagesLookup = [];
        foreach ($packages as $p) {
          $packagesLookup[$p->id] = $p->title_ar . '-' . $p->title_en;
        }
        return view('admin.admins.edit', ["admin" => $admin,'packages'=>$packagesLookup]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminRequest $request)
    {
        try {
            $id = $request->id;
            $username = $request->username;
            $packageId = $request->offical_price_package_id;
            $password = $request->has('password') && $request->password ? Hash::make($request->password) : null;
            $admin = Admin::find($id);
            $admin->username = $username;
            $admin->offical_price_package_id = $packageId;
            if ($password)
                $admin->password = $password;
            $admin->save();

            return redirect()->route('admins.index')->with('success_message', trans('Updated Successfully'));

        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/admins/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $admin = Admin::findOrFail($id);
           if(! $admin->secureDelete('users')){
            return redirect()->back()->with('error_message',trans('Deletion Failed'));

           }

           activity()
           ->performedOn(Auth::guard('admin')->user())
           ->causedBy(Auth::guard('admin')->user())
           ->log('Admin delete admin with id ' . $id);
            

        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        
        }
    }
}

// END V_6 UPDATES 06/01/2022