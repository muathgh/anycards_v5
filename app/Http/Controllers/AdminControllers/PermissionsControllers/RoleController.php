<?php



namespace App\Http\Controllers\AdminControllers\PermissionsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\AsignRole;
use App\Http\Requests\AdminRequests\AsignRoleRequest;
use App\Http\Requests\AdminRequests\AssignPermissionsRequest;
use App\Http\Requests\AdminRequests\AssignRoleRequest;
use App\Http\Requests\AdminRequests\RoleRequest;
use App\Models\Admin;
use App\Models\Permissions\AdminSubPermission;
use App\ServicesInterface\PermissionsServiceInterface;
use App\ServicesInterface\RoleServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Permissions\SubPermission;

class RoleController extends Controller
{
    private $_roleService, $_permssionsService, $_userService;
    public function __construct(RoleServiceInterface $roleService, PermissionsServiceInterface $permssionsService, UsersServiceInterface $userService)
    {
        $this->_roleService = $roleService;
        $this->_permssionsService = $permssionsService;
        $this->_userService = $userService;
    }


    public function create()
    {
      
        $permissions = $this->_permssionsService->lookup();
        return view('admin.roles.create', ["permissions" => $permissions]);
    }

    public function index()
    {
        if (request()->ajax()) {
            $roles = $this->_roleService->getDtData();
            return datatables()->of($roles)->toJson();
        }

        return view('admin.roles.index');
    }


    public function permissions(Request $request){
        $admins = Admin::all()->pluck("username", "id");
        $subScreens = SubPermission::all()->pluck('child_screen','id');
        return view('admin.roles.permissions',['admins'=>$admins,'subScreens'=>$subScreens]);
    }

    public function assignPermissions(AssignPermissionsRequest $request){
        try {
            $validated = $request->validated();
            AdminSubPermission::where("admin_id",$validated['admin_id'])->delete();
            $ids = $validated['sub_screen_id'];
            foreach($ids as $id){
                AdminSubPermission::create([
                    "admin_id"=>$validated['admin_id'],
                    "permission_id"=>$id
                ]);
            }

            return redirect()->back()->with('success_message','Saved Successfully');
          
            
        }
        catch(\Exception $e){
            dd($e->getMessage());
            return redirect()->back()->with('error_message','Failure');
return redirect()->back()->withInput();
        }
    }

    public function assign()
    {
        $admins = Admin::all()->pluck("username", "id");
        $roles = $this->_roleService->lookup();
        return view('admin.roles.assign', ["admins" => $admins, "roles" => $roles]);
    }

    public function assignPost(AssignRoleRequest $request)
    {
        try {

            $validated = $request->validated();
            DB::table("model_has_roles")->where('model_id',$validated['admin_id'])->delete();
            $dbRole = DB::table('roles')->where('name',$validated["role"])->first();
           
            $admin = Admin::findOrFail($validated["admin_id"]);
            $role = $validated["role"];


            $admin->assignRole($role);

            if($dbRole->is_super_admin){
                $subPerm = DB::table('sub_permissions')->select("*")->get();
                foreach($subPerm as $p){
                    $adminSubPerm = DB::table('admins_sub_permissions')->where('permission_id',$p->id)->
                    where('admin_id',$admin->id)->first();
                    if(!$adminSubPerm){
                        AdminSubPermission::create([
                            "permission_id"=>$p->id,
                            "admin_id"=>$admin->id
                        ]);
                    }
                }
            }


            return redirect()->route('role.index')->with('success_message', trans('Saved Successfully'));
        } catch (\Exception $e) {
          
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function store(RoleRequest $request)
    {
        try {
            $validated = $request->validated();
            $isSuperAdmin = $request->is_super_admin;
           
            $name = $validated["name"];
            $accessType = $request->has('access_type') ? $request->access_type : null;
            
            $permissions = $request->has('permissions') ? $request->permissions : null;
            if($isSuperAdmin == "true"){
                $accessType = "all";
                $role =   $this->_roleService->create($name,$accessType,'admin',true);
                $this->_roleService->syncPermissions($role,$this->_permssionsService->all());
                return redirect()->route('role.index')->with('success_message', trans('Saved Successfully'));
            }
            $role =   $this->_roleService->create($name,$accessType,'admin',false);
            $data = array();
            foreach ($permissions as $permission) {
                $data[] = $this->_permssionsService->getByCondition("name", $permission);
            }

            $this->_roleService->syncPermissions($role, $data);

            return redirect()->route('role.index')->with('success_message', trans('Saved Successfully'));
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $role = $this->_roleService->findById($id);
        $roleHasPermissions = DB::table("role_has_permissions")->where('role_id', $role->id)->select("permission_id")->get();
        $permissionsIds = array();
        foreach ($roleHasPermissions as $p) {
            $permissionsIds[] = $p->permission_id;
        }
        $selectedPermissions =   $this->_permssionsService->getByWhereIn("id", $permissionsIds)->pluck('name', "name")->toArray();
        $selectedPermissions = implode(",",$selectedPermissions);
        $permissions = $this->_permssionsService->lookup();
        return view('admin.roles.edit',["permissions"=>$permissions,"selectedPermissions"=>$selectedPermissions,"role"=>$role]);
    }
    public function update(RoleRequest $request)
    {
        try {
            $validated = $request->validated();
            $id = $request->id;
            $isSuperAdmin = $request->is_super_admin;
            $name = $validated["name"];
            $accessType = $request->has('access_type') ? $request->access_type : null;
            
            $permissions = $request->has('permissions') ? $request->permissions : null;
          
            if($isSuperAdmin == "true"){
                $accessType = "all";
                $permissions = $this->_permssionsService->all();
                $role =   $this->_roleService->update($id,["name"=>$name,"access_type"=>$accessType,'is_super_admin'=>true]);
                $this->_roleService->syncPermissions($role, $permissions);
                return redirect()->route('role.index')->with('success_message', trans('Updated Successfully'));
            }
            $role =   $this->_roleService->update($id,["name"=>$name,"access_type"=>$accessType,'is_super_admin'=>false]);
            DB::table("role_has_permissions")->where('role_id', $role->id)->delete();

            $permissions = $validated["permissions"];
            $data = array();
            foreach ($permissions as $permission) {
                $data[] = $this->_permssionsService->getByCondition("name", $permission);
            }

            $this->_roleService->syncPermissions($role, $data);

            return redirect()->route('role.index')->with('success_message', trans('Updated Successfully'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
    public function destroy(Request $request){
        try{

            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/role/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $relation = DB::table('model_has_roles')->where('role_id',$id)->first();
            if($relation){
                return redirect()->back()->with('error_message',trans('Deletion Failed'));

            }
            DB::table('role_has_permissions')->where('role_id',$id)->delete();
            DB::table('roles')->where('id',$id)->delete();
            return redirect()->back()->with('success_message','Deleted Successfully');

        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}

