<?php

namespace App\Http\Controllers\AdminControllers\ReportsControllers;

use App\Exports\NetProfitExport;
use App\Exports\NetProfitExportFromView;
use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTransactionsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\DataTable;
use App\Models\Admin;
use App\Models\CardModels\Card;
use App\Models\CategoryModels\CardType;
use App\Models\CategoryModels\Provider;
use App\Models\CategoryModels\SubProvider;
use App\Models\ReportsModels\NetProfit;
use App\Models\SettingModels\Receivables;
use App\Models\SettingModels\WholeSalePrice;
use App\Models\TransactionModels\Transaction;
use App\Models\User;
use App\ServicesInterface\NetProfitServiceInterface;
use App\ServicesInterface\TransactionsServiceInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class ReportController extends Controller
{


    use DataTable;
    private $_cardTransactionService, $_userService, $_categoryService, $_providerService, $_subProviderService, $_cardTypeService, $_transactionsService, $_netProfitService;
    private $datetimeFormat = 'Y-m-d H:i:s';
    public function __construct(
        CardTransactionsServiceInterface $cardTransactionService,
        UsersServiceInterface $usersService,
        CategoryServiceInterface $categoryService,
        ProviderServiceInterface $providerService,
        SubProviderServiceInterface $subProvderService,
        CardsTypesServiceInterface $cardTypeService,
        TransactionsServiceInterface $transactionsService,
        NetProfitServiceInterface $netProfitService
    ) {
        $this->_cardTransactionService = $cardTransactionService;
        $this->_userService = $usersService;
        $this->_categoryService = $categoryService;
        $this->_providerService = $providerService;
        $this->_subProviderService = $subProvderService;
        $this->_cardTypeService = $cardTypeService;
        $this->_transactionsService = $transactionsService;
        $this->_netProfitService = $netProfitService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // V_5 UPDATES 29/12/2021
    public function index(Request $request)
    {
        if(!HelperFacade::checkAdminPermission("Report"))
        {
return redirect()->back();
        }
        $usersLookup = $this->_userService->lookupByAccessType();
        $categoriesLookup = $this->_categoryService->lookup();
        $adminsLookup = Admin::all()->pluck('username','id');
        $admin = Auth::guard("admin")->user();
        $adminId= $admin->id;

        if (request()->ajax()) {

            $params = $request->all();

            $filter = $request->filter;
            $draw = $params['draw'];
            $start = $params['start'];
            $rowperpage  = $params['length'];
    
    
            $search  = $params['search'];
            $searchvalue = $search['value'];
            $cardTypeId = $filter["card_type_id"];
            $subProviderId = $filter["sub_provider_id"];
            $providerId = $filter["provider_id"];
            $categoryId = $filter["category_id"];
            $fromDate = $filter["from_date"];
            $toDate = $filter["to_date"];
            $userId = null;
            $filterAdminId = $filter["admin_id"];;
            $integerIDs = null;
            if (array_key_exists("user_id", $filter) && $filter['user_id']) {

                $userId = $filter["user_id"];
                $ids = implode(",", $userId);
                $integerIDs = array_map('intval', explode(',', $ids));
            }

            $start = $request->get("start");

            $rowperpage = $request->get("length");
            $cardTransactions = DB::table('card_transactions')
                ->join('cards', 'card_transactions.card_id', '=', 'cards.id')
                ->join('cards_types', 'cards.card_type_id', '=', 'cards_types.id')
                ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
                ->join('providers', 'sub_providers.provider_id', '=', 'providers.id')
                ->join('categories', 'providers.category_id', '=', 'categories.id')

                ->join('transactions', 'transactions.id', '=', 'card_transactions.transaction_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')

                ->join('whole_sale_prices', 'whole_sale_prices.card_type_id', '=', 'cards_types.id');
            if (HelperFacade::containsOnlyNull($filter)) {


                $cardTransactions =    $cardTransactions->selectRaw(
                    "SUM(transactions.amount) - SUM(whole_sale_prices.price)  as profit , 
                    SUM(transactions.amount) as cost_price,
                    SUM(whole_sale_prices.price) as whole_price,
                    COUNT(card_transactions.id) as transactions,
                   
            cards_types.title_en as title_en,
            cards_types.title_ar as title_ar,
            cards_types.id as card_type_id,
            users.name as full_name,
            users.id as user_id,
            card_transactions.created_at as created_at
           

            ",
                )->whereRaw('DAY(card_transactions.created_at) = DAY(CURDATE())')
                    ->whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())')
                    ->whereRaw('YEAR(card_transactions.created_at) = YEAR(CURDATE())')
                    
                 
                   ;
                   if ($admin->accessType() == "created_users") {
                       $cardTransactions = $cardTransactions->whereRaw("users.created_by_id =$adminId ");
                   }
                   $cardTransactions = $cardTransactions->groupBy("cards_types.title_en", "cards_types.title_ar", "cards_types.id", 'users.name', "users.id")->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
            } else

            if ($cardTypeId) {
                $cardTransactions =  $cardTransactions->selectRaw(
                    "SUM(transactions.amount) - SUM(whole_sale_prices.price)  as profit , 
                    COUNT(card_transactions.id) as transactions,
                   
                    SUM(transactions.amount) as cost_price,
                    SUM(whole_sale_prices.price) as whole_price,
                    
                cards_types.title_en as title_en,
                cards_types.title_ar as title_ar,
                cards_types.id as card_type_id,
                users.name as full_name,
                users.id as user_id,
                card_transactions.created_at as created_at
                ",


                )->where('cards_types.id', '=', $cardTypeId);
                if ($integerIDs)
                    $cardTransactions =  $cardTransactions->whereIn('user_id', $integerIDs);
                    else {
                    if ($admin->accessType() == "created_users") {
                        $cardTransactions = $cardTransactions->where("users.created_by_id",$adminId);
                    }
                }
                if ($fromDate && $toDate)
                    $cardTransactions  = $cardTransactions->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($fromDate))->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($toDate));

                    if($filterAdminId && $filterAdminId != 0){
                        $cardTransactions = $cardTransactions->where('users.created_by_id',$filterAdminId);
                    }
                   

                $cardTransactions = $cardTransactions->groupBy("cards_types.title_en", "cards_types.title_ar", "cards_types.id", 'users.name', "users.id")->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
            } else if ($subProviderId) {
                $cardTransactions =  $cardTransactions->selectRaw(
                    "SUM(transactions.amount) - SUM(whole_sale_prices.price)  as profit , 
                    COUNT(card_transactions.id) as transactions,
                   
                    SUM(transactions.amount) as cost_price,
                    SUM(whole_sale_prices.price) as whole_price,
                cards_types.title_en as title_en,
                cards_types.title_ar as title_ar,
                cards_types.id as card_type_id,
                users.name as full_name,
                users.id as user_id,
                card_transactions.created_at as created_at
                ",


                )->where('cards_types.sub_provider_id', '=', $subProviderId);
                if ($integerIDs)
                    $cardTransactions =  $cardTransactions->whereIn('user_id', $integerIDs);
                    else {
                        if ($admin->accessType() == "created_users") {
                            $cardTransactions = $cardTransactions->where("users.created_by_id",$adminId);
                        }
                    }
                if ($fromDate && $toDate)
                    $cardTransactions  = $cardTransactions->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($fromDate))->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($toDate));

                    if($filterAdminId && $filterAdminId != 0){
                        $cardTransactions = $cardTransactions->where('users.created_by_id',$filterAdminId);
                    }

                $cardTransactions = $cardTransactions->groupBy("cards_types.title_en", "cards_types.title_ar", "cards_types.id", 'users.name', "users.id")->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
            } else if ($providerId) {
                $cardTransactions =  $cardTransactions->selectRaw(
                    "SUM(transactions.amount) - SUM(whole_sale_prices.price)  as profit , 
                    COUNT(card_transactions.id) as transactions,
                   
                    SUM(transactions.amount) as cost_price,
                    SUM(whole_sale_prices.price) as whole_price,
                sub_providers.title_en as title_en,
                sub_providers.title_ar as title_ar,
                sub_providers.id as sub_provider_id,
              
                users.name as full_name,
                users.id as user_id,
                card_transactions.created_at as created_at
                ",


                )->where('sub_providers.provider_id', '=', $providerId);
                if ($integerIDs)
                    $cardTransactions =  $cardTransactions->whereIn('user_id', $integerIDs);
                    else {
                        if ($admin->accessType() == "created_users") {
                            $cardTransactions = $cardTransactions->where("users.created_by_id",$adminId);
                        }
                    }
                if ($fromDate && $toDate)
                    $cardTransactions  = $cardTransactions->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($fromDate))->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($toDate));
                    if($filterAdminId && $filterAdminId != 0){
                        $cardTransactions = $cardTransactions->where('users.created_by_id',$filterAdminId);
                    }
                $cardTransactions = $cardTransactions->groupBy("cards_types.title_en", "cards_types.title_ar", "sub_providers.id", 'users.name', "users.id")->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
            } else if ($categoryId) {

                $cardTransactions =  $cardTransactions->selectRaw(
                    "SUM(transactions.amount) - SUM(whole_sale_prices.price)  as profit , 
                  
                    COUNT(card_transactions.id) as transactions,
                   
                    SUM(transactions.amount) as cost_price,
                    SUM(whole_sale_prices.price) as whole_price,
                providers.title_en as title_en,
                providers.title_ar as title_ar,
                providers.id as provider_id,
               
                users.name as full_name,
                users.id as user_id,
                card_transactions.created_at as created_at
                
                ",


                )->where('providers.category_id', '=', $categoryId);
                if ($integerIDs)
                    $cardTransactions =  $cardTransactions->whereIn('user_id', $integerIDs);
                    else {
                        if ($admin->accessType() == "created_users") {
                            $cardTransactions = $cardTransactions->where("users.created_by_id",$adminId);
                        }
                    }
                if ($fromDate && $toDate)
                    $cardTransactions  = $cardTransactions->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($fromDate))->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($toDate));
                    if($filterAdminId && $filterAdminId != 0){
                        $cardTransactions = $cardTransactions->where('users.created_by_id',$filterAdminId);
                    }
                $cardTransactions = $cardTransactions->groupBy("cards_types.title_en", "cards_types.title_ar", "providers.id", 'users.name', "users.id")->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
            } else if ($integerIDs || ($fromDate && $toDate) || ($filterAdminId && $filterAdminId != 0) ) {


                $cardTransactions =    $cardTransactions->selectRaw(
                    "SUM(transactions.amount) - SUM(whole_sale_prices.price)  as profit , 
                    COUNT(card_transactions.id) as transactions,
                   
                    SUM(transactions.amount) as cost_price,
                    SUM(whole_sale_prices.price) as whole_price,
            cards_types.title_en as title_en,
            cards_types.title_ar as title_ar,
            cards_types.id as card_type_id,
            users.name as full_name,
            users.id as user_id,
            card_transactions.created_at as created_at
            ",
                );

                if ($integerIDs) {

                    $cardTransactions = $cardTransactions->whereIn("users.id", $integerIDs);
                }
               
                if (($fromDate && $toDate)) {
                    if ($admin->accessType() == "created_users") {
                        $cardTransactions = $cardTransactions->where("users.created_by_id",$adminId);
                    }
                    $cardTransactions =     $cardTransactions->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($fromDate))->whereDate('card_transactions.created_at', '<=', \Carbon\Carbon::parse($toDate));
                }
                if($filterAdminId && $filterAdminId != 0){
                    $cardTransactions = $cardTransactions->where('users.created_by_id',$filterAdminId);
                }

                $cardTransactions = $cardTransactions->groupBy("cards_types.title_en", "cards_types.title_ar", "cards_types.id", 'users.name', "users.id")->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
            }



          


            $cardTransactions = $cardTransactions->get();
          


            $data = array();

            foreach ($cardTransactions as $r) {



                $numberOfCards = null;
                if (isset($r->card_type_id)) {
                    $numberOfCards = Card::where('card_type_id', $r->card_type_id)->count();
                } else if (isset($r->sub_provider_id)) {
                    $numberOfCards = 0;

                    $cardType = CardType::where('sub_provider_id', $r->sub_provider_id)->get();
                    $cardType->each(function ($c) use (&$numberOfCards) {
                        $numberOfCards += $c->cards()->count();
                    });
                } else if (isset($r->provider_id)) {
                    $numberOfCards = 0;
                    $subProvider = SubProvider::where('provider_id', 1)->get();
                    $subProvider->each(function ($s) use (&$numberOfCards) {
                        $s->cardsTypes->each(function ($c) use (&$numberOfCards) {
                            $numberOfCards += $c->cards()->count();
                        });
                    });
                }





                $data[] = [
                    "title_en" => $r->title_en,
                    "title_ar" => $r->title_ar,
                    "profit" => $r->profit,
                    "full_name" => $r->full_name,
                    "transactions" => $r->transactions,
                    "number_of_cards" => $numberOfCards,
                    "cost_price" => $r->cost_price,
                    "whole_price" => $r->whole_price




                ];
            }
         

            return datatables()->of($data)->make(true);
        }














        return view('admin.reports.index', ['adminsLookup'=>$adminsLookup,'usersLookup' => $usersLookup, 'categoriesLookup' => $categoriesLookup]);
    }

    //END V_5 UPDATES 29/12/2021

   // V_5 UPDATES 28/12/2021
public function cardsReport(Request $request)
    {
        if(!HelperFacade::checkAdminPermission("Cards Stock"))
        {
return redirect()->back();
        }
        $categoriesLookup = $this->_categoryService->lookup();
        $cardsList = [];
        $cardsdb = null;

        if (request()->ajax()) {
            $filter = $request->filter;
            $cardTypeId = $filter["card_type_id"];
            $subProviderId = $filter["sub_provider_id"];
            $providerId = $filter["provider_id"];
            $categoryId = $filter["category_id"];
            $fromDate = $filter["from_date"];
            $toDate = $filter["to_date"];
            $fromTime = $filter["from_time"];
            $toTime = $filter['to_time'];
            $start = $request->get("start");
            $rowperpage = $request->get("length");

            $cards = DB::table('cards')->join('cards_types', 'cards_types.id', '=', 'cards.card_type_id')->join('sub_providers', 'sub_providers.id', 'cards_types.sub_provider_id')->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
                ->join('categories', 'providers.category_id', '=', 'categories.id');

            if (HelperFacade::containsOnlyNull($filter)) {
                // $cards =  $cards->selectRaw("COUNT(cards.id) as number_of_cards ,
                // cards_types.title_en as title_en,cards_types.id as card_type_id")->where('cards.is_used', '=', '0')

                $cards =  $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                cards_types.title_en as title_en,cards_types.id as card_type_id")
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1)
                
                

                    ->groupBy("cards_types.title_en", 'cards_types.id')
                    ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
           $cardsdb = $cards->get();
                } else if ($cardTypeId) {
                   
                // $cards =   $cards->selectRaw("COUNT(cards.id) as number_of_cards ,
                // cards_types.title_en as title_en,cards_types.id as card_type_id")->where('cards_types.id', '=', $cardTypeId)
                //     ->where('cards.is_used', '=', '0')
                //     ->groupBy("cards_types.title_en");
                $cards =   $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                cards_types.title_en as title_en,cards_types.id as card_type_id")->where('cards_types.id', '=', $cardTypeId)
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1)
                    ->groupBy("cards_types.title_en");
                    $cards = $cards->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
                    if ($fromDate && $toDate && $fromTime && $toTime) {
                    $fdate = explode('-', $fromDate);
                    $tdate = explode('-', $toDate);

                    // $ftime = explode(':',$fromTime);
                    $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                    $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
                    $fromDateTime = new \DateTime();

                    $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);

                    $fromDateTime = $fromDateTime->format($this->datetimeFormat);

                    $toDateTime = new \DateTime();

                    $toDateTime =  $toDateTime->setTimestamp($tdatetime);

                    $toDateTime = $toDateTime->format($this->datetimeFormat);

                    // filter time

                    $frdate = Carbon::parse($fdatetime)->format('Y-m-d');
                    $frtime = Carbon::parse($fdatetime)->toTimeString();
                    $tidate = Carbon::parse($tdatetime)->format('Y-m-d');
                    $titime = Carbon::parse($tdatetime)->toTimeString();
                    
        
                 

                    $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                    ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
                } else if ($fromDate && $toDate) {
                    $cards = $cards->whereDate("cards.created_at", ">=", \Carbon\Carbon::parse($fromDate))->whereDate("cards.created_at", "<=", \Carbon\Carbon::parse($toDate));
                }

             $cardsdb = $cards->get();


                   
            } else if ($subProviderId) {
                
                
                // $cards =   $cards->selectRaw("COUNT(cards.id) as number_of_cards ,
                // cards_types.title_en as title_en,cards_types.id as card_type_id")->where('cards_types.sub_provider_id', '=', $subProviderId)
                // ->groupBy("cards_types.title_en")   
                // ->where('cards.is_used', '=', '0');
                
                $cards =   $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                cards_types.title_en as title_en,cards_types.id as card_type_id")->where('cards_types.sub_provider_id', '=', $subProviderId)
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1)
                ->groupBy("cards_types.title_en")   ;


                    $cards = $cards->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
                    if ($fromDate && $toDate && $fromTime && $toTime) {
                    $fdate = explode('-', $fromDate);
                    $tdate = explode('-', $toDate);

                    // $ftime = explode(':',$fromTime);
                    $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                    $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
                    $fromDateTime = new \DateTime();

                    $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);

                    $fromDateTime = $fromDateTime->format($this->datetimeFormat);

                    $toDateTime = new \DateTime();

                    $toDateTime =  $toDateTime->setTimestamp($tdatetime);

                    $toDateTime = $toDateTime->format($this->datetimeFormat);
                    // filter time

                    $frdate = Carbon::parse($fdatetime)->format('Y-m-d');
                    $frtime = Carbon::parse($fdatetime)->toTimeString();
                    $tidate = Carbon::parse($tdatetime)->format('Y-m-d');
                    $titime = Carbon::parse($tdatetime)->toTimeString();
                    $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                    ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
                } else if ($fromDate && $toDate) {
                    $cards = $cards->whereDate("cards.created_at", ">=", \Carbon\Carbon::parse($fromDate))->whereDate("cards.created_at", "<=", \Carbon\Carbon::parse($toDate));
                }

                
                $cardsdb = $cards->get();
               
           
                } else if ($providerId) {
                    
                // $cards =  $cards->selectRaw("COUNT(cards.id) as number_of_cards ,
                // sub_providers.title_en as title_en,sub_providers.id as sub_provider_id")->where('sub_providers.provider_id', '=', $providerId)
                // ->groupBy("sub_providers.title_en")   
                // ->where('cards.is_used', '=', '0');

                $cards =  $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                sub_providers.title_en as title_en,sub_providers.id as sub_provider_id")->where('sub_providers.provider_id', '=', $providerId)
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1)
                ->groupBy("sub_providers.title_en")   ;
      
                if ($fromDate && $toDate && $fromTime && $toTime) {
                    $fdate = explode('-', $fromDate);
                    $tdate = explode('-', $toDate);

                    // $ftime = explode(':',$fromTime);
                    $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                    $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
                    $fromDateTime = new \DateTime();

                    $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);

                    $fromDateTime = $fromDateTime->format($this->datetimeFormat);

                    $toDateTime = new \DateTime();

                    $toDateTime =  $toDateTime->setTimestamp($tdatetime);

                    $toDateTime = $toDateTime->format($this->datetimeFormat);
                    // filter time

                    $frdate = Carbon::parse($fdatetime)->format('Y-m-d');
                    $frtime = Carbon::parse($fdatetime)->toTimeString();
                    $tidate = Carbon::parse($tdatetime)->format('Y-m-d');
                    $titime = Carbon::parse($tdatetime)->toTimeString();
                    $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                    ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
                } else  if ($fromDate && $toDate) {
                    $cards = $cards->whereDate("cards.created_at", ">=", \Carbon\Carbon::parse($fromDate))->whereDate("cards.created_at", "<=", \Carbon\Carbon::parse($toDate));
                }
                $cards = $cards->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
                   $cardsdb = $cards->get();
                   
    
                } else if ($categoryId) {
                   
                // $cards =   $cards->selectRaw("COUNT(cards.id) as number_of_cards ,
                // providers.title_en as title_en,providers.id as provider_id")->where('providers.category_id', '=', $categoryId)
                // ->groupBy("providers.title_en") 
                // ->where('cards.is_used', '=', '0');

                    
                $cards =   $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                providers.title_en as title_en,providers.id as provider_id")->where('providers.category_id', '=', $categoryId)
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1)
                ->groupBy("providers.title_en") ;
       

                    $cards = $cards->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
                    

                if ($fromDate && $toDate && $fromTime && $toTime) {
                   
                    $fdate = explode('-', $fromDate);
                    $tdate = explode('-', $toDate);

                    // $ftime = explode(':',$fromTime);
                    $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                    $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
                    $fromDateTime = new \DateTime();

                    $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);
                   

                    $fromDateTime = $fromDateTime->format($this->datetimeFormat);

                    $toDateTime = new \DateTime();

                    $toDateTime =  $toDateTime->setTimestamp($tdatetime);

                    $toDateTime = $toDateTime->format($this->datetimeFormat);
                    // filter time

                    $frdate = Carbon::parse($fromDateTime)->format('Y-m-d');
                    $frtime = Carbon::parse($fromDateTime)->toTimeString();
                   
                    
               
                    $tidate = Carbon::parse($toDateTime)->format('Y-m-d');
                    $titime = Carbon::parse($toDateTime)->toTimeString();
                    $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                    ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
                   

                    
                } else  if ($fromDate && $toDate) {
                    $cards = $cards->whereDate("cards.created_at", ">=", \Carbon\Carbon::parse($fromDate))->whereDate("cards.created_at", "<=", \Carbon\Carbon::parse($toDate));
                }

           $cardsdb = $cards->get();

           
                
               
               
                
            } else   if ($fromDate && $toDate && $fromTime && $toTime) {
                $fdate = explode('-', $fromDate);
                $tdate = explode('-', $toDate);

                // $ftime = explode(':',$fromTime);
                $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");

                $fromDateTime = new \DateTime();

                $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);


                $toDateTime = new \DateTime();

                $toDateTime =  $toDateTime->setTimestamp($tdatetime);

                //    $toDateTime = $toDateTime->format($this->datetimeFormat);
               
                $cards = $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                cards_types.title_en as title_en,cards_types.id as card_type_id")
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1);
               $cards = $cards
               ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
                // filter time

                $frdate = Carbon::parse($fdatetime)->format('Y-m-d');
                $frtime = Carbon::parse($fdatetime)->toTimeString();
                $tidate = Carbon::parse($tdatetime)->format('Y-m-d');
                $titime = Carbon::parse($tdatetime)->toTimeString();
                $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");

                //->whereRaw("unix_timestamp(cards.created_at) >= $fdatetime")->whereRaw("unix_timestamp(cards.created_at) <= $tdatetime");
               $cardsdb = $cards->get();
                   
         
                } else if ($fromDate && $toDate) {
                    
                // $cards =  $cards->selectRaw("COUNT(cards.id) as number_of_cards ,
                // cards_types.title_en as title_en,cards_types.id as card_type_id")->where('cards.is_used', '=', '0')->whereDate("cards.created_at", ">=", \Carbon\Carbon::parse($fromDate))->whereDate("cards.created_at", "<=", \Carbon\Carbon::parse($toDate))

                    
                $cards =  $cards->selectRaw("SUM(if(cards.is_used = 0, 1, 0)) as number_of_cards ,
                cards_types.title_en as title_en,cards_types.id as card_type_id")->whereDate("cards.created_at", ">=", \Carbon\Carbon::parse($fromDate))->whereDate("cards.created_at", "<=", \Carbon\Carbon::parse($toDate))
                ->where("categories.is_active",'=',1)
                ->where("providers.is_active",'=',1)
                ->where("sub_providers.is_active",'=',1)
                ->where("cards_types.is_active",'=',1)
                ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order');
          $cardsdb = $cards->get();
                    
    
                 
          
                }

            $data = [];

          
                foreach ($cardsdb  as $card) {
                    $officalPrice = 0;
    
                    if (HelperFacade::containsOnlyNull($filter) || ($fromDate && $toDate && (!$cardTypeId && !$subProviderId && !$providerId && !$categoryId))) {
                        $wholePrice = WholeSalePrice::where('card_type_id', $card->card_type_id)->first();
                        if ($wholePrice) {
    
                            $officalPrice = HelperFacade::truncate_number($wholePrice->price * $card->number_of_cards, 3);
                        }
                    } else {
    
                        if ($cardTypeId) {
    
                            $wholePrice = WholeSalePrice::where('card_type_id', $cardTypeId)->first();
                            if ($wholePrice) {
    
                                $officalPrice = HelperFacade::truncate_number($wholePrice->price * $card->number_of_cards, 3);
                            }
                        } else if ($subProviderId) {
    
                            $wholePrice = WholeSalePrice::where('card_type_id', $card->card_type_id)->first();
                            if ($wholePrice) {
    
                                $officalPrice = HelperFacade::truncate_number($wholePrice->price * $card->number_of_cards, 3);
                            }
                        } else if ($providerId) {
                            $provider = Provider::find($providerId);
                            $subProvider = SubProvider::find($card->sub_provider_id);
                            $cardTypes = $subProvider->cardsTypes;
                            $price = 0;
    
    
    
    
                            foreach ($cardTypes as $c) {
                                $wholePrice = WholeSalePrice::where('card_type_id', $c->id)->first();
                                if ($fromDate && $toDate && $fromTime && $toTime) {
                                    $fdate = explode('-', $fromDate);
                                    $tdate = explode('-', $toDate);
    
                                    // $ftime = explode(':',$fromTime);
                                    $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                                    $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
    
                                    $fromDateTime = new \DateTime();
    
                                    $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);
    
    
                                    $toDateTime = new \DateTime();
    
                                    $toDateTime =  $toDateTime->setTimestamp($tdatetime);
    
                                    // filter time
    
                                   
                                    $cards = $c->cards()->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                                    ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
    
                                    // $cards = $c->cards()->whereRaw("unix_timestamp(cards.created_at) >= $fdatetime")
                                    //     ->whereRaw("unix_timestamp(cards.created_at) <= $fdatetime")->whereRaw('cards.is_used = 0');
                                    if ($wholePrice && $cards->count() > 0) {
    
    
                                        $price += $wholePrice->price * $cards->count();
                                    }
                                } else if ($fromDate && $toDate) {
                                    $cards = $c->cards()->whereDate('cards.created_at', '>=', \Carbon\Carbon::parse($fromDate))
                                        ->whereDate('cards.created_at', '<=', \Carbon\Carbon::parse($toDate))->where('cards.is_used', 0);
                                    if ($wholePrice && $cards->count() > 0) {
    
    
                                        $price += HelperFacade::truncate_number($wholePrice->price * $cards->count(), 3);
                                    }
                                } else {
                                    if ($wholePrice && $c->cards()->count() > 0) {
    
    
                                        $price += HelperFacade::truncate_number($wholePrice->price * $c->cards()->where('is_used', 0)->count(), 3);
                                    }
                                }
                            }
    
                            $officalPrice = $price;
                        } else if ($categoryId) {
                            $provider = Provider::find($card->provider_id);
                            $subProviders = $provider->subProviders;
    
                            foreach ($subProviders as $s) {
                                $price = 0;
                                $cardTypes = $s->cardsTypes;
                                foreach ($cardTypes as $c) {
                                    $wholePrice = WholeSalePrice::where('card_type_id', $c->id)->first();
    
                                    if ($fromDate && $toDate && $fromTime && $toTime) {
                                        $fdate = explode('-', $fromDate);
                                        $tdate = explode('-', $toDate);
    
                                        // $ftime = explode(':',$fromTime);
                                        $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
                                        $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
    
                                        $fromDateTime = new \DateTime();
    
                                        $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);
    
    
                                        $toDateTime = new \DateTime();
    
                                        $toDateTime =  $toDateTime->setTimestamp($tdatetime);
                                        
                                        $cards = $c->cards()->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
                                        ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
                                        // $cards = $c->cards()->whereRaw("unix_timestamp(cards.created_at) >= $fdatetime")
                                        //     ->whereRaw("unix_timestamp(cards.created_at) <= $fdatetime")->whereRaw('cards.is_used = 0');
                                        if ($wholePrice && $cards->count() > 0) {
                                            $price = HelperFacade::truncate_number($cards->count() * $wholePrice->price, 3);
                                            $officalPrice += $price;
                                        }
                                    } else if ($fromDate && $toDate) {
                                        $cards = $c->cards()->whereDate('cards.created_at', '>=', \Carbon\Carbon::parse($fromDate))
                                            ->whereDate('cards.created_at', '<=', \Carbon\Carbon::parse($toDate))->where('cards.is_used', 0);
                                        if ($wholePrice && $cards->count() > 0) {
                                            $price = HelperFacade::truncate_number($cards->count() * $wholePrice->price, 3);
                                            $officalPrice += $price;
                                        }
                                    } else {
                                        if ($wholePrice && $c->cards()->count() > 0) {
    
    
                                            $price = HelperFacade::truncate_number($c->cards()->where('is_used', 0)->count() * $wholePrice->price, 3);
                                            $officalPrice += $price;
                                        }
                                    }
                                }
                            }
                        }
                    }
    
                    $data[] = [
                        "title_en" => $card->title_en,
                        "number_of_cards" => $card->number_of_cards,
                        "offical_prices" => $officalPrice
                    ];
                }
           

            return datatables()->of($data)->make(true);
        }

        return view('admin.reports.cardsreports', ['categoriesLookup' => $categoriesLookup]);
    }
       //END V_5 UPDATES 28/12/2021


    // UPDATES V_6 04/01/2022
  public function balanceTransactionsReport(Request $request)
    {

        if (!HelperFacade::checkAdminPermission("Balance Transactions Report")) {
            return redirect()->back();
        }
        $currentAdmin = Auth::guard('admin')->user();
        $usersLookup = $this->_userService->lookupByAccessType();
        $adminsLookup = Admin::all()->pluck('username', 'id');
        $filter = $request->filter;
        $fromDate = null;
        $toDate = null;
        $userId = null;
        $adminId = null;
        if($filter){
        $fromDate = $filter["from_date"];
        $toDate = $filter["to_date"];
        $userId = $filter["user_id"];
        $adminId = $filter['admin_id'];
        }

        if (request()->ajax()) {
            $params = $request->all();
            $draw = $params['draw'];
            $start = $params['start'];
            $rowperpage  = $params['length'];
            $search  = $params['search'];
            $searchvalue = $search['value'];

            $data = DB::table('users')->join('transactions', 'transactions.user_id', '=', 'users.id')
            ->leftJoin('admins', 'transactions.created_by_id', '=', 'admins.id')
                ->leftJoin('request_recharges','request_recharges.transaction_id','=','transactions.id')

                ->select(
                    'users.id as user_id',
                    'request_recharges.id as request_recharge_id',
                    'users.name as full_name',
                    'users.username as username',
                    'transactions.id as transaction_id',
                    'transactions.amount as amount',
                    'transactions.type as type',
                    'transactions.created_at as created_at',
                    'transactions.notes as notes',
                    'transactions.receivables as receivables',
                    
                    'users.created_by_id as user_created_by_id',
                    'transactions.created_by_id as transaction_created_by_id',
                    'admins.username as transaction_created_by_username'
                );


           
          
          // filter 

          if ($fromDate && $toDate && $userId && $adminId) {
              $data = $data->whereDate('transactions.created_at',">=",\Carbon\Carbon::parse($fromDate))
              ->whereDate('transactions.created_at',"<=",\Carbon\Carbon::parse($toDate))
              ->where("users.id",$userId)->where('users.created_by_id',$adminId);

          }else if($fromDate && $toDate && $userId){
            $data = $data->whereDate('transactions.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('transactions.created_at',"<=",\Carbon\Carbon::parse($toDate))
            ->where("users.id",$userId);

          }else if($fromDate && $toDate && $adminId) {
            $data = $data->whereDate('transactions.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('transactions.created_at',"<=",\Carbon\Carbon::parse($toDate))
         ->where('users.created_by_id',$adminId);
      
          }else if($userId && $adminId) {
              $fromDate = \Carbon\Carbon::now();
              $toDate = \Carbon\Carbon::now();
            $data = $data->whereDate('transactions.created_at',">=",$fromDate)
            ->whereDate('transactions.created_at',"<=",$toDate)
         ->where('users.created_by_id',$adminId);
          }else if($fromDate && $toDate){
            $data = $data->whereDate('transactions.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('transactions.created_at',"<=",\Carbon\Carbon::parse($toDate));
          }else if($userId){
            $fromDate = \Carbon\Carbon::now();
            $toDate = \Carbon\Carbon::now();
            $data = $data->whereDate('transactions.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('transactions.created_at',"<=",\Carbon\Carbon::parse($toDate))
            ->where("users.id",$userId);
          }else if($currentAdmin->accessType() == "all" && $adminId && $adminId !=0){
            $fromDate = \Carbon\Carbon::now();
            $toDate = \Carbon\Carbon::now();
            $data = $data->whereDate('transactions.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('transactions.created_at',"<=",\Carbon\Carbon::parse($toDate))
            ->where('users.created_by_id',$adminId)->where('transactions.created_by_id',$adminId);
          }  else{
           
            $data = $data->whereYear('transactions.created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('transactions.created_at',\Carbon\Carbon::now()->month)
            ->WhereDay('transactions.created_at',\Carbon\Carbon::now()->day);
            if($currentAdmin->accessType()=="created_users")
            {
           $data = $data->where('users.created_by_id',$currentAdmin->id);
            }
        
           
          }
       
          $data = $data->where(function($query){
              return $query->where('transactions.type', Config::get('constants.TRANSACTION_TYPE.DEPOSIT'))->orWhere('transactions.type', Config::get('constants.TRANSACTION_TYPE.WITHDRAW'));
          });
       
            $totalRecords = $data->count();


            $data = $data->where(function ($query) use ($searchvalue) {
                $query->where('users.name', 'like', '%' . $searchvalue . '%')
                    ->orWhere('users.username', 'like', '%' . $searchvalue . '%')
                    ->orWhere('transactions.amount', 'like', '%' . $searchvalue . '%')
                    ->orWhere('transactions.type', 'like', '%' . $searchvalue . '%')
                    ->orWhere('transactions.notes', 'like', '%' . $searchvalue . '%')
                    ->orWhere('transactions.receivables', 'like', '%' . $searchvalue . '%')
                    ->orWhere('admins.username', 'like', '%' . $searchvalue . '%');
            });
            $totalRecordsWithFilter = $data->count();

            if ($rowperpage != -1)
                $data = $data->skip($start)->take($rowperpage);


            $data = $data->latest('transactions.created_at')->get();

            


            $result = $data->map(function($d){
                $item['full_name'] =$d->full_name;
                
                $item['amount'] = $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount;
                $item['type'] = $d->type;
                $item['created_at'] = $d->created_at;
                $item['notes'] = $d->notes ? $d->notes : null;
                $item['receivables'] =$d->receivables ? $d->receivables : null;
                $item['user_id'] = $d->user_id;
                $item['transaction_id'] = $d->transaction_id;
                $item['request_recharge_id'] = $d->request_recharge_id;
                $item['created_by'] = $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : "";
                return $item;


            });

            
            // foreach ($data as $d) {
            //     if (HelperFacade::containsOnlyNull($filter)) {
            //         if (\Carbon\Carbon::parse($d->created_at)->format('d-m-Y') == \Carbon\Carbon::now()->format('d-m-Y')) {
            //             $result[] = [
            //                 "full_name" => $d->full_name,
            //                 "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                 "type" => $d->type,
            //                 "created_at" => $d->created_at,
            //                 "notes" => $d->notes ? $d->notes : null,
            //                 "receivables" => $d->receivables ? $d->receivables : null,
            //                 "user_id" => $d->user_id,
            //                 "transaction_id" => $d->transaction_id,
            //                 "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //             ];
            //         }
            //     } else {
            //         $fromDate = $filter["from_date"];
            //         $toDate = $filter["to_date"];
            //         $userId = $filter["user_id"];
            //         $adminId = $filter['admin_id'];

            //         if ($fromDate && $toDate && $userId && $adminId) {
            //             $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
            //             if (
            //                 \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
            //                 && $d->user_id == $userId && $d->transaction_created_by_id == $adminId
            //             ) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }
            //         } 
            //        else if ($fromDate && $toDate && $userId) {
            //             $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
            //             if (
            //                 \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
            //                 && $d->user_id == $userId && $d->user_created_by_id == $adminId && $d->transaction_created_by_id == $adminId
            //             ) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }

            //         } 
            //         else if ($fromDate && $toDate && $adminId) {
            //             $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');

            //             if (
            //                 \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
            //                 && $d->transaction_created_by_id == $adminId && $d->user_created_by_id == $adminId
            //             ) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }
            //         }
            //         else if ($userId && $adminId) {
            //             $fromDate = \Carbon\Carbon::now()->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::now()->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
            //             if ($d->user_id == $userId && $d->created_by_id == $adminId && \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }
            //         }
            //         else if ($fromDate && $toDate) {
            //             $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
            //             if (
            //                 \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
            //             ) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }
            //         } else if ($userId) {
            //             $fromDate = \Carbon\Carbon::now()->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::now()->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
            //             if ($d->user_id == $userId && \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }
            //         }else if ($adminId) {
            //             $fromDate = \Carbon\Carbon::now()->format('d-m-Y');
            //             $toDate = \Carbon\Carbon::now()->format('d-m-Y');
            //             $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
            //             if ($d->user_created_by_id == $adminId && $d->transaction_created_by_id == $adminId && \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))) {
            //                 $result[] = [
            //                     "full_name" => $d->full_name,
            //                     "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
            //                     "type" => $d->type,
            //                     "created_at" => $d->created_at,
            //                     "user_id" => $d->user_id,
            //                     "notes" => $d->notes ? $d->notes : null,
            //                     "receivables" => $d->receivables ? $d->receivables : null,
            //                     "transaction_id" => $d->transaction_id,
            //                     "created_by" => $d->created_by_id ? Admin::find($d->created_by_id)->username : ""
            //                 ];
            //             }
            //         }
            //     }
            // }

            return array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordsWithFilter,
                "aaData" => $result
            );
            // return datatables()->of($result)->make(true);
        }

        return view('admin.reports.balancetransactionsreports', ['usersLookup' => $usersLookup, 'adminsLookup' => $adminsLookup]);
    }

    // END UPDATES V_6 04/01/2022

    public function userReport(Request $request)
    {
        if(!HelperFacade::checkAdminPermission("UnReceivables Amounts Report"))
        {
return redirect()->back();
        }
        $admin = Auth::guard('admin')->user();
        $usersLookup = $this->_userService->lookupByAccessType();
        $adminsLookup = Admin::all()->pluck('username','id');
        $filter = $request->filter;

        if (request()->ajax()) {
            $data = DB::table('users')->join('transactions', 'transactions.user_id', '=', 'users.id')

                ->select(
                    'users.id as user_id',
                    'users.name as full_name',
                    'transactions.id as transaction_id',
                    'transactions.amount as amount',
                    'transactions.type as type',
                    'transactions.created_at as created_at',
                    'transactions.notes2 as notes',
                    'transactions.unreceived as unreceived',
                    'transactions.created_by_id as transaction_created_by_id',
                    'users.created_by_id as user_created_by_id'
                );


            $data = $data->where(function($query){
              return $query->where('transactions.type', Config::get('constants.TRANSACTION_TYPE.DEPOSIT'))->orWhere('transactions.type', Config::get('constants.TRANSACTION_TYPE.WITHDRAW'));
            });
            if($admin->accessType() == "created_users"){
                $data = $data->where('users.created_by_id',$admin->id);
            }
            $data = $data->latest('transactions.created_at')->get();
            $result = [];
            foreach ($data as $d) {
                if (HelperFacade::containsOnlyNull($filter)) {
                    if (\Carbon\Carbon::parse($d->created_at)->format('d-m-Y') == \Carbon\Carbon::now()->format('d-m-Y')) {
                        $result[] = [
                            "full_name" => $d->full_name,
                            "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                            "type" => $d->type,
                            "created_at" => $d->created_at,
                            "notes" => $d->notes ? $d->notes : null,
                            "unreceived" => $d->unreceived ? $d->unreceived : null,
                            "user_id" => $d->user_id,
                            "transaction_id" => $d->transaction_id,
                            "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                        ];
                    }
                } else {
                    $fromDate = $filter["from_date"];
                    $toDate = $filter["to_date"];
                    $userId = $filter["user_id"];
                    $adminId= $filter["admin_id"];
                    if ($fromDate && $toDate && $userId && $adminId) {
                        $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
                        $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
                        $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
                        if (
                            \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
                            && $d->user_id == $userId && $d->user_created_by_id == $adminId
                        ) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    } 
                   else if ($fromDate && $toDate && $userId) {
                        $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
                        $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
                        $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
                        if (
                            \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
                            && $d->user_id == $userId
                        ) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    } 
                    else if ($fromDate && $toDate && $adminId) {
                        $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
                        $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
                        $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
                        if (
                            \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
                            && $d->user_created_by_id == $adminId
                        ) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    }
                    else if ($userId && $adminId) {
                        if ($d->user_id == $userId && $d->user_created_by_id == $adminId) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    }
                    else if ($userId) {
                        if ($d->user_id == $userId) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    }
                    else if ($adminId) {
                        if ($d->user_created_by_id == $adminId) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    }
                    else if ($fromDate && $toDate) {
                        $fromDate = \Carbon\Carbon::parse($fromDate)->format('d-m-Y');
                        $toDate = \Carbon\Carbon::parse($toDate)->format('d-m-Y');
                        $createdAt = \Carbon\Carbon::parse($d->created_at)->format('d-m-Y');
                        if (
                            \Carbon\Carbon::parse($createdAt)->gte(\Carbon\Carbon::parse($fromDate)) && \Carbon\Carbon::parse($createdAt)->lte(\Carbon\Carbon::parse($toDate))
                        ) {
                            $result[] = [
                                "full_name" => $d->full_name,
                                "amount" => $d->type ==  Config::get('constants.TRANSACTION_TYPE.WITHDRAW')   ? $d->amount - ($d->amount * 2) : $d->amount,
                                "type" => $d->type,
                                "created_at" => $d->created_at,
                                "user_id" => $d->user_id,
                                "notes" => $d->notes ? $d->notes : null,
                                "unreceived" => $d->unreceived ? $d->unreceived : null,
                                "transaction_id" => $d->transaction_id,
                                "created_by" => $d->transaction_created_by_id ? Admin::find($d->transaction_created_by_id)->username : ""
                            ];
                        }
                    }
                }
            }


            return datatables()->of($result)->make(true);
        }

        return view('admin.reports.usersreports', ['usersLookup' => $usersLookup,'adminsLookup'=>$adminsLookup]);
    }


    public function notesPopup(Request $request)
    {
        $userId = $request->user_id;
        $transactionId = $request->transaction_id;
        $html = view('admin.reports.notes', ['userId' => $userId, "transactionId" => $transactionId])->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    public function notesUserPopup(Request $request)
    {
        $userId = $request->user_id;
        $transactionId = $request->transaction_id;
        $html = view('admin.reports.notes2', ['userId' => $userId, "transactionId" => $transactionId])->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    public function deletenotesPopup(Request $request)
    {
        $id = $request->id;
        $html = view('admin.reports.deletenotes', ['id' => $id])->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    public function deletenotesUserPopup(Request $request)
    {
        $id = $request->id;
        $html = view('admin.reports.deletenotes2', ['id' => $id])->render();
        return ResponseWrapperFacade::successWithData($html);
    }

    public function deletenotes(Request $request)
    {
        try {
            $id = $request->id;
            $transaction = Transaction::findOrFail($id);
            $transaction->receivables = null;
            $transaction->notes = null;
            $transaction->save();
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    public function deletenotesUser(Request $request)
    {
        try {
            $id = $request->id;
            $transaction = Transaction::findOrFail($id);
            $transaction->unreceived = null;
            $transaction->notes2 = null;
            $transaction->save();
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    public function storeUnreceived(Request $request)
    {
        try {


            $userId = $request->user_id;
            $transactionId = $request->transaction_id;
            $notes = $request->notes;
            $unreceived = $request->unreceived;
            $transaction = $this->_transactionsService->findById($transactionId);
            $transaction->unreceived = $unreceived;
            $transaction->notes2 = $notes;
            $transaction->save();

            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function storeReceivables(Request $request)
    {
        try {


            $userId = $request->user_id;
            $transactionId = $request->transaction_id;
            $notes = $request->notes;
            $receivables = $request->receivables;
            $transaction = $this->_transactionsService->findById($transactionId);
            $transaction->receivables = $receivables;
            $transaction->notes = $notes;
            $transaction->save();

            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function netProfitReport(Request $request)
    {
        if(!HelperFacade::checkAdminPermission("Net Profit Report"))
        {
return redirect()->back();
        }
        try {

            $totalStock = 0;
            $totalBalance = 0;
            $netProfit = 0;
            $totalReceivables = 0;
            $totalUnreceived = 0;
            $totalOthers = 0;
            $cardTypes = $this->_cardTypeService->all();
            $users = $this->_userService->all();
            $netProfits = $this->_netProfitService->all();

            foreach ($cardTypes as $c) {
                $totalNumberOfCards = $c->cards()->where('is_used', false)->count();
                $officalPrice = $c->wholePrice->price;
                $totalStock += HelperFacade::truncate_number($officalPrice * $totalNumberOfCards, 3);
            }
            foreach ($users as $user) {
                $balance = $user->currentBalance();
                $totalBalance += $balance;
            }
            $currentMonth = \Carbon\Carbon::now()->format("m");
            $currentYear =  \Carbon\Carbon::now()->year;


            $firstDateOfCurrentMonth = \Carbon\Carbon::createFromFormat('Y-m-d', "$currentYear-$currentMonth-01");

            $monthlyProfit = DB::table('card_transactions')
                ->join('transactions', 'transactions.id', '=', 'card_transactions.transaction_id')
                ->join('cards', 'card_transactions.card_id', '=', 'cards.id')
                ->join('cards_types', 'cards.card_type_id', '=', 'cards_types.id')
                ->join('whole_sale_prices', 'whole_sale_prices.card_type_id', '=', 'cards_types.id')
                ->selectRaw("SUM(transactions.amount) - SUM(whole_sale_prices.price) as monthlyProfit ,
                card_transactions.created_at as created_at")
                ->whereDate("card_transactions.created_at", ">=", $firstDateOfCurrentMonth)
                ->whereDate("card_transactions.created_at", "<=", \Carbon\Carbon::now())
                ->get();

            $monthlyProfit =   $monthlyProfit[0]->monthlyProfit;


            $totalReceivables = $this->_netProfitService->sum("receivables_amount");
            $totalUnreceived = $this->_netProfitService->sum("unreceived_amount");
            $totalOthers = $this->_netProfitService->sum("other_amount");
            $totalCurrentBalance = $this->_netProfitService->sum('current_balance_amount');
            $totalBalance += $totalCurrentBalance;
            $totalNetProfit = $this->_netProfitService->sum('net_profit_amount');
            $stockAmount = $this->_netProfitService->sum('stock_amount');
            $totalmonthlyProfitAmount = $this->_netProfitService->sum('monthly_profit_amount');
            $totalStock += $stockAmount;
            $totalmonthlyProfitAmount += $monthlyProfit;

            $netProfit = ($totalStock - $totalReceivables - $totalBalance  - $totalOthers - $totalNetProfit) + $totalUnreceived;
            $netProfit = $netProfit - $totalmonthlyProfitAmount;
            return view('admin.reports.netprofitreports', ['totalStock' => HelperFacade::truncate_number($totalStock, 3), "totalBalance" => $totalBalance, "netProfit" => $netProfit, "totalReceivables" => $totalReceivables, "totalUnreceived" => $totalUnreceived, "totalOthers" => $totalOthers, "totalmonthlyProfitAmount" => $totalmonthlyProfitAmount, "data" => $netProfits]);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            dd($e->getMessage());
            return redirect()->back();
        }
    }

    public function saveNetProfit(Request $request)
    {
        try {
            $data = json_decode($request->data);

            foreach ($data as $d) {
                if ($d->status == "new") {
                    $this->_netProfitService->create([
                        "receivables_amount" => $d->receivables_amount ? $d->receivables_amount : null,
                        "receivables_notes" => $d->receivables_notes ? $d->receivables_notes : null,
                        "unreceived_amount" => $d->unreceived_amount ? $d->unreceived_amount : null,
                        "unreceived_notes" => $d->unreceived_notes ? $d->unreceived_notes : null,
                        "other_amount" => $d->other_amount ? $d->other_amount  : null,
                        "other_notes" => $d->other_notes ? $d->other_notes : null,
                        "current_balance_amount" => $d->current_balance_amount ? $d->current_balance_amount : null,
                        "current_balance_notes" => $d->current_balance_notes ? $d->current_balance_notes : null,
                        "net_profit_amount" => $d->net_profit_amount ? $d->net_profit_amount : null,
                        "net_profit_notes" => $d->net_profit_notes ? $d->net_profit_notes : null,
                        "stock_amount" => $d->stock_amount ? $d->stock_amount : null,
                        "stock_notes" => $d->stock_notes ? $d->stock_notes : null,
                        "monthly_profit_amount" => $d->monthly_profit_amount ? $d->monthly_profit_amount : null,
                        "monthly_profit_notes" => $d->monthly_profit_notes ? $d->monthly_profit_notes : null
                    ]);
                } else {
                    $this->_netProfitService->update($d->id, [
                        "receivables_amount" => $d->receivables_amount ? $d->receivables_amount : null,
                        "receivables_notes" => $d->receivables_notes ? $d->receivables_notes : null,
                        "unreceived_amount" => $d->unreceived_amount ? $d->unreceived_amount : null,
                        "unreceived_notes" => $d->unreceived_notes ? $d->unreceived_notes : null,
                        "other_amount" => $d->other_amount ? $d->other_amount  : null,
                        "other_notes" => $d->other_notes ? $d->other_notes : null,
                        "current_balance_amount" => $d->current_balance_amount ? $d->current_balance_amount : null,
                        "current_balance_notes" => $d->current_balance_notes ? $d->current_balance_notes : null,
                        "net_profit_amount" => $d->net_profit_amount ? $d->net_profit_amount : null,
                        "net_profit_notes" => $d->net_profit_notes ? $d->net_profit_notes : null,
                        "stock_amount" => $d->stock_amount ? $d->stock_amount : null,
                        "stock_notes" => $d->stock_notes ? $d->stock_notes : null,
                        "monthly_profit_amount" => $d->monthly_profit_amount ? $d->monthly_profit_amount : null,
                        "monthly_profit_notes" => $d->monthly_profit_notes ? $d->monthly_profit_notes : null,
                    ]);
                }
            }
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    public function deleteNetProfit(Request $request)
    {

        try {
            $id = $request->id;
            $this->_netProfitService->delete($id);
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    public function exportNetProfit()
    {


        return Excel::download(new NetProfitExportFromView, "netprofit.xlsx");
    }
}
