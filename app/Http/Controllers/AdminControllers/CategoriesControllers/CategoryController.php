<?php

namespace App\Http\Controllers\AdminControllers\CategoriesControllers;

use App\Facades\ResponseWrapperFacade;
use App\Facades\UploadImageFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\CreateCategoryRequest;
use App\Http\Requests\AdminRequests\UpdateCategoryRequest;
use App\ServicesInterface\CategoryServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{


    private $categoryService;
    public function __construct(CategoryServiceInterface $_categoryService)
    {
        $this->categoryService = $_categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }


    public function getData()
    {
        try {
            return datatables()->of($this->categoryService->latest())->toJson();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $order = $request->order;
            $isActive = $request->is_active;
            $image = $request->file('image');
            $url =  UploadImageFacade::upload($image, "Categories");
            if ($this->categoryService->create([
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "order"=>$order,
                "is_active"=>$isActive,
                "image" => $url
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('User add a new category');
                return redirect()->route('categories.index')->with('success_message',trans('Saved Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $category = $this->categoryService->findById($id);
            // $url = $category->image;
            // $url = explode('/',$url);
            // dd($url[6]);
            return view('admin.categories.edit', compact('category'));
        } catch (\Exception $e) {
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request)
    {
        try {
            $id = $request->id;
            $category = $this->categoryService->findById($id);
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $order = $request->order;
            $isActive = $request->is_active;
            $imageUrl = $category->image;
            $prevDestination = null;
            
            $prevName = null;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $url = $category->image;
                if ($url != null) {
                    $url = explode('/', $url);
                    $prevDestination = $url[5];
                    $prevName = $url[6];
                }
                $imageUrl = UploadImageFacade::upload($image, "Categories",$prevDestination, $prevName);
            }
            if ($this->categoryService->update($id, [
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "order"=>$order,
                "is_active"=>$isActive,
                "image" => $imageUrl
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update category with id ' . $id);
                return redirect()->route('categories.index')->with('success_message',trans('Updated Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/categories/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            $category = $this->categoryService->findById($id);
            if(!$this->categoryService->delete($id))
            return redirect()->back()->with('error_message',trans('Deletion Failed')); 
            $url = $category->image;
            if ($url) {
                $url = explode('/', $url);
                $prevDestination = $url[5];
                UploadImageFacade::removeDir('Categories', $prevDestination);
            }
            
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete category with id ' . $id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
