<?php

namespace App\Http\Controllers\AdminControllers\CategoriesControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\Facades\UploadImageFacade;
use App\Http\Requests\AdminRequests\CreateProviderRequest;
use App\Http\Requests\AdminRequests\UpdateProviderRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class ProviderController extends Controller
{

    private $providerService, $categoryService;

    public function __construct(ProviderServiceInterface $_providerService, CategoryServiceInterface $_categoryService)
    {
        $this->providerService = $_providerService;
        $this->categoryService = $_categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(){
      
        return view('admin.providers.index');
     }

    public function getData()
    {
        try {
            return datatables()->of($this->providerService->getDtData())->toJson();;
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $lookup = $this->lookup();
        $templatesLookup = [
            Config::get('constants.TEMPLATES.ZAIN_TEMPLATE') => trans('Zain Template'),
            Config::get('constants.TEMPLATES.UMNIAH_TEMPLATE') => trans('Umniah Template'),
            Config::get('constants.TEMPLATES.ORANGE_TEMPLATE') => trans('Orange Template'),
            Config::get('constants.TEMPLATES.WITHOUT_TEMPLATE') => trans('Without Template'),

        ];
        return view('admin.providers.create',compact('lookup','templatesLookup'));
    }

    private function lookup(){

        $data = $this->categoryService->lookup();
        return $data;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProviderRequest $request)
    {
        try {
            $categoryId = $request->category_id;
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $template = $request->template;
            $order = $request->order;
            $isActive = $request->is_active;
            $skipSubProviders = $request->skip_subproviders;
            $image = $request->file('image');
            $url =  UploadImageFacade::upload($image, "Providers");
            if ($this->providerService->create([
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "image" => $url,
                "order"=>$order,
                "is_active"=>$isActive,
                "template"=>$template,
                "category_id" => $categoryId,
                "skip_subproviders"=>$skipSubProviders
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add a new provider');
                return redirect()->route('providers.index')->with('success_message',trans('Saved Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = $this->providerService->findById($id);
        $lookup = $this->lookup();
        $templatesLookup = [
            Config::get('constants.TEMPLATES.ZAIN_TEMPLATE') => trans('Zain Template'),
            Config::get('constants.TEMPLATES.UMNIAH_TEMPLATE') => trans('Umniah Template'),
            Config::get('constants.TEMPLATES.ORANGE_TEMPLATE') => trans('Orange Template'),
            Config::get('constants.TEMPLATES.WITHOUT_TEMPLATE') => trans('Without Template'),

        ];
        return view('admin.providers.edit', compact('provider','lookup','templatesLookup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProviderRequest $request)
    {
       
        try {
            
            $id = $request->id;
            $provider = $this->providerService->findById($id);

           

            // check from sub providers
            if($request->skip_subproviders == true)
            {
                if($provider->subProviders()->count()>1)
                {
                    return redirect()->back()->withInput($request->only("category_id",'title_en', 'title_ar',"description_en","description_ar","order","is_active","skip_subproviders"))->with('error_message', trans('Provider should have at most 1 sub provider'));
                }
            }
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $category_id = $request->category_id;
            $template = $request->template;
            $imageUrl = $provider->image;
            $order = $request->order;
            $skipSubProviders = $request->skip_subproviders;
         
            $isActive = $request->is_active;
            $prevDestination = null;
            $prevName = null;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $url = $provider->image;
                if ($url != null) {
                    $url = explode('/', $url);
                    $prevDestination = $url[5];
                    $prevName = $url[6];
                }
                $imageUrl = UploadImageFacade::upload($image, "Providers", $prevDestination, $prevName);
            }
            if ($this->providerService->update($id, [
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "category_id"=>$category_id,
                "order"=>$order,
                "is_active"=>$isActive,
                "template"=>$template,
                "image" => $imageUrl,
                "skip_subproviders"=>$skipSubProviders
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update provider with id ' . $id);
                return redirect()->route('providers.index')->with('success_message',trans('Updated Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/providers/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
            if(! $this->providerService->delete($id))
            return redirect()->back()->with('error_message',trans('Deletion Failed'));
            $provider = $this->providerService->findById($id);
            $url = $provider->image;
            if ($url) {
                $url = explode('/', $url);
                $prevDestination = $url[5];
                UploadImageFacade::removeDir('Providers', $prevDestination);
            }
            
           
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete provider with id ' . $id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
