<?php

namespace App\Http\Controllers\AdminControllers\CategoriesControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\Http\Requests\AdminRequests\CreateCardTypeRequest;
use App\Http\Requests\AdminRequests\UpdateCardTypeRequest;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\Facades\UploadImageFacade;
use DataTables;
class CardTypeController extends Controller
{

    private $_cardsTypeService, $_subProviderService, $_categoryService, $_providerService;
    public function __construct(CardsTypesServiceInterface $cardsTypesService, CategoryServiceInterface $categoryService, SubProviderServiceInterface $subProviderService, ProviderServiceInterface $providerService)
    {
        $this->_cardsTypeService = $cardsTypesService;
        $this->_subProviderService = $subProviderService;
        $this->_providerService = $providerService;
        $this->_categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoriesLookup = $this->_categoryService->lookup();
        return view('admin.cardsTypes.index', compact('categoriesLookup'));
    }
    public function getData(Request $request)
    {
        try {
            $params = $request->all();
          $data=   $this->_cardsTypeService->getDtData($params);
         
            //   return response()->json([
            //     "draw"=>intval($data["draw"]),
            //     "iTotalRecords"=>$data['iTotalRecords'],
            //     "iTotalDisplayRecords"=>$data["iTotalDisplayRecords"],
            //     "data"=>$data['data']
            //   ]
          
                
              //);
              return datatables()->of($data)->toJson();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $lookup = $this->lookup();
        return view('admin.cardsTypes.create', compact('lookup'));
    }

    private function lookup()
    {

        $providers = $this->_providerService->all();

        $data = [];

        foreach ($providers as $provider) {
            $items = [];
            if ($provider->subProviders()->count() > 0) {
                foreach ($provider->subProviders as $subProvider) {

                    $items[$subProvider->id] = $subProvider->title_en;
                }

                $data[$provider->title_en] = $items;
            }
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCardTypeRequest $request)
    {
        try {
            $sub_provider_id = $request->sub_provider_id;
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $price = $request->price;
            $image = $request->file('image');
            $url =  UploadImageFacade::upload($image, "CardsTypes");
            $order = $request->order;
            $isActive = $request->is_active;
            $template = $request->template;
            $code = $request->code;

            if ($this->_cardsTypeService->create([
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "price" => $price,
                "image" => $url,
                "order"=>$order,
                "is_active"=>$isActive,
                "sub_provider_id" => $sub_provider_id,
                "template"=>$template,
                'card_code'=>$code
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add a new card type');
                return redirect()->route('cardsTypes.index')->with('success_message',trans("Saved Successfully"));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cardType = $this->_cardsTypeService->findById($id);
        $lookup = $this->lookup();
        return view('admin.cardsTypes.edit', compact("cardType", "lookup"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCardTypeRequest $request)
    {
        try {
            $id = $request->id;
        
            $cardType = $this->_cardsTypeService->findById($id);
            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $price = $request->price;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $subProviderId = $request->sub_provider_id;

            $imageUrl = $cardType->image;
            $prevDestination = null;
            $prevName = null;
            $order = $request->order;
            $isActive = $request->is_active;
            $template = $request->template;
            $code = $request->card_code;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $url = $cardType->image;
                if ($url != null) {
                    $url = explode('/', $url);
                    $prevDestination = $url[5];
                    $prevName = $url[6];
                }
                $imageUrl = UploadImageFacade::upload($image, "CardsTypes", $prevDestination, $prevName);
            }

            if ($this->_cardsTypeService->update($id, [
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "price" => $price,
                "image" => $imageUrl,
                "order"=>$order,
                "is_active"=>$isActive,
                "sub_provider_id" => $subProviderId,
                "template"=>$template,
                'card_code'=>$code
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update card type with id ' . $id);
                return redirect()->route('cardsTypes.index')->with('success_message',trans("Updated Successfully"));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/cardsTypes/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }

            if( !$this->_cardsTypeService->delete($id))
            return redirect()->back()->with('error_message',trans('Deletion Failed'));

            $cardType = $this->_cardsTypeService->findById($id);
            $url = $cardType->image;
            if ($url) {
                $url = explode('/', $url);
                $prevDestination = $url[5];
                UploadImageFacade::removeDir('CardsTypes', $prevDestination);
            }
           
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete card type with id ' . $id);
            return redirect()->back()->with('success_message','Deleted Successfully');
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
