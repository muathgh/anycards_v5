<?php

namespace App\Http\Controllers\AdminControllers\CategoriesControllers;

use App\Exceptions\ConstraintsDeleteException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Facades\ResponseWrapperFacade;
use App\Http\Requests\AdminRequests\CreateSubProviderRequest;
use App\Http\Requests\AdminRequests\UpdateSubProviderRequest;
use App\Services\ProviderService;
use App\Services\SubProviderService;
use Illuminate\Support\Facades\Auth;
use App\Facades\UploadImageFacade;
use App\ServicesInterface\CategoryServiceInterface;

class SubProviderController extends Controller
{


    private $_categoryService,$subProviderService, $providerService;
    public function __construct(CategoryServiceInterface $categoryService,SubProviderService $_subProviderService,ProviderService $_providerService)
    {
        $this->subProviderService = $_subProviderService;
        $this->providerService = $_providerService;
        $this->_categoryService= $categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoriesLookup = $this->_categoryService->lookup();
        return view('admin.subProviders.index',["categoriesLookup"=>$categoriesLookup]);
    }

    public function getData(Request $request)
    {
        try {
            $data = $request->all();
            return datatables()->of($this->subProviderService->getDtData($data))->toJson();;
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookup = $this->lookup();
        return view('admin.subProviders.create',compact('lookup'));
    }

    private function lookup(){

        $data = $this->providerService->lookup();
        return $data;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSubProviderRequest $request)
    {
        try {
            $providerId = $request->provider_id;
            $provider = $this->providerService->findById($providerId);
            //get sub providers
           
            if($provider->subProviders()->count()>0 && $provider->skip_subproviders)
            {
                return redirect()->back()->withInput($request->only('title_en', 'title_ar',"description_en","description_ar","order","is_active","provider_id"))->with('error_message', trans('Provider already have sub providers'));
            }

            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
            $image = $request->file('image');
            $order = $request->order;
            $isActive = $request->is_active;
            $template = $request->template;
            if($template)
            $template = trim($template);
            $url =  UploadImageFacade::upload($image, "SubProviders" );
            if ($this->subProviderService->create([
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "image" => $url,
                "order"=>$order,
                "is_active"=>$isActive,
                "provider_id" => $providerId,
                "template"=>$template
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin add a new sub provider');
                return redirect()->route('subProviders.index')->with('success_message',trans('Saved Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = $this->providerService->findById($id);
        return view('admin.subProviders.show', compact("provider"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subProvider= $this->subProviderService->findById($id);
        $lookup = $this->lookup();
        return view('admin.subProviders.edit',compact('subProvider','lookup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubProviderRequest $request)
    {
        try {
            $id = $request->id;
            $subProvider = $this->subProviderService->findById($id);
            $providerId = $request->provider_id;
            $provider = $this->providerService->findById($providerId);
            if($provider->subProviders()->where("id","!=",$id)->count()>0 && $provider->skip_subproviders)
            {
                return redirect()->back()->withInput($request->only('title_en', 'title_ar',"description_en","description_ar","order","is_active","provider_id"))->with('error_message', trans('Provider already have sub providers'));

            }

            $titleEn = $request->title_en;
            $titleAr = $request->title_ar;
            $descriptionEn = $request->description_en;
            $descriptionAr = $request->description_ar;
           
            $imageUrl = $subProvider->image;
            $order = $request->order;
            $isActive = $request->is_active;
            $template = $request->template;
            if($template)
            $template = trim($template);
            $prevDestination = null;
            $prevName = null;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $url = $subProvider->image;
                if ($url != null) {
                    $url = explode('/', $url);
                    $prevDestination = $url[5];
                    $prevName = $url[6];
                }
                $imageUrl = UploadImageFacade::upload($image, "SubProviders",$prevDestination, $prevName);
            }
       
            if ($this->subProviderService->update($id, [
                "title_en" => $titleEn,
                "title_ar" => $titleAr,
                "description_en" => $descriptionEn,
                "description_ar" => $descriptionAr,
                "provider_id"=>$providerId,
                "order"=>$order,
                "is_active"=>$isActive,
                "image" => $imageUrl,
                "template"=>$template
            ])) {
                activity()
                    ->performedOn(Auth::guard('admin')->user())
                    ->causedBy(Auth::guard('admin')->user())
                    ->log('Admin update sub provider with id ' . $id);
                return redirect()->route('subProviders.index')->with('success_message',trans('Updated Successfully'));
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $id = $request->id;
            if(request()->ajax()){
               
                $view = view('layouts.includes.delete-popup',["id"=>$id,"url"=>url("/admin/subProviders/destroy?id=".$id)])->render();
                
                return ResponseWrapperFacade::successWithData(["html"=>$view]);
            }
           $subProvider = $this->subProviderService->findById($id);
           if(!$this->subProviderService->delete($id))
           return redirect()->back()->with('error_message',trans('Deletion Failed'));
         
            $url = $subProvider->image;
            if ($url) {
                $url = explode('/', $url);
                $prevDestination = $url[5];
                UploadImageFacade::removeDir('SubProviders', $prevDestination);
            }
            
            activity()
                ->performedOn(Auth::guard('admin')->user())
                ->causedBy(Auth::guard('admin')->user())
                ->log('Admin delete sub provider with id ' . $id);
            return redirect()->back()->with('success_message',trans('Deleted Successfully'));
        } 
        
        catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return redirect()->back();
        }
    }
}
