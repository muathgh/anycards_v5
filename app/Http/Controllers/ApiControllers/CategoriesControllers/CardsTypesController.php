<?php

namespace App\Http\Controllers\ApiControllers\CategoriesControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTypePointsServiceInterface;
use App\ServicesInterface\UsersPriceOfSaleServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CardsTypesController extends Controller
{


    private $_cardTypeService, $_cardTypePointsService,$_userPriceOfSaleService;
    public function __construct(CardsTypesServiceInterface $cardTypeService, CardTypePointsServiceInterface $cardTypePointsService,UsersPriceOfSaleServiceInterface $usersPriceOfSaleService)
    {
        $this->_cardTypeService = $cardTypeService;
        $this->_cardTypePointsService = $cardTypePointsService;
        $this->_userPriceOfSaleService = $usersPriceOfSaleService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subProviderId)
    {
        try {
            $data = $this->_cardTypeService->getCardTypeBySubProviderIdByStatus($subProviderId);
            return ResponseWrapperFacade::successWithData($data);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }


    public function getCardsTypePoints(Request $request)
    {
        try {

            $providerId = $request->providerId;
            $data = $this->_cardTypePointsService->getCardTypePointsBySubProviderResource($providerId);

            return ResponseWrapperFacade::successWithData($data);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function getUserPriceOfSale(Request $request){
        try {

            $providerId = $request->providerId;
            $data = $this->_userPriceOfSaleService->getUserPriceOfSaleResource($providerId);

            return ResponseWrapperFacade::successWithData($data);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function updateUserPriceOfSale(Request $request){
        try{
        $cardTypeId = $request->card_type_id;
        $value = $request->value;
        $response = $this->_userPriceOfSaleService->updateUserPriceOfSale($cardTypeId,$value);
        return ResponseWrapperFacade::success();
        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $cardType = $this->_cardTypeService->getCardTypeResource($id);
            return ResponseWrapperFacade::successWithData($cardType);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    public function getCardTypesBySubProviderId($id)
    {
        try {

            $data = $this->_cardTypeService->getCardTypeBySubProviderId($id);
            return ResponseWrapperFacade::successWithData($data);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
