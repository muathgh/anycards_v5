<?php

namespace App\Http\Controllers\ApiControllers\UserControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\ServicesInterface\NotificationsServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{


    private $_userService, $_notificationService;
    public function __construct(UsersServiceInterface $userService, NotificationsServiceInterface $notificationService)
    {
        $this->_userService = $userService;
        $this->_notificationService = $notificationService;
    }

    public function updateFcmToken(Request $request)
    {
        try {
            $userId = auth("api")->user()->id;
            $token = $request->token;
            $fcmToken = DB::table("user_fcm_tokens")->where('user_id', $userId)->first();
            if (!$fcmToken) {
                DB::table('user_fcm_tokens')->insert(["token" => $token, "user_id" => $userId]);
            } else if ($fcmToken != $token) {
                DB::table('user_fcm_tokens')->where("user_id", $userId)->update(["token" => $token]);
            }
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->_userService->getUserData();
        return ResponseWrapperFacade::successWithData($data);
    }

    public function userNotificationsCount(Request $request)
    {
        try {

            return ResponseWrapperFacade::successWithData($this->_notificationService->userNotificationCount());
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function updateProfile(Request $request)
    {
        try {
            $user = auth("api")->user();

            $mobileNumber = $request->mobile_number;
            $duplicateUser = User::where('mobile_number', $mobileNumber)->where('id', '!=', $user->id)->first();
            if ($duplicateUser) {
                return ResponseWrapperFacade::successWithData([
                    "duplicate" => true
                ]);
            }
            if ($request->has('current_password') && $request->has('new_password')) {
                if (Hash::check($request->current_password, $user->password)) {
                    $password = Hash::make($request->new_password);

                    $this->_userService->update($user->id, $password, [
                        "mobile_number" => $mobileNumber
                    ]);
                } else {
                    return ResponseWrapperFacade::successWithData([
                        "incorrect_password" => true
                    ]);
                }
            } else {
                $this->_userService->update($user->id, null, [
                    "mobile_number" => $mobileNumber
                ]);
            }

            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function updateFirstTimeLogin(Request $request){
        try{
auth("api")->user()->first_time_login = true;
auth("api")->user()->save();
return ResponseWrapperFacade::success();
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }

    }

    public function userNotifications(Request $request)
    {
        try {
            $params = $request->all();
            return ResponseWrapperFacade::successWithData($this->_notificationService->userNotificationLatest($params));
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function updateNotificationsToSeen(Request $request)
    {
        try {
            $this->_notificationService->updateUserNotification();
            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function fullname(Request $request){
        try{
            $username = $request->username;
            $currentUser = auth("api")->user();
            $user = $this->_userService->query()->where('username',$username)->first();
            if(!$user){
                return ResponseWrapperFacade::failureWithMessage("user_not_exist");
            }
            if($currentUser->id == $user->id)
            {
                return ResponseWrapperFacade::failureWithMessage("cant_send_to_same_user");
            }
            return ResponseWrapperFacade::successWithData([
                "fullname"=>$user->name
            ]);
        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
