<?php

namespace App\Http\Controllers\ApiControllers\UserControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Facades\SmsSenderFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequests\LoginRequest;
use App\Mail\OtpMail;
use App\ServicesInterface\UserDeviceInfoServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Ichtrojan\Otp\OtpFacade;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $username;
    private $_userDeviceInfoService, $_userService;
    public function __construct(UserDeviceInfoServiceInterface $userDeviceInfoService, UsersServiceInterface $userService)

    {

        $this->username = 'username';
        $this->_userDeviceInfoService = $userDeviceInfoService;
        $this->_userService = $userService;
    }

    public function username()
    {
        return $this->username; //or whatever field
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginRequest $request)
    {
        try {

            $username = $request->username;
            $password = $request->password;

            $uniqueId = $request->unique_id;

            if (!$token = auth("api")->attempt(["username" => $username, "password" => $password])) {

                return ResponseWrapperFacade::failureWithCodeAndError(401, "Unauthorized");
            }
           
            $userDeviceInfo = $this->_userDeviceInfoService->findByUniqueIdAndUserName($uniqueId,$username);
            if (auth("api")->user()->status != "active") {
                return ResponseWrapperFacade::failureWithCodeAndError(401, "InActive");
            }
            if (!$userDeviceInfo) {
                $otp =   OtpFacade::generate($username, 5, 10);

                //find user by username

                $user = $this->_userService->getUserByUserName($username);
                if ($user->email)
                    Mail::to($user->email)->send(new OtpMail($otp->token));

                if($user->mobile_number){
                    $mobileNumber = HelperFacade::formatMobileNumber($user->mobile_number);
                    SmsSenderFacade::sendOtp($otp->token,$mobileNumber);
                }

                $response = response()->json([
                    "otp" => $otp,
                    "status" => "new_device"

                ], 200);


                return $response;
            }
            $user = auth("api")->user();
            $user->last_login = now();
            $user->save();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }

        return $this->createNewToken($token);
    }

    public function reSendOtp(Request $request)
    {
        try {
            $username = $request->username;
            $otp =   OtpFacade::generate($username, 5, 10);
            $user = $this->_userService->getUserByUserName($username);
            if ($user->email)
                Mail::to($user->email)->send(new OtpMail($otp->token));

            if($user->mobile_number){
                $mobileNumber = HelperFacade::formatMobileNumber($user->mobile_number);
                SmsSenderFacade::sendOtp($otp->token,$mobileNumber);
            }

            $response = response()->json([
                "otp" => $otp,
                "status" => "new_device"

            ], 200);


            return $response;
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function validateOtp(Request $request)
    {
        try {

            $uniqueId = $request->unique_id;
            $device = $request->device;
            $otp = $request->otp;
          
            $username = $request->username;
            $isOtpValid = OtpFacade::validate($username, $otp);
           
            if (!$isOtpValid->status) {
                return response()->json([
                    "status" => false,
                    "message" => "invalid_otp"
                ], 500);
            }

            $this->_userDeviceInfoService->create([
                "device_name" => $device,
                "unique_id" => $uniqueId,
                "username" => $username
            ]);

            return response()->json([
                "status" => true,
                "message" => "success"
            ], 200);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth("api")->factory()->getTTL() * 60,
            'user' => auth("api")->user()
        ]);
    }
    public function refresh()
    {
        return $this->createNewToken(auth("api")->refresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
