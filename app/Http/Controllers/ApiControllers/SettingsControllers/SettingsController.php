<?php

namespace App\Http\Controllers\ApiControllers\SettingsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\SettingModels\Setting;
use Illuminate\Http\Request;


class SettingsController extends Controller{


    public function index(Request $request){
        $settings = Setting::where('key','APP_VERSION')->first();
        if($settings){
            return ResponseWrapperFacade::successWithData($settings->value);
        }

        return ResponseWrapperFacade::success();
    }
}