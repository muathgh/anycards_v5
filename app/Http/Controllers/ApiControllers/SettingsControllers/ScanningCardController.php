<?php

namespace App\Http\Controllers\ApiControllers\SettingsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\ServicesInterface\SettingsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ScanningCardController extends Controller
{

    private $_settingService;
    public function __construct(SettingsServiceInterface $settingService)
    {
        $this->_settingService = $settingService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $startScan = $this->_settingService->getValueByKey('START_SCAN');
        return ResponseWrapperFacade::successWithData($startScan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            
            $rules = [
            "code"=>"required",
            "serial"=>"required"
            ];
            $customMessages = [
                "code.required"=>trans('This field is required'),
                "serial.required"=>trans('This field is required')
            ];
            $validator = Validator::make($request->all(), $rules,$customMessages);
        /*IF the validators fail*/
        if ($validator->fails()) {
            return ResponseWrapperFacade::failure();
        }

        $isScanStarted  = $this->_settingService->getValueByKey('START_SCAN');
        if($isScanStarted ==0)
        return ResponseWrapperFacade::successWithMessage('SCAN_STOPPED');

            $code = $request->code;
            $serial = $request->serial;
            $data = ["code"=>$code,"serial"=>$serial];
            DB::table('scanned_cards')->insert($data);

            return ResponseWrapperFacade::successWithMessage("SUCCESS");
            


        }catch(\Exception $e){

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
