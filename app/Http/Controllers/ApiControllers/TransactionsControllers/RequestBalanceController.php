<?php

namespace App\Http\Controllers\ApiControllers\TransactionsControllers;

use App\Facades\FcmNotificationFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\SettingModels\UserNotification;
use App\ServicesInterface\RequestRechargeServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RequestBalanceController extends Controller
{

    private $_requestRechargeService;
    public function __construct(RequestRechargeServiceInterface $requestRechargeService)
    {

        $this->_requestRechargeService = $requestRechargeService;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = $this->_requestRechargeService->getRequestRechargeResource();
            return ResponseWrapperFacade::successWithData($data);

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $amount = $request->amount;
            $type = $request->type;
            $text= null;
            $image = null;
            if($type=="text"){
                $text = $request->text;
            }else if($type=="image"){
                $image = $request->image;
            }

            $user = auth("api")->user();
            if ($this->_requestRechargeService->checkIfUserHavePendingRequests($user->id) > 0)
            return ResponseWrapperFacade::failureWithMessage(Config::get('constants.REQUEST_RECHARGE_MESSAGES.USER_ALREADY_HAVE_PENDING_REQUEST'));

            $this->_requestRechargeService->create([
                "amount" => $amount,
                "status" => Config::get('constants.REQUEST_RECHARGE_STATUS.PENDING'),
                "user_id" => $user->id,
                "attachment_text"=>$text,
                "attachment_mobile_image"=>$image
            ]);
            
            $notification = new UserNotification();
            $notification->user_id = $user->id;
            $notification->title_ar = "طلب رصيد";
            $notification->title_en = "Request Balance";
            $notification->description_en = "Request Balance";
            $notification->description_ar = "طلبك قيد المراجعة من قبل الادارة";
            $notification->type = "REQUEST_BALANCE";
            $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $user->id)->first();
            $notification->save();
            if ($fcmToken) {
                $token = $fcmToken->token;
                FcmNotificationFacade::sendNotification($token,$notification->description_ar,$notification->title_ar);

            }
            

            return ResponseWrapperFacade::success();
        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
