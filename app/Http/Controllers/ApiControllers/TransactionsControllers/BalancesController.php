<?php

namespace App\Http\Controllers\ApiControllers\TransactionsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\ServicesInterface\BalancesServiceInterface;
use App\ServicesInterface\BalanceTransactionsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BalancesController extends Controller
{

    private $_balanceService,$_balanceTransactionService;
    public function __construct(BalancesServiceInterface $balancesService,BalanceTransactionsServiceInterface $balanceTransactionService)
    {
        $this->_balanceService = $balancesService;
        $this->_balanceTransactionService = $balanceTransactionService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
$user = auth("api")->user();
$user = User::find($user->id);
$balance = $this->_balanceService->userCurrentBalance($user->id);
$paidUp =$user->paidUp();

return ResponseWrapperFacade::successWithData(["balance"=>$balance,"paidup"=>$paidUp ]);

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function balanceTransaction(Request $request){
try{
    return ResponseWrapperFacade::successWithData(array_values($this->_balanceTransactionService->getBalanceTransactionsResource($request->all())->toArray()));

}catch(\Exception $e){
    Log::channel('mysql')->error($e->getMessage());
    return ResponseWrapperFacade::failureWithMessage($e->getMessage());
}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
