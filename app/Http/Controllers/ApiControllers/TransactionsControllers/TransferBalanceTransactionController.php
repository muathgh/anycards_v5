<?php

namespace App\Http\Controllers\ApiControllers\TransactionsControllers;

use App\Facades\HelperFacade;
use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest\TransferBalanceRequest;
use App\ServicesInterface\TransferBalanceServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransferBalanceTransactionController extends Controller
{

    private $_transferBalanceService;
    public function __construct(TransferBalanceServiceInterface $transferBalanceService)
    {
        $this->_transferBalanceService = $transferBalanceService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $result = $this->_transferBalanceService->getResourceData($request->all());
            return ResponseWrapperFacade::successWithData([
                "data"=>$result['data'],
                "total"=>HelperFacade::truncate_number($result['total'],3)
            ]);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransferBalanceRequest $request)
    {
        try{
            $username = $request->username;
            $amount = $request->amount;
           
            $fromUser = auth("api")->user();
            if($fromUser->username == $username)
           return ResponseWrapperFacade::failureWithMessage("You Must Select Another User");

           $result =  $fromUser->transferBalance($username,$amount);

           if(!$result['success']){
            return ResponseWrapperFacade::failureWithMessage($result['error']);
               
           }
           return ResponseWrapperFacade::success();

        }catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
