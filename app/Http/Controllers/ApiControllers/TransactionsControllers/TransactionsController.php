<?php

namespace App\Http\Controllers\ApiControllers\TransactionsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Mail\CardMail;
use App\Services\CardsTypesService;
use App\ServicesInterface\BalancesServiceInterface;
use App\ServicesInterface\CardQuantityLimitServiceInterface;
use App\ServicesInterface\OrdersServiceInterface;
use App\ServicesInterface\TransactionsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class TransactionsController extends Controller
{

    private $_cardTypeService, $_transactionService, $_balanceService, $_orderService,$_quantityLimitService;

    public function __construct(CardsTypesService $cardTypeService, TransactionsServiceInterface $transactionService, BalancesServiceInterface $balanceService, OrdersServiceInterface $orderService,
    CardQuantityLimitServiceInterface $quantityLimitService)
    {
        $this->_cardTypeService = $cardTypeService;
        $this->_transactionService = $transactionService;
        $this->_balanceService = $balanceService;
        $this->_orderService = $orderService;
        $this->_quantityLimitService = $quantityLimitService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function getMyOrders(Request $request)
    {

        try {
            $params = $request->all();

            $data = $this->_orderService->getMyOrdersResources($params);
            return ResponseWrapperFacade::successWithData($data);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    public function getMyOrder($cardTransactionId)
    {
        try {

            $order = $this->_orderService->getMyOrder($cardTransactionId);
            return ResponseWrapperFacade::successWithData($order);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = auth('api')->user();
            $quantity = $request->quantity;
            $cardTypeId = $request->card_type_id;
            $isSendByMail = $request->send_by_mail;
            $cardType = $this->_cardTypeService->findById($cardTypeId);
            $cardsCount = $cardType->cards()->where('is_used', 0)->count();
            if ($cardsCount <= 0 ) {
                return ResponseWrapperFacade::failureWithMessage(Config::get("constants.MESSAGES.NO_CARDS"));
            }
            if($cardsCount < $quantity){
                return ResponseWrapperFacade::failureWithMessage(Config::get("constants.MESSAGES.QUANTITY_EXCEEDS_AVAILABLE_HOLDING_QUANTITY"));

            }

            $todayTransactionsCount =DB::table('card_transactions')->
            join('cards','cards.id','=','card_transactions.card_id')->
            join('cards_types','cards_types.id','=','cards.card_type_id')->
            join('transactions','transactions.id','=','card_transactions.transaction_id')->
           
            whereYear('card_transactions.created_at',\Carbon\Carbon::now()->year)->
            whereMonth('card_transactions.created_at',\Carbon\Carbon::now()->month)->
            whereDay('card_transactions.created_at',\Carbon\Carbon::now()->day)
            ->where('cards_types.id',$cardTypeId)
            ->where('transactions.user_id',$user->id)->count();
            

            $quantityLimit = $this->_quantityLimitService->query()->where('card_type_id',$cardTypeId)->first();
            if($quantityLimit && $quantityLimit->quantity_limit != 0)
            {
            if(($todayTransactionsCount+$quantity) > $quantityLimit->quantity_limit){
                return ResponseWrapperFacade::failureWithMessage(Config::get('constants.MESSAGES.QUANTITY_LIMIT'));

            }
        }

           
            $price = $this->_cardTypeService->getUserCardTypePrice($user->id, $cardTypeId);
            $totalPrice = $price * $quantity;
            // get currentBalance
            $balance = $this->_balanceService->userCurrentBalance($user->id);
            $cards = [];
            $codes = [];

            if ($balance < $totalPrice)
                return ResponseWrapperFacade::failureWithMessage(Config::get('constants.MESSAGES.NOT_ENOUGH_PRICE'));
            for ($i = 0; $i < $quantity; $i++) {
            

                $transaction =   $this->_transactionService->orderCard($cardTypeId, $user->id, $price);
                    $balanceAfterDeduction = $this->_balanceService->balanceDeduction($user->id, $price);
                $this->_balanceService->changeUserBalance($user, $balanceAfterDeduction, $transaction['transaction_id']);
                $card = $transaction['card'];
                $cards[] = [
                    "id" => $card->id,
                    "code" => $card->code,
                    "serial" => $card->serial,
                    "order_number"=>$card->cardTransaction->order_number

                ];
                $codes[] = $card->code;
            }
            $info = [


                "category_title_en" => $card->cardType->subProvider->provider->category->title_en,
                "provider_title_en" => $card->cardType->subProvider->provider->title_en,
                "sub_provider_title_en" => $card->cardType->subProvider->title_en,
                "card_type_title_en" => $card->cardType->title_en,
                "category_title_ar" => $card->cardType->subProvider->provider->category->title_ar,
                "provider_title_ar" => $card->cardType->subProvider->provider->title_ar,
                "sub_provider_title_ar" => $card->cardType->subProvider->title_ar,
                "card_type_title_ar" => $card->cardType->title_ar,
                "expiry_date" => $card->expiry_date,
                "created_at" => $card->cardTransaction->created_at,
                "user" => $user,
                "order_number" => $card->cardTransaction->order_number,
                "template" => $card->cardType->subProvider->template,
                "skip_subproviders" => $card->cardType->subProvider->provider->skip_subproviders,

                "order_date"=>$card->cardTransaction->created_at,
            ];
            $data = [
                "info" => $info,
                "cards" => $cards
            ];

            if ($isSendByMail) {
                $email = $request->email;
                $cardType = $card->cardType;
                $subProvider = $cardType->subProvider;
                $provider = $subProvider->provider;
                $category = $provider->category;
                $cardTypeTitle = $cardType->title_en;
                $subProviderTitle = $subProvider->title_en;
                $providerTitle = $provider->title_en;
                $categoryTitle = $category->title_en;
                $this->sendCodeByEmail($email, $codes,$cardTypeTitle,$subProviderTitle,$providerTitle,$categoryTitle);
            }
            return ResponseWrapperFacade::successWithData($data);
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    private function sendCodeByEmail($email, $codes,$cardTypeTitle,$subProviderTitle,$providerTitle,$categoryTitle)
    {
        try {

            Mail::to($email)->send(new CardMail($cardTypeTitle,$subProviderTitle,$providerTitle,$categoryTitle,$codes));

            return ResponseWrapperFacade::success();
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
