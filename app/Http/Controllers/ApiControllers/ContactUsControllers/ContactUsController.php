<?php

namespace App\Http\Controllers\ApiControllers\ContactUsControllers;

use App\Facades\ResponseWrapperFacade;
use App\Http\Controllers\Controller;
use App\Mail\ContactUsMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function store(Request $request){
        try{

            $name = $request->name;
            $mobileNumber = $request->mobile_number;
            $body = $request->body;
            Mail::to("gharablipro2017@gmail.com")->send(new ContactUsMail($name,$mobileNumber,$body));

            return ResponseWrapperFacade::success();

        }catch(\Exception $e){
            Log::channel('mysql')->error($e->getMessage());
            return ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }

    }
}
