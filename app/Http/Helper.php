<?php

namespace App\Http;

use App\Facades\SettingFacade;

class Helper{

    public function getMaxNotificationTitle(){
       
        return SettingFacade::getValueByKey("MAX_NOTIFICATION_TITLE");
    }
    public function getMaxNotificationDescription(){
        return SettingFacade::getValueByKey("MAX_NOTIFICATION_DESCRIPTION");
    }
}