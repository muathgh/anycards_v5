<?php

namespace App\Http\Middleware;

use App\Facades\ResponseWrapperFacade;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CheckWebUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
  
      
        $response = $next($request);
        //If the status is not approved redirect to login 
        if(Auth::check() && Auth::user()->status != 'active'){
            Auth::logout();
            return redirect()->route("user.login");
        }
        if(request()->ajax()){
            if(Auth::check() && Auth::user()->status != 'active')
            {
            return ResponseWrapperFacade::failureWithMessage("Unauthenticated");
            }
        }
        return $response;
    
}
}
