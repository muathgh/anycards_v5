<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      
       if(Session::has('user_changed_language')){
        app()->setLocale(Session::get('user_changed_language'));
       }else
        if(Session::has('user_locale')){
           
            app()->setLocale(Session::get('user_locale'));
        }
       
        return $next($request);
    }
}
