<?php

namespace App\Http\Middleware;

use App\Facades\ResponseWrapperFacade;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CheckMobileUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
  
      
        $response = $next($request);
        //If the status is not approved redirect to login 
        if(Auth::guard('api')->check() && Auth::guard("api")->user()->status != 'active'){
            
            return ResponseWrapperFacade::failureWithMessage("Unauthenticated");
        }
       
        return $response;
    
}
}
