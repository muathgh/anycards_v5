<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckFirstTimeLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        
        if(Auth::check() && !Auth::user()->first_time_login){
            Auth::user()->first_time_login = true;
            Auth::user()->save();
            return redirect()->route("profile.index")->with('warning_message',trans('You should change your password'));
        }

        return $response;
    }
}
