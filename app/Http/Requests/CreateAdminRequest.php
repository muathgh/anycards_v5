<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "username"=>"required|unique:admins",
            "password"=>"required",
            "offical_price_package_id"=>"required",
        ];
    }

    public function messages()
    {
        return [
"username.required"=>trans('This field is required'),
"password.required"=>trans('This field is required'),
"username.unique" => trans('Username already exist'),
"offical_price_package_id.required"=>trans('This field is required'),
        ];
    }
}
