<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class ChargeBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "amount"=>"required|numeric",
            "user_id"=>"required",
            "type"=>"required"
        ];
    }

    public function messages()
    {
        return [
"amount.required"=>trans('This field is required'),
"user_id.required"=>trans('This field is required'),
"type.required"=>trans('This field is required'),
"amount.numeric" => trans('Amount must be numeric')
        ];
    }
}
