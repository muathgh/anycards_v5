<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class AssignRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "admin_id"=>"required",
            "role"=>"required"
        ];
    }

    public function messages()
    {
        return [
            "admin_id.required"=>trans('This field is required'),
            "role.required"=>trans('This field is required'),
        ];
    }
}
