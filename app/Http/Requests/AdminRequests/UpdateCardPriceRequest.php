<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCardPriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id"=>"required",
            "user_id"=>"required",
            "card_type_id"=>"required",
            "price"=>"required",
            
        ];
    }

    public function messages()
    {
        return [
            "user_id.required"=>trans('This field is required'),
            "card_type_id.required"=>trans('This field is required'),
            "price.required"=>trans('This field is required'),
        ];
    }
}
