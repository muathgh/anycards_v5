<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCardLimitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id"=>"required",
            "category_id"=>"required",
            "provider_id"=>"required",
            "sub_provider_id"=>"required",
            "card_type_id"=>"required",
            "value"=>"required"
        ];
    }

    public function messages()
    {
        return [
        "category_id.required"=>trans('This field is required'),
        "provider_id.required"=>trans('This field is required'),
        "sub_provider_id.required"=>trans('This field is required'),
        "card_type_id.required"=>trans('This field is required'),
        "value.required"=>trans('This field is required'),
        ];
    }
}
