<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "username" => "required|unique:users",
            "full_name" => "required",
            "password" => "required",
            "email" => "nullable|email",
          
            "logo" => "image|mimes:jpeg,png,jpg,gif,svg",
        ];
    }

    public function messages()
    {
        return [
            "username.required" => trans('This field is required'),
            "full_name.required" => trans('This field is required'),
            "password.required" => trans('This field is required'),
            
           
            "email.email" => trans('Enter a valid email'),
            "username.unique" => trans('Username already exist'),
           
           
            "logo.image" => trans('The uploaded file must be an image')

        ];
    }
}
