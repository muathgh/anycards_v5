<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "max_notification_title"=>"required",
            "max_notification_description"=>"required",
            "user_balance_min_limit"=>"required"
        ];
    }

    public function messages()
    {
        return [
"max_notification_title.required"=>trans('This field is required'),
"max_notification_description.required"=>trans('This field is required'),
"user_balance_min_limit.required"=>trans("This field is required")
        ];
    }
}
