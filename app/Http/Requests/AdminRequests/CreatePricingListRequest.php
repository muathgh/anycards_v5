<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePricingListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "package_id"=>"required",
            "card_type_id"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "package_id"=>trans('This field is required'),
            "card_type_id"=>trans('This field is required'),
        ];
    }
}
