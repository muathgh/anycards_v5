<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class OfficalPriceDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "provider_id"=>"required",
            "package_id"=>"required",
            "discount"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "provider_id.required"=>trans('This field is required'),
            "package_id.required"=>trans('This field is required'),
            "discount.required"=>trans('This field is required'),
          
        ];
    }
}
