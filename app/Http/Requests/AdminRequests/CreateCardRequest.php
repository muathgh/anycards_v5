<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            "category_id"=>"required",
            "provider_id"=>"required",
            "sub_provider_id"=>"required",
            "card_type_id"=>"required",
            "code"=>"required",
            "expiry_date"=>"required|date_format:d-m-Y",
        ];
    }

    public function messages()
    {
        return [
            "category_id.required"=>trans('This field is required'),
            "provider_id.required"=>trans('This field is required'),
            "sub_provider_id.required"=>trans('This field is required'),
            "card_type_id.required"=>trans('This field is required'),
            "code.required"=>trans('This field is required'),
            "expiry_date.required"=>trans('This field is required'),
            "expiry_date.date_format"=>trans('Date should be valid'),



            
        ];
    }
}
