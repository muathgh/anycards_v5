<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class PushNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"=>"required",
            "description"=>"required",
            "users"=>"required"
        ];
    }

    public function messages()
    {
        return [
            "title.required"=>trans('This field is required'),
            "description.required"=>trans('This field is required'),
            "users.required"=>trans('This field is required'),
            "title.max"=>trans("The maximum number of characters is 10"),
            "description.max"=>trans("The maximum number of characters is 10")
        ];
    }
}
