<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class UserLimitBalanceNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        
            "notification_ar"=>"required",
            "notification_en"=>"required"
        ];
    }

    public function messages()
    {
        return [
        
            "notification_ar.required"=>trans('This field is required'),
            "notification_en.required"=>trans('This field is required'),
        ];
    }
}
