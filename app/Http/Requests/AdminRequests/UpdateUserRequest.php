<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id"=>"required",
            "username"=>"required|unique:users,username,".$this->id,
            "full_name"=>"required",
            "email"=>"nullable|email",
            "logo"=>"image|mimes:jpeg,png,jpg,gif,svg",
            "created_by_id"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "username.required"=>trans('This field is required'),
            "full_name.required"=>trans('This field is required'), 
            "created_by_id"=>trans('This field is required'),         
         
            "email.email"=>trans('Enter a valid email'),
            "logo.image"=>trans('The uploaded file must be an image')
            
        ];
    }
}
