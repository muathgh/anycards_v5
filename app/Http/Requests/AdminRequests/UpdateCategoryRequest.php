<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title_en"=>"required",
            "title_ar"=>"required",
            "description_en"=>"required",
            "description_ar"=>"required",
            "image"=>"image|mimes:jpeg,png,jpg,gif,svg",
            "is_active"=>"required",
            "order"=>"required"
        ];
    }

    public function messages()
    {
        return [
            "title_en.required"=>trans('This field is required'),
            "title_ar.required"=>trans('This field is required'),
            "description_en.required"=>trans('This field is required'),
            "description_ar.required"=>trans('This field is required'),
            "image.image"=>trans('The uploaded file must be an image'),
            "order.required"=>trans('This field is required'),
            "is_active.required"=>trans('This field is required'),
        ];
    }
}
