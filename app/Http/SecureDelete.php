<?php
namespace App\Http ;

trait SecureDelete {
    public function secureDelete(String ...$relations)
    {
        $hasRelation = false;
        foreach ($relations as $relation) {
            if ($this->$relation()->exists()) {
                $hasRelation = true;
                break;
            }
        }

        if ($hasRelation) {
            
            return false;
        } else {
            $this->delete();
        }

        return true;
    }
}