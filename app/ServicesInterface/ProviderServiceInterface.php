<?php
namespace App\ServicesInterface;

interface ProviderServiceInterface{
    public function findById($id);
    public function getWithCondition($columnName,$value);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getProvidersByCategoryId($categoryId);
    public function getDtData();
    public function lookup();
    public function orderd();
    public function orderdResource();
    public function getProviderStatistics();
}