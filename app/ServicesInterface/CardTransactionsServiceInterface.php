<?php
namespace App\ServicesInterface;

interface CardTransactionsServiceInterface {
    public function findById($id);
    public function all();
    public function latest();
    public function getDtData($data);
    public function count();
    public function query();
}