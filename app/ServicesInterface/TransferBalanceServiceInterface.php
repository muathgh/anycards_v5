<?php
namespace App\ServicesInterface;

interface TransferBalanceServiceInterface {
    public function getDtData($params);
    public function getAdminDtData($params);
    public function getResourceData($data);
}