<?php
namespace App\ServicesInterface;

interface AdminOfficalPriceServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getDtData($params);
    public function getOfficalPriceByPackageId($packageId);
    public function updateByCardTypeAndPackageId($packageId,$cardTypeId,$data);
    public function getAdminOfficalPriceByDoubleCondition($columnName1,$columnName2,$value1,$value2);
    public function query();
}