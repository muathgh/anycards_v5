<?php
namespace App\ServicesInterface;

interface CardTypePointsServiceInterface{
    public function getDtData($params);
    public function getCardTypePointsBySubProviderResource($subProviderId);
}