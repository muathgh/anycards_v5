<?php
namespace App\ServicesInterface;

interface BalancesServiceInterface {
    public function changeUserBalance($user,$balance,$transactionId);
    public function getDtData();
    public function userCurrentBalance($userId);
    public function balanceDeduction($userId,$amount);
    
}