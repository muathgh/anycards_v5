<?php
namespace App\ServicesInterface;

interface TransactionsServiceInterface {
    public function addTransactionToUser($user,$amount,$type);
    public function getDtBalancesTransactionsDataByUser($data);
    public function orderCard($cardTypeId,$userId,$amount);
    public function findById($id);
}