<?php
namespace App\ServicesInterface;

interface CardsServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getCardsByCardTypeId($cardTypeId);
    public function getDtData($data);
    public function getFirstCardByCondition($columnName,$value);
    public function whereRaw($query,$param);
}