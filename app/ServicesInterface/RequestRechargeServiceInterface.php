<?php
namespace App\ServicesInterface;

interface RequestRechargeServiceInterface{
    public function getDataDt($params);
    public function create($data);
    public function checkIfUserHavePendingRequests($userId);
    public function getRequestRechargeResource();
    public function getAdminDtData($params);
    public function findById($id);
    public function query();
}