<?php
namespace App\ServicesInterface;



interface PermissionsServiceInterface{
    public function all();
    public function lookup();
    public function create($name,$guard = 'admin');
    public function getByCondition($column,$value);
    public function getByWhereIn($column,$ids);
}