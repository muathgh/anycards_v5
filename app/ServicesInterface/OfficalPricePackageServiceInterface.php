<?php
namespace App\ServicesInterface;

interface OfficalPricePackageServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function lookup($packageId =null);
    public function getDtData($params);

}