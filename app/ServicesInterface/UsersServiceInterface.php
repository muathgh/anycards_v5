<?php
namespace App\ServicesInterface;

interface UsersServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$password,$data);
    public function delete($id);
    public function getDtData($params);
    public function lookup();
    public function lookupByAccessType();
    public function lookUpFullNameAccessType();
    public function usersByAccessType();
    public function count();
    public function getUserData();
    public function whereIdsIn($ids);
    public function getUserByUserName($username);
    public function getUsersOtpsDtData($params);
    public function query();
    
}

