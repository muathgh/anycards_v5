<?php
namespace App\ServicesInterface;

interface RoleServiceInterface {

    public function create($name,$accessType,$guard = "admin",$isSuperAdmin);
    public function syncPermissions($role,$permissions);
    public function getDtData();
    public function lookup();
    public function findById($id);
    public function update($id,$data);
    

}