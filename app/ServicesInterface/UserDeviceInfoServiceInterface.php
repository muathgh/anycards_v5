<?php
namespace App\ServicesInterface;

interface UserDeviceInfoServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function findByUniqueIdAndUserName($uniqueId,$username);
    public function findByUserId($userId);
}