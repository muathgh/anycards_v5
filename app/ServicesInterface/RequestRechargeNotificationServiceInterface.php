<?php
namespace App\ServicesInterface;

interface RequestRechargeNotificationServiceInterface {
    public function clearAll();
    public function clearNotification($id);
        
    public function latest();
    public function findById($id);

    
}