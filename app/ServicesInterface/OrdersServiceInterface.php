<?php
namespace App\ServicesInterface;

interface OrdersServiceInterface {
    public function getDtData($params);
    public function getMyOrdersResources($params);
    public function getMyOrder($cardTransactionId);
    public function getPriceOfSalesDt($params);
    
}