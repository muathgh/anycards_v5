<?php
namespace App\ServicesInterface;

interface SubProviderServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getSubProviderByProviderId($providerId);
    public function getSubProviderByProviderIdOrderd($providerId);
    public function getDtData($data);
    public function getSubProviderByCondition($columnName,$value);
    
    public function lookup();
}