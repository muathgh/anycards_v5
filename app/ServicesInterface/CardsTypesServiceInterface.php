<?php
namespace App\ServicesInterface;

interface CardsTypesServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getCardTypeBySubProviderId($subProviderId);
    public function getDtData($params);
    public function lookup();
    public function getCardsTypePricesDtData();
    public function createCardPrice($data);
    public function updateCardPrice($id,$data);
    public function deleteCardPrice($id);
    public function findCardPriceById($id);
    public function findCardPriceByCondition($columnName,$value);
    public function findCardPriceByDoubleCondition($columnName1,$columnName2,$value1,$value2);
    public function getUserCardTypePrice($userId,$cardTypeId);
    public function getCardsTypeStatistics();
    public function paginate();
    public function paginateBySubProviderId($subProviderId);
    public function paginateBySubProviderIdOrderd($subProviderId);
    public function getCardTypeResource($cardTypeId);
    public function getCardTypeBySubProviderIdByStatus($subProviderId);
    public function query();
}