<?php
namespace App\ServicesInterface;

interface CardsLimitServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getDtData();
    public function getCardLimitByCardTypeId($id);
    public function updateByColumn($column,$value,$data);

}