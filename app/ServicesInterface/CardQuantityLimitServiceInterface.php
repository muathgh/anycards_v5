<?php
namespace App\ServicesInterface;

interface CardQuantityLimitServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);

    public function getDtData($params);

    public function getQuantityByCardType($cardTypeId);
    public function updateByColumn($column,$value,$data);
    public function delete($id);
    public function query();
   
}