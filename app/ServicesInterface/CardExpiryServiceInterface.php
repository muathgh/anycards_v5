<?php
namespace App\ServicesInterface;

interface CardExpiryServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getDtData();
    public function getCardExpiryByCardTypeId($id);
    public function updateByColumn($column,$value,$data);

}