<?php
namespace App\ServicesInterface;

interface NotificationsServiceInterface {

    public function findById($id);
    public function clearAll();
    public function clearNotification($id);
        
    public function latest();
    public function userNotificationLatest($params);
    public function updateUserNotification();
    public function userNotificationCount();
    
}