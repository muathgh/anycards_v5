<?php
namespace App\ServicesInterface;

interface PricingListServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getDtData($params);
    public function getPricingListByDoubleCondition($columnName1,$columnName2,$value1,$value2);
    public function getPricingListByPackageId($packageId);
    public function updateByCardTypeAndPackageId($packageId,$cardTypeId,$data);
}