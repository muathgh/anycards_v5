<?php
namespace App\ServicesInterface;

interface BalanceTransactionsServiceInterface {
    public function getDtData($params);
    public function getBalanceTransactionsResource($data);
}