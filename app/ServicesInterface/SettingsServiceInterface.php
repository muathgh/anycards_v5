<?php
namespace App\ServicesInterface;

interface SettingsServiceInterface {
    public function getValueByKey($key);
    public function setValueByKey($key,$value);
}