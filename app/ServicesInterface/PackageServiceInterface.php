<?php
namespace App\ServicesInterface;

interface PackageServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function lookup($packageId =null);
    public function lookupByAccessType($packageId = null);
    public function getIncludedPackages($cardTypeId);
    public function getDtData($params);
}