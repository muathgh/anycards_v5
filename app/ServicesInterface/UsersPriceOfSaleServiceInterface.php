<?php
namespace App\ServicesInterface;

interface UsersPriceOfSaleServiceInterface{
    public function findById($id);
    public function all();
    public function latest();
    public function create($data);
    public function update($id,$data);
    public function delete($id);
    public function getDtData($params);
    public function query();
    public function updateByColumn($column,$value,$data);
    public function getUserPriceOfSaleResource($providerId);
    public function updateUserPriceOfSale($cardTypeId,$value);
}