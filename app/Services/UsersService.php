<?php

namespace App\Services;

use App\Facades\HelperFacade;

use App\Models\User;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UsersService implements UsersServiceInterface
{
    public function findById($id)
    {
        return User::findOrFail($id);
    }
    public function all()
    {
        return User::all();
    }
    public function latest()
    {
        return User::latest()->get();
    }
    public function create($data)
    {
        $user = new User($data);
        if ($user->save())
            return $user;
        return false;
    }
    public function count()
    {
        if(Auth::guard("admin")->user()->accessType()=="created_users"){
            return User::where('created_by_id',Auth::guard('admin')->user()->id)->count();
        }
        return User::all()->count();
    }
    public function update($id, $password, $data)
    {
        $user = User::findOrFail($id);
        $user->update($data);
        if ($password)
            $user->password = $password;
        if ($user->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        return  User::findOrFail($id)->secureDelete("balances", "transactions", "requestRecharges", "receivables");
    }

    public function getUserData()
    {
        $user = auth("api")->user();
        $balance = HelperFacade::currentUserBalance($user->id);

        return ["user" => $user, "balance" => $balance];
    }

    public function getDtData($params)
    {

        // current admin
        $admin = Auth::guard('admin')->user();
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];


        $package = $params['package'];
        $adminId = $params['admin_id'];


        $search  = $params['search'];
        // $users = User::orderBy("name",'ASC');
        $users = DB::table('users')
            ->leftJoin('packages', 'users.package_id', 'packages.id')
            ->leftJoin('admins as createdByAdmins', 'users.created_by_id', 'createdByAdmins.id')
            ->leftJoin('admins as updatedByAdmins','users.updated_by_id', 'updatedByAdmins.id')
            
            // ->join('balances', 'balances.user_id', 'users.id')
            ->select(
                'users.id as id',
                'users.name as name',
                'users.username as username',
                'users.email as email',
                'users.mobile_number as mobile_number',
                'users.status as status',
                'users.logo as logo',
                'packages.title_en as package_title',

                'users.created_at as created_at',
                'users.updated_at as updated_at',
                'users.created_by_id as created_by_id',
                'users.updated_by_id as updated_by_id',
                'createdByAdmins.username as created_by',
                'updatedByAdmins.username as updated_by' 
            )->orderBy("users.name", "ASC");

        if ($package) {
            $users = $users->where("packages.id", $package);
        }


        if($admin->accessType() == "created_users"){
            $users=$users->where('users.created_by_id',$admin->id);
        }

        if($adminId){
            $users = $users->where('users.created_by_id',$adminId);
        }


        $totalRecords = $users->count();
        $users = $users->where(function ($query) use ($search) {
            $query->where('users.name', 'like', '%' . $search['value'] . '%')
                ->orWhere('users.username', 'like', '%' . $search['value'] . '%')
                ->orWhere('users.email', 'like', '%' . $search['value'] . '%')
                ->orWhere('packages.title_en', 'like', '%' . $search['value'] . '%')
                ->orWhere('users.created_at', 'like', '%' . $search['value'] . '%')
                ->orWhere('createdByAdmins.username', 'like', '%' . $search['value'] . '%')
                ->orWhere('updatedByAdmins.username', 'like', '%' . $search['value'] . '%')
                ->orWhere('users.mobile_number', 'like', '%' . $search['value'] . '%');
        });
        $totalRecordsWithFilter = $users->count();

        if ($rowperpage != -1)
            $users = $users->skip($start)->take($rowperpage);

        $data = $users->get();


        $data =  $data->map(function ($u) {

            $item['id'] = $u->id;
            $item['name'] = $u->name;
            $item['username'] = $u->username;
            $item['email'] = $u->email;
            $item['mobile_number'] = $u->mobile_number;
            $item['status'] = $u->status;
            $item['logo'] = $u->logo;
            $item['package_title'] = $u->package_title;

            $item['balance'] = HelperFacade::currentUserBalance($u->id);
            $item['created_at'] = $u->created_at;
            $item['created_by'] = $u->created_at != $u->updated_at && $u->updated_by_id != null ? $u->updated_by . " (e) " : $u->created_by;
            return $item;
        });

        // $data =  $data->map(function($u){
        //     $package = $u->package;
        //     $item['id'] = $u->id;
        //     $item['name']= $u->name;
        //     $item['username'] = $u->username;
        //     $item['email'] = $u->email;
        //     $item['mobile_number'] = $u->mobile_number;
        //     $item['status'] = $u->status;
        //     $item['logo'] = $u->logo_or_default;
        //     $item['package_title']=$package ? HelperFacade::getTitle($package) :trans('No Package');

        //     $item['balance'] = $u->balances ? $u->balances()->latest('id')->first() ? $u->balances()->latest('id')->first()->balance :trans('No Balance'): trans('No Balance');
        //  $item['created_at'] = $u->created_at;
        //  $item['created_by'] = $u->createdBy ? $u->createdBy->username : null;
        //     return $item;

        // });


        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );
    }

    public function whereIdsIn($ids)
    {
        return User::whereIn('id', $ids)->get();
    }
    public function getUserByUserName($username)
    {
        return User::where('username', $username)->firstOrFail();
    }
    public function lookup()
    {
        return User::all()->pluck("username", "id");
    }

    public function lookupByAccessType(){
        $admin = Auth::guard("admin")->user();
        if($admin->accessType() == "created_users"){
        return User::where("created_by_id",$admin->id)->pluck("username", "id");
        }

        return User::all()->pluck("username", "id");
    }

    public function lookUpFullNameAccessType(){
        $admin = Auth::guard("admin")->user();
        if($admin->accessType() == "created_users"){
        return User::where("created_by_id",$admin->id)->get()->pluck("full_user_name", "id");
        }

        return User::all()->pluck("full_user_name", "id");
    }

    public function query()
    {
        return User::query();
    }

    public function usersByAccessType() {
        $admin = Auth::guard("admin")->user();
        if($admin->accessType() == "created_users"){
            return User::where("created_by_id",$admin->id)->get();
        }
        return User::all();

    }

    public function getUsersOtpsDtData($params)
    {
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];
        $search  = $params['search'];

        $otps = DB::table("otps")->orderBy("created_at", "desc");
        $totalRecords = $otps->count();

        $otps = $otps->where(function ($query) use ($search) {
            $query->where('otps.identifier', 'like', '%' . $search['value'] . '%')
                ->orWhere('otps.token', 'like', '%' . $search['value'] . '%')
                ->orWhere('otps.created_at', 'like', '%' . $search['value'] . '%');
        });
        $totalRecordsWithFilter = $otps->count();

        if ($rowperpage != -1)
            $otps = $otps->skip($start)->take($rowperpage);

        $data = $otps->get();


        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );
    }
}
