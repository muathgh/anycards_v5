<?php

namespace App\Services;

use App\Models\UserDeviceInfo;
use App\ServicesInterface\UserDeviceInfoServiceInterface;

class UserDeviceInfoService implements UserDeviceInfoServiceInterface{

    public function findById($id){
        return UserDeviceInfo::findOrFail($id);
    }

    public function all(){
        return UserDeviceInfo::all();
    }
    public function latest(){
        return UserDeviceInfo::latest()->get();
    }
    public function create($data){
        $userDeviceInfo = new UserDeviceInfo($data);
        if ($userDeviceInfo->save())
            return $userDeviceInfo;
        return false;
    }
    public function findByUniqueIdAndUserName($uniqueId,$username){
        return UserDeviceInfo::where('unique_id',$uniqueId)->where('username',$username)->first();
    }
    public function findByUserId($userId){
        return UserDeviceInfo::where('user_id',$userId)->first();
    }
}