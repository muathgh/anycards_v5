<?php

namespace App\Services;

use App\Models\CategoryModels\SubProvider;
use App\ServicesInterface\SubProviderServiceInterface;
use Illuminate\Support\Facades\DB;

class SubProviderService implements SubProviderServiceInterface
{
    public function findById($id)
    {

        return SubProvider::findOrFail($id);
    }
    public function all()
    {
        return SubProvider::all();
    }
    public function latest()
    {
        return SubProvider::latest()->get();
    }
    public function create($data)
    {
        $subProvider = new SubProvider($data);
        if ($subProvider->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $subProvider = SubProvider::findOrFail($id);
        $subProvider->update($data);
        if ($subProvider->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        $subProvider = SubProvider::findOrFail($id);
        return $subProvider->SecureDelete("cardsTypes");
    }
    public function getSubProviderByProviderId($providerId)
    {
        return SubProvider::where('provider_id', $providerId)->orderBy('order', 'ASC')->get();
    }
    public function getSubProviderByProviderIdOrderd($providerId)
    {
        return SubProvider::where('provider_id', $providerId)->where('is_active', true)->orderBy('order', 'ASC')->get();
    }

    public function getDtData($params)
    {

        

        $data = DB::Table('sub_providers')->join("providers", 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->select(
                "sub_providers.id as id",
                'sub_providers.title_en as title_en',
                "sub_providers.title_ar as title_ar",
                "sub_providers.description_en as description_en",
                "sub_providers.description_ar as description_ar",
                "sub_providers.image as image",
                "providers.title_en as provider_title_en",
                "providers.title_ar as provider_title_ar",
               "sub_providers.created_at as created_at",
                "sub_providers.is_active as is_active"
            );
        if ($params["provider_id"]) {
            $data = $data->where('providers.id', '=', $params['provider_id']);
        } else if ($params['category_id']) {
            $data = $data->where('categories.id', '=', $params['category_id']);
        }
        $data = $data->orderBy('providers.order')->orderBy('sub_providers.order');
      
        $data = $data->get()->map(function ($s) {
            $item['id'] = $s->id;
            $item['title_en'] = $s->title_en;
            $item['title_ar'] = $s->title_ar;
            $item['description_en'] = $s->description_en;
            $item['description_ar'] = $s->description_ar;
            $item['image'] = $s->image;
            $item['provider_title_en'] = $s->provider_title_en;
            $item['provider_title_ar'] = $s->provider_title_ar;
            $item['created_at'] = $s->created_at;
            $item['is_active'] = $s->is_active;
            return $item;
        });

        return $data;
    }
    public function getSubProviderByCondition($columnName, $value)
    {
        return SubProvider::where($columnName, $value);
    }
    public function lookup()
    {
        return SubProvider::orderBy('order','ASC')->pluck(app()->getLocale() == "ar" ? "title_ar" : "title_en", "id");
    }
}
