<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\Admin;
use App\Models\PricingListModels\Package;
use App\Models\PricingListModels\PricingList;
use App\ServicesInterface\PackageServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PackageService implements PackageServiceInterface
{


    public function findById($id)
    {
        return Package::findOrFail($id);
    }
    public function all()
    {
        return Package::all();
    }
    public function latest()
    {
        return Package::latest("created_at")->get();
    }

    public function getDtData($params)
    {
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];


        $search  = $params['search'];
        $searchvalue = $search['value'];
        $adminId = $params['admin_id'];
        $admin = Auth::guard('admin')->user();

        $packages = DB::table('packages')->leftJoin('admins as createdByAdmins', 'packages.created_by_id', '=', 'createdByAdmins.id')
            ->leftJoin('admins as updatedByAdmins', 'packages.updated_by_id', '=', 'updatedByAdmins.id')
            ->select(
                "packages.title_ar as title_ar",
                'packages.id as id',
                'packages.created_at as created_at',
                'packages.updated_at as updated_at',
                'packages.updated_by_id as updated_by_id',
                'packages.created_by_id as created_by_id',
                'createdByAdmins.username as created_by',
                'updatedByAdmins.username as updated_by'
            );

        if ($adminId && $adminId != 0) {
            $packages = $packages->where('packages.created_by_id', $adminId);
        }


        // if ($admin->accessType() == "created_users") {
        //     $packages = $packages->where('created_by_id', $admin->id);
        // }
        $totalRecords = $packages->count();
        $packages = $packages->where(function ($query) use ($searchvalue) {
            return $query->where('packages.title_ar', 'like', '%' . $searchvalue . '%')
                ->orWhere('packages.created_at', 'like', '%' . $searchvalue . '%');
        });
        $totalRecordsWithFilter = $packages->count();
        if ($rowperpage != -1)
            $packages = $packages->skip($start)->take($rowperpage);
        $packages = $packages->latest();
        $data = $packages->get()->map(function ($p) use ($admin) {

            if ($admin->accessType() == "all") {
                $item['created_by'] =   $p->created_at != $p->updated_at && $p->updated_by_id != null ? $p->updated_by . " (e) " : $p->created_by;

                $item['id'] = $p->id;
                $item['title_ar']  = $p->title_ar;


                $item['created_at'] = $p->created_at;
                $item['show_options'] = $this->showOptions($p, $admin);
                return $item;
            } else if (!$p->created_by_id) {
                $item['created_by'] =   $p->created_at != $p->updated_at && $p->updated_by_id != null ? $p->updated_by . " (e) " : $p->created_by;

                $item['id'] = $p->id;
                $item['title_ar']  = $p->title_ar;


                $item['created_at'] = $p->created_at;
                $item['show_options'] = $this->showOptions($p, $admin);
                return $item;
            } else {
                $packageAdmin = Admin::find($p->created_by_id);
                if ($packageAdmin->isSuperAdmin()) {
                    $item['created_by'] =   $p->created_at != $p->updated_at && $p->updated_by_id != null ? $p->updated_by . " (e) " : $p->created_by;

                    $item['id'] = $p->id;
                    $item['title_ar']  = $p->title_ar;


                    $item['created_at'] = $p->created_at;
                    $item['show_options'] = $this->showOptions($p, $admin);
                    return $item;
                } else if ($admin->id == $p->created_by_id) {

                    $item['created_by'] =   $p->created_at != $p->updated_at && $p->updated_by_id != null ? $p->updated_by . " (e) " : $p->created_by;

                    $item['id'] = $p->id;
                    $item['title_ar']  = $p->title_ar;


                    $item['created_at'] = $p->created_at;
                    $item['show_options'] = $this->showOptions($p, $admin);
                    return $item;
                }
            }
            return null;
        })->filter(function ($value) {
            return !is_null($value);
        });

        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => array_values($data->toArray())
        );
    }


    public function showOptions($p, $admin)
    {
        if ($admin->accessType() == "all") {
            return true;
        }
        if ($p->created_by_id == $admin->id) {
            return true;
        }
        return false;
    }

    public function create($data)
    {
        $package = new Package($data);
        if ($package->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $package = Package::findOrFail($id);
        $package->update($data);
        if ($package->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        return   Package::findOrFail($id)->secureDelete("users");
    }
    // V_5 UPDATES 26/12/2021
    public function lookup($packageId = null)
    {
        // if ($packageId != null)
        //     return  Package::where("id", "!=", $packageId)->get()->pluck(app()->getLocale() == "ar" ? "title_ar" : "title_en", "id");
        // return Package::all()->pluck(app()->getLocale() == "ar" ? "title_ar" : "title_en", "id");
        $lookup = [];
        if ($packageId != null) {
            $packages = Package::where("id", "!=", $packageId)->get();
            foreach ($packages as $package) {
                $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
            }
        } else {
            $packages = Package::all();
            foreach ($packages as $package) {
                $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
            }
        }
        return $lookup;
    }
    // END V_5 UPDATES 26/12/2021
    // V_5 UPDATES 26/12/2021
    public function lookupByAccessType($packageId = null)
    {
        $admin = Auth::guard("admin")->user();
        $locale = app()->getLocale() == "ar" ? "title_ar" : "title_en";
        $lookup  = [];
        if ($packageId != null) {
            if ($admin->accessType() == "created_users") {
                $packages = Package::where("id", "!=", $packageId)
                    ->where('created_by_id', $admin->id)->get();

                foreach ($packages as $package) {
                    $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
                }

                return $lookup;
            }
            $packages = Package::where("id", "!=", $packageId)->get();
            foreach ($packages as $package) {
                $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
            }

            return $lookup;
        }
        if ($admin->accessType() == "created_users") {
            $packages = Package::where('created_by_id', $admin->id)->get();

            foreach ($packages as $package) {
                $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
            }

            return $lookup;
        }


        $packages = Package::all();
        foreach ($packages as $package) {
            $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
        }

        return $lookup;
    }
    // END V_5 UPDATES 26/12/2021
    public function getIncludedPackages($cardTypeId)
    {
        $data = DB::select(DB::raw("
SELECT * FROM packages AS pck WHERE pck.id NOT IN (SELECT package_id FROM pricing_lists as pri where pri.card_type_id = :cardTypeId )", ["cardTypeId" =>
        $cardTypeId]));
        $data = collect($data);
        $data = $data->pluck('title_en', 'id');
        return $data;
        // $packages = DB::table("packages")->whereNotIn("")

    }
}
