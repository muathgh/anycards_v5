<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\CategoryModels\Provider;
use App\ServicesInterface\ProviderServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ProviderService implements ProviderServiceInterface
{
    const CACHE_KEY = "USER.PROVIDERS";
    public function findById($id)
    {
        return Provider::findOrFail($id);
    }
    public function getWithCondition($columnName, $value)
    {
        return Provider::where($columnName, $value)->get();
    }
    public function all()
    {
        return Provider::all();
    }
    public function latest()
    {

        return Provider::latest()->get();
    }
    public function orderd()
    {
        $cacheKey = "order";
        $key = $this->getCacheKey($cacheKey);


        return   Cache::remember($key, 15, function () {
            return Provider::where('is_active', true)->orderBy("order", 'ASC')->get();
        });
    }

    public function orderdResource()
    {
        $data = Provider::where('is_active', true)->orderBy("order", 'ASC')->get()->map(function ($p) {
            $item['id'] = $p->id;
            $item['title_ar'] = $p->title_ar;
            $item['title_en'] = $p->title_en;
            $item['image'] = $p->skip_subproviders ? $p->subProviders()->first()->image :  $p->image;
            $item['skip_subproviders'] = $p->skip_subproviders;
            $item['sub_providers_count'] = $p->subProviders()->count();
            $item['sub_provider_id'] = $p->skip_subproviders && $p->subProviders()->first() ?  $p->subProviders()->first()->id : 0;
            return $item;
        });
        return $data;
    }
    public function create($data)
    {
        $provider = new Provider($data);
        if ($provider->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $provider = Provider::findOrFail($id);
        $provider->update($data);
        if ($provider->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        return   Provider::findOrFail($id)->secureDelete("subProviders");
    }
    public function getProvidersByCategoryId($categoryId)
    {
        return Provider::where('category_id', $categoryId)->orderBy('order', 'ASC')->get();
    }
    public function getDtData()
    {

        $data =  Provider::orderBy('order', 'ASC')->get()->map(function ($p) {
            $item['id'] = $p->id;
            $item['title_en'] = $p->title_en;
            $item['title_ar'] = $p->title_ar;
            $item['description_en'] = $p->description_en;
            $item['description_ar'] = $p->description_ar;
            $item['image_or_default'] = $p->image_or_default;
            $item['category_title_en'] = $p->category->title_en;
            $item['category_title_ar'] = $p->category->title_ar;

            $item['created_at'] = $p->created_at;
            $item['is_active'] = $p->is_active;
            return $item;
        });

        return $data;
    }

    public function lookup()
    {
        return Provider::orderBy('order', 'ASC')->pluck(app()->getLocale() == "en" ? "title_ar" : "title_en", "id");
    }

    public function getProviderStatistics()
    {
        $cardsTypes = null;
        if (Auth::guard("admin")->user()->accessType() == "created_users") {
            $cardsTypes = DB::table("card_transactions")
            ->join('transactions','transactions.id','=','card_transactions.transaction_id')
            ->join('users','users.id','=','transactions.user_id')
            ->join('cards', 'cards.id', '=', 'card_transactions.card_id')
            ->join('cards_types', 'cards_types.id', '=', 'cards.card_type_id')
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->selectRaw('COUNT(card_transactions.id) as count,providers.title_en as title_en,providers.title_ar as title_ar,providers.id as provider_id')
            ->whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())')
            ->where('users.created_by_id',Auth::guard('admin')->user()->id)
            ->groupBy('providers.id', 'providers.title_en', 'providers.title_ar')->get();
        } else {
            $cardsTypes = DB::table("card_transactions")->join('cards', 'cards.id', '=', 'card_transactions.card_id')->join('cards_types', 'cards_types.id', '=', 'cards.card_type_id')->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')->join('providers', 'providers.id', '=', 'sub_providers.provider_id')->selectRaw('COUNT(card_transactions.id) as count,providers.title_en as title_en,providers.title_ar as title_ar,providers.id as provider_id')->whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())')->groupBy('providers.id', 'providers.title_en', 'providers.title_ar')->get();
        }


        return $cardsTypes;
    }

    private function getCacheKey($key)
    {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }
}
