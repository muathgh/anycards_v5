<?php

namespace App\Services;

use App\Models\Admin;
use App\Models\PricingListModels\PriceOfSale;
use App\ServicesInterface\PriceOfSaleServiceInterface;
use Illuminate\Support\Facades\DB;

class PriceOfSaleService implements PriceOfSaleServiceInterface
{
    public function findById($id)
    {
        return PriceOfSale::findOrFail($id);
    }
    public function all()
    {
        return PriceOfSale::all();
    }
    public function latest()
    {
        return PriceOfSale::latest()->get();
    }
    public function create($data)
    {
        $priceOfSale = new PriceOfSale($data);
        if ($priceOfSale->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $priceOfSale = PriceOfSale::findOrFail($id);
        $priceOfSale->update($data);
        if ($priceOfSale->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        PriceOfSale::findOrFail($id)->delete();
    }

    public function getDtData($params)
    {
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];


        $search  = $params['search'];
        $searchvalue = $search['value'];

        $priceOfSaleData = DB::table('price_of_sales')->join(
            'cards_types',
            'price_of_sales.card_type_id',
            '=',
            'cards_types.id'
        )
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->leftJoin("admins as createdByAdmins",'price_of_sales.created_by_id','=','createdByAdmins.id')
            ->leftJoin("admins as updatedByAdmins",'price_of_sales.updated_by_id','=','updatedByAdmins.id')
            ->select(
                'price_of_sales.id as id',
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                'price_of_sales.price as price',
                // "price_of_sales.created_by_id as created_by_id",
                "createdByAdmins.username as created_by",
                "updatedByAdmins.username as updated_by",
                "price_of_sales.created_at as created_at",
                "price_of_sales.updated_at as updated_at"
            )
            ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')
            ->orderBy('cards_types.order');
        $totalRecords = $priceOfSaleData->count();
        $priceOfSaleData = $priceOfSaleData->where(function ($query) use ($searchvalue) {
            $query->where('cards_types.title_en', 'like', '%' . $searchvalue . '%')
            ->orWhere('cards_types.title_ar','like', '%' . $searchvalue . '%')
            ->orWhere('cards_types.title_ar','like', '%' . $searchvalue . '%')
            ->orWhere('price_of_sales.price','like', '%' . $searchvalue . '%')
            ->orWhere('price_of_sales.created_at','like', '%' . $searchvalue . '%')
            ->orWhere('createdByAdmins.username','like', '%' . $searchvalue . '%')
            ->orWhere('updatedByAdmins.username','like', '%' . $searchvalue . '%');
        });
        
        $totalRecordsWithFilter = $priceOfSaleData->count();
        if ($rowperpage != -1)
            $cards = $priceOfSaleData->skip($start)->take($rowperpage);

        $data = $priceOfSaleData->get()->map(function ($p) {

            $item['id'] = $p->id;
            $item['title_en'] = $p->title_en;
            $item['title_ar'] = $p->title_ar;
            $item['price'] = $p->price;
            $item['created_at'] = $p->created_at;
            $item['created_by'] = null;
            if($p->created_by)
            {
                $item['created_by'] = $p->created_by;
            }else if($p->updated_by){
                $item['created_by'] = $p->updated_by;
            }
            return $item;
        });

        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );

    }
    public function getPriceOfSaleByCardTypeId($cardTypeId)
    {
        return PriceOfSale::where("card_type_id", $cardTypeId)->first();
    }
    public function updateByColumn($column, $value, $data)
    {
        $priceOfSale = PriceOfSale::where($column, $value)->first();
        $priceOfSale->update($data);
        if ($priceOfSale->save())
            return $priceOfSale;
        return false;
    }
}
