<?php

namespace App\Services;

use App\Models\ReportsModels\NetProfit;
use App\ServicesInterface\NetProfitServiceInterface;

class NetProfitService implements NetProfitServiceInterface{
    public function findById($id){

        return NetProfit::findOrFail($id);
    }
    public function all(){
        return NetProfit::all();
    }
    public function latest(){
        return NetProfit::latest()->get();
    }
    public function create($data){ 
        $netProfit = new NetProfit($data);
        if ($netProfit->save())
            return true;
        return false;}
    
    public function update($id,$data){
        $netProfit = NetProfit::findOrFail($id);
        $netProfit->update($data);
        if ($netProfit->save())
            return true;
        return false;
        
    }
    public function delete($id){
        NetProfit::findOrFail($id)->delete();

    }
    public function sum($column){
       $netProfit =  NetProfit::query();
       return $netProfit->sum($column);
    }

}