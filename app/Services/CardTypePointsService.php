<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\CategoryModels\CardType;
use App\Models\PricingListModels\PricingList;
use App\ServicesInterface\CardTypePointsServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CardTypePointsService implements CardTypePointsServiceInterface
{
    public function getDtData($params)
    {

        $cardTypes = DB::table('cards_types')->join("sub_providers", 'sub_providers.id', '=', 'cards_types.sub_provider_id')->join('providers', 'providers.id', '=', 'sub_providers.provider_id')->join('categories', 'categories.id', '=', 'providers.category_id')
        ->select("cards_types.title_en as title_en",
        "cards_types.title_ar as title_ar",
        "cards_types.price as points",
    "cards_types.created_at as created_at",
"cards_types.id as id",
"cards_types.price as price");

        if ($params['sub_provider_id']) {
            $cardTypes = $cardTypes->where('sub_providers.id', $params['sub_provider_id']);
        } else if ($params['provider_id']) {
            $cardTypes = $cardTypes->where('providers.id', $params['provider_id']);
        } else if ($params['category_id']) {
            $cardTypes = $cardTypes->where('categories.id', $params['category_id']);
        }


        $data =  $cardTypes->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order')->get()->map(function ($c) {
            $item['id'] = $c->id;
            $item['title_en'] = $c->title_en;
            $item['title_ar'] = $c->title_ar;
            $item['points'] = $c->price;
            $packageId = Auth::user()->package_id;
            $priceList = PricingList::where('package_id', $packageId)->where('card_type_id', $c->id)->first();
            if ($priceList) {
                $item['points'] = $priceList->price;
            }
            return $item;
        });
        return $data;
    }

    public function getCardTypePointsBySubProviderResource($providerId)
    {
        $cardTypes = DB::table('cards_types')->join("sub_providers", 'sub_providers.id', '=', 'cards_types.sub_provider_id')->join('providers', 'providers.id', '=', 'sub_providers.provider_id')->join('categories', 'categories.id', '=', 'providers.category_id')
        ->select("cards_types.title_en as title_en",
        "cards_types.title_ar as title_ar",
        "cards_types.price as points",
    "cards_types.created_at as created_at",
"cards_types.id as id",
"cards_types.price as price",
"cards_types.image as image");

      


        $data =  $cardTypes->where('providers.id','=',$providerId)->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order')->get()->map(function ($c) {
            $item['id'] = $c->id;
            $item['title_en'] = $c->title_en;
            $item['title_ar'] = $c->title_ar;
            $item['points'] = $c->price;
            $item['image'] = $c->image;
            $packageId = auth("api")->user()->package_id;
            $priceList = PricingList::where('package_id', $packageId)->where('card_type_id', $c->id)->first();
            if ($priceList) {
                $item['points'] = $priceList->price;
            }
            return $item;
        });
        return $data;
    }
}
