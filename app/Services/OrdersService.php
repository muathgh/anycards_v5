<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\PricingListModels\PricingList;
use App\Models\PricingListModels\UserPriceOfSale;
use App\Models\TransactionModels\CardTransaction;
use App\Models\TransactionModels\Transaction;
use App\ServicesInterface\OrdersServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class OrdersService implements OrdersServiceInterface
{
    public function getDtData($params)
    {
        $userId = Auth::user()->id;
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];
        $search = $params['search'];


        $data = DB::table('transactions')->join("card_transactions", "card_transactions.transaction_id", '=', 'transactions.id')->join("cards", 'card_transactions.card_id', '=', 'cards.id')->join("cards_types", "cards.card_type_id", '=', 'cards_types.id')
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->select(
                "card_transactions.id as card_transaction_id",
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                "cards.serial as serial",
                "card_transactions.created_at as created_at",
                "card_transactions.order_number as transaction_number"
            )->where('transactions.type', '=', Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))->where('transactions.user_id', '=', $userId);


        if ($params['to_date'] && $params["from_date"]) {
            $data = $data->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($params['to_date']))->whereDate("card_transactions.created_at", ">=", \Carbon\Carbon::parse($params['from_date']));
        } else if ($params['from_date']) {
            $data = $data->whereDate("card_transactions.created_at", ">=", \Carbon\Carbon::parse($params['from_date']));
        } else if ($params['to_date']) {
            $data = $data->whereDate('card_transactions.created_at', "<=", ($params['to_date']));
        }
        if ($params['card_type_id']) {
            $data = $data->where('cards_types.id', '=', $params['card_type_id']);
        } else

        if ($params['sub_provider_id']) {
            $data = $data->where('sub_providers.id', '=', $params['sub_provider_id']);
        } else if ($params["provider_id"]) {
            $data = $data->where('providers.id', '=', $params['provider_id']);
        } else if ($params['category_id']) {
            $data = $data->where('categories.id', '=', $params['category_id']);
        }


        if (
            !$params['to_date'] && !$params['from_date'] && !$params['card_type_id']
            && !$params['sub_provider_id'] && !$params['provider_id'] && !$params['category_id']
        ) {
            $data = $data->whereRaw('DAY(card_transactions.created_at) = DAY(CURDATE())')->whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())')->whereRaw('YEAR(card_transactions.created_at) = YEAR(CURDATE())');
        }

        if ($search['value']) {
            $data = $data->where('cards_types.title_en', 'like', '%' . $search['value'] . '%')
                ->orWhere('cards_types.title_ar', 'like', '%' . $search['value'] . '%')
                ->orWhere('card_transactions.order_number', 'like', '%' . $search['value'] . '%')
                ->orWhere('cards.serial', 'like', '%' . $search['value'] . '%')
                ->orWhere('card_transactions.created_at', 'like', '%' . $search['value'] . '%');
        }

        $total = $data->count();
        // if ($rowperpage != -1) {
        //     $data = $data->skip($start)->take($rowperpage);
        // }

        $data = $data->latest("created_at")->get()->map(function ($t) {

            $item['card_transaction_id'] = $t->card_transaction_id;
            $item['transaction_number'] = $t->transaction_number;
            $item['title_en'] = $t->title_en;
            $item['title_ar'] = $t->title_ar;
            $item['serial'] = $t->serial ? $t->serial : trans('No Serial');
            $item['order_date'] = $t->created_at;
            return $item;
        });
        return $data;
        // return ["data" => $data, "total" => $total, "draw" => $draw];
    }

    public function getPriceOfSalesDt($params)
    {
$user = auth()->user();
        $currentMonth = date('m');
        $currentYear = date('Y');
        $userId = Auth::user()->id;
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];
        $search = $params['search'];


        $data = DB::table('transactions')->join("card_transactions", "card_transactions.transaction_id", '=', 'transactions.id')->join("cards", 'card_transactions.card_id', '=', 'cards.id')->join("cards_types", "cards.card_type_id", '=', 'cards_types.id')
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->join('price_of_sales', 'price_of_sales.card_type_id', '=', 'cards_types.id')
            
            ->join('whole_sale_prices', 'whole_sale_prices.card_type_id', '=', 'cards_types.id')

            ->select(
                "cards_types.id as card_type_id",
                "card_transactions.id as card_transaction_id",
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                "cards.serial as serial",
                "card_transactions.created_at as created_at",
                "card_transactions.order_number as transaction_number",
                "price_of_sales.price as price_of_sale",
                "cards_types.price as price",
                "transactions.amount as whole_sale_price",
                "card_transactions.price_of_sale as card_transaction_price_of_sale"

            )->where('transactions.type', '=', Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))->where('transactions.user_id', '=', $userId);




        if ($params['to_date'] && $params["from_date"]) {
            $data = $data->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($params['to_date']))->whereDate("card_transactions.created_at", ">=", \Carbon\Carbon::parse($params['from_date']));
        } else if ($params['from_date']) {
            $data = $data->whereDate("card_transactions.created_at", ">=", \Carbon\Carbon::parse($params['from_date']));
        } else if ($params['to_date']) {
            $data = $data->whereDate('card_transactions.created_at', "<=", ($params['to_date']));
        }
        if ($params['card_type_id']) {
            $data = $data->where('cards_types.id', '=', $params['card_type_id']);
        } else

        if ($params['sub_provider_id']) {
            $data = $data->where('sub_providers.id', '=', $params['sub_provider_id']);
        } else if ($params["provider_id"]) {
            $data = $data->where('providers.id', '=', $params['provider_id']);
        } else if ($params['category_id']) {
            $data = $data->where('categories.id', '=', $params['category_id']);
        }

        if ($search['value']) {
            $data = $data->where('cards_types.title_en', 'like', '%' . $search['value'] . '%')
                ->orWhere('cards_types.title_ar', 'like', '%' . $search['value'] . '%')
                ->orWhere('card_transactions.order_number', 'like', '%' . $search['value'] . '%')
                ->orWhere('price_of_sales.price', 'like', '%' . $search['value'] . '%')
                ->orWhere('card_transactions.created_at', 'like', '%' . $search['value'] . '%');
        }
        if (
            !$params['to_date'] && !$params['from_date'] && !$params['card_type_id']
            && !$params['sub_provider_id'] && !$params['provider_id'] && !$params['category_id']
        ) {

            $data = $data->whereRaw('DAY(card_transactions.created_at) = DAY(CURDATE())')->whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())')->whereRaw('YEAR(card_transactions.created_at) = YEAR(CURDATE())');
        }
        $total = $data->count();
        // $data = $data->skip($start)
        //     ->take($rowperpage);
        $packageId = Auth::user()->package_id;
        $data = $data->latest("created_at")->get()->map(function ($t) use ($user) {

            $item['card_transaction_id'] = $t->card_transaction_id;
            $item['transaction_number'] = $t->transaction_number;
            $item['title_en'] = $t->title_en;
            $item['title_ar'] = $t->title_ar;
            $item['serial'] = $t->serial ? $t->serial : trans('No Serial');
            $item['order_date'] = $t->created_at;
            $userPriceOfSale = UserPriceOfSale::where('card_type_id',$t->card_type_id)
            ->where('user_id',$user->id)->first();
            $pofsale = 0;
            if($t->card_transaction_price_of_sale != null){
                $pofsale = $t->card_transaction_price_of_sale;
            }else if($userPriceOfSale){

$pofsale = $userPriceOfSale->price;
            }else{
$pofsale = $t->price_of_sale;
            }
            
$item['price_of_sale'] =  $pofsale;
            // $priceList = PricingList::where('package_id', $packageId)->where('card_type_id', $t->card_type_id)->first();
            $item['wholeprice'] = $t->whole_sale_price;
            $item['profit'] = round($item['price_of_sale'] - $item['wholeprice'], 3);
            return $item;
        });
        return ["data" => $data, "total" => $total, "draw" => $draw];
    }
    public function getMyOrdersResources($params)
    {

        $user = auth("api")->user();
        $userId = $user->id;
        $packageId = $user->package_id;
        $page = 0;
        $take = 10;
        if (array_key_exists("page", $params) && $params["page"] != "null") {
            $page = $params["page"];
        }

        $data =   DB::table("transactions")
            ->join("card_transactions", 'card_transactions.transaction_id', '=', 'transactions.id')
            ->join("cards", "cards.id", '=', "card_transactions.card_id")
            ->join('cards_types', 'cards_types.id', '=', 'cards.card_type_id')
            ->join('sub_providers', 'cards_types.sub_provider_id', '=', 'sub_providers.id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join("users", 'users.id', '=', 'transactions.user_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->join('price_of_sales', 'price_of_sales.card_type_id', '=', 'cards_types.id')
            ->join('whole_sale_prices', 'whole_sale_prices.card_type_id', '=', 'cards_types.id')
          

            ->select(
                'card_transactions.id as card_transaction_id',
                "card_transactions.order_number as order_number",
                'card_transactions.price_of_sale as card_transaction_price_of_sale',
                "categories.title_en as categories_title_en",
                "providers.title_en as provider_title_en",
                "providers.title_ar as provider_title_ar",
                "sub_providers.title_en as subprovider_title_en",
                'sub_providers.title_ar as subprovider_title_ar',
                "providers.skip_subproviders as skip_subproviders",
                "card_transactions.created_at as created_at",
                "cards_types.id as card_type_id",
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                "cards.serial as serial",
                "cards.code as code",
                "users.name as user",
                "sub_providers.template as template",
                "transactions.amount as amount",
                "price_of_sales.price as price_of_sale",
                "whole_sale_prices.price as whole_sale_price",
                
                DB::raw("DATE_FORMAT(card_transactions.created_at , '%d-%c-%Y %H:%i:%s') as order_date"),
                DB::raw("DATE_FORMAT(cards.expiry_date,'%Y-%c-%d') as expiry_date")
            );

        $data = $data->where('users.id', '=', $userId);
        $data = $data->where("transactions.type", '=', Config::get('constants.TRANSACTION_TYPE.DEDUCTION'));
        $data =  $data->orderBy("transactions.created_at", "DESC");



        if (array_key_exists("cardtype_id", $params) && $params["cardtype_id"] != null) {
            $data = $data->where('cards_types.id', '=', $params['cardtype_id']);
        } else if (array_key_exists("subprovider_id", $params) && $params["cardtype_id"] != null) {
            $data = $data->where('sub_providers.id', '=', $params['subprovider_id']);
        } else if (array_key_exists("provider_id", $params) && $params["provider_id"] != null) {


            $data = $data->where('providers.id', '=', $params['provider_id']);
        } else if (array_key_exists("category_id", $params) && $params["category_id"] != null) {
            $data = $data->where('categories.id', '=', $params['category_id']);
        }
        if ((array_key_exists("from_date", $params) && $params["from_date"] != null)
            && (array_key_exists("to_date", $params) && $params["to_date"] != null)
        ) {

            $data = $data->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($params["from_date"]))->whereDate('card_transactions.created_at', "<=", \Carbon\Carbon::parse($params["to_date"]));
        } else if ((array_key_exists("from_date", $params) && $params["from_date"] != null)) {
            $data = $data->whereDate('card_transactions.created_at', '>=', \Carbon\Carbon::parse($params["from_date"]));
        } else if ((array_key_exists("to_date", $params) && $params["to_date"] != null)) {
            $data = $data->whereDate('card_transactions.created_at', '<=', \Carbon\Carbon::parse($params["to_date"]));
        }



        if (
            !$params["cardtype_id"] &&  !$params['subprovider_id'] && !$params['provider_id'] &&
            !$params['category_id'] && !$params['from_date'] && !$params['to_date']
        ) {

            $data = $data->whereRaw('DAY(card_transactions.created_at) = DAY(CURDATE())')
                ->whereRaw("MONTH(card_transactions.created_at) = MONTH(CURDATE())")
                ->whereRaw('YEAR(card_transactions.created_at) = YEAR(CURDATE())');
        }


        $result = $data;

        $total = 0;
        $wholePriceTotal = 0;
        $priceOfSaleTotal = 0;

        foreach ($result->get() as $d) {
            $total += $d->amount;
            // $priceList = PricingList::where('package_id', $packageId)->where('card_type_id', $d->card_type_id)->first();
            //    $price = $priceList ? $priceList->price : $d->price;
            $userPriceOfSale = UserPriceOfSale::where('card_type_id',$d->card_type_id)
            ->where('user_id',$userId)->first();
            $price = 0;
            if($d->card_transaction_price_of_sale != null)
            {
                $price = $d->card_transaction_price_of_sale;
            }else if($userPriceOfSale){
                $price = $userPriceOfSale->price;
            }else{
                $price = $d->price_of_sale;
            }
            // $price = $userPriceOfSale ? $userPriceOfSale->price:  $d->price_of_sale;
            $priceOfSaleTotal += $price;
            $wholePriceTotal +=$d->amount;


         


        }

        $profitTotal =  bcdiv($priceOfSaleTotal - $wholePriceTotal, 1, 2);

        $transactionsTotal = $data->count();
        $pagedData = $data;
        if (!$params['is_searching'])
            $pagedData = $pagedData->skip($page * $take)->take($take)->get();
        else
            $pagedData = $pagedData->get();
        // $data = $data->get();



        //skip($page * $take)->take($take)->get();

        return ["result" => $pagedData, "priceofsale" => bcdiv($priceOfSaleTotal, 1, 3), "profit" => bcdiv($profitTotal, 1, 3), "total" => bcdiv($total, 1, 3),"wholePriceTotal"=>bcdiv($wholePriceTotal,1,3), "transactionsCount" => $transactionsTotal, 'totalResult' => sizeof($pagedData)];
    }

    public function getMyOrder($cardTransactionId)

    {
        $cardTransaction = CardTransaction::find($cardTransactionId);

        $card = [
            "code" => $cardTransaction->card->code,
            "expiry_date" => $cardTransaction->card->expiry_date,
            "category"=>$cardTransaction->card->cardType->subProvider->provider->category,
            "provider" => $cardTransaction->card->cardType->subProvider->provider,
            "sub_provider" => $cardTransaction->card->cardType->subProvider,
            "card_type" => $cardTransaction->card->cardType,
            "serial" => $cardTransaction->card->serial,
            "user" => $cardTransaction->transaction->user,
            "skip_subproviders" => $cardTransaction->card->cardType->subProvider->provider->skip_subproviders,
            "order_number" => $cardTransaction->order_number,
            "template"=>$cardTransaction->card->cardType->subProvider->template,
            "created_at"=>$cardTransaction->created_at
        ];
        return $card;
    }
}
