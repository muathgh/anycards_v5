<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\Admin;
use App\Models\OfficalPriceModels\OfficalPricePackage;
use App\ServicesInterface\OfficalPricePackageServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OfficalPricePackageService implements OfficalPricePackageServiceInterface {

    public function findById($id)
    {
        return OfficalPricePackage::findOrFail($id);
    }
    public function all()
    {
        return OfficalPricePackage::all();
    }
    public function latest()
    {
        return OfficalPricePackage::latest("created_at")->get();
    }

    public function create($data)
    {
        $package = new OfficalPricePackage($data);
        if ($package->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $package = OfficalPricePackage::findOrFail($id);
        $package->update($data);
        if ($package->save())
            return true;
        return false;
    }

    public function lookup($packageId = null)
    {
        $lookup = [];
        if ($packageId != null) {
            $packages = OfficalPricePackage::where("id", "!=", $packageId)->get();
            foreach ($packages as $package) {
                $lookup[$package->id] = $package->title_ar . '-' . $package->title_en;
            }
        }

        return $lookup;
    }

    public function delete($id)
    {
        return   OfficalPricePackage::findOrFail($id)->secureDelete("admins");
    }

    public function getDtData($params){
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];


        $search  = $params['search'];
        $searchvalue = $search['value'];
        $adminId = $params['admin_id'];
        $admin = Auth::guard('admin')->user();
        $packages = DB::table('offical_price_packages')->leftJoin('admins as createdByAdmins', 'offical_price_packages.created_by_id', '=', 'createdByAdmins.id')
        ->leftJoin('admins as updatedByAdmins', 'offical_price_packages.updated_by_id', '=', 'updatedByAdmins.id')
        ->select(
            "offical_price_packages.title_ar as title_ar",
            'offical_price_packages.id as id',
            'offical_price_packages.created_at as created_at',
            'offical_price_packages.updated_at as updated_at',
            'offical_price_packages.updated_by_id as updated_by_id',
            'offical_price_packages.created_by_id as created_by_id',
            'createdByAdmins.username as created_by',
            'updatedByAdmins.username as updated_by'
        );

        if ($adminId && $adminId != 0) {
            $packages = $packages->where('offical_price_packages.created_by_id', $adminId);
        }

        $totalRecords = $packages->count();
        $packages = $packages->where(function ($query) use ($searchvalue) {
            return $query->where('offical_price_packages.title_ar', 'like', '%' . $searchvalue . '%')
                ->orWhere('offical_price_packages.created_at', 'like', '%' . $searchvalue . '%');
        });
        $totalRecordsWithFilter = $packages->count();
        if ($rowperpage != -1)
            $packages = $packages->skip($start)->take($rowperpage);
        $packages = $packages->latest();

        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $packages->get(),
        );
    }

}