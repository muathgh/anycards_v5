<?php

namespace App\Services;

use App\Models\PricingListModels\Package;
use App\Models\PricingListModels\PricingList;
use App\ServicesInterface\PricingListServiceInterface;
use Illuminate\Support\Facades\Auth;

class PricingListService implements PricingListServiceInterface
{
    public function findById($id)
    {
        return PricingList::findOrFail($id);
    }
    public function all()
    {
        return PricingList::all();
    }
    public function latest()
    {
        return PricingList::latest()->get();
    }
    public function create($data)
    {
        $pricingList = new PricingList($data);
        if ($pricingList->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $pricingList = PricingList::findOrFail($id);
        $pricingList->update($data);
        if ($pricingList->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        PricingList::where('package_id', $id)->delete();
    }
    
    public function getDtData($params)
    {

        $admin = Auth::guard('admin')->user();
        $adminId = $params['admin_id'];
        $data = Package::query();

        if($adminId && $adminId != 0){
            $data = $data->where('created_by_id',$adminId);
        }
        $data = $data->latest("created_at");
      
    
        $data = $data->get()->map(function ($p) use($admin) {
if($admin->accessType()=="all"){
            $createdBy = null;
            if ($p->createdBy) {
                if ($p->updated_at != $p->created_at ) {
                    if ($p->updatedBy) {
                        $createdBy = $p->updatedBy->username . " (e) ";
                    }else{
                        $createdBy =  $p->createdBy->username;
                    }
                } else {
                    $createdBy =  $p->createdBy->username;
                }
            }
            $item['id'] = $p->id;
            $item['title_en'] = $p->title_en;
            $item['title_ar'] = $p->title_ar;
            $item['created_at'] = $p->created_at;
            $item['created_by'] = $createdBy;

            $item['card_types_number'] = $p->pricingLists()->count();
            return $item;
        }else{
            if($p->createdBy){
                $createdByAdmin = $p->createdBy;
                if($createdByAdmin->isSuperAdmin()){
                    $createdBy = null;
                    if ($p->createdBy) {
                        if ($p->updated_at != $p->created_at ) {
                            if ($p->updatedBy) {
                                $createdBy = $p->updatedBy->username . " (e) ";
                            }else{
                                $createdBy =  $p->createdBy->username;
                            }
                        } else {
                            $createdBy =  $p->createdBy->username;
                        }
                    }
                    $item['id'] = $p->id;
                    $item['title_en'] = $p->title_en;
                    $item['title_ar'] = $p->title_ar;
                    $item['created_at'] = $p->created_at;
                    $item['created_by'] = $createdBy;
        
                    $item['card_types_number'] = $p->pricingLists()->count();
                    return $item;
                }else if($p->createdBy->id == $admin->id){
                    $createdBy = null;
                    if ($p->createdBy) {
                        if ($p->updated_at != $p->created_at ) {
                            if ($p->updatedBy) {
                                $createdBy = $p->updatedBy->username . " (e) ";
                            }else{
                                $createdBy =  $p->createdBy->username;
                            }
                        } else {
                            $createdBy =  $p->createdBy->username;
                        }
                    }
                    $item['id'] = $p->id;
                    $item['title_en'] = $p->title_en;
                    $item['title_ar'] = $p->title_ar;
                    $item['created_at'] = $p->created_at;
                    $item['created_by'] = $createdBy;
        
                    $item['card_types_number'] = $p->pricingLists()->count();
                    $item['can_delete'] = $this->canDelete($p,$admin);
                    return $item;
                }
            }
        }
        return null;
        })->filter(function ($value) { return !is_null($value); });
        return $data;
    }
       
  
    private function canDelete($p,$admin){
if($admin->isSuperAdmin())
return true;
if($p->createdBy){
    if($p->createdBy->id == $admin->id)
    return true;
}
return false;
    }
      
 
    public function getPricingListByDoubleCondition($column1, $column2, $value1, $value2)
    {
        return PricingList::where($column1, $value1)->where($column2, $value2)->first();
    }
    public function getPricingListByPackageId($packageId)
    {
        return PricingList::where("package_id", $packageId)->get();
    }
    public function updateByCardTypeAndPackageId($packageId, $cardTypeId, $data)
    {
        $pricingList = PricingList::where("package_id", $packageId)->where('card_type_id', $cardTypeId)->first();
        if ($pricingList) {
            $pricingList->update($data);
            if ($pricingList->save())
                return true;
        }
        return false;
    }
}
