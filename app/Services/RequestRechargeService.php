<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\RequestRechargeModels\RequestRecharge;
use App\Models\TransactionModels\Transaction;
use App\ServicesInterface\RequestRechargeServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class RequestRechargeService implements RequestRechargeServiceInterface
{

    public function findById($id){
        return RequestRecharge::findOrFail($id);
    }

    public function query(){
        return RequestRecharge::query();
    }
    
    public function getAdminDtData($params)
    {
        $admin = Auth::guard("admin")->user();
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];

        $fromDate =HelperFacade::isKeyExist('from_date', $params) ? $params['from_date'] : null;
        $toDate =HelperFacade::isKeyExist('to_date', $params) ? $params['to_date'] : null;


        $search  = $params['search'];
        $searchvalue = $search['value'];
        $requests = DB::table('request_recharges')
            ->join('users', 'users.id', '=', 'request_recharges.user_id')
            ->leftJoin('transactions','request_recharges.transaction_id','=','transactions.id')
            ->select(
                'users.username as username',
                'users.name as full_name',
                'request_recharges.id as id',
                    "users.created_by_id as user_created_by_id",
                'request_recharges.amount as amount',
                'request_recharges.status as status',
              
                'request_recharges.created_at as created_at',
                'transactions.amount as transaction_amount'
            );

            if($fromDate && $toDate){
           
                $requests = $requests->whereDate('request_recharges.created_at',"<=",\Carbon\Carbon::parse($toDate))->
                whereDate("request_recharges.created_at",">=",\Carbon\Carbon::parse($fromDate));
            }else

            if(!$fromDate && !$toDate){
            $requests = $requests->whereYear('request_recharges.created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('request_recharges.created_at',\Carbon\Carbon::now()->month)
            ->whereDay('request_recharges.created_at',\Carbon\Carbon::now()->day);
            }
            
         

        $totalRecords = $requests->count();
        $requests = $requests->where(function ($query) use ($searchvalue) {
            $query->where('users.name', 'like', '%' . $searchvalue . '%')
                ->orWhere('request_recharges.amount', 'like', '%' . $searchvalue . '%')
                ->orWhere('request_recharges.created_at', 'like', '%' . $searchvalue . '%')
                ->orWhere('request_recharges.status', 'like', '%' . $searchvalue . '%');
        });
        $totalRecordsWithFilter = $requests->count();
        if ($rowperpage != -1)
            $requests = $requests->skip($start)->take($rowperpage);

  $data = $requests->latest('created_at')->get()->map(function ($r) use ($admin) {
     

            if($admin->accessType()=="all")
            {
            $item['id']= $r->id;
            $item['full_name'] = $r->full_name;
            $item['amount'] = $r->amount;
            $item['transaction_amount'] = $r->transaction_amount ? $r->transaction_amount : null;
            $item['status'] = $r->status;

            $item['created_at'] = $r->created_at;
            return $item;
            }else if($admin->id == $r->user_created_by_id){
                $item['id']= $r->id;
                $item['full_name'] = $r->full_name;
                $item['amount'] = $r->amount;
                $item['transaction_amount'] = $r->transaction_amount ? $r->transaction_amount : null;
                $item['status'] = $r->status;
     
                $item['created_at'] = $r->created_at;
                return $item;
            }else{
            return null;
            }
        })->filter(function ($value) { return !is_null($value); });
     
        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => array_values($data->toArray())
        );
    }
     

    private function getImageSource($r){
        if($r->attachment_image){
            return "web";
        }else if($r->attachment_mobile_image)
        {
            return "mobile";
        }

        return null;
    }
    private function getImage($r){
        if($r->attachment_image){
            return $r->attachment_image;
        }else if($r->attachment_mobile_image)
        {
            return $r->attachment_mobile_image;
        }

        return null;
    }
    public function getDataDt($params)
    {
        $user = Auth::user();
        $fromDate =HelperFacade::isKeyExist('from_date', $params) ? $params['from_date'] : null;
        $toDate =HelperFacade::isKeyExist('to_date', $params) ? $params['to_date'] : null;
        $data = RequestRecharge::query();
        $data = $data->where('user_id',$user->id);
        if($fromDate && $toDate){
            $data = $data->whereDate('created_at','>=',\Carbon\Carbon::parse($fromDate))
            ->whereDate('created_at','<=',\Carbon\Carbon::parse($toDate));
        }
        else{
            $data= $data->whereYear('request_recharges.created_at',\Carbon\Carbon::now()->year)
             ->whereMonth('request_recharges.created_at',\Carbon\Carbon::now()->month)
             ->whereDay('request_recharges.created_at',\Carbon\Carbon::now()->day);
         }
        $data = $data->orderBy('created_at','DESC')->get()->map(function ($r) {

            $item['id'] = $r->id;
            $item['amount'] = $r->amount;
            $item['status'] = $r->status;
            $item["transaction_amount"] = Transaction::find($r->transaction_id) ? Transaction::find($r->transaction_id)->amount : null;
            $item['created_at'] = $r->created_at;
            return $item;
        });
        return $data;
    }

    public function create($data)
    {
        $requestRecharge = new RequestRecharge($data);
        if ($requestRecharge->save())
            return true;
        return false;
    }
    public function checkIfUserHavePendingRequests($userId)
    {

        $requests = RequestRecharge::where('user_id', $userId)
        ->whereYear('created_at',\Carbon\Carbon::now()->year)
        ->whereMonth('created_at',\Carbon\Carbon::now()->month)
        ->whereDay('created_at',\Carbon\Carbon::now()->day)
        ->where('status', Config::get('constants.REQUEST_RECHARGE_STATUS.PENDING'))->count();
        return $requests;
    }

    public function getRequestRechargeResource()
    {
        $user = auth("api")->user();
        $data = RequestRecharge::query();
        $data = $data->where('user_id',$user->id);
        $data = $data->whereYear('created_at',\Carbon\Carbon::now()->year)
        ->whereMonth('created_at', \Carbon\Carbon::now()->month)
        ->whereDay('created_at',\Carbon\Carbon::now()->day);
    
        $data = $data->orderBy('created_at','DESC')->get()->map(function ($r) {
            $item['id'] = $r->id;
            $item['amount'] = $r->amount;
            $item['status'] = $r->status;
            $item["transaction_amount"] = Transaction::find($r->transaction_id) ? Transaction::find($r->transaction_id)->amount : null;
            $item['created_at'] = strtotime($r->created_at);
            return $item;
        });
        return $data;
    }
}
