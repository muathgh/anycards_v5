<?php

namespace App\Services;

use App\ServicesInterface\PermissionsServiceInterface;
use Spatie\Permission\Models\Permission;

class PermissionsService implements PermissionsServiceInterface{
    public function all(){
        return Permission::all();
    }
    public function lookup(){
        return Permission::all()->pluck("name","name");
    }

    public function getByCondition($column,$value){
        return Permission::where($column,$value)->first();
    }

    public function getByWhereIn($column,$ids){
        return Permission::whereIn($column,$ids);
    }

    public function create($name,$guard = 'admin'){

        $permission = Permission::create(["guard_name"=>$guard,"name"=>$name]);
        return $permission;

    }
}