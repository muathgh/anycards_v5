<?php

namespace App\Services;

use App\Models\SettingModels\WholeSalePrice;
use App\ServicesInterface\WholesalePriceServiceInterface;
use Illuminate\Support\Facades\DB;

class WholesalePriceService implements WholesalePriceServiceInterface
{
    public function findById($id)
    {
        return WholeSalePrice::findOrFail($id);
    }
    public function all()
    {
        return WholeSalePrice::all();
    }
    public function latest()
    {
        return WholeSalePrice::latest()->get();
    }
    public function create($data)
    {
        $wholeSalePrice = new WholeSalePrice($data);
        if ($wholeSalePrice->save())
            return $wholeSalePrice;

        return false;
    }
    public function update($id, $data)
    {
        $wholeSalePrice = WholeSalePrice::findOrFail($id);
        $wholeSalePrice->update($data);
        if ($wholeSalePrice->save())
            return $wholeSalePrice;
        return false;
    }

    public function updateByColumn($column,$value,$data){
        $wholeSalePrice = WholeSalePrice::where($column,$value)->first();
        $wholeSalePrice->update($data);
        if ($wholeSalePrice->save())
            return $wholeSalePrice;
        return false;
    }
    public function delete($id)
    {
        WholeSalePrice::findOrFail($id)->delete();
    }
    public function getDtData($params)
    {
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];


        $search  = $params['search'];
        $searchvalue = $search['value'];

      $wholeSalePriceData =   DB::table('whole_sale_prices')->
        join('cards_types','cards_types.id','=','whole_sale_prices.card_type_id')
        
        ->join('sub_providers','sub_providers.id','=','cards_types.sub_provider_id')
        ->join('providers','providers.id','=','sub_providers.provider_id')
        ->join('categories','categories.id','=','providers.category_id')
        ->select('cards_types.title_en as title_en',"cards_types.title_ar as title_ar",
        "whole_sale_prices.price as price",'whole_sale_prices.created_at as created_at')
        ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')
        ->orderBy('cards_types.order');
        $totalRecords = $wholeSalePriceData->count();
        $wholeSalePriceData =    $wholeSalePriceData->where(function ($query) use ($searchvalue) {
            $query->where('cards_types.title_en', 'like', '%' . $searchvalue . '%')
                ->orWhere('cards_types.title_ar', 'like', '%' . $searchvalue . '%')
                ->orWhere('whole_sale_prices.price','like', '%' . $searchvalue . '%')
                ->orWhere('whole_sale_prices.created_at','like', '%' . $searchvalue . '%');
               
                
        });
        
        $totalRecordsWithFilter = $wholeSalePriceData->count();
        if ($rowperpage != -1)
            $wholeSalePriceData = $wholeSalePriceData->skip($start)->take($rowperpage);


            return array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordsWithFilter,
                "aaData" => $wholeSalePriceData->get()
            );

    }

    public function getByCondition($columnName,$value){
        return WholeSalePrice::where($columnName,$value)->first();

    }
}
