<?php

namespace App\Services;

use App\Models\SettingModels\CardLimit;
use App\ServicesInterface\CardsLimitServiceInterface;
use Illuminate\Support\Facades\DB;

class CardsLimitService implements CardsLimitServiceInterface{
    public function findById($id){
        return CardLimit::findOrFail($id);
    }
    public function all(){
        return CardLimit::all();
    }
    public function latest(){
        return CardLimit::latest()->get();
    }
    public function create($data){
        $cardLimit = new CardLimit($data);
        if ($cardLimit->save())
            return true;
        return false;
    }
    public function update($id,$data){
        $cardLimit = CardLimit::findOrFail($id);
        $cardLimit->update($data);
        if ($cardLimit->save())
            return true;
        return false;
    }
    public function delete($id){
        CardLimit::findOrFail($id)->delete();
    }
    public function getDtData(){
        return DB::table('card_limits')->
        join('cards_types','cards_types.id','=','card_limits.card_type_id')
        
        ->join('sub_providers','sub_providers.id','=','cards_types.sub_provider_id')
        ->join('providers','providers.id','=','sub_providers.provider_id')
        ->join('categories','categories.id','=','providers.category_id')
        ->select('card_limits.id as id','cards_types.title_en as title_en','cards_types.title_ar as title_ar',
        'card_limits.value as limit', 'card_limits.created_at as created_at' )
        ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')
        ->orderBy('cards_types.order')->get();

       
    }
    public function getCardLimitByCardTypeId($id){
        return CardLimit::where('card_type_id',$id)->first();
    }
    public function updateByColumn($column,$value,$data){
        $cardLimit = CardLimit::where($column,$value)->first();
        $cardLimit->update($data);
        if ($cardLimit->save())
            return $cardLimit;
        return false;
    }
}