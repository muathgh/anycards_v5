<?php

namespace App\Services;

use App\Models\TransactionModels\Balance;
use App\Models\User;
use App\ServicesInterface\BalancesServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class BalancesService implements BalancesServiceInterface
{
    public function changeUserBalance($user, $balance, $transactionId)
    {
        $balance = $user->balances()->create([
            "balance" => $balance,
            "transaction_id" => $transactionId
        ]);
        return $balance->id;
    }



    public function getDtData()
    {
        $admin = Auth::guard('admin')->user();
        $data =  User::latest();

        if($admin->accessType() == "created_users"){
            $data=$data->where('users.created_by_id',$admin->id);
        }
        
        $data= $data->get()->map(function ($u) {
            
            
                $balance =  $u->balances()->latest("id")->first();

                $item['user_id'] = $u->id;
                $item['full_name'] = $u->name;
                $item['username'] = $u->username;
                $item['balance'] = $balance ? $balance->balance : 0;
                $item['updated_at'] = $balance ? $balance->last_update : $u->created_at;
                return $item;
                
            
        })->reject(function($value){
            return $value === false;
        })->toArray();
       
        // uasort($data,   function ($a,$b)
        // {
        //     return $a["updated_at"] > $b["updated_at"];
        // });
           usort($data,   function ($a,$b)
        {
            return $a["updated_at"] < $b["updated_at"];
        });

        return $data;
    }

    function sort_by_name($a,$b)
{
    return $a["full_name"] > $b["full_name"];
}

    public function userCurrentBalance($userId)
    {
        $balance = Balance::where('user_id', $userId)->latest('id')->first();
        if ($balance)
            return $balance->balance;
        return 0;
    }

    public function balanceDeduction($userId,$amount){
        $balance = Balance::where('user_id', $userId)->latest('id')->first();
        if($balance)
        {
            return $balance->balance - $amount;
        }
        return 0;

    }
}
