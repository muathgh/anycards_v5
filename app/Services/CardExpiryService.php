<?php

namespace App\Services;

use App\Models\SettingModels\CardExpiry;
use App\ServicesInterface\CardExpiryServiceInterface;
use Illuminate\Support\Facades\DB;

class CardExpiryService implements CardExpiryServiceInterface{
    public function findById($id){
        return CardExpiry::findOrFail($id);
    }
    public function all(){
        return CardExpiry::all();
    }
    public function latest(){
        return CardExpiry::latest()->get();
    }
    public function create($data){
        $cardExpiry = new CardExpiry($data);
        if ($cardExpiry->save())
            return true;
        return false;
    }
    public function update($id,$data){
        $cardExpiry = CardExpiry::findOrFail($id);
        $cardExpiry->update($data);
        if ($cardExpiry->save())
            return true;
        return false;
    }
    public function delete($id){
        CardExpiry::findOrFail($id)->delete();
    }
    public function getDtData(){
        return DB::table('card_expiries')->
        join('cards_types','cards_types.id','=','card_expiries.card_type_id')
        
        ->join('sub_providers','sub_providers.id','=','cards_types.sub_provider_id')
        ->join('providers','providers.id','=','sub_providers.provider_id')
        ->join('categories','categories.id','=','providers.category_id')
        ->select('card_expiries.id as id','cards_types.title_en as title_en','cards_types.title_ar as title_ar',
        'card_expiries.value as expiry', 'card_expiries.created_at as created_at' )
        ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')
        ->orderBy('cards_types.order')->get();

       
    }
    public function getCardExpiryByCardTypeId($id){
        return CardExpiry::where('card_type_id',$id)->first();
    }
    public function updateByColumn($column,$value,$data){
        $cardExpiry = CardExpiry::where($column,$value)->first();
        $cardExpiry->update($data);
        if ($cardExpiry->save())
            return $cardExpiry;
        return false;
    }
}