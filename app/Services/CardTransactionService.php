<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\TransactionModels\CardTransaction;
use App\ServicesInterface\CardTransactionsServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class CardTransactionService implements CardTransactionsServiceInterface
{

    public function findById($id){
        return CardTransaction::findOrFail($id);
    }
    public function all(){
        return CardTransaction::all();
    }
    public function latest(){
        return CardTransaction::latest()->get();
    }

    public function count(){
        return CardTransaction::count();
    }
// V_6 UPDATES 04/01/2022
    public function getDtData($data)
    {

        // current admin
        $admin = Auth::guard('admin')->user();
        $userId = HelperFacade::isKeyExist('user_id', $data) ? $data['user_id'] : null;
        $adminId = HelperFacade::isKeyExist('admin_id', $data) ? $data['admin_id'] : null;
        $categoryId = HelperFacade::isKeyExist('category_id', $data) ? $data['category_id'] : null;
        $providerId = HelperFacade::isKeyExist('provider_id', $data) ? $data['provider_id'] : null;
        $subProviderId = HelperFacade::isKeyExist('sub_provider_id', $data) ? $data['sub_provider_id'] : null;
        $cardTypeId = HelperFacade::isKeyExist('card_type_id', $data) ? $data['card_type_id'] : null;
        $fromDate =HelperFacade::isKeyExist('from_date', $data) ? $data['from_date'] : null;
        $toDate =HelperFacade::isKeyExist('to_date', $data) ? $data['to_date'] : null;
        $draw = $data['draw'];
        $start = $data['start'];
        $rowperpage  = $data['length'];
       
        
        $search  = $data['search'];
        
       
       
    



        $data = [];
        $records = DB::table("card_transactions")->
        
        join('transactions','transactions.id','=','card_transactions.transaction_id')->
        join('users','users.id','=','transactions.user_id')->
        join('cards','cards.id','=','card_transactions.card_id')->
        join('cards_types','cards_types.id','=','cards.card_type_id')->
        join('sub_providers','sub_providers.id','=','cards_types.sub_provider_id')->
        join('providers','providers.id','=','sub_providers.provider_id')->
        join('categories','categories.id','=','providers.category_id')->
        select('cards_types.title_en as title_en','cards_types.title_ar as title_ar',
        "cards.code as pincode","users.name as full_name","cards.serial as serial",
        "transactions.amount as amount",'card_transactions.created_at as created_at',
        "sub_providers.id as sub_provider_id",'providers.id as provider_id','categories.id as category_id',
    "card_transactions.order_number as transaction_number");
  
      
        if($userId)
        {
            $records->where("users.id",$userId);
        }
        if($adminId && $adminId != 0){
            $records->where('users.created_by_id',$adminId);
        }

        if($fromDate && $toDate){
            $records->whereDate('card_transactions.created_at',"<=",\Carbon\Carbon::parse($toDate))->
            whereDate("card_transactions.created_at",">=",\Carbon\Carbon::parse($fromDate));
        }
        // if($fromDate == null && $toDate == null)
        // {
        //     $records->whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())');
        // }

        if($cardTypeId){
            $records->where('cards_types.id',$cardTypeId);
        }else if($subProviderId){
            $records->where('sub_providers.id',$subProviderId);
        }
        else if($providerId){
            $records->where('providers.id',$providerId);
        }
        else if($categoryId){
            $records->where('categories.id',$categoryId);
        }
        
       
        

        if(!$userId && !$fromDate && !$toDate && !$cardTypeId && !$subProviderId && !$providerId && !$categoryId){
         $records =   $records->whereRaw('DAY(card_transactions.created_at) = DAY(CURDATE())')->
            whereRaw('MONTH(card_transactions.created_at) = MONTH(CURDATE())')->whereRaw('YEAR(card_transactions.created_at) = YEAR(CURDATE())');
        }

        if($admin->accessType() == "created_users"){
            $records=$records->where('users.created_by_id',$admin->id);
        }

        $totalRecords = $records->count();
        $records = $records->where(function($query) use ($search){
$query->where('cards_types.title_en','like','%' .$search['value'] . '%')
->orWhere('cards_types.title_ar','like','%' .$search['value'] . '%')
->orWhere('cards.code','like','%' .$search['value'] . '%')->
orWhere('users.name','like','%' .$search['value'] . '%')->
orWhere('cards.serial','like','%' .$search['value'] . '%')->
orWhere('transactions.amount','like','%' .$search['value'] . '%')->
orWhere('card_transactions.created_at','like','%' .$search['value'] . '%')->
orWhere('card_transactions.order_number','like','%' .$search['value'] . '%');
        });
        $totalRecordsWithFilter = $records->count();
        if($rowperpage != -1)
        $records = $records->skip($start)->take($rowperpage);
       
        $records = $records->latest('created_at')->get();


        foreach ($records as $record) {
         
            $item['transaction_number'] = $record->transaction_number;
            $item['title_en'] = $record->title_en;
            $item['title_ar'] = $record->title_ar;
            $item['pincode'] = $record->pincode;
            $item['full_name'] = $record->full_name;
            $item['serial'] = $record->serial ? $record->serial : trans('No Serial');
            $item['amount'] = $record->amount;
            $item['created_date'] = $record->created_at;
            $data[]=$item;

        }


        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );
        
    }
    //END V_6 UPDATES 04/01/2022
    public function query(){
        return CardTransaction::query();
    }
}
