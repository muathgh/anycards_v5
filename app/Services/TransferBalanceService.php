<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\TransactionModels\UserBalanceTransaction;
use App\ServicesInterface\TransferBalanceServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class TransferBalanceService implements TransferBalanceServiceInterface
{

    public function getResourceData($data)
    {
        $fromDate = null;
        $toDate = null;
        if (HelperFacade::isKeyExist('from_date',$data ) && $data['from_date'] != null)
            $fromDate = $data['from_date'];
        if (HelperFacade::isKeyExist('to_date',$data) && $data['to_date'] != null)
            $toDate = $data['to_date'];
        $user = auth("api")->user();
        $data = UserBalanceTransaction::query();


        $data = $data->where(function($query)use($user){
            return  $query->where('from_user_id', $user->id)->orWhere("to_user_id", $user->id);
           });

         
           
           if ($fromDate && $toDate) {
            
            
            $data = $data->whereDate('created_at', ">=", \Carbon\Carbon::parse($fromDate))
                ->whereDate('created_at', "<=", \Carbon\Carbon::parse($toDate));
                

               
        } else {
            $data = $data->whereYear('created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('created_at', \Carbon\Carbon::now()->month)
            ->whereDay('created_at',\Carbon\Carbon::now()->day);
        
        }

        $total = 0;
        $data->each(function($t) use(&$total){
$total+=$t->senderTransaction->amount;
        });
       
    
        $data = $data->orderBy('created_at','DESC')->get()->map(function ($t) use ($user) {

            $item["user"] = $user->id == $t->toUser->id ? $t->fromUser->username :
                $t->toUser->username;

            $item['amount'] = $t->senderTransaction->amount;
            $item['type'] = $t->from_user_id == $user->id ? "transfer" :
                "reception";
            $item['created_at'] = $t->created_at;
            return $item;
        });
        return ["data"=>$data,"total"=>$total];
    }
    public function getDtData($params)
    {


        $user = Auth::user();
        $data = UserBalanceTransaction::query();

        if (!$params["from_date"] && !$params["to_date"] && !$params["type"]) {
            $data = UserBalanceTransaction::where(function($query) use($user){
                return $query->where('from_user_id', $user->id)->orWhere('to_user_id', $user->id);
            });
            $data = $data->whereYear('created_at', \Carbon\Carbon::now()->year)
            ->whereMonth('created_at', \Carbon\Carbon::now()->month)
            ->whereDay('created_at',\Carbon\Carbon::now()->day);

        } else {

            if ($params['from_date'] && $params['to_date']) {

                $data =   $data->whereDate('created_at', "<=", \Carbon\Carbon::parse($params['to_date']))->whereDate("created_at", ">=", \Carbon\Carbon::parse($params['from_date']));
            }

            if ($params['type'] && $params['type'] == "transfer") {

                $data =  $data->where('from_user_id', $user->id);
            } else if ($params['type'] && $params['type'] == "recepion") {
                $data =  $data->where('to_user_id', $user->id);
            }
        }


        $data = $data->orderBy('created_at', 'DESC')->get()->map(function ($t) use ($user) {

            $item['from_user_id'] = $t->from_user_id;
            $item['to_user_id'] = $t->to_user_id;
            $item["user"] = $user->id == $t->toUser->id ? $t->fromUser->username :
                $t->toUser->username;

            $item['amount'] = $t->senderTransaction->amount;
            $item['type'] = $t->from_user_id == $user->id ? trans("Transfer") :
                trans("Recepion");
            $item['created_at'] = $t->created_at;
            return $item;
        });

        return $data;
    }
         public function getAdminDtData($params)
    {
        $admin = Auth::guard("admin")->user();
        $data = DB::table("users_balance_transactions")->join("users as from_users",'from_users.id','=','users_balance_transactions.from_user_id')
        ->join('users as to_users','to_users.id','=','users_balance_transactions.to_user_id')
        ->join('transactions','transactions.id','=','users_balance_transactions.sender_transaction_id')
        ->join('admins as admins_from_users','admins_from_users.id','=','from_users.created_by_id')
        ->join('admins as admins_to_users','admins_to_users.id','=','to_users.created_by_id')
        ->select(
            "from_users.username as from_user",
            "from_users.id as from_user_id",
            "from_users.created_by_id as from_user_created_by_id",
            "to_users.created_by_id as to_user_created_by_id",  
            "to_users.id as to_user_id",
            "to_users.username as to_user",
            "transactions.amount as amount",
            "users_balance_transactions.created_at as created_date",
            "admins_from_users.id as admin_from_user_id",
            "admins_to_users.id as admin_to_user_id"


        );
       

        if($admin->accessType()=="created_users"){
            $data->where('from_users.created_by_id',$admin->id)
            ->where('to_users.created_by_id',$admin->id);
        }else if($params["admin_id"] && $params['admin_id'] != 0){
            $data->where(function($query) use($params){
return $query->where("from_users.created_by_id",$params['admin_id'])
            ->orWhere('to_users.created_by_id',$params['admin_id']);
            });
        }
       
        if ($params["user_id"]) {
            $user_id = $params['user_id'];
            $data = $data->where(function ($query) use ($user_id) {
                return $query->where('from_users.id', $user_id)->orWhere("to_users.id", $user_id);
            });
        }
     
        if ($params["from_date"] && $params['to_date']) {
            $data = $data->whereDate("users_balance_transactions.created_at", ">=", \Carbon\Carbon::parse($params["from_date"]))
                ->whereDate("users_balance_transactions.created_at", "<=", \Carbon\Carbon::parse($params["to_date"]));
        }
        
        if ((!$params['admin_id'] || $params['admin_id'] == 0) && !$params['user_id'] && !$params['from_date'] && !$params['to_date']) {
            $data = $data->whereYear('users_balance_transactions.created_at', \Carbon\Carbon::now()->year)
                ->whereMonth('users_balance_transactions.created_at', \Carbon\Carbon::now()->month)
                ->whereDay('users_balance_transactions.created_at',\Carbon\Carbon::now()->day);
        }



    
        return $data->orderBy('users_balance_transactions.created_at', 'DESC')->get();
    }
}
