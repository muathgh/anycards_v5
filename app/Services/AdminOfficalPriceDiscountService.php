<?php

namespace App\Services;

use App\Models\OfficalPriceModels\OfficalPriceDiscount;
use App\ServicesInterface\AdminOfficalPriceDiscountServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminOfficalPriceDiscountService implements AdminOfficalPriceDiscountServiceInterface{

    public function findById($id)
    {
        return OfficalPriceDiscount::findOrFail($id);
    }
    public function all()
    {
        return OfficalPriceDiscount::all();
    }

    public function latest()
    {
        return OfficalPriceDiscount::latest()->get();
    }
    public function create($data)
    {
        $discount = new OfficalPriceDiscount($data);
        if ($discount->save())
            return true;
        return false;
    }

    public function update($id, $data)
    {
        $discount = OfficalPriceDiscount::findOrFail($id);
        $discount->update($data);
        if ($discount->save())
            return true;
        return false;
    }

    public function getDtData($params)
    {


        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];
       
        
        $search  = $params['search'];
        $admin = Auth::guard('admin')->user();
        $adminId = $params['admin_id'];
        $data = DB::table('offical_price_discounts')
        ->join('providers','providers.id','=','offical_price_discounts.provider_id')
        ->join('offical_price_packages','offical_price_packages.id','=','offical_price_discounts.offical_price_package_id')
        ->join('admins as adminsCreatedBy','adminsCreatedBy.id','=','offical_price_discounts.created_by_id')
        ->leftJoin('admins as adminsUpdatedBy','adminsUpdatedBy.id','offical_price_discounts.updated_by_id')
        ->select('offical_price_discounts.id as id',
        'offical_price_packages.title_ar as package',
    'offical_price_discounts.discount as discount',
    'providers.title_ar as provider','adminsCreatedBy.username as admin_created_by','adminsUpdatedBy.username as admin_updated_by','offical_price_discounts.created_at as created_at');

        if($adminId && $adminId != 0){
            $data = $data->where('offical_price_discounts.created_by_id',$adminId);
        }
        $data = $data->latest("created_at");

        $totalRecords = $data->count();

        $data = $data->where(function($query) use ($search){
            $query->where('providers.title_ar','like','%' .$search['value'] . '%')
            ->orWhere('offical_price_discounts.created_at','like','%' .$search['value'] . '%');
          
                    });

                    $totalRecordsWithFilter = $data->count();
                    if($rowperpage != -1)
                    $data = $data->skip($start)->take($rowperpage);
      
    

                    return array(
                        "draw" => intval($draw),
                        "iTotalRecords" => $totalRecords,
                        "iTotalDisplayRecords" => $totalRecordsWithFilter,
                        "aaData" => $data->get()
                    );
    }

    public function delete($id)
    {
      
    return OfficalPriceDiscount::find($id)->delete();
    }

    public function query(){
        return OfficalPriceDiscount::query();
    }

}