<?php

namespace App\Services;

use App\Models\SettingModels\RequestRechargeNotification;
use App\Models\SettingModels\UserNotification;
use App\ServicesInterface\RequestRechargeNotificationServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RequestRechargeNotificationService implements RequestRechargeNotificationServiceInterface
{

    public function findById($id){
        return RequestRechargeNotification::findOrFail($id);

    }
    public function clearAll()
    {
        DB::table('request_recharge_notifications')->update(array('is_seen' => true, 'is_cleared' => true));
    }

    public function latest()
    {
        return RequestRechargeNotification::latest("created_at")->take(200)->get();
    }
    public function clearNotification($id)
    {
        $notification = RequestRechargeNotification::findOrFail($id);

        $notification->is_seen = true;
        $notification->is_cleared = true;
        if ($notification->is_delete_when_seen)
            $notification->delete();
        else
            $notification->save();
    }

  

   

}
