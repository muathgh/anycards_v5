<?php

namespace App\Services;

use App\Models\SettingModels\Setting;
use App\ServicesInterface\SettingsServiceInterface;

class SettingsService implements SettingsServiceInterface
{
    public function getValueByKey($key)
    {
        return Setting::where('key', $key)->first()->value;
    }
    
    public function setValueByKey($key, $value)
    {
        $setting = Setting::where('key', $key)->first();
        $setting->value = $value;
        if ($setting->save())
            return true;

        return false;
    }
}
