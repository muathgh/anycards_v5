<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\ServicesInterface\BalanceLimitServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BalanceLimitService implements BalanceLimitServiceInterface {

    public function getDtData($data)
    {
        $draw = $data['draw'];
        $start = $data['start'];
        $rowperpage  = $data['length'];
        $search  = $data['search'];
        $searchvalue = $search['value'];
        $admin = Auth::guard('admin')->user();
        $fromDate =HelperFacade::isKeyExist('from_date', $data) ? $data['from_date'] : null;
        $toDate =HelperFacade::isKeyExist('to_date', $data) ? $data['to_date'] : null;
        $balances = DB::table('admin_balance_limit_transactions')->where('admin_id',$admin->id)
        ->select("admin_balance_limit_transactions.id as id","admin_balance_limit_transactions.amount as amount",'admin_balance_limit_transactions.type as type',
    "admin_balance_limit_transactions.created_at as created_at");

    if($fromDate && $toDate){
        $balances->whereDate("admin_balance_limit_transactions.created_at",'>=',\Carbon\Carbon::parse($fromDate))
        ->whereDate('admin_balance_limit_transactions.created_at','<=',\Carbon\Carbon::parse($toDate));
    }
        // HelperFacade::convertNumber2Negative

        $totalRecords = $balances->count();
        $balances =    $balances->where(function ($query) use ($searchvalue) {
            $query->where('amount', 'like', '%' . $searchvalue . '%')
            ->orWhere('type', 'like', '%' . $searchvalue . '%');
               
        });

        $totalRecordsWithFilter = $balances->count();

        if ($rowperpage != -1)
        $balances = $balances->skip($start)->take($rowperpage);

        $data = $balances->latest('admin_balance_limit_transactions.created_at')->get()->map(function($b){
$item['id'] =  $b->id;
$item['amount'] = $b->type == "withdraw_admin_balance" ? HelperFacade::convertNumber2Negative($b->amount) : $b->amount;
$item['type'] = $b->type;
$item['created_at'] = $b->created_at;
return $item;
        });

        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );
    }

}