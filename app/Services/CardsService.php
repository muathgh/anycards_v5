<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\Admin;
use App\Models\CardModels\Card;
use App\Models\CardModels\CardPrice;
use App\ServicesInterface\CardsServiceInterface;
use Illuminate\Support\Facades\DB;

class CardsService implements CardsServiceInterface
{
    private $datetimeFormat = 'Y-m-d H:i:s';
    public function findById($id)
    {
        return Card::findOrFail($id);
    }
    public function all()
    {
        return Card::all();
    }
    public function latest()
    {
        return Card::latest()->get();
    }
    public function create($data)
    {
        $card = new Card($data);
        if ($card->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $card = Card::findOrFail($id);
        $card->update($data);
        if ($card->save())
            return true;
        return false;
    }
    public function delete($id)
    {
        return  Card::findOrFail($id)->secureDelete("cardTransaction");
    }
    public function getCardsByCardTypeId($cardTypeId)
    {
        return Card::latest()->where('card_type_id', $cardTypeId)->get();
    }
    public function getDtData($data)
    {
        //dt 
        $draw = $data['draw'];
        $start = $data['start'];
        $rowperpage  = $data['length'];


        $search  = $data['search'];
        $searchvalue = $search['value'];

        $cardTypeId = $data['card_type_id'];
        $subProviderId = $data['sub_provider_id'];
        $providerId = $data['provider_id'];
        $categoryId = $data['category_id'];
        $fromDate = $data['from_date'];
        $toDate = $data['to_date'];
        $fromTime = $data["from_time"];
        $toTime = $data['to_time'];
        $adminId= $data['admin_id'];
        $cards = null;
        $cards = DB::table("cards")->join("cards_types", 'cards_types.id', '=', 'cards.card_type_id')
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->leftJoin('admins as createdByAdmins', 'cards.created_by_id', '=', 'createdByAdmins.id')
            ->leftJoin('admins as updatedByAdmins','cards.updated_by_id', '=', 'updatedByAdmins.id')
            ->leftJoin('whole_sale_prices','whole_sale_prices.card_type_id','=','cards_types.id')
            ->select(
                'cards.id as id',
                'cards_types.title_en as title_en',
                'cards_types.title_ar as title_ar',
                'cards.code as code',
                'cards.serial as serial',
                'cards.expiry_date as expiry_date',
                'cards.is_used as is_used',
                'cards.created_at as created_at',
                'cards.updated_at as updated_at',
                'createdByAdmins.username as created_by',
                'updatedByAdmins.username as updated_by',
                'cards.created_by_id as created_by_id',
                'cards.updated_by_id as updated_by_id',
                'whole_sale_prices.price as offical_price',
                'cards_types.order as card_type_order',
                "sub_providers.order as sub_provider_order",
                "providers.order as provider_order"
                
            );



        if ($cardTypeId) {
            $cards = $cards->where('cards_types.id', $cardTypeId);
        } else if ($subProviderId) {
            $cards = $cards->where('sub_providers.id', $subProviderId);
        } else if ($providerId) {
            $cards = $cards->where('providers.id', $providerId);
        } else if ($categoryId) {
            $cards = $cards->where('categories.id', $categoryId);
        }

        if($adminId && $adminId != 0){
            $cards = $cards->where('cards.created_by_id',$adminId);

        }

        if ($fromDate && $toDate && $fromTime && $toTime) {
            
            $fdate = explode('-', $fromDate);
            $tdate = explode('-', $toDate);
            
            $ftime = explode(":",$fromTime);
            $ttime = explode(":",$toTime);
            $ftime = str_pad($ftime[0], 2, '0', STR_PAD_LEFT) . ":".$ftime[1];
            $ttime = str_pad($ttime[0], 2, '0', STR_PAD_LEFT) . ":".$ttime[1];
      
          
            $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
            $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
            $fromDateTime = new \DateTime();

            $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);

            $fromDateTime = $fromDateTime->format($this->datetimeFormat);

            $toDateTime = new \DateTime();

            $toDateTime =  $toDateTime->setTimestamp($tdatetime);

            $toDateTime = $toDateTime->format($this->datetimeFormat);

            // $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
            //     ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
            $cards = $cards->whereDate('cards.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('cards.created_at',"<=",\Carbon\Carbon::parse($toDate));
            
            $cards = $cards->whereRaw("time(cards.created_at) >= '$ftime' and time(cards.created_at) <= '$ttime'");
        }

        else if($fromTime && $toTime){
            $currentDate = \Carbon\Carbon::now()->format('d-m-Y');
            $fdate = explode('-', $currentDate);
            $tdate = explode('-', $currentDate);
            
            $ftime = explode(":",$fromTime);
            $ttime = explode(":",$toTime);
            $ftime = str_pad($ftime[0], 2, '0', STR_PAD_LEFT) . ":".$ftime[1];
            $ttime = str_pad($ttime[0], 2, '0', STR_PAD_LEFT) . ":".$ttime[1];
      
          
            $fdatetime =  strtotime("$fdate[2]-$fdate[1]-$fdate[0] $fromTime");
            $tdatetime = strtotime("$tdate[2]-$tdate[1]-$tdate[0] $toTime");
            $fromDateTime = new \DateTime();

            $fromDateTime =  $fromDateTime->setTimestamp($fdatetime);

            $fromDateTime = $fromDateTime->format($this->datetimeFormat);

            $toDateTime = new \DateTime();

            $toDateTime =  $toDateTime->setTimestamp($tdatetime);

            $toDateTime = $toDateTime->format($this->datetimeFormat);

            // $cards = $cards->whereRaw("UNIX_TIMESTAMP(cards.created_at) >= $fdatetime")
            //     ->whereRaw("UNIX_TIMESTAMP(cards.created_at) <= $tdatetime");
            $cards = $cards->whereDate('cards.created_at',">=",\Carbon\Carbon::parse($fromDate))
            ->whereDate('cards.created_at',"<=",\Carbon\Carbon::parse($toDate));
            
            $cards = $cards->whereRaw("time(cards.created_at) >= '$ftime' and time(cards.created_at) <= '$ttime'");
        }

        else if ($fromDate && $toDate) {
            $cards = $cards->whereDate('cards.created_at', '>=', \Carbon\Carbon::parse($fromDate))->whereDate('cards.created_at', '<=', \Carbon\Carbon::parse($toDate));
        }

        if (!$cardTypeId && !$subProviderId && !$providerId && !$categoryId && !$fromDate && !$toDate) {
            $cards = $cards->whereRaw('DAY(cards.created_at) = DAY(CURDATE())')
                ->whereRaw('MONTH(cards.created_at) = MONTH(CURDATE())')
                ->whereRaw('YEAR(cards.created_at) = YEAR(CURDATE())');
        }
        
        $totalRecords = $cards->count();
        $cards =    $cards->where(function ($query) use ($searchvalue) {
            $query->where('serial', 'like', '%' . $searchvalue . '%')
                ->orWhere('code', 'like', '%' . $searchvalue . '%')
                ->orWhere('cards_types.title_en', 'like', '%' . $searchvalue . '%')
                ->orWhere('cards_types.title_ar', 'like', '%' . $searchvalue . '%')
                ->orWhere('expiry_date', 'like', '%' . $searchvalue . '%')
                ->orWhere('cards.created_at', 'like', '%' . $searchvalue . '%')
                ->orWhere('createdByAdmins.username', 'like', '%' . $searchvalue . '%')
                ->orWhere('updatedByAdmins.username', 'like', '%' . $searchvalue . '%');
        });




        $totalRecordsWithFilter = $cards->count();
        if ($rowperpage != -1)
            $cards = $cards->skip($start)->take($rowperpage);




        //   $cards =  $cards->orderBy('serial','ASC');
        //   $cards = $cards->orderBy('card_type_order','ASC');
        $cards = $cards->orderBy('provider_order','ASC')->orderBy('sub_provider_order','ASC')->orderBy('card_type_order','ASC')->orderBy('created_at','DESC');

        $data = $cards->get()->map(function ($c) {
            $item['id'] = $c->id;
            $item['title_en'] = $c->title_en;
            $item['title_ar'] = $c->title_ar;
            $item['code'] = $c->code;
            $item['serial'] = $c->serial ? $c->serial : trans('No Serial');
            $item['expiry_date'] = \Carbon\Carbon::parse($c->expiry_date)->format('d-m-Y');
            $item['offical_price'] = $c->offical_price;


            $item["is_used"] = $c->is_used;
            $item['created_at'] = $c->created_at;
            $item['created_by'] =   $c->created_at != $c->updated_at && $c->updated_by_id != null ? $c->updated_by . " (e) " : $c->created_by;
            return $item;
        });


        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );
        // return $data;
    }

    public function getFirstCardByCondition($columnName, $value)
    {
        return Card::where($columnName, $value)->first();
    }

    public function orderCard($cardTypeId)
    {
        $card = Card::where("card_type_id", $cardTypeId)->first();
    }

    public function whereRaw($query, $param)
    {
        return Card::whereRaw($query, [$param]);
    }
}
