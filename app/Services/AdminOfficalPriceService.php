<?php

namespace App\Services;

use App\Models\OfficalPriceModels\AdminOfficalPrice;
use App\Models\OfficalPriceModels\OfficalPricePackage;

use App\ServicesInterface\AdminOfficalPriceServiceInterface;
use Illuminate\Support\Facades\Auth;

class AdminOfficalPriceService implements AdminOfficalPriceServiceInterface{

    public function findById($id)
    {
        return AdminOfficalPrice::findOrFail($id);
    }
    public function all()
    {
        return AdminOfficalPrice::all();
    }

    public function latest()
    {
        return AdminOfficalPrice::latest()->get();
    }
    public function create($data)
    {
        $officalPrice = new AdminOfficalPrice($data);
        if ($officalPrice->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $officalPrice = AdminOfficalPrice::findOrFail($id);
        $officalPrice->update($data);
        if ($officalPrice->save())
            return true;
        return false;
    }

    public function delete($id)
    {
        return AdminOfficalPrice::where('offical_price_package_id', $id)->delete();
    }

    public function getDtData($params)
    {

        $admin = Auth::guard('admin')->user();
        $adminId = $params['admin_id'];
        $data = OfficalPricePackage::query();

        if($adminId && $adminId != 0){
            $data = $data->where('created_by_id',$adminId);
        }
        $data = $data->latest("created_at");
      
    
        $data = $data->get()->map(function ($p) use($admin) {
            $item['id'] = $p->id;
            $item['title_en'] = $p->title_en;
            $item['title_ar'] = $p->title_ar;
            $item['created_at'] = $p->created_at;
            $createdBy = null;
            if ($p->createdBy) {
                if ($p->updated_at != $p->created_at ) {
                    if ($p->updatedBy) {
                        $createdBy = $p->updatedBy->username . " (e) ";
                    }else{
                        $createdBy =  $p->createdBy->username;
                    }
                } else {
                    $createdBy =  $p->createdBy->username;
                }
            }
            $item['created_by'] = $createdBy;

            $item['card_types_number'] = $p->adminOfficalPrices()->count();
            return $item;
        });
        return $data;
    }

    public function getOfficalPriceByPackageId($packageId){
        return AdminOfficalPrice::where("offical_price_package_id", $packageId)->get();
    }
    public function updateByCardTypeAndPackageId($packageId,$cardTypeId,$data){
        $officalPrice = AdminOfficalPrice::where("offical_price_package_id", $packageId)->where('card_type_id', $cardTypeId)->first();
        if ($officalPrice) {
            $officalPrice->update($data);
            if ($officalPrice->save())
                return true;
        }
        return false;
    }

    public function getAdminOfficalPriceByDoubleCondition($columnName1,$columnName2,$value1,$value2){
        return AdminOfficalPrice::where($columnName1, $value1)->where($columnName2, $value2)->first();
    }

    public function query(){
        return AdminOfficalPrice::query();
    }
  
}