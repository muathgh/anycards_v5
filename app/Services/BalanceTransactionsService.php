<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\ServicesInterface\BalanceTransactionsServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class BalanceTransactionsService implements BalanceTransactionsServiceInterface{
    public function getDtData($params){
        
        $user = Auth::user();
        $fromDate =HelperFacade::isKeyExist('from_date', $params) ? $params['from_date'] : null;
        $toDate =HelperFacade::isKeyExist('to_date', $params) ? $params['to_date'] : null;
        $data = $user->balances();
        if($fromDate && $toDate){
            $data->whereDate('created_at','>=',\Carbon\Carbon::parse($fromDate))
            ->whereDate('created_at','<=',\Carbon\Carbon::parse($toDate));
        }else{
            $data= $data->whereYear('created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('created_at',\Carbon\Carbon::now()->month)
            ->whereDay('created_at',\Carbon\Carbon::now()->day);

        }
       $data =  $data->latest("created_at")->get()->map(function($b){
           $transaction = $b->transaction;
           if(!$transaction || $transaction->type == Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))
           return false;
           $item['id']  = $b->id;
           $item['transaction_amount'] = $transaction->amount;
           $item['type'] = $transaction->type;
           $item['created_at'] =$transaction->created_at;
           return $item;

       })->reject(function($d){
           return $d==false;
       });
       return $data;

    }

    public function getBalanceTransactionsResource($params){
        $user = auth("api")->user();
        $fromDate = null;
        $toDate = null;
        if (HelperFacade::isKeyExist('from_date',$params ) && $params['from_date'] != null)
            $fromDate = $params['from_date'];
        if (HelperFacade::isKeyExist('to_date',$params) && $params['to_date'] != null)
            $toDate = $params['to_date'];
        $data = null;
        if($fromDate && $toDate){
            $data = $user->balances()->whereDate('created_at', ">=", \Carbon\Carbon::parse($fromDate))
            ->whereDate('created_at', "<=", \Carbon\Carbon::parse($toDate));
        }else{
            $data = $user->balances()->whereYear('created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('created_at', \Carbon\Carbon::now()->month)
            ->whereDay('created_at',\Carbon\Carbon::now()->day);
        }
        $data =  $data->latest("created_at")->get()->map(function($b){
            $transaction = $b->transaction;
            if(!$transaction || $transaction->type == Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))
            return false;
            $item['id']  = $b->id;
            $item['transaction_amount'] = $transaction->amount;
            $item['type'] = $transaction->type;
            $item['created_at'] =$transaction->created_at;
            return $item;
 
        })->reject(function($d){
            return $d==false;
        });
        return $data;
    }
}