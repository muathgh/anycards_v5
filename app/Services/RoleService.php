<?php

namespace App\Services;

use App\ServicesInterface\RoleServiceInterface;
use Spatie\Permission\Models\Role;
class RoleService implements RoleServiceInterface{
    public function create($name,$accessType,$guard = "admin",$isSuperAdmin){
$role = Role::create(["guard_name"=>$guard,"name"=>$name,"access_type"=>$accessType,'is_super_admin'=>$isSuperAdmin]);
return $role;

    }
    public function syncPermissions($role,$permissions){

        $role->syncPermissions($permissions);
    }

    public function getDtData(){
        $role_permissions = Role::with('permissions')->latest()->get();
        return $role_permissions;
    }

    public function lookup(){
        return Role::all()->pluck("name","name");
    }

    public function findById($id){
        return Role::findOrFail($id);
    }

    public function update($id,$data){
        $role = Role::findOrFail($id);
        $role->update($data);
        return $role;

    }
}