<?php

namespace App\Services;

use App\Models\PricingListModels\PriceOfSale;
use App\Models\User;
use App\Models\PricingListModels\UserPriceOfSale;
use App\ServicesInterface\UsersPriceOfSaleServiceInterface;
use Illuminate\Support\Facades\DB;

class UsersPriceOfSaleService implements UsersPriceOfSaleServiceInterface
{
    public function findById($id)
    {
        return UserPriceOfSale::findOrFail($id);
    }

    public function all()
    {
        return UserPriceOfSale::all();
    }

    public function latest()
    {
        return UserPriceOfSale::latest()->get();
    }
    public function create($data)
    {
        $priceOfSale = new UserPriceOfSale($data);
        if ($priceOfSale->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $priceOfSale = UserPriceOfSale::findOrFail($id);
        $priceOfSale->update($data);
        if ($priceOfSale->save())
            return true;
        return false;
    }

    public function delete($id)
    {
        UserPriceOfSale::findOrFail($id)->delete();
    }

    public function getDtData($params)

    {
        return [];
    }

    public function query()
    {
        return UserPriceOfSale::query();
    }
    public function updateByColumn($column, $value, $data)
    {
        $priceOfSale = UserPriceOfSale::where($column, $value)->first();
        $priceOfSale->update($data);
        if ($priceOfSale->save())
            return $priceOfSale;
        return false;
    }

    public function getUserPriceOfSaleResource($providerId)
    {
        $cardTypes = DB::table('cards_types')->join("sub_providers", 'sub_providers.id', '=', 'cards_types.sub_provider_id')->join('providers', 'providers.id', '=', 'sub_providers.provider_id')->join('categories', 'categories.id', '=', 'providers.category_id')
            ->select(
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                "cards_types.price as points",
                "cards_types.created_at as created_at",
                "cards_types.id as id",
                "cards_types.price as price",
                "cards_types.image as image",
                'providers.id as provider_id'
            );
        $data =  $cardTypes->where('providers.id', '=', $providerId)->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order')->get()->map(function ($c) {
            $item['id'] = $c->id;
            $item['title_en'] = $c->title_en;
            $item['title_ar'] = $c->title_ar;
            $item['points'] = $c->price;
            $item['image'] = $c->image;
            $item['price_of_sale']= $this->getUserCardTypePriceOfSale($c->id);
            $item['provider_id'] = $c->provider_id;
            return $item;
        });

        return $data;
    }

    private function getUserCardTypePriceOfSale($cardTypeId){
        $user = auth("api")->user();
        $userPriceOfSale = UserPriceOfSale::where('card_type_id', $cardTypeId)->where('user_id', $user->id)->first();
        $priceOfSale = PriceOfSale::where('card_type_id', $cardTypeId)->first();
        if ($userPriceOfSale)
            return $userPriceOfSale->price;
        if ($priceOfSale)
            return $priceOfSale->price;

        return null;
    }
   public function updateUserPriceOfSale($cardTypeId,$value){
        
        $user = auth("api")->user();
//         UserPriceOfSale::where('card_type_id',$cardTypeId)->where('user_id',$user->id)->update([
// "price"=>$value
//         ]);
        UserPriceOfSale::updateOrCreate([
            "card_type_id"=>$cardTypeId,
            "user_id"=>$user->id
        ],[
            "price"=>$value
        ]);
        return true;
    }
}
