<?php

namespace App\Services;

use App\Facades\FcmNotificationFacade;
use App\Facades\HelperFacade;
use App\Models\Admin;
use App\Models\CardModels\Card;
use App\Models\CategoryModels\CardType;
use App\Models\PricingListModels\PriceOfSale;
use App\Models\PricingListModels\UserPriceOfSale;
use App\Models\RequestRechargeModels\RequestRecharge;
use App\Models\SettingModels\UserNotification;
use App\Models\TransactionModels\CardTransaction;
use App\Models\TransactionModels\Transaction;
use App\Models\User;
use App\ServicesInterface\TransactionsServiceInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class TransactionsService implements TransactionsServiceInterface
{

    public function findById($id){
        return Transaction::findOrFail($id);
    }
    public function addTransactionToUser($user, $amount, $type)
    {
        $transaction = $user->transactions()->create([
            "amount" => $amount,
            "type" => $type
        ]);

        // change request status
        if ($type == Config::get('constants.TRANSACTION_TYPE.DEPOSIT')) {
            $requestRecharge =        RequestRecharge::where('user_id', $user->id)->where('status', Config::get('constants.REQUEST_RECHARGE_STATUS.PENDING'))
                ->first();

            $requestRecharge->status = Config::get('constants.REQUEST_RECHARGE_STATUS.APPROVED');
            $requestRecharge->transaction_id = $transaction->id;
            $requestRecharge->save();
        }
        if ($type == Config::get('constants.TRANSACTION_TYPE.DEPOSIT') || $type == Config::get('constants.TRANSACTION_TYPE.WITHDRAW')) {
            $notification = new UserNotification();
            $notification->user_id = $user->id;
            if ($type == Config::get('constants.TRANSACTION_TYPE.DEPOSIT')) {
                $notification->title_en = "Balance Deduction";
                $notification->title_ar = "إضافة رصيد";
                $notification->description_en  = sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_DEPOSIT_EN'), $amount);
                $notification->description_ar  = sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_DEPOSIT_AR'), $amount);
                $notification->type = Config::get('constants.USER_NOTIFICATION_TYPE.BALANCE_DEPOSIT');
            } else {
                $notification->title_en = "Balance Withdraw";
                $notification->title_ar ="سحب رصيد";
                $notification->description_en  = sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_WITHDRAW_EN'), $amount);
                $notification->description_ar  = sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_WITHDRAW_AR'), $amount);
                $notification->type = Config::get('constants.USER_NOTIFICATION_TYPE.BALANCE_WITHDRAW');
            }
            $notification->save();
            $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $notification->user_id)->first();
            if ($fcmToken) {
                $token = $fcmToken->token;
                if ($type == Config::get('constants.TRANSACTION_TYPE.DEPOSIT')) {
                    FcmNotificationFacade::sendNotification($token, sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_DEPOSIT_AR'), $amount), "إضافة رصيد");
                } else {
                    FcmNotificationFacade::sendNotification($token, sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_WITHDRAW_AR'), $amount), "سحب رصيد");
                }
            }
        }
        return $transaction->id;
    }
    public function getDtBalancesTransactionsDataByUser($data)
    {
        $userId = $data['id'];
        $draw = $data['draw'];
        $start = $data['start'];
        $rowperpage  = $data['length'];
       
        
        $search  = $data['search'];
        $transactions = Transaction::where('user_id', $userId);
        if($data['from_date'] && $data['to_date']){
            $transactions = $transactions->whereDate('created_at','>=',\Carbon\Carbon::parse($data['from_date']))->
            whereDate('created_at','<=',\Carbon\Carbon::parse($data['to_date']));
        }
        if(!$data['from_date'] && !$data['to_date']){
            $transactions = $transactions->whereYear('created_at',\Carbon\Carbon::now()->year)
            ->whereMonth('created_at',\Carbon\Carbon::now()->month);

        }
        $totalRecords = $transactions->count();
        $totalRecordsWithFilter = $totalRecords;
        if($rowperpage != -1)
        $transactions = $transactions->skip($start)->take($rowperpage);
        
        $transactions = $transactions->orderBy("created_at", "DESC")->get();
        $data =   $transactions->map(function ($t) {
            if (!$t->cardTransaction) {

                $item['id'] = $t->id;
                $item['amount'] = $t->type == "withdraw" ? HelperFacade::convertNumber2Negative($t->amount) : $t->amount;
                $item['type'] = $t->type;
                $item['created_at'] = $t->created_at;
                $item['created_by'] = $t->created_by_id ?$t->created_at != $t->updated_at ? Admin::find($t->created_by_id)->username ." (e) " : Admin::find($t->created_by_id)->username :"";
                return $item;
            } else {
                return false;
            }
        })->reject(function ($value) {
            return $value === false;
        });

        return $data;
    }
    public function orderCard($cardTypeId, $userId, $amount)
    {
         // $cardType = CardType::find($cardTypeId);
        $card = Card::where('card_type_id', $cardTypeId)->where('is_used', 0)->orderBy('expiry_date', 'ASC')->first();
        $card->is_used = true;
        $card->save();
        $transaction = new Transaction([
            "amount" => $amount,
            "type" => Config::get('constants.TRANSACTION_TYPE.DEDUCTION'),
            "user_id" => $userId

        ]);
        $transaction->save();
      
        $cardTransaction = new CardTransaction([
            "card_id" => $card->id,
            "transaction_id" => $transaction->id,
            'price_of_sale'=>$this->getPriceOfSale($userId,$cardTypeId)
        ]);
        $cardTransaction->save();

        return ["card" => $card, "transaction_id" => $transaction->id];
    }

    private function getPriceOfSale($userId,$cardTypeId)
    {
        $user = User::find($userId);
        $userPriceOfSale = UserPriceOfSale::where('card_type_id', $cardTypeId)->where('user_id', $user->id)->first();
        $priceOfSale = PriceOfSale::where('card_type_id', $cardTypeId)->first();
        if ($userPriceOfSale)
            return $userPriceOfSale->price;
        if ($priceOfSale)
            return $priceOfSale->price;

        return null;
    }
}
