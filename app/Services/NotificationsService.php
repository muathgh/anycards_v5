<?php

namespace App\Services;

use App\Models\SettingModels\Notification;
use App\Models\SettingModels\UserNotification;
use App\ServicesInterface\NotificationsServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationsService implements NotificationsServiceInterface
{

    public function findById($id){
        return Notification::findOrFail($id);
    }
    public function clearAll()
    {
        DB::table('notifications')->update(array('is_seen' => true, 'is_cleared' => true));
    }

    public function latest()
    {
        return Notification::latest("created_at")->take(200)->get();
    }
    public function clearNotification($id)
    {
        $notification = Notification::findOrFail($id);

        $notification->is_seen = true;
        $notification->is_cleared = true;
        if ($notification->is_delete_when_seen)
            $notification->delete();
        else
            $notification->save();
    }

    

    public function userNotificationLatest($params)
    {
        $user = Auth::user();
        $page = 0;
        $take = 10;
        if (array_key_exists("page", $params) && $params["page"] != "null") {
            $page = $params["page"];
        }
        $total = UserNotification::where('user_id', $user->id)->count();
        $notifications = UserNotification::where('user_id', $user->id)->latest('id')->get();
        $notifications = $notifications->map(function($n){
$item['title_en'] = $n->title_en;
$item['title_ar'] = $n->title_ar;
$item['description_ar']  = $n->description_ar;
$item['created_at'] = strtotime($n->created_at);
return $item;
        });
        return ["notifications" => $notifications, "total" => $total];
    }

    public function userNotificationCount()
    {
        $user = Auth::user();
        return UserNotification::where('user_id', $user->id)->where('is_seen', false)->count();
    }

    public function updateUserNotification()
    {
        DB::table('user_notifications')->where('user_id', Auth::user()->id)->update(array('is_seen' => true));
    }
}
