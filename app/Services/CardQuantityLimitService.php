<?php

namespace App\Services;

use App\Models\Admin;
use App\Models\SettingModels\CardQuantityLimit;
use App\ServicesInterface\CardQuantityLimitServiceInterface;
use Illuminate\Support\Facades\DB;

class CardQuantityLimitService implements CardQuantityLimitServiceInterface
{
    public function findById($id)
    {
        return CardQuantityLimit::findOrFail($id);
    }
    public function all()
    {
        return CardQuantityLimit::all();
    }
    public function latest()
    {
        return CardQuantityLimit::latest()->get();
    }
    public function create($data)
    {
        $quantityLimit = new CardQuantityLimit($data);
        if ($quantityLimit->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $quantityLimit = CardQuantityLimit::findOrFail($id);
        $quantityLimit->update($data);
        if ($quantityLimit->save())
            return true;
        return false;
    }
   

    public function getDtData($params)
    {
        $draw = $params['draw'];
        $start = $params['start'];
        $rowperpage  = $params['length'];


        $search  = $params['search'];
        $searchvalue = $search['value'];

        $quantityLimitData = DB::table('cards_quantity_limits')->join(
            'cards_types',
            'cards_quantity_limits.card_type_id',
            '=',
            'cards_types.id'
        )
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
      
            ->select(
                'cards_quantity_limits.id as id',
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                'cards_quantity_limits.quantity_limit as quantity_limit',
                "cards_quantity_limits.created_at as created_at",
           
              
            )
            ->orderBy('categories.order')->orderBy('providers.order')->orderBy('sub_providers.order')
            ->orderBy('cards_types.order');
        $totalRecords = $quantityLimitData->count();
        $quantityLimitData = $quantityLimitData->where(function ($query) use ($searchvalue) {
            $query->where('cards_types.title_en', 'like', '%' . $searchvalue . '%')
            ->orWhere('cards_types.title_ar','like', '%' . $searchvalue . '%')
            ->orWhere('cards_types.title_ar','like', '%' . $searchvalue . '%')
            ->orWhere('cards_quantity_limits.quantity_limit','like', '%' . $searchvalue . '%')
            ->orWhere('cards_quantity_limits.created_at','like', '%' . $searchvalue . '%');
     
        });
        
        $totalRecordsWithFilter = $quantityLimitData->count();
        if ($rowperpage != -1)
            $cards = $quantityLimitData->skip($start)->take($rowperpage);

        $data = $quantityLimitData->get()->map(function ($p) {

            $item['id'] = $p->id;
            $item['title_en'] = $p->title_en;
            $item['title_ar'] = $p->title_ar;
            $item['quantity_limit'] = $p->quantity_limit;
            $item['created_at'] = $p->created_at;
            return $item;
        });

        return array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsWithFilter,
            "aaData" => $data
        );

    }

    public function getQuantityByCardType($cardTypeId){
        return CardQuantityLimit::where('card_type_id',$cardTypeId)->first();
    }

    public function updateByColumn($column, $value, $data)
    {
        $quantityLimit = CardQuantityLimit::where($column, $value)->first();
        $quantityLimit->update($data);
        if ($quantityLimit->save())
            return $quantityLimit;
        return false;
    }

    public function delete($id){
        CardQuantityLimit::where('id',$id)->delete();
    
    }

    public function query(){ 
        return CardQuantityLimit::query();
    }

}
