<?php

namespace App\Services;

use App\Models\CategoryModels\Category;
use App\ServicesInterface\CategoryServiceInterface;

class CategoryService implements CategoryServiceInterface
{
    public function findById($id)
    {
        return Category::findOrFail($id);
    }
    public function all()
    {
        return Category::all();
    }
    public function latest(){
        return Category::orderBy("order","ASC")->get();
    }
    public function create($data)
    {
        $category = new Category($data);
        if ($category->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $category = Category::findOrFail($id);
        $category->update($data);
        if ($category->save())
            return true;
        return false;
    }
    public function delete($id)
    {
       return Category::findOrFail($id)->secureDelete("providers");
    }
    public function lookup(){
        return Category::orderBy('order','ASC')->pluck(app()->getLocale()=="ar" ? "title_ar":"title_en","id");
    }
}
