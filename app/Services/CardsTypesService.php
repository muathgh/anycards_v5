<?php

namespace App\Services;

use App\Facades\HelperFacade;
use App\Models\CardModels\CardPrice;
use App\Models\CategoryModels\CardType;
use App\Models\PricingListModels\PricingList;
use App\Models\User;
use App\ServicesInterface\CardsTypesServiceInterface;
use Illuminate\Support\Facades\DB;

class CardsTypesService implements CardsTypesServiceInterface
{
    public function findById($id)
    {
        return CardType::findOrFail($id);
    }
    public function all()
    {
        return CardType::all();
    }
    public function latest()
    {
        return CardType::latest()->get();
    }
    public function create($data)
    {
        $cardType = new CardType($data);
        if ($cardType->save())
            return true;
        return false;
    }
    public function update($id, $data)
    {
        $cardType = CardType::findOrFail($id);
        $cardType->update($data);
        if ($cardType->save())
            return true;
        return false;
    }
    public function delete($id)
    {
      return  CardType::findOrFail($id)->secureDelete("cards","pricingLisits","cardLimit","priceOfSale");
    }
    public function getCardTypeBySubProviderId($subProviderId)
    {
        return CardType::where('sub_provider_id', $subProviderId)->orderBy('order','ASC')->get();
    }

    public function getCardTypeBySubProviderIdByStatus($subProviderId)
    {
        return CardType::where('sub_provider_id', $subProviderId)->where('is_active',true)->orderBy('order','ASC')
        ->whereHas('cards',function($q){
            return $q->where('is_used',false);
        })->get();
    }

    public function getDtData($params)
    {

       
        $start = $params['start'];
        $draw = $params['draw'];
        $search = $params['search'];
        

        $data = DB::table('cards_types')
            ->join('sub_providers', 'sub_providers.id', '=', 'cards_types.sub_provider_id')
            ->join('providers', 'providers.id', '=', 'sub_providers.provider_id')
            ->join('categories', 'categories.id', '=', 'providers.category_id')
            ->select(
                "cards_types.id as id",
                "cards_types.title_en as title_en",
                "cards_types.title_ar as title_ar",
                "cards_types.description_en as description_en",
                "cards_types.description_ar as description_ar",
                "cards_types.price as price",
                "cards_types.image as image",
                "sub_providers.title_en as subprovider_title_en",
                "sub_providers.title_ar as subprovider_title_ar",
                "cards_types.created_at as created_at",
                "cards_types.is_active as is_active"
            );
        if ($params["sub_provider_id"]) {
            $data = $data->where('sub_providers.id', $params['sub_provider_id']);
        } else if ($params["provider_id"]) {
            $data = $data->where('providers.id', $params['provider_id']);
        } else if ($params["category_id"]) {
            $data = $data->where('categories.id', $params['category_id']);
        }
        if($search['value'])
        {
            $data= $data->where('cards_types.title_en','like','%' .$search['value'] . '%')
            ->orWhere('cards_types.title_ar','like','%' .$search['value'] . '%')
            ->orWhere('cards_types.description_en','like','%' .$search['value'] . '%')
            ->orWhere('cards_types.description_ar','like','%' .$search['value'] . '%')
            ->orWhere('cards_types.price','like','%' .$search['value'] . '%')
            ->orWhere('sub_providers.title_en','like','%' .$search['value'] . '%')
            ->orWhere('sub_providers.title_ar','like','%' .$search['value'] . '%')
            ->orWhere('cards_types.created_at','like','%'.$search['value'].'%');
        }

        $totalRecords = $data->count();
  

        $data = $data->orderBy('providers.order')->orderBy('sub_providers.order')->orderBy('cards_types.order')->get()->map(function ($s) {
            $item['id'] = $s->id;
            $item['title_en'] = $s->title_en;
            $item['title_ar'] = $s->title_ar;
            $item['description_en'] = $s->description_en;
            $item['description_ar'] = $s->description_ar;
            $item['price'] = $s->price;
            $item['image'] = $s->image;
            $item['subprovider_title_en'] = $s->subprovider_title_en ? $s->subprovider_title_en  : "";
            $item['subprovider_title_ar'] = $s->subprovider_title_ar ? $s->subprovider_title_ar : "";
            $item['created_at'] = $s->created_at;
            $item['is_active'] = $s->is_active;
 
          
            return $item;
        });

        
return $data;
        // return ["data"=>$data,"draw"=>$draw,"iTotalRecords" => $totalRecords , "iTotalDisplayRecords"=>$totalRecords];
       
    }
    public function lookup()
    {
        return CardType::orderBy('order','ASC')->pluck(app()->getLocale() == "ar" ? "title_ar" : "title_en", "id");
    }
    public function getCardsTypePricesDtData()
    {
        $data = CardPrice::latest()->get()->map(function ($c) {

            $user = $c->user;
            $cardType = $c->cardType;
            $item['id'] = $c->id;
            $item['username'] = $user->username;
            $item['full_name'] = $user->name;
            $item['card_type'] = HelperFacade::getTitle($cardType);
            $item['price'] = $c->price;
            return $item;
        });

        return $data;
    }

    public function findCardPriceById($id)
    {
        return CardPrice::findOrFail($id);
    }


    public function createCardPrice($data)
    {
        $cardPrice = new CardPrice($data);
        if ($cardPrice->save())
            return true;
        return false;
    }
    public function updateCardPrice($id, $data)
    {
        $cardPrice = CardPrice::findOrFail($id);
        $cardPrice->update($data);
        if ($cardPrice->save())
            return true;
        return false;
    }
    public function deleteCardPrice($id)
    {
   CardPrice::findOrFail($id)->delete();
    }
    public function findCardPriceByCondition($columnName, $value)
    {
        return CardPrice::where($columnName, $value);
    }
    public function findCardPriceByDoubleCondition($columnName1, $columnName2, $value1, $value2)
    {
        return CardPrice::where($columnName1, $value1)->where($columnName2, $value2)->first();
    }


    public function getCardTypeResource($cardTypeId)
    {
        $cardType = CardType::findOrFail($cardTypeId);
        $user = auth("api")->user();
        return [
            "id" => $cardType->id,
            "title_en" => $cardType->title_en,
            "title_ar" => $cardType->title_ar,
            "image" => $cardType->image,
            "order" => $cardType->order,
            "price" => $this->getUserCardTypePrice($user->id, $cardTypeId),
            "price_of_sale"=>$cardType->priceOfSale ? $cardType->priceOfSale->price : null,
            "is_active" => $cardType->is_active,
            "is_available" => $cardType->cards()->where('is_used', false)->count() > 0

        ];
    }

    public function getUserCardTypePrice($userId, $cardTypeId)
    {

        //get price from package
        $user = User::findOrFail($userId);
        $packageId = $user->package_id;
        if ($packageId) {
            $price = PricingList::where('package_id', $packageId)->where('card_type_id', $cardTypeId)->first();
            if ($price && $price->price != 0) {
                return $price->price;
            }
        }
        $cardType = CardType::findOrFail($cardTypeId);

        return $cardType->price;
    }

    public function getCardsTypeStatistics()
    {

        $cardsTypes = DB::table("card_transactions")->join('cards', 'cards.id', '=', 'card_transactions.card_id')->join('cards_types', 'cards_types.id', '=', 'cards.card_type_id')->selectRaw('COUNT(card_transactions.id) as count,cards_types.title_en as title_en,cards_types.title_ar as title_ar,cards.card_type_id as card_type_id')->groupBy('cards.card_type_id', 'cards_types.title_en', 'cards_types.title_ar')->get();



        return $cardsTypes;
    }
    public function paginate()
    {
        return CardType::paginate(9);
    }
    public function paginateBySubProviderId($subProviderId)
    {
        return CardType::where('sub_provider_id', $subProviderId)->paginate(10);
    }

    public function paginateBySubProviderIdOrderd($subProviderId)
    {
        return CardType::where('sub_provider_id', $subProviderId)->where('is_active', true)
        ->whereHas('cards',function($q){
            return $q->where('is_used',false);
        })->orderBy('order', 'ASC')->paginate(20);
    }

    public function query(){
        return CardType::query();
    }
}
