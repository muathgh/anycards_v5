<?php

namespace App\Listeners;

use App\Models\CardModels\Card;
use App\Models\SettingModels\CardLimit;
use App\Models\SettingModels\Notification;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Login $user)
    {
        if ($user->guard == "admin") {


            CardLimit::all()->each(function ($c) {
              $cardType = $c->cardType;
             
              if($cardType->is_active)
              {
              
                    if ($cardType->cards()->where('is_used', false)->count() <= $c->value) {
                        $prevNotification = Notification::where('data',$cardType->id)->where('type',Config::get('constants.NOTIFICATION_TYPE.CARD_LIMIT'))->latest()->first();
                      if(!$prevNotification ||  !$prevNotification->next_notification_at)
                      { 
                          $notification = new Notification([
                        "title_en" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_EN'), $cardType->title_en),
                        "title_ar" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_AR'), $cardType->title_ar),
                        "description_en" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_EN'), $cardType->title_en),
                        "description_ar" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_AR'), $cardType->title_ar),
                        "type" => Config::get('constants.NOTIFICATION_TYPE.CARD_LIMIT'),
                        "href" => route("cards.create", ["card_type_id" => $cardType->id]),
                        "is_delete_when_seen" => true,
                        "data"=>$cardType->id,
                        "next_notification_at"=>\Carbon\Carbon::now()->addHours(3),

                    ]);
                    $notification->save();

                      }else
                      
                        if ( \Carbon\Carbon::now()->gt(\Carbon\Carbon::parse($prevNotification->next_notification_at))) {
                        $notification = new Notification([
                            "title_en" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_EN'), $cardType->title_en),
                            "title_ar" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_AR'), $cardType->title_ar),
                            "description_en" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_EN'), $cardType->title_en),
                            "description_ar" => sprintf(Config::get('constants.NOTIFICATION_MESSAGES.CARD_TYPE_REACHED_LIMIT_VALUE_AR'), $cardType->title_ar),
                            "type" => Config::get('constants.NOTIFICATION_TYPE.CARD_LIMIT'),
                            "href" => route("cards.create", ["card_type_id" => $cardType->id]),
                            "is_delete_when_seen" => true,
                            "data"=>$cardType->id,
                            "next_notification_at"=>\Carbon\Carbon::now()->addMinutes(3),

                        ]);
                        $notification->save();
                    }
                    }
              }
                
            });




            //    Card::notUsed()->each(function ($c) {
            //             $reminder = $c->cardType->cardExpiry ?? null;


            //             if ($reminder) {
            //                 $value = $reminder->value;

            //                 if (  \Carbon\Carbon::parse($c->expiry_date)->lte(\Carbon\Carbon::now()->addDays($value))) {

            //                     Notification::create([
            //                         "title_en" => "Cards with this code $c->code will expiry soon",
            //                         "title_ar" => "البطاقة التي تحمل الكود $c->code سوف تنتهي قريبا",
            //                         "description_en" => "Cards with this code $c->code will expiry soon",
            //                         "description_ar" => "البطاقة التي تحمل الكود $c->code سوف تنتهي قريبا",
            //                         "type" => Config::get('constants.NOTIFICATION_TYPE.CARDS_EXPIRY_SOON'),
            //                         "is_seen" => false,
            //                         "href" => URL::to('/admin/cards/edit/' . $c->id)

            //                     ]);
            //                 }
            //             }
            //         });




        }
    }
}
