<?php

namespace App\Facades;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use File as FileManager;


class UploadFile {


    public function upload($file,$destination){

        $current_timestamp = Carbon::now()->timestamp;
        $filename    = $file->getClientOriginalName();
        $path = $destination . "/" . $current_timestamp;
        $storagePath = Storage::disk('public')->put($path, $file);
        return [
            'full_path'=>$storagePath,
            'dir'=>$current_timestamp,

        ];

    }

    public function remove($destination,$dir){
       

        Storage::disk('public')->deleteDirectory($destination.'/'.$dir);
    }
}