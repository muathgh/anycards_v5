<?php

namespace App\Facades;

use App\Models\SettingModels\Setting as SettingModelsSetting;
use Illuminate\Support\Facades\DB;

class Setting {


    public function getValueByKey($key)
    {
        $setting =  SettingModelsSetting::where("key", $key)->first();
        if ($setting)
            return $setting->value;
        return null;
    }

    public function setValueByKey($key, $value)
    {
        $record = DB::table('settings')->where('key', $key)->first();
        if (!$record) {
            DB::table('settings')->insert([
                "key" => $key,
                "value" => $value
            ]);
        } else {
            DB::table("settings")->where("key", $key)->update(["value" => $value]);
        }
    }
}