<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;
class ResponseWrapperFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'responseWrapper';
    }
}