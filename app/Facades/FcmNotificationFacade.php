<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;
class FcmNotificationFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'fcmNotification';
    }
    
}