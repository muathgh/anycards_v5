<?php

namespace App\Facades;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class FcmNotification
{

    public function sendNotification($recipients, $title, $body, $dataTitle = null, $dataBody = null)
    {

        try {
            $header = array();
            $header[] = 'Content-type: application/json';
            $header[] = 'Authorization: key=' . "AAAAuPucwyA:APA91bGqbVJWiMYKqZZmaiULd1ic-PZNRDVLyCR25tPyqEs31qOe3DVVM5vH7bd4cXj73notkMo0LnR3_GTneMSmrnqL_ePPHyDRjWa4r9qMCMwKhlBXRE3bKJV7of_PV5CIGeUk0p3E";

            $payload = [
                'to' =>  $recipients,
                'notification' => [
                    "sound" => "default",
                    "body" =>  $title,
                    "title" => $body,
                    "content_available" => true,
                    "priority" => "high"
                ],
                "data" => [
                    "title" => $dataTitle,
                    "body" => $dataBody
                ]
            ];

            $crl = curl_init();
            curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($payload));

            curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, false); should be off on production
            // curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false); shoule be off on production

            curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);

            $rest = curl_exec($crl);
            if ($rest === false) {
                return curl_error($crl);
            }
            curl_close($crl);
            return $rest;
        } catch (\Exception $e) {
            Log::channel('mysql')->error($e->getMessage());
            ResponseWrapperFacade::failureWithMessage($e->getMessage());
        }
    }
}
