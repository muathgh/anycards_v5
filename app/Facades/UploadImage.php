<?php

namespace App\Facades;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use File as FileManager;
use Intervention\Image\ImageManagerStatic as Image;
class UploadImage
{


   
    public function reszieMobileImage($base64){
    
        ini_set('memory_limit', '-1');
           
     $image = str_replace('data:image/png;base64,', '', $base64);
     $image = str_replace(' ', '+', $base64);
     $image = base64_decode($image);
  
   
       $imageName = time()  . '.png';
       $val = Storage::disk('public')->put($imageName, $image);
      
       $path = public_path('storage/RequestRecharges/'.$imageName);
       Image::make($base64)->resize(500, 500)->orientate()->save($path);
       $webPath = url('/').'/storage/RequestRecharges/'.$imageName;
       return ["url"=>$webPath,"name"=>$imageName];
      
 
         
     }
     
     
    public function upload($file, $destination, $width = 350,$height=250,$prevDestination = null,  $prevImage = null)
    {

        if ($prevImage != null) {
            $public_path = public_path('storage/' . $destination . "/" . $prevDestination);
            if (FileManager::exists($public_path)) FileManager::deleteDirectory($public_path);
        }
        $current_timestamp = Carbon::now()->timestamp;
        $filename    = $file->getClientOriginalName();
        $path = $destination . "/" . $current_timestamp;
      
        // if(!FileManager::exists(public_path('storage/'.$destination . "/" . $current_timestamp)))
        // FileManager::makeDirectory(public_path('storage/'.$destination . "/" . $current_timestamp));
       
        // $image_resize = Image::make($file->getRealPath());              
        // $image_resize->resize($width, $height);
        $storagePath = Storage::disk('public')->put($path, $file);
       
        // $fullPath = public_path('storage/' .$storagePath);
        // $image_resize->save($fullPath);
        $webPath = url('/').'/storage/'.$storagePath;
        return $webPath;
    }

    public function remove($prevDestination,  $prevImage)
    {
        $public_path = public_path('storage/categories' . $prevDestination . '/' . $prevImage);
        if (FileManager::exists($public_path)) FileManager::delete($public_path);
    }

    public function removeDir($folder, $prevDestination)
    {
        $public_path = public_path('storage/' . $folder . "/" . $prevDestination);
        if (FileManager::exists($public_path)) FileManager::deleteDirectory($public_path);
    }
}
