<?php

namespace App\Facades;

use App\Models\CategoryModels\CardType;
use App\Models\Permissions\AdminSubPermission;
use App\Models\Permissions\SubPermission;
use App\Models\PricingListModels\PricingList;
use App\Models\SettingModels\Notification;
use App\Models\SettingModels\RequestRechargeNotification;
use App\Models\SettingModels\UserNotification;
use App\Models\TransactionModels\Balance;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Helper
{
    public function getTitle($obj)
    {
        if (app()->getLocale() == "en")
            return $obj->title_en;

        return $obj->title_ar;
    }
    public function getDescription($obj)
    {
        if (app()->getLocale() == "en")
            return $obj->description_en;

        return $obj->description_ar;
    }

    public function isKeyExist($array, $key)
    {
        return array_key_exists($array, $key);
    }
    public function currentUserBalance($userId = null)
    {
        if(!$userId)
        $userId = Auth::user()->id;
        $balance = Balance::where('user_id',$userId )->latest('id')->first();
        if ($balance)
            return $balance->balance;
        return 0;
    }

 

    public function getRequestRechargeStatus($status)
    {
        if ($status == Config::get('constants.REQUEST_RECHARGE_STATUS.PENDING'))
            return trans('Pending');
        if ($status == Config::get('constants.REQUEST_RECHARGE_STATUS.REJECTED'))
            return trans('Rejected');

        return trans('Approved');
    }
    public function getCardTypePriceByPackage($cardType){
        if(!Auth::check())
        return $cardType->price;
        
        $user = Auth::user();
        $packageId = $user->package_id;
        if($packageId){
            $price = PricingList::where('package_id',$packageId)->
            where('card_type_id',$cardType->id)->first();
            if($price && $price->price != 0)
            {
                return $price->price;
            }

            return $cardType->price;

        }
        

        return $cardType->price;
    }

    public function getCardTypePriceUser($cardTypeId,$userId){
       
        $cardType = CardType::find($cardTypeId);
        $user = User::find($userId);
        $packageId = $user->package_id;
        if($packageId){
            $price = PricingList::where('package_id',$packageId)->
            where('card_type_id',$cardType->id)->first();
            if($price && $price->price != 0)
            {
                return $price->price;
            }

        }
        

        return $cardType->price;
    }

    public function getNotificationNumber()
    {
        $notifications  = Notification::where('is_seen', false)->count();
        return $notifications;
    }
//V_5 UPDATES 27/12/2021
    public function getRequestRechargeNotificationNumber(){
        if(!Auth::guard("admin")->user()->isSuperAdmin())
        $notifications = DB::table('request_recharge_notifications')->join('users','users.id','=','request_recharge_notifications.user_id')
            ->where('users.created_by_id',Auth::guard('admin')->user()->id)->where('is_seen',false);
        else
        $notifications = DB::table('request_recharge_notifications')->where('is_seen',false);
     
        return $notifications->count();
    }
    //END V_5 UPDATES 27/12/2021

    public function getLastNotifications()
    {
        $notifications = Notification::where('is_seen', false)->latest()->take(5)->get();
        return $notifications;
    }
//V_5 UPDATES 27/12/2021
    public function getLastRequestRechargeNotifications()
    {
        if(!Auth::guard("admin")->user()->isSuperAdmin())
        $notifications = DB::table('request_recharge_notifications')->join('users','users.id','=','request_recharge_notifications.user_id')
            ->where('users.created_by_id',Auth::guard('admin')->user()->id)->where('is_seen',false)->select("request_recharge_notifications.id as id","request_recharge_notifications.created_at as created_at",'request_recharge_notifications.title_en as title_en','request_recharge_notifications.title_ar as title_ar')->latest()->take(5)->get();
        else 
        $notifications = DB::table('request_recharge_notifications')
        ->where('is_seen',false)->select("request_recharge_notifications.id as id","request_recharge_notifications.created_at as created_at",'request_recharge_notifications.title_en as title_en','request_recharge_notifications.title_ar as title_ar')->latest()->take(5)->get();

            return $notifications;
    }
//END V_5 UPDATES 27/12/2021
    public function getPointsFormat($value)
    {
        if ($value > 10)
            return trans('Point');

        return trans('Points');
    }

    public function hyphenate($str) {
        if(strpos($str,"-") !==true && strlen($str)<=15){
        return implode("-", str_split($str, 3));
        }

        return $str;
    }

public function getPriceOfSale($cardType)
{
    if($cardType->priceOfSale){
        return $cardType->priceOfSale->price;
    }
    return 0;
}

public function getUserNotificationsNumber(){
    return UserNotification::where('is_seen',false)->where('user_id',Auth::user()->id)->count();
}
public function templateMapper($template){
    $mapper = [
        Config::get('constants.TEMPLATES.ZAIN_TEMPLATE') => trans('Zain Template'),
        Config::get('constants.TEMPLATES.UMNIAH_TEMPLATE') => trans('Umniah Template'),
        Config::get('constants.TEMPLATES.ORANGE_TEMPLATE') => trans('Orange Template'),
        Config::get('constants.TEMPLATES.WITHOUT_TEMPLATE') => trans('Without Template'),
    ];

    return $mapper[$template];
}

function containsOnlyNull($input)
{
    return empty(array_filter($input, function ($a) { return $a !== null;}));
}

function truncate_number( $number, $precision = 2) {
    // Zero causes issues, and no need to truncate
    if ( 0 == (int)$number ) {
        return $number;
    }
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}
function removeDash($string){
    $result = str_replace("-", "", $string);
    return $result;
}
function formatMobileNumber($mobileNumber){
    $mobileNumber = ltrim($mobileNumber, '0');
    $mobileNumber = "962".$mobileNumber;
    return $mobileNumber;
}
function convertNumber2Negative($number){
    return -1 * abs($number);
}
function array_multi_unique($multiArray){

    $uniqueArray = array();
  
    foreach($multiArray as $subArray){
  
      if(!in_array($subArray, $uniqueArray)){
        $uniqueArray[] = $subArray;
      }
    }
    return $uniqueArray;
  }

  public function checkAdminPermission($screen){
      $screen = SubPermission::where('child_screen',$screen)->first();
      if(!$screen)
      return true;
      if(AdminSubPermission::where("admin_id",Auth::guard("admin")->user()->id)
      ->where('permission_id',$screen->id)->count()>0){
          return true;
      }

      return false;
      
  }
}


