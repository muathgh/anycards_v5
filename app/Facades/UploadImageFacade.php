<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;
class UploadImageFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'upload';
    }
}