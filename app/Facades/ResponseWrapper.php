<?php
namespace App\Facades;
use Config;
class ResponseWrapper{
    public function success()
    {
        return response()->json([Config::get('constants.RESPONSES.STATUS')=>Config::get('constants.RESPONSES.SUCCESS')],200);
    }
    public function successWithData($data)
    {
        return response()->json([Config::get('constants.RESPONSES.STATUS')=>Config::get('constants.RESPONSES.SUCCESS'),"data"=>$data],200);
    }

    public function successWithMessage($msg){
        return response()->json([Config::get('constants.RESPONSES.STATUS')=>Config::get('constants.RESPONSES.SUCCESS'),"message"=>$msg],200);
    }

    public function failure(){
        return response()->json([Config::get('constants.RESPONSES.STATUS')=>Config::get('constants.RESPONSES.FALIURE')],500);
    }
    public function failureWithMessage($message){
        return response()->json([Config::get('constants.RESPONSES.STATUS')=>Config::get('constants.RESPONSES.FALIURE'),"message"=>$message],500);
    }

    public function failureWithCodeAndError($code,$error){
        return response()->json(['error' => $error], $code); 
    }
}