<?php

namespace App\Providers;

use App\Facades\ResponseWrapper;
use Illuminate\Support\ServiceProvider;

class ResponseFacadeProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('responseWrapper',function(){
            return new ResponseWrapper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
