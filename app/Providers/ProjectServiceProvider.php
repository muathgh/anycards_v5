<?php

namespace App\Providers;

use App\Models\CsvGroupImport\OrangeGroupImport;
use App\Models\CsvGroupImport\OtherGroupImport;
use App\Models\CsvGroupImport\UmniahGroupImport;
use App\Models\CsvGroupImport\ZainGroupImport;
use App\Services\AdminOfficalPriceDiscountService;
use App\Services\AdminOfficalPriceService;
use App\Services\BalanceLimitService;
use App\Services\BalancesService;
use App\Services\BalanceTransactionsService;
use App\Services\CardExpiryService;
use App\Services\CardQuantityLimitService;
use App\Services\CardsLimitService;
use App\Services\CardsService;
use App\Services\CardsTypesService;
use App\Services\CardTransactionService;
use App\Services\CardTypePointsService;
use App\Services\CategoryService;
use App\Services\NetProfitService;
use App\Services\NotificationsService;
use App\Services\OfficalPricePackageService;
use App\Services\PackageService;
use App\Services\PricingListService;
use App\Services\ProviderService;
use App\Services\SettingsService;
use App\Services\SubProviderService;
use App\Services\TransactionsService;
use App\Services\UsersService;
use App\ServicesInterface\BalancesServiceInterface;
use App\ServicesInterface\CardsLimitServiceInterface;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use App\ServicesInterface\CardTransactionsServiceInterface;
use App\ServicesInterface\CategoryServiceInterface;
use App\ServicesInterface\PackageServiceInterface;
use App\ServicesInterface\PricingListServiceInterface;
use App\ServicesInterface\ProviderServiceInterface;
use App\ServicesInterface\SettingsServiceInterface;
use App\ServicesInterface\SubProviderServiceInterface;
use App\ServicesInterface\TransactionsServiceInterface;
use App\ServicesInterface\UsersServiceInterface;
use Illuminate\Support\ServiceProvider;
use App\ServicesInterface\OrdersServiceInterface;
use App\Services\OrdersService;
use App\Services\PermissionsService;
use App\Services\PriceOfSaleService;
use App\Services\RequestRechargeService;
use App\Services\RoleService;
use App\Services\TransferBalanceService;
use App\Services\UserDeviceInfoService;
use App\Services\UsersPriceOfSaleService;
use App\Services\WholesalePriceService;
use App\ServicesInterface\BalanceTransactionsServiceInterface;
use App\ServicesInterface\CardExpiryServiceInterface;
use App\ServicesInterface\CardTypePointsServiceInterface;
use App\ServicesInterface\NetProfitServiceInterface;
use App\ServicesInterface\NotificationsServiceInterface;
use App\ServicesInterface\PermissionsServiceInterface;
use App\ServicesInterface\PriceOfSaleServiceInterface;
use App\ServicesInterface\RedeemCodeServiceInterface;
use App\Services\RedeemCodeService;
use App\Services\RequestRechargeNotificationService;
use App\ServicesInterface\AdminOfficalPriceDiscountServiceInterface;
use App\ServicesInterface\AdminOfficalPriceServiceInterface;
use App\ServicesInterface\BalanceLimitServiceInterface;
use App\ServicesInterface\CardQuantityLimitServiceInterface;
use App\ServicesInterface\OfficalPricePackageServiceInterface;
use App\ServicesInterface\RequestRechargeNotificationServiceInterface;
use App\ServicesInterface\RequestRechargeServiceInterface;
use App\ServicesInterface\RoleServiceInterface;
use App\ServicesInterface\TransferBalanceServiceInterface;
use App\ServicesInterface\UserDeviceInfoServiceInterface;
use App\ServicesInterface\UsersPriceOfSaleServiceInterface;
use App\ServicesInterface\WholesalePriceServiceInterface;


class ProjectServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(

            CategoryServiceInterface::class,
            CategoryService::class

        );
        $this->app->bind(

            ProviderServiceInterface::class,
            ProviderService::class

        );
        $this->app->bind(

            SubProviderServiceInterface::class,
            SubProviderService::class

        );
        $this->app->bind(

            CardsTypesServiceInterface::class,
            CardsTypesService::class

        );
        $this->app->bind(

            CardsServiceInterface::class,
            CardsService::class

        );
        $this->app->bind(

            UsersServiceInterface::class,
            UsersService::class

        );
        $this->app->bind(
            TransactionsServiceInterface::class,
            TransactionsService::class
        );
        $this->app->bind(
            BalancesServiceInterface::class,
            BalancesService::class
        );
        $this->app->bind(
            PackageServiceInterface::class,
            PackageService::class
        );
        $this->app->bind(
            PricingListServiceInterface::class,
            PricingListService::class
        );
        $this->app->bind(
            SettingsServiceInterface::class,
            SettingsService::class
        );
        $this->app->bind(
            CardsLimitServiceInterface::class,
            CardsLimitService::class
        );
        $this->app->bind(
            CardTransactionsServiceInterface::class,
            CardTransactionService::class
        );
        $this->app->bind(
            OrdersServiceInterface::class,
            OrdersService::class
        );
        $this->app->bind(
            RequestRechargeServiceInterface::class,
            RequestRechargeService::class
        );
        $this->app->bind(
            BalanceTransactionsServiceInterface::class,
            BalanceTransactionsService::class
        );
        $this->app->bind(
            NotificationsServiceInterface::class,
            NotificationsService::class
        );
        $this->app->bind(
            PriceOfSaleServiceInterface::class,
            PriceOfSaleService::class
        );
        $this->app->bind(
            CardTypePointsServiceInterface::class,
            CardTypePointsService::class
        );
        $this->app->bind(
            WholesalePriceServiceInterface::class,
            WholesalePriceService::class
        );
        $this->app->bind(
            RoleServiceInterface::class,
            RoleService::class
        );
        $this->app->bind(
            PermissionsServiceInterface::class,
            PermissionsService::class
        );

        $this->app->bind(
            CardExpiryServiceInterface::class,
            CardExpiryService::class
        );
        $this->app->bind(
            TransferBalanceServiceInterface::class,
            TransferBalanceService::class
        );
        $this->app->bind(
            NetProfitServiceInterface::class,
            NetProfitService::class
        );
        $this->app->bind(
            UserDeviceInfoServiceInterface::class,
            UserDeviceInfoService::class
        );
        $this->app->bind(
            UsersPriceOfSaleServiceInterface::class,
            UsersPriceOfSaleService::class
        );

        $this->app->bind(
            RedeemCodeServiceInterface::class,
            RedeemCodeService::class
        );

        $this->app->bind(
            RequestRechargeNotificationServiceInterface::class,
            RequestRechargeNotificationService::class
        );

        $this->app->bind(
            CardQuantityLimitServiceInterface::class,
            CardQuantityLimitService::class
        );

        // V_6 UPDATES 06/01/2022
        $this->app->bind(OfficalPricePackageServiceInterface::class, OfficalPricePackageService::class);
        $this->app->bind(AdminOfficalPriceServiceInterface::class,AdminOfficalPriceService::class);
        // END V_6 UPDATES 06/01/2022

        // V_6 UPDATES 08/01/2022
        $this->app->bind(AdminOfficalPriceDiscountServiceInterface::class,AdminOfficalPriceDiscountService::class);
        // END V_6 UPDATES 08/01/2022

        $this->app->singleton(ZainGroupImport::class, function ($app) {
            return new ZainGroupImport(new CardsService(), new CardsTypesService());
        });

        $this->app->singleton(OrangeGroupImport::class, function ($app) {
            return new OrangeGroupImport(new CardsService(), new CardsTypesService());
        });

        $this->app->singleton(UmniahGroupImport::class, function ($app) {
            return new UmniahGroupImport(new CardsService(), new CardsTypesService());
        });

        // V_5 UPDATES 28/12/2021

        $this->app->bind(BalanceLimitServiceInterface::class, BalanceLimitService::class);

        // END V_5 UPDATES 28/12/2021

        // V_5 UPDATES 30/12/2021

        $this->app->singleton(OtherGroupImport::class, function ($app) {
            return new OtherGroupImport(new CardsService(), new CardsTypesService());
        });

        //END V_5 UPDATES 30/12/2021
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
