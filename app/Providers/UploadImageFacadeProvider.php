<?php

namespace App\Providers;

use App\Facades\UploadImage;
use Illuminate\Support\ServiceProvider;

class UploadImageFacadeProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('upload',function(){
            return new UploadImage();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
