<?php

namespace App\Providers;

use App\Facades\Helper;
use Illuminate\Support\ServiceProvider;

class HelperFacadeProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('helper',function(){
            return new Helper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
