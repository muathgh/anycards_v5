<?php

namespace App\Providers;

use App\Facades\FcmNotification;
use App\Facades\SmsSender;
use App\Facades\UploadFile;
use App\Models\CardModels\Card;
use App\Models\RequestRechargeModels\RequestRecharge;
use App\Models\TransactionModels\CardTransaction;
use App\Observers\CardTransactionObserver;
use App\Observers\RequestRechargeObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        
        // $this->app->bind('path.public', function() {
        //     return realpath(base_path().'/../public_html');
        //     });

            $this->app->bind('fcmNotification',function(){
                return new FcmNotification();
            });
            $this->app->bind('upload-file',function(){
                return new UploadFile();
            });
            $this->app->bind('smsSender',function(){
                return new SmsSender();
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        RequestRecharge::observe(RequestRechargeObserver::class);
        CardTransaction::observe(CardTransactionObserver::class);
        \Debugbar::disable();
    }
}
