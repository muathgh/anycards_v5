<?php

namespace App\Models;

use App\Facades\FcmNotificationFacade;
use App\Http\SecureDelete;
use App\Models\CardModels\CardPrice;
use App\Models\PricingListModels\Package;
use App\Models\RequestRechargeModels\RequestRecharge;
use App\Models\SettingModels\Receivables;
use App\Models\SettingModels\UserNotification;
use App\Models\TransactionModels\UserBalanceTransaction;
use App\Models\TransactionModels\Balance;
use App\Models\TransactionModels\Transaction;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRoles,SecureDelete;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'password',
        'username',
        'email',
        'mobile_number',
        'status',
        'package_id',
        "ip_address",
        'last_login',
        "created_by_id",
        'updated_by_id',
        "first_time_login",
        'logo'
    ];

    protected $appends = array('logo_or_default','full_user_name');
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($user){
            $user->notifications()->delete();
        });
    }

    protected $hidden = [
        'password',
        'remember_token',
    ];


    public function getLastLoginAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function setLastLoginAttribute($value)
    {
        $this->attributes['last_login'] = \Carbon\Carbon::parse($value);
    }

    public function getLogoOrDefaultAttribute()
    {

        return $this->attributes['logo'] ? $this->attributes['logo'] : asset('/images/defaultuser.jpg');
    }

    public function getFullUserNameAttribute(){
        return $this->attributes['name'] . " (".$this->attributes['username'].")";
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }

    public function balances()
    {
        return $this->hasMany(Balance::class);
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function cardPrices()
    {
        return $this->hasMany(CardPrice::class);
    }

    public function requestRecharges()
    {
        return $this->hasMany(RequestRecharge::class);
    }

    public function notifications()
    {
        return $this->hasMany(UserNotification::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(Admin::class, "created_by_id");
    }

    public function updatedBy(){
        return $this->belongsTo(Admin::class, "updated_by_id");
    }

    public function receivables(){
        return $this->hasOne(Receivables::class);
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function withDrawBalance($amount){
      
        $transaction =  $this->transactions()->create([
            "amount" => $amount,
            "type" => Config::get('constants.TRANSACTION_TYPE.WITHDRAW'),
            "created_by_id"=>Auth::guard('admin')->user()->id
            
        ]);
    

        $currentBalance = $this->currentBalance();
        $newBalance = $currentBalance - $amount;
        if($newBalance <=0)
        $newBalance =0;

        $this->balances()->create([
            "balance" => $newBalance,
            "transaction_id" => $transaction->id,
            "last_update"=>now()
        ]);

        $this->notifications()->Create([
            "title_en"=>"Balance Withdraw",
            "title_ar"=>"سحب رصيد",
            "description_en"=>sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_WITHDRAW_EN'), $amount),
            "description_ar"=>sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_WITHDRAW_AR'), $amount),
            "type"=>Config::get('constants.USER_NOTIFICATION_TYPE.BALANCE_WITHDRAW')
            ]);

            $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $this->id)->first();
            if ($fcmToken) {
                $token = $fcmToken->token;
                FcmNotificationFacade::sendNotification($token, sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_WITHDRAW_AR'), $amount), "سحب رصيد");

            }

    }

    public function sendFcmNotification($title,$description){
      
        $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $this->id)->first();
        if ($fcmToken) {
            $token = $fcmToken->token;
            FcmNotificationFacade::sendNotification($token,$description,$title);

        }
    }

    public function overAllDepositTransactionsAmount(){
        return $this->transactions()->where('type',Config::get('constants.TRANSACTION_TYPE.DEPOSIT'))->sum('amount');
       
    }

    public function lastDepositTransaction(){
        return $this->transactions()->where('type',Config::get('constants.TRANSACTION_TYPE.DEPOSIT'))->latest('id')->first();
    }

    public function transferBalance($username,$amount){
        $toUser =User::where("username",$username)->first();
        if(!$toUser){
            return ["success"=>false,"error"=>"User does not exist"];
        }
        $fromUserCurrentBalance = $this->currentBalance();
        if($fromUserCurrentBalance < $amount)
        {
            return ["success"=>false,"error"=>"Your balance is less than amount"];  
        }

        $lastTransaction = UserBalanceTransaction::where('from_user_id',$this->id)
        ->where('to_user_id',$toUser->id)->latest()->first();
        if($lastTransaction)
        {
            $transaction = $lastTransaction->senderTransaction;
            if($transaction->amount == number_format((float)$amount, 2, '.', '') && $lastTransaction->next_transaction)
            {
            if(\Carbon\Carbon::now()->lte(\Carbon\Carbon::parse($lastTransaction->next_transaction))){
                return ["success"=>false,"error"=>"Next transaction must be after 5 min"];  
            }
        }
        }

         $deductTransaction =  $this->transactions()->create([
            "amount" => $amount,
            "type" => Config::get('constants.TRANSACTION_TYPE.WITHDRAW'),
            "created_by_id"=>null
            
        ]);
       
        $fromUsernewBalance = $fromUserCurrentBalance - $amount;
        if($fromUsernewBalance <=0)
        $fromUsernewBalance =0;

        $this->balances()->create([
            "balance" => $fromUsernewBalance,
            "transaction_id" => $deductTransaction->id,
            "last_update"=>now()
        ]);
// 
        $depositTransaction =  $toUser->transactions()->create([
            "amount" => $amount,
            "type" => Config::get('constants.TRANSACTION_TYPE.DEPOSIT'),
            "created_by_id"=>null
            
        ]);
        $toUserCurrentBalance = $toUser->currentBalance();
        $toUserNewBalance = $toUserCurrentBalance + $amount;
      

        $toUser->balances()->create([
            "balance" => $toUserNewBalance,
            "transaction_id" => $depositTransaction->id,
            "last_update"=>now()
        ]);

        UserBalanceTransaction::create([
            'from_user_id'=>$this->id,
            "to_user_id"=>$toUser->id,
            
            "sender_transaction_id"=>$deductTransaction->id,
            "receiver_transaction_id"=>$depositTransaction->id,
            'next_transaction'=>\Carbon\Carbon::now()->addMinutes(5)
        ]);

// notification to sender
$username = $toUser->username;
$titleEn = "Balance Transfered";
$titleAr="تحويل رصيد";
$senderNotificationAr = "تم تحويل";
$senderNotificationAr.=" $amount ";

$senderNotificationAr .=" نقطة الى المستخدم ";
$senderNotificationAr.=" $username ";
$this->notifications()->Create([
    "title_en"=>$titleEn,
    "title_ar"=>$titleAr,
    "description_en"=>$senderNotificationAr,
    "description_ar"=>$senderNotificationAr,
    "type"=>""
    ]);
    $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $this->id)->first();
    if ($fcmToken) {
        $token = $fcmToken->token;
        FcmNotificationFacade::sendNotification($token, $senderNotificationAr,$titleAr);

    }


    // receiver notification
    $receiverTitleEn="Balance received";
    $receiverTitleAr="استلام رصيد";
    $receiverNotificationAr = "تم استلام قيمة";
    $receiverNotificationAr.=" $amount ";
    $receiverNotificationAr .=" من المستخدم ";
    $receiverNotificationAr.=" $this->username ";
    $toUser->notifications()->Create([
        "title_en"=>$receiverTitleEn,
        "title_ar"=>$receiverTitleAr,
        "description_en"=>$receiverNotificationAr,
        "description_ar"=>$receiverNotificationAr,
        "type"=>""
        ]);
    $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $toUser->id)->first();
    if ($fcmToken) {
        $token = $fcmToken->token;
        FcmNotificationFacade::sendNotification($token, $receiverNotificationAr,$receiverTitleAr);

    }
        return ["success"=>true,"error"=>null];
    }

    public function paidUp(){
        $sum = 0;
     $this->transactions()->where('type',Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))->get()->each(function($t) use(&$sum){
         $sum+=$t->amount;

     });

     return $sum;
    }
    

    public function addBalance($amount)
    {
        $transaction =  $this->transactions()->create([
            "amount" => $amount,
            "type" => Config::get('constants.TRANSACTION_TYPE.DEPOSIT'),
            "created_by_id"=>Auth::guard('admin')->user()->id,
            "next_transaction"=>\Carbon\Carbon::now()->addMinutes(5)
        ]);

        $requestRecharge =        $this->requestRecharges()->where('status', Config::get('constants.REQUEST_RECHARGE_STATUS.PENDING'))
            ->first();
        if ($requestRecharge) {
            $requestRecharge->status = Config::get('constants.REQUEST_RECHARGE_STATUS.APPROVED');
            $requestRecharge->transaction_id = $transaction->id;
            $requestRecharge->save();
        }
        $newBalance = $amount;
        if ($this->balances()->count() > 0) {
            $currentBalance = $this->currentBalance();
            $newBalance = $currentBalance + $amount;
        }
        $this->balances()->create([
            "balance" => $newBalance,
            "transaction_id" => $transaction->id,
            "last_update"=>now()
        ]);
        $descriptionAr = " تم إضافة ";
        $descriptionAr.=" $amount نقطة ";
        $descriptionAr.=" الى رصيدك ";
        $this->notifications()->Create([
            "title_en"=>"balance Recharge",
            "title_ar"=>"شحن رصيد",
            "description_en"=>sprintf(Config::get('constants.USER_NOTIFICATION_MESSAGES.BALANCE_DEPOSIT_EN'), $amount),
            "description_ar"=>$descriptionAr,
            "type"=>Config::get('constants.USER_NOTIFICATION_TYPE.BALANCE_DEPOSIT')
            ]);

            $fcmToken = DB::table('user_fcm_tokens')->where('user_id', $this->id)->first();
            if ($fcmToken) {
                $token = $fcmToken->token;
                FcmNotificationFacade::sendNotification($token,$descriptionAr ,"إضافة رصيد" );

            }

            
    }

    public function scopeTotalOfReceivables($query){
        $total = 0;
        foreach(User::all() as $user){
            $total += $user->receivables && $user->receivables->notes != "" ? $user->receivables->notes : 0;
        }

        return $total;
    }

    public function getUserBalancePerYear(){
        $currentYear = date("Y");
      
        $transactions = $this->transactions()->where('type',Config::get('constants.TRANSACTION_TYPE.DEPOSIT'))->get();
        $total = 0;
        $transactions->each(function($t) use(&$total,$currentYear){
$createdAt = \Carbon\Carbon::parse($t->created_at)->format('Y');

if($createdAt == $currentYear){
    $total+=$t->amount;

}

        });

        return $total;

    }

    public function currentBalance()
    {

        // $balance = DB::table('balances')->
        // join('transactions','transactions.id','=','balances.transaction_id')
        // ->where('balances.user_id',$this->id)
        // ->where('transactions.type','!=',Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))
        // ->latest('balances.id')->first();

        $balance =  $this->balances()->latest("id")->first();
        if ($balance)
            return $balance->balance;
        return 0;
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function scopeTotalPriceOfSaleTransactions($query){
        $userId = auth("web")->user()->id;
        $sum =0;
        $query->where('id',$userId)->first()->transactions()->where
        ('type',Config::get('constants.TRANSACTION_TYPE.DEDUCTION'))->get()->each(function($t) use(&$sum){

$card = $t->cardTransaction->card;
$priceOfSale = $card->cardType->priceOfSale ? $card->cardType->priceOfSale->price : 0;
$sum+=$priceOfSale;

        });

        return $sum;
    }

    public function lastBalanceTransaction(){
        
        return Transaction::where('user_id',$this->id)->where('type',Config::get('constants.TRANSACTION_TYPE.DEPOSIT'))->orWhere('type',Config::get('constants.TRANSACTION_TYPE.WITHDRAW'))
        ->latest('created_at')->first();
    }
}
