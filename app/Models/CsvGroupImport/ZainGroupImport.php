<?php
namespace App\Models\CsvGroupImport;

use App\Facades\HelperFacade;
use App\Models\CardModels\Card;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use League\Csv\Reader;
// V_5 UPDATES 27/12/2021
class ZainGroupImport
{
    private $_cardsService, $_cardTypeService;
    private $duplicate = [];
    private $warning_messages = [];
    public function __construct(CardsServiceInterface $cardsService, CardsTypesServiceInterface $cardTypeService)
    {
        $this->_cardsService = $cardsService;
        $this->_cardTypeService = $cardTypeService;
    }

    function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }

    private function importCsv($file)
    {


        try{
        // validate file name
        $fileName  = $file->getClientOriginalName();
        if(!$this->startsWith($fileName,"ZAIN"))
        return "invalid file";
        $file_handle = fopen($file, 'r');
  



        while (!feof($file_handle)) {

            $line_of_text[] = fgetcsv($file_handle, 0);
        }

        fclose($file_handle);
        
        for ($i = 1; $i < sizeof($line_of_text); $i++) {
            set_time_limit(0);
            $v = $line_of_text[$i];
            
         
     

            $values = $v;
           
            $expiryDate = $values[0];
            
            $expiryDate = explode('-', $expiryDate);
          
            $month = (int)$expiryDate[1];
            $year = $expiryDate[2];
            $expiryDate = \Carbon\Carbon::createFromDate($year, $month, \Carbon\Carbon::now()->day);
            $productType = $values[1];
            $productType = rtrim($productType);
            $productType = ltrim($productType);
            $denomination = $values[2];
            $denomination = rtrim($denomination);
            $denomination = ltrim($denomination);
            $denomination = (string)((float) $denomination);
            $type = $productType . " " . $denomination;
           
            $pinCode = $values[3];
            $serial = $values[4];
            $codeToCheck = HelperFacade::removeDash($pinCode);
            $codeToCheck = trim($codeToCheck);
        
            $card = $this->_cardsService->whereRaw('REPLACE(cards.code,"-","") = ?', [$codeToCheck])->first();
           
            // $type = $this->mapDenomination()[$type];
       
            $cardType = $this->_cardTypeService->query()->where('card_code', $type)->first();
        
                if ($cardType) {
                    if (!$card) {
                        if (strlen(str_replace("-", "", $pinCode)) == 14) {
                            Card::create([
                                "expiry_date" => $expiryDate,
                                "code" => $pinCode,
                                "serial" => $serial,
                                "card_type_id" => $cardType->id,
                                "created_by_id" => Auth::guard('admin')->user()->id,

                            ]);
                        } else {
                            $this->warning_messages[] = "large_number_of_digits";
                            Session::put('warning_message', $this->warning_messages);
                        }
                    } else {
                        $this->warning_messages[] = 'duplicate_cards';
                        Session::put('warning_message', $this->warning_messages);
                        $this->duplicate[] = $card->code;
                        Session::put("duplicate_cards", $this->duplicate);
                    }
                }
            
        }
     
    }catch(\Exception $e){
      
    }


    }

    public function upload($file)
    {
        return   $this->importCsv($file);
    }

    // private function mapDenomination()
    // {
    //     return [
    //         "GSM 0.5" => "Zain Prepaid 1/2",
    //         "GSM 1" => "Zain Prepaid 1 JD",
    //         "GSM 1.65"=>"Zain 1.65 JD",
    //         "GSM 3" => "Zain Prepaid 3 JD",
    //         "GSM 5" => "Zain Prepaid 5 JD",
    //         "GSM 6" => "Zain Prepaid 6 JD",
    //         "GSM 9" => "Zain Prepaid 9 JD",
    //         "GSM 12" => "Zain Prepaid 12 JD",
    //         "GSM 20" => "Zain Prepaid 20 JD",
    //         "GSM 36" => "Zain Prepaid 36 JD",
    //         "Data 1" => "Broad Band 1",
    //         "Data 2" => "Broad Band 2",
    //         "Data 3" => "Broad Band 3",
    //         "Data 4" => "Broad Band 4",
    //         "Data 6" => "Broad Band 6",
    //         "Data 8" => "Broad Band 8",
    //         "Data 9" => "Broad Band 9",
    //         "Data 10" => "Broad Band 10",
    //         "Data 11" => "Broad Band 11",
    //         "Data 15"=>"Broad Band 15",           
    //         "Data 20"=>"Broad Band 20",            
    //         "Mix 2.5" => "Mix 2.5 * 2.5",
    //         "Mix 3" => "Mix 3 * 3",
    //         "Mix 3.5" => "Mix 3.5 * 3.5",
    //         "Mix 4" => "Mix 4 * 4",
    //         "Mix 5" => "Mix 5 * 5",
    //         "Mix 5.5" => "Mix 5.5 * 5.5",
    //         "Mix 7.5" => "Mix 7.5 * 7.5",



    //     ];
    // }
}


//END V_5 UPDATES 27/12/2021