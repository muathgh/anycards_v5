<?php

namespace App\Models\CsvGroupImport;

use App\Facades\HelperFacade;
use App\Models\CardModels\Card;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use League\Csv\Reader;
// V_5 UPDATES 27/12/2021
class OtherGroupImport
{
    private $_cardsService, $_cardTypeService;
    private $duplicate = [];
    private $warning_messages = [];
    public function __construct(CardsServiceInterface $cardsService, CardsTypesServiceInterface $cardTypeService)
    {
        $this->_cardsService = $cardsService;
        $this->_cardTypeService = $cardTypeService;
    }

    function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }

    private function importCsv($file)
    {


        try{
                  // validate file name
        $fileName  = $file->getClientOriginalName();
      
        if(!$this->startsWith($fileName,"OTHER"))
        {
           
            return "invalid file";
        }
       
        $file_handle = fopen($file, 'r');
  



        while (!feof($file_handle)) {

            $line_of_text[] = fgetcsv($file_handle, 0);
        }

        fclose($file_handle);

        for ($i = 1; $i < sizeof($line_of_text); $i++) {
            if(!is_array($line_of_text[$i]))
            continue;
            $v = $line_of_text[$i];
          
            
         
           
            
         
     

            $values = explode(";",$v[0]);
          
           
            $expiryDate = $values[0];
            
            $expiryDate = explode('/', $expiryDate);
         
          
            $month = (int)$expiryDate[1];
      
            $year = $expiryDate[2];

            
            $expiryDate = \Carbon\Carbon::createFromDate($year, $month, \Carbon\Carbon::now()->day);
         
          
            
            $productType = $values[1];
           
           
           
            $pinCode = $values[2];
         
          
            $codeToCheck = HelperFacade::removeDash($pinCode);
            $codeToCheck = trim($codeToCheck);
            $card = $this->_cardsService->whereRaw('REPLACE(cards.code,"-","") = ?', [$codeToCheck])->first();
            $cardType = $this->_cardTypeService->query()->where('card_code', $productType)->first();
         
                if ($cardType) {
                    
                    if (!$card) {
                    
                        Card::create([
                            "expiry_date" => $expiryDate,
                            "code" => $pinCode,
                          
                            "card_type_id" => $cardType->id,
                            "created_by_id" => Auth::guard('admin')->user()->id,

                        ]);
                        
                    } else {
                        $this->warning_messages[] = 'duplicate_cards';
                        Session::put('warning_message', $this->warning_messages);
                        $this->duplicate[] = $card->code;
                        Session::put("duplicate_cards", $this->duplicate);
                    }
                }
            
        }
    }catch(\Exception $e){

    }


    }

    public function upload($file)
    {
      return  $this->importCsv($file);
    }

}
//END V_5 UPDATES 27/12/2021