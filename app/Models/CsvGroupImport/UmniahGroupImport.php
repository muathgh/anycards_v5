<?php

namespace App\Models\CsvGroupImport;
use App\Facades\HelperFacade;
use App\Models\CardModels\Card;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use League\Csv\Reader;
// V_5 UPDATES 27/12/2021
class UmniahGroupImport {
private $_cardsService,$_cardTypeService;
private $duplicate=[];
private $warning_messages = [];
public function __construct(CardsServiceInterface $cardsService,CardsTypesServiceInterface $cardTypeService)
{
    $this->_cardsService = $cardsService;
    $this->_cardTypeService = $cardTypeService;
}

function startsWith ($string, $startString)
{
    
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}

private function importCsv($file)
{


    try{
              // validate file name
              $fileName  = $file->getClientOriginalName();
             
              if(!$this->startsWith($fileName,"UMNIAH"))
              return "invalid file";
    $file_handle = fopen($file, 'r');




    while (!feof($file_handle)) {

        $line_of_text[] = fgetcsv($file_handle, 0);
    }

    fclose($file_handle);

    for ($i = 1; $i < sizeof($line_of_text); $i++) {
    
        $v = $line_of_text[$i];
        
     
 

        $values = $v;
       
        $expiryDate = $values[0];
 
        
        $expiryDate = explode('-', $expiryDate);
      
        $month = (int)$expiryDate[1];
        $year = $expiryDate[2];
       
        $expiryDate = \Carbon\Carbon::createFromDate($year, $month, \Carbon\Carbon::now()->day);
        dd($expiryDate);
        $productType = $values[1];
        $productType = rtrim($productType);
        $productType = ltrim($productType);
        $denomination = $values[2];
        $denomination = rtrim($denomination);
        $denomination = ltrim($denomination);
        $denomination = (string)((float) $denomination);
        $type = $productType . " " . $denomination;
       
        $pinCode = $values[3];
        $serial = $values[4];
        $codeToCheck = HelperFacade::removeDash($pinCode);
        $codeToCheck = trim($codeToCheck);
        $card = $this->_cardsService->whereRaw('REPLACE(cards.code,"-","") = ?', [$codeToCheck])->first();
        $type = $this->mapDenomination()[$type];
   
        $cardType = $this->_cardTypeService->query()->where('title_en', $type)->first();
       
            if ($cardType) {
                if (!$card) {
                    if (strlen(str_replace("-", "", $pinCode)) == 14) {
                        Card::create([
                            "expiry_date" => $expiryDate,
                            "code" => $pinCode,
                            "serial" => $serial,
                            "card_type_id" => $cardType->id,
                            "created_by_id" => Auth::guard('admin')->user()->id,

                        ]);
                    } else {
                        $this->warning_messages[] = "large_number_of_digits";
                        Session::put('warning_message', $this->warning_messages);
                    }
                } else {
                    $this->warning_messages[] = 'duplicate_cards';
                    Session::put('warning_message', $this->warning_messages);
                    $this->duplicate[] = $card->code;
                    Session::put("duplicate_cards", $this->duplicate);
                }
            }
        
    }
}catch(\Exception $e){

}


}

public function upload($file){
   return $this->importCsv($file);
}

private function mapDenomination(){
    return [
"GSM 1"=>"Umniah 1 JD",
"GSM 3"=>"Umniah 3 JD",
"GSM 5"=>"Umniah 5 JD",
"GSM 10"=>"Umniah 10 JD",
"Smart 1"=>"Smart 1",
"Smart 2"=>"Smart 2",
"Smart 3"=>"Smart 3",
"Smart 4"=>"Smart 4",
"Smart 5"=>"Smart 5",
"Smart 6"=>"Smart 6",
"Smart 7"=>"Smart 7",
"Smart 8"=>"Smart 8",
"Smart 9"=>"Smart 9",
"Smart 10"=>"Smart 10",
"Smart 12"=>"Smart 12",
"Smart 13"=>"Smart 13",
"Data 1"=>"Evo 1",
"Data 2"=>"Evo 2",
"Data 3"=>"Evo 3",
"Data 4"=>"Evo 4",
"Data 5"=>"Evo 5",
"Data 6"=>"Evo 6",
"Data 7"=>"Evo 7",
"Data 8"=>"Evo 8",
"Data 9"=>"Evo 9",
"Data 10"=>"Evo 10",
"Data 15"=>"Evo 15",



    ];
}


}
//END V_5 UPDATES 27/12/2021