<?php

namespace App\Models\CsvGroupImport;

use App\Facades\HelperFacade;
use App\Models\CardModels\Card;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\ServicesInterface\CardsServiceInterface;
use App\ServicesInterface\CardsTypesServiceInterface;
use League\Csv\Reader;
// V_5 UPDATES 27/12/2021
class OrangeGroupImport
{
    private $_cardsService, $_cardTypeService;
    private $duplicate = [];
    private $warning_messages = [];
    public function __construct(CardsServiceInterface $cardsService, CardsTypesServiceInterface $cardTypeService)
    {
        $this->_cardsService = $cardsService;
        $this->_cardTypeService = $cardTypeService;
    }

    function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }

    private function importCsv($file)
    {


        try{
                  // validate file name
        $fileName  = $file->getClientOriginalName();
        if(!$this->startsWith($fileName,"ORANGE"))
        return "invalid file";
        $file_handle = fopen($file, 'r');
  



        while (!feof($file_handle)) {

            $line_of_text[] = fgetcsv($file_handle, 0);
        }

        fclose($file_handle);

        for ($i = 1; $i < sizeof($line_of_text); $i++) {
        
            $v = $line_of_text[$i];
            
         
     

            $values = $v;
           
            $expiryDate = $values[0];
            
            $expiryDate = explode('-', $expiryDate);
         
          
            $month = (int)$expiryDate[1];
      
            $year = $expiryDate[2];

            
            $expiryDate = \Carbon\Carbon::createFromDate($year, $month, \Carbon\Carbon::now()->day);
         
          
            
            $productType = $values[1];
            $productType = rtrim($productType);
            $productType = ltrim($productType);
            $denomination = $values[2];
            $denomination = rtrim($denomination);
            $denomination = ltrim($denomination);
            $denomination = (string)((float) $denomination);
            $type = $productType . " " . $denomination;
           
            $pinCode = $values[3];
            $serial = $values[4];
            $codeToCheck = HelperFacade::removeDash($pinCode);
            $codeToCheck = trim($codeToCheck);
            $card = $this->_cardsService->whereRaw('REPLACE(cards.code,"-","") = ?', [$codeToCheck])->first();
            $cardType = $this->_cardTypeService->query()->where('card_code', $type)->first();
           
                if ($cardType) {
                    if (!$card) {
                        if (strlen(str_replace("-", "", $pinCode)) == 14) {
                            Card::create([
                                "expiry_date" => $expiryDate,
                                "code" => $pinCode,
                                "serial" => $serial,
                                "card_type_id" => $cardType->id,
                                "created_by_id" => Auth::guard('admin')->user()->id,

                            ]);
                        } else {
                            $this->warning_messages[] = "large_number_of_digits";
                            Session::put('warning_message', $this->warning_messages);
                        }
                    } else {
                        $this->warning_messages[] = 'duplicate_cards';
                        Session::put('warning_message', $this->warning_messages);
                        $this->duplicate[] = $card->code;
                        Session::put("duplicate_cards", $this->duplicate);
                    }
                }
            
        }
    }catch(\Exception $e){
     
    }


    }

    public function upload($file)
    {
       return $this->importCsv($file);
    }

    private function mapDenomination()
    {
        return [
            "Voice 0.5" => "Orange 1/2 JD",
            "Voice 1" => "Orange 1 JD",
            "Voice 3" => "Orange 3 JD",
            "Voice 5" => "Orange 5 JD",
            "Voice 6" => "Orange 6 JD",
            "Voice 10" => "Orange 10 JD",
            "Voice 12" => "Orange 12 JD",
            "Voice 22" => "Orange 22 JD",
            "Voice 32" => "Orange 32 JD",
            "Nos b Nos 2" => "Nos B Nos 2",
            "Nos b Nos 5" => "Nos B Nos 5",
            "Nos b Nos 6" => "Nos B Nos 6",
            "Nos b Nos 6.5" => "Nos B Nos 6.5",
            "Nos b Nos 7" => "Nos B Nos 7",
            "Nos b Nos 8" => "Nos B Nos 8",
            "Nos b Nos 9" => "Nos B Nos 9",
            "Nos b Nos 10" => "Nos B Nos 10",
            "Nos b Nos 11" => "Nos B Nos 11",
            "Nos b Nos 12" => "Nos B Nos 12",
            "IEW 1" => "Orange Data IEW 1 JD",
            "IEW 2" => "Orange Data IEW 2 JD",
            "IEW 3" => "Orange Data IEW 3 JD",
            "IEW 4" => "Orange Data IEW 4 JD",
            "IEW 5" => "Orange Data IEW 5 JD",
            "IEW 6" => "Orange Data IEW 6 JD",
            "IEW 7" => "Orange Data IEW 7 JD",
            "IEW 8" => "Orange Data IEW 8 JD",
            "IEW 9" => "Orange Data IEW 9 JD",
            "IEW 10" => "Orange Data IEW 10 JD",
            "IEW 11" => "Orange Data IEW 11 JD",
            "IEW 12" => "Orange Data IEW 12 JD",
            "IEW 15" => "Orange Data IEW 15 JD",
            "IEW 16" => "Orange Data IEW 16 JD",
            "Weinak 2" => "Weinak 2 JD",



        ];
    }
}
//END V_5 UPDATES 27/12/2021