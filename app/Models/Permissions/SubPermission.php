<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubPermission extends Model {
    protected $table = "sub_permissions";
    protected $fillable =["parent_screen","child_screen"];

}