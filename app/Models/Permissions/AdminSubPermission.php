<?php

namespace App\Models\Permissions;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminSubPermission extends Model {
    protected $table = "admins_sub_permissions";
    protected $fillable =["permission_id","admin_id"];

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_id');
    }
    
    public function permission(){
        return $this->belongsTo(SubPermission::class,'permission_id');
    }
}