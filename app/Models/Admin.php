<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Http\SecureDelete;
use App\Models\PricingListModels\OfficalPricePackage;
use App\Models\SettingModels\BalanceLimit;
// V_6 UPDATES 06/01/2022
class Admin extends Authenticatable
{
    use HasRoles,SecureDelete;
    protected $guard = 'admin';
    protected $fillable=['username','password','offical_price_package_id'];
    protected $hidden = [
        'password','remember_token'
   
    ];
    use HasFactory;

    public function users(){
        return $this->hasMany(User::class,"created_by_id");
    }

    public function officalPricePackage(){
        return $this->belongsTo(OfficalPricePackage::class,"offical_price_package_id");
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }


    public function accessType(){
        if(sizeof($this->roles)>0){
            $role = $this->roles[0];
            $accessType= $role->access_type;
            return $accessType;
        }
        return null;
    }

    public function isSuperAdmin(){
        if(sizeof($this->roles)>0){
            $role = $this->roles[0];
            return $role->is_super_admin;
        }
        return false;
    }

   public function balance(){
       return $this->hasOne(BalanceLimit::class);
   }

   public function currentBalance(){
       if($this->balance){
           return $this->balance->amount;
       }
       return null;
   }
}

// END V_6 UPDATES 06/01/2022