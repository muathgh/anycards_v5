<?php

namespace App\Models\OfficalPriceModels;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
use App\Models\Admin;
use App\Models\CategoryModels\Provider;

class OfficalPriceDiscount extends Model
{
    protected $table = "offical_price_discounts";
    protected $fillable=["offical_price_package_id","provider_id","discount",'created_by_id','updated_by_id'];
    use HasFactory;
use SecureDelete;

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function provider(){
        return $this->belongsTo(Provider::class,'provider_id');
    }

    public function officalPricePackage(){
        return $this->belongsTo(OfficalPricePackage::class,"offical_price_package_id");
    }

    public function createdBy(){
        return $this->belongsTo(Admin::class,"created_by_id");
    }

    public function updatedBy(){
        return $this->belongsTo(Admin::class,"updated_by_id");
    }

}
