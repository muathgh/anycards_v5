<?php

namespace App\Models\OfficalPriceModels;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
use App\Models\Admin;

class OfficalPricePackage extends Model
{
    protected $table ="offical_price_packages";
    protected $fillable=["title_en","title_ar","description_en","description_ar",'created_by_id','updated_by_id'];
    use HasFactory;
use SecureDelete;

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function createdBy(){
        return $this->belongsTo(Admin::class,"created_by_id");
    }

    public function updatedBy(){
        return $this->belongsTo(Admin::class,"updated_by_id");
    }

    public function admins(){
        return $this->hasMany(Admin::class);
    }

    public function adminOfficalPrices(){
        return $this->hasMany(AdminOfficalPrice::class);
    }

}
