<?php

namespace App\Models\OfficalPriceModels;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
use App\Models\Admin;
use App\Models\CategoryModels\CardType;

class AdminOfficalPrice extends Model
{
    protected $table = "admins_offical_price";
    protected $fillable=["offical_price_package_id","card_type_id","price","price_after_discount",'created_by_id','updated_by_id'];
    use HasFactory;
use SecureDelete;

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function cardType(){
        return $this->belongsTo(CardType::class,"card_type_id");
    }
    public function officalPricePackage(){
        return $this->belongsTo(OfficalPricePackage::class,"offical_price_package_id");
    }

    public function createdBy(){
        return $this->belongsTo(Admin::class,"created_by_id");
    }

    public function updatedBy(){
        return $this->belongsTo(Admin::class,"updated_by_id");
    }

}
