<?php

namespace App\Models\PricingListModels;

use App\Models\Admin;
use App\Models\CategoryModels\CardType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricingList extends Model
{
    protected $fillable=["package_id","card_type_id","price",'created_by_id','updated_by_id'];
    use HasFactory;

    public function package(){
        return $this->belongsTo(Package::class,'package_id');
    }

    public function cardType(){
        return $this->belongsTo(CardType::class,'card_type_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function createdBy(){
        return $this->belongsTo(Admin::class,"created_by_id");
    }

    public function updatedBy(){
        return $this->belongsTo(Admin::class,"updated_by_id");
    }

    
}
