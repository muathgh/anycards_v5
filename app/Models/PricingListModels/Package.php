<?php

namespace App\Models\PricingListModels;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
use App\Models\Admin;

class Package extends Model
{
    protected $fillable=["title_en","title_ar","description_en","description_ar",'created_by_id','updated_by_id'];
    use HasFactory;
use SecureDelete;
    public function pricingLists(){
        return $this->hasMany(PricingList::class);
    }
    public function users(){
        return $this->hasMany(User::class);
    }
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function createdBy(){
        return $this->belongsTo(Admin::class,"created_by_id");
    }

    public function updatedBy(){
        return $this->belongsTo(Admin::class,"updated_by_id");
    }

}
