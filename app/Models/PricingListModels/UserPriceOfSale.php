<?php

namespace App\Models\PricingListModels;

use App\Models\User;
use App\Models\CategoryModels\CardType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPriceOfSale extends Model {
    protected $table="users_price_of_sale";
    protected $fillable = ["card_type_id","price","user_id"];

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function cardType(){
        return $this->belongsTo(CardType::class,"card_type_id");
    }

    public function user(){
        return $this->belongsTo(User::class,"user_id");
    }
}
