<?php

namespace App\Models\RequestRechargeModels;

use App\Models\TransactionModels\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestRecharge extends Model
{
    protected $fillable=["user_id","amount","status","transaction_id",'attachment_text','attachment_image','attachment_mobile_image','resized_image_name','resized_image_path'];
    use HasFactory;
    
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

     public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

  
}

