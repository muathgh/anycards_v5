<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserDeviceInfo extends Model {
use HasFactory;

protected $fillable=["device_name","unique_id","username"];
protected $table = 'users_device_info';


}