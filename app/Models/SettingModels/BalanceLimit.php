<?php

namespace App\Models\SettingModels;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceLimit extends Model
{
    protected $table = "balance_limits";
    protected $fillable=["admin_id",'amount'];

    use HasFactory;

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

}
