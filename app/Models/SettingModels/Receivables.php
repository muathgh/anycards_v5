<?php

namespace App\Models\SettingModels;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receivables extends Model
{

    protected $fillable=["user_id",'transaction_id','notes','note'];
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
