<?php

namespace App\Models\SettingModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestRechargeNotification extends Model
{
    
    use HasFactory;
    protected $table = 'request_recharge_notifications';
    protected $fillable = ["title_en","title_ar","description_en","description_ar","is_seen","href",'is_delete_when_seen','user_id'];
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    

}
