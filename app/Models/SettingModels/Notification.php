<?php

namespace App\Models\SettingModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Notification extends Model
{
    
    use HasFactory,SoftDeletes;
    protected $fillable = ["title_en","title_ar","description_en","description_ar","is_seen","type","href",'is_delete_when_seen',"data","next_notification_at"];
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

}
