<?php

namespace App\Models\SettingModels;

use App\Models\CategoryModels\CardType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardQuantityLimit extends Model
{
   protected $table = 'cards_quantity_limits';
    protected $fillable=["card_type_id",'quantity_limit'];

    use HasFactory;

    public function cardType(){
        return $this->belongsTo(CardType::class,'card_type_id');
    }
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

}
