<?php

namespace App\Models\SettingModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ["key","value"];
    use HasFactory;
}
