<?php

namespace App\Models\ExcelImportModels;

use App\Facades\HelperFacade;
use App\Models\CardModels\Card;
use App\Models\CategoryModels\CardType;
use App\ServicesInterface\CardsServiceInterface;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class CardsCodeWithSerialImport implements ToCollection, WithStartRow
{
    private $data;
    private $_cardsService;
    private $duplicate = [];
    private $warning_messages = [];
    public function __construct(array $data = [], CardsServiceInterface $cardsService)
    {
        $this->data = $data;
        $this->_cardsService = $cardsService;
    }
    public function startRow(): int
    {
        return 2;
    }

  
    public function collection(Collection $rows)
    {

        $rows->each(function ($row, $key) {
            $telecommunication = Config::get("constants.CATEGORIES.Telecommunication");
            if ($row[0] != null) {
                $codeToCheck = HelperFacade::removeDash($row[0]);
                $codeToCheck = trim($codeToCheck);
                $card = $this->_cardsService->whereRaw('REPLACE(cards.code,"-","") = ?', [$codeToCheck])->first();
                if (!$card) {
                    if ($this->data['title'] != $telecommunication) {
                        Card::create(array_merge([
                            'code'   => $row[0],
                            'serial'  => $row[1],
                            "created_by_id"=>Auth::guard('admin')->user()->id,
                            // Want it to be possible to add my package here
                            //'package' => $package
                        ], $this->data));
                    } else {
                        
                        if (strlen(str_replace("-", "", $row[0])) == 14) {
                           

                            Card::create(array_merge([
                                'code'   => $row[0],
                                'serial'  => $row[1],
                                "created_by_id" => Auth::guard('admin')->user()->id,
                                // Want it to be possible to add my package here
                                //'package' => $package
                            ], $this->data));
                            
                        } else {
                            $this->warning_messages[] = "large_number_of_digits";
                            Session::put('warning_message', $this->warning_messages);
                        }
                    }
                } else {
                    $this->warning_messages[] = 'duplicate_cards';
                    Session::put('warning_message', $this->warning_messages);
                    $this->duplicate[] = $card->code;
                    Session::put("duplicate_cards", $this->duplicate);
                }
            }
        });
    }
}
