<?php

namespace App\Models\ReportsModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NetProfit extends Model
{
    protected $table="net_profits";
    protected $fillable = ["receivables_amount","receivables_notes",
    "unreceived_amount","unreceived_notes","other_amount","other_notes","current_balance_amount",
"current_balance_notes","monthly_profit_amount","monthly_profit_notes","net_profit_amount","net_profit_notes","stock_amount","stock_notes"];
    use HasFactory;
}
