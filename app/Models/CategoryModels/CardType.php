<?php

namespace App\Models\CategoryModels;

use App\Models\CardModels\Card;
use App\Models\CardModels\CardPrice;
use App\Models\PricingListModels\PriceOfSale;
use App\Models\PricingListModels\PricingList;
use App\Models\SettingModels\CardLimit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
use App\Models\OfficalPriceModels\AdminOfficalPrice;
use App\Models\PricingListModels\UserPriceOfSale;
use App\Models\SettingModels\CardExpiry;
use App\Models\SettingModels\CardQuantityLimit;
use App\Models\SettingModels\WholeSalePrice;

class CardType extends Model
{
    protected $table = "cards_types";
    protected $fillable = ["title_en", "title_ar", "description_en", 'description_ar', "sub_provider_id", "image", "price", 'order', 'is_active','card_code'];
    use HasFactory;
    use SecureDelete;
    public function subProvider()
    {
        return $this->belongsTo(SubProvider::class, 'sub_provider_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function cardPrices()
    {
        return $this->hasMany(CardPrice::class);
    }

    public function pricingLisits()
    {
        return $this->hasMany(PricingList::class);
    }

    public function adminOfficalPrice(){
        return $this->hasMany(AdminOfficalPrice::class);
    }

    public function cardLimit()
    {
        return $this->hasOne(CardLimit::class);
    }

    public function priceOfSale()
    {
        return $this->hasOne(PriceOfSale::class);
    }
    public function quantityLimit()
    {
        return $this->hasOne(CardQuantityLimit::class);
    }
    public function wholePrice()
    {
        return $this->hasOne(WholeSalePrice::class);
    }

    public function cardExpiry()
    {
        return $this->hasOne(CardExpiry::class);
    }



    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getPriceOfSale()
    {
        $user = auth()->user();
        $userPriceOfSale = null;
        if($user){
            
        $userPriceOfSale = UserPriceOfSale::where('card_type_id', $this->id)->where('user_id', $user->id)->first();
        }
        $priceOfSale = PriceOfSale::where('card_type_id', $this->id)->first();
        if ($userPriceOfSale)
            return $userPriceOfSale->price;
        if ($priceOfSale)
            return $priceOfSale->price;
   

        return null;
    }


    public function getCardPrice(){
        $user = auth()->user();
        if(!$user)
        return $this->price;

        $package = $user->package;
        if(!$package)
        return $this->price;

        $priceList =PricingList::where('card_type_id',$this->id)
        ->where('package_id',$package->id)->first();
        if(!$priceList)
        return $this->price;

        return $priceList->price;
    }
}
