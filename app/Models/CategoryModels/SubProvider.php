<?php

namespace App\Models\CategoryModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
class SubProvider extends Model
{
    use SecureDelete;
    protected $fillable=["title_en","title_ar","description_en",'description_ar',"image","provider_id",'order','is_active','template'];
    use HasFactory;

    public function provider(){
        return $this->belongsTo(Provider::class,'provider_id');
    }
    public function cardsTypes(){
        return $this->hasMany(CardType::class);
    }
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

 
}
