<?php

namespace App\Models\CategoryModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
class Category extends Model
{
    protected $fillable=["title_en","title_ar","description_en",'description_ar','image','order','is_active'];
    protected $appends = array('image_or_default');
    use HasFactory;
    use SecureDelete;
    public function providers(){
        return $this->hasMany(Provider::class);
    }

    
    public function getImageOrDefaultAttribute()
    {
        return $this->attributes['image'] ? $this->attributes['image']: asset('/images/default.jpg');
    }

     public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

 
}
