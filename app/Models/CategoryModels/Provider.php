<?php

namespace App\Models\CategoryModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
use App\Models\CategoryModels\SubProvider;
class Provider extends Model
{
    protected $fillable=["title_en","title_ar","description_en",'description_ar',"image","category_id",'template','order','is_active','skip_subproviders'];
    protected $appends = array('image_or_default');
    use HasFactory;
    use SecureDelete;
    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function subProviders(){
        return $this->hasMany(SubProvider::class);
    }
    public function getImageOrDefaultAttribute()
    {
        return $this->attributes['image'] ? $this->attributes['image']: asset('/images/default.jpg');
    }

   
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

}
