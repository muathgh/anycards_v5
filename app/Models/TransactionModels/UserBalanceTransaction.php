<?php

namespace App\Models\TransactionModels;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBalanceTransaction extends Model
{
    protected $table="users_balance_transactions";
    protected $fillable=["from_user_id","to_user_id",'sender_transaction_id'
,'receiver_transaction_id','next_transaction'];
    use HasFactory;

    public function fromUser(){
        return $this->belongsTo(User::class,"from_user_id");
    }
    public function toUser(){
        return $this->belongsTo(User::class,"to_user_id");
    }

    public function senderTransaction(){
        return $this->belongsTo(Transaction::class,"sender_transaction_id");
    }
    public function receiverTransaction(){
        return $this->belongsTo(Transaction::class,"receiver_transaction_id");
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    } 

}
