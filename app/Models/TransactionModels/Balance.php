<?php

namespace App\Models\TransactionModels;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable=["balance","user_id","transaction_id","last_update"];

    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function transaction(){
        return $this->belongsTo(Transaction::class,'transaction_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    } 
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    // public function getLastUpdateAttribute($value)
    // {
    //     return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    // }


}
