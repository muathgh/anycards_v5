<?php

namespace App\Models\TransactionModels;

use App\Models\CardModels\Card;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CardTransaction extends Model
{
    use HasFactory;
    protected $fillable =['card_id','transaction_id',"order_number",'price_of_sale'];

    public function transaction(){
        return $this->belongsTo(Transaction::class,'transaction_id');
    }
    public function card(){
        return $this->belongsTo(Card::class,"card_id");
    }
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    
public static function boot()
{
    parent::boot();
    self::creating(function ($model) {
        $cardTransaction = DB::table('card_transactions')->latest('id')->first();
        $id = 1;
      
        if ($cardTransaction) {
            $id = $cardTransaction->id;
        }
        $orderNumber = (string)str_pad($id + 1, 5, '0', STR_PAD_LEFT);
     
        $model->order_number = $orderNumber;
    });
}

}
