<?php
namespace App\Models\TransactionModels;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminBalanceLimitTransaction extends Model {
    protected $table = "admin_balance_limit_transactions";
    protected $fillable=["admin_id",'type','amount','next_transaction','created_by_id','notes','received_amount'];

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    } 

}
