<?php

namespace App\Models\TransactionModels;

use App\Models\RequestRechargeModels\RequestRecharge;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ["amount","type","user_id",'receivables',"unreceived",'notes',"notes2",'created_by_id','updated_by_id','next_transaction'];
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class,'user_id');
        
    }

    public function cardTransaction(){
        return $this->hasOne(CardTransaction::class);
    }
    
  
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }
}
