<?php

namespace App\Models\CardModels;

use App\Models\CategoryModels\CardType;
use App\Models\TransactionModels\CardTransaction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\SecureDelete;
class Card extends Model
{
    protected $fillable = [

        "code", "serial", "expiry_date", "is_used", "card_type_id",'created_by_id','updated_by_id'
    ];
    
    use HasFactory;
    use SecureDelete;

    public function cardType()
    {
        return $this->belongsTo(CardType::class, 'card_type_id');
    }

    public function cardTransaction()
    {
        return $this->hasOne(CardTransaction::class);
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    public function getExpiryDateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function setExpiryDateAttribute($value)
    {
        $this->attributes['expiry_date'] = \Carbon\Carbon::parse($value);
    }

    public function isUsed()
    {

    return $this->is_used;
    }
    public function used(){
        $this->update(['is_used'=>true]);
        $this->save();
    }

    public function scopeExpirySoon($query){
        
return $query->where('is_used',false)->where("expiry_date",">",\Carbon\Carbon::now())
->where('expiry_date','<=',\Carbon\Carbon::now()->addDays(7));
    }

    public function scopeNotUsed($query){
        return $query->where('is_used',false);
    }
  

}
