<?php

namespace App\Models\CardModels;

use App\Models\CategoryModels\CardType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardPrice extends Model
{
    use HasFactory;
    protected $fillable=["price","user_id","card_type_id"];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function cardType(){
        return $this->belongsTo(CardType::class,'card_type_id');
    }
    
}
